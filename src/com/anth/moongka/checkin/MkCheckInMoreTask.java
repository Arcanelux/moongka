/**
 * MkCheckInMoreTask
 *  발자취 더보기 액션
 */
package com.anth.moongka.checkin;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class MkCheckInMoreTask extends MkTask{
	private final String TAG = "MK_MkCheckInMoreTask";
	private Context mContext;
	
	private boolean isCert = false;
	private ArrayList<MkCheckIn> checkInList;
	private ArrayAdapter mAdapter;
	
	private String mem_idx, bpi_nm, cc_cd, cc_name, cc_type, cc_tel;
	private int page_num;

	public MkCheckInMoreTask(String mem_idx, int page_num, String bpi_nm, String cc_cd, String cc_name, String cc_type, String cc_tel, ArrayList<MkCheckIn> checkInList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.mem_idx = mem_idx;
		this.page_num = page_num;
		this.bpi_nm = bpi_nm;
		this.cc_cd = cc_cd;
		this.cc_name = cc_name;
		this.cc_type = cc_type;
		this.cc_tel = cc_tel;
		this.checkInList = checkInList;
		this.mAdapter = mAdapter;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		CheckInActivity.isAdding2 = true;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.checkInDetailList(mem_idx, page_num, bpi_nm, cc_cd, cc_name, cc_type, cc_tel, checkInList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			/**
			 * 더보기가 정상 처리될 경우,
			 *  발자취 액티비티의 리스트뷰2를 VISIBLE로 바꾸고(더보기 리스트뷰)
			 *  발자취 page_num2에 1을 더해줌
			 *  전달받은 ArrayAdapter에 notifyDataSetChanged()를 해준다
			 */
			CheckInActivity.lvCheckIn1.setVisibility(View.GONE);
			CheckInActivity.lvCheckIn2.setVisibility(View.VISIBLE);
//			CheckInActivity.viewCheckIn2Top.setVisibility(View.VISIBLE);
			mAdapter.notifyDataSetChanged();
			CheckInActivity.mode = CheckInActivity.LIST2;
			CheckInActivity.page_num2 += 1;
		}
		CheckInActivity.isAdding2 = false;
	}

	
	
}
