/**
 * CheckInLikeTask
 *  발자취 좋아요 액션
 */
package com.anth.moongka.checkin;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class CheckInLikeTask extends MkTask{
	private final String TAG = "MK_CheckInLikeTask";
	private Context mContext;
	
	private ArrayList<MkCheckIn> checkInList;
	private ArrayAdapter mAdapter;
	private int position;
	private boolean isCert = false;
	
	private String mem_idx, bod_idx, bod_type;
	

	public CheckInLikeTask(String bod_idx, String bod_type, ArrayList<MkCheckIn> checkInList, ArrayAdapter mAdapter, int position, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.mAdapter = mAdapter;
		this.position = position;
		this.checkInList = checkInList;
		this.bod_idx = bod_idx;
		this.bod_type = bod_type;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.likeToggle(C.mem_idx, bod_idx, bod_type);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			/** 
			 * 좋아요가 정상처리될 경우,
			 * 전달받은 ArrayList position부분의 값을 변경시킨 후
			 * 전달받은 ArrayAdapter를 notifyDataSetChanged() 시켜준다 
			 */
			int oriNumLike = checkInList.get(position).getNumLike();
			boolean oriIsLike = checkInList.get(position).isLike();
			if(oriIsLike){
				checkInList.get(position).setNumLike(oriNumLike-1);
				checkInList.get(position).setLike(false);
			} else{
				checkInList.get(position).setNumLike(oriNumLike+1);
				checkInList.get(position).setLike(true);
			}
			mAdapter.notifyDataSetChanged();
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

}
