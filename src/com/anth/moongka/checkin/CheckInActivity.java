/**
 * CheckInActivity
 *  발자취
 *   발자취목록과 발자취더보기가 한 액티비티내에 존재
 *   경우에따라 리스트뷰의 어댑터와 Visibility를 바꾸어 2개의 액티비티처럼 보이게함
 *   isAdding = true일 경우(AsyncTask가 돌고있을경우) 에는 글 추가액션을 실행하지않음
 */
package com.anth.moongka.checkin;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.comment.CommentActivity;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.C;
import com.anth.moongka.profile.ProfileActivity;

public class CheckInActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_CheckInActivity";
	private Context mContext;
	public static final int LIST1 = 3283;
	public static final int LIST2 = 3284;

	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;

	public static LinearLayout chMain;
	public static ListView lvCheckIn1;
	public static ListView lvCheckIn2;
	public static View viewCheckIn2Top;
	private ArrayList<MkCheckIn> checkInList = new ArrayList<MkCheckIn>();
	private ArrayList<MkCheckIn> checkInList2 = new ArrayList<MkCheckIn>();
	private CheckInAA mAdapter;
	private CheckInAA2 mAdapter2;
	public static boolean isAdding = false;
	public static boolean isAdding2 = false;
	public static int page_num = 1;
	public static int page_num2 = 1;

	public static int mode =LIST1;

	public static String bci_id;
	public static String cc_cd;
	public static String cc_name;
	public static String cc_type;
	public static String cc_tel;
	

	private TextView tvMoreName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin);
		mContext = this;

		viewTop = findViewById(R.id.viewTopCheckIn);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("발자취");

		viewCheckIn2Top = findViewById(R.id.llCheckIn2Top);
		tvMoreName = (TextView) findViewById(R.id.tvCheckInTop1);
		
		chMain = (LinearLayout) findViewById(R.id.checkinMain);
		lvCheckIn1 = (ListView) findViewById(R.id.lvCheckIn);
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View footer = inflater.inflate(R.layout.main_list_footer, null);
		lvCheckIn1.addFooterView(footer);
		mAdapter = new CheckInAA(mContext, R.layout.checkin_list_item, checkInList);
		lvCheckIn1.setAdapter(mAdapter);

		lvCheckIn2 = (ListView) findViewById(R.id.lvCheckIn2);
		mAdapter2 = new CheckInAA2(mContext, android.R.layout.simple_list_item_1, checkInList2);
		lvCheckIn2.setAdapter(mAdapter2);

		
		new MkCheckInTask(C.mem_idx, page_num, checkInList, mAdapter, "불러오는 중...", true, mContext).execute();
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		MkCheckIn curCheckIn = null;
		int position = 0;
		try{
			position = (Integer) v.getTag();
			curCheckIn = checkInList.get(position);
			//curCheckIn = checkInList2.get(position);
			
		} catch(Exception e){
			e.printStackTrace();
		}
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		case R.id.ibCheckInItemPhoto:
			/** 프로필이미지 누를경우 **/
			intent = new Intent(CheckInActivity.this, ProfileActivity.class);
			intent.putExtra("profile_idx", curCheckIn.getUser_idx());
			startActivity(intent);
			break;
		case R.id.ibCheckInItemLike:
			/** 좋아요버튼 **/
			position = (Integer) v.getTag();
			new CheckInLikeTask(curCheckIn.getIdx(), "CFB_ID", checkInList2, mAdapter2, position, "좋아요 처리중...", true, mContext).execute();
			break;
		case R.id.ibCheckInItemComment:
			/** 댓글버튼 **/
			intent = new Intent(CheckInActivity.this, CommentActivity.class);
			String post_idx = curCheckIn.getIdx();
			intent.putExtra("post_idx", post_idx);
			intent.putExtra("post_type", "CFB_ID");
			intent.putExtra("postPosition", position);
			startActivityForResult(intent, 2);
			break;
		case R.id.ibCheckInItemScrap:
			/** 스크랩버튼 **/
//			new MkCheckInScrapTask(curCheckIn.getIdx(), checkInList2, position, "스크랩 처리중...", true, mContext, mAdapter2).execute();
			new MkCheckInScrapTask(curCheckIn.getIdx(), checkInList2, position, "스크랩 처리중...", true, mContext, mAdapter2).execute();
			break;
		case R.id.btnCheckInItemMore:
			/** 더보기 버튼 **/
			position = (Integer) v.getTag();
			curCheckIn = checkInList.get(position);
//			bci_id = curCheckIn.getBci_idx();
			String bpi_nm = curCheckIn.getBci_nm();
			tvMoreName.setText(bpi_nm);
			cc_cd = curCheckIn.getShopCode();
			cc_name = curCheckIn.getBci_nm();
			cc_type = curCheckIn.getShopName();
			cc_tel = curCheckIn.getCall();
			new MkCheckInMoreTask(C.mem_idx, page_num2, bpi_nm, curCheckIn.getShopCode(), curCheckIn.getBci_nm(), curCheckIn.getShopName(), curCheckIn.getCall(), checkInList2, mAdapter2, "불러오는 중...", true, mContext).execute();
			break;
		}	
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode){
		case 2:
			/** 댓글보기 후 돌아왔을 때 **/
			int commentNum = data.getIntExtra("commentNum", 0);
			int postPosition = data.getIntExtra("postPosition", 0);
			checkInList2.get(postPosition).setNumComment(commentNum);
			mAdapter2.notifyDataSetChanged();
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed() {
		switch(mode){
		case LIST1:
			finish();
			break;
		case LIST2:
			CheckInActivity.lvCheckIn1.setVisibility(View.VISIBLE);
			CheckInActivity.lvCheckIn2.setVisibility(View.GONE);
			CheckInActivity.viewCheckIn2Top.setVisibility(View.GONE);
			checkInList2.clear();
			mode =LIST1;
			page_num2 = 1;
			break;
		}
	}

	@Override
	public void finish() {
		super.finish();
		isAdding = false;
		isAdding2 = false;
		page_num = 1;
		page_num2 = 1;

		mode =LIST1;

		checkInList.clear();
		checkInList2.clear();
	}


}
