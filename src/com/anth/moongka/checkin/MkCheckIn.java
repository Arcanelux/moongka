/**
 * MkCheckIn
 *  ������ ��ü
 */
package com.anth.moongka.checkin;

import java.io.Serializable;

public class MkCheckIn implements Serializable{
	private String idx;
	private String bci_idx;
	private String write_date;
	private String bci_nm;
	private String url_user_photo;
	private String user_nm;
	private String user_idx;
	private String content;
	private int numLike;
	private int numComment;
	private int numScrap;
	private String call;
	private String shopCode, shopName;
	private boolean isLike;
	private boolean isScrap;
	private int numImage;
	private String image_idx;
	private String url_image;
	private String tag;
	public MkCheckIn(String idx, String bci_idx, String write_date,
			String bci_nm, String url_user_photo, String user_nm,
			String user_idx, String content, int numLike, int numComment,
			int numScrap, String call, String shopCode, String shopName,
			boolean isLike, boolean isScrap, int numImage, String image_idx, String url_image,
			String tag) {
		super();
		this.idx = idx;
		this.bci_idx = bci_idx;
		this.write_date = write_date;
		this.bci_nm = bci_nm;
		this.url_user_photo = url_user_photo;
		this.user_nm = user_nm;
		this.user_idx = user_idx;
		this.content = content;
		this.numLike = numLike;
		this.numComment = numComment;
		this.numScrap = numScrap;
		this.call = call;
		this.shopCode = shopCode;
		this.shopName = shopName;
		this.isLike = isLike;
		this.isScrap = isScrap;
		this.numImage = numImage;
		this.image_idx = image_idx;
		this.url_image = url_image;
		this.tag = tag;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getBci_idx() {
		return bci_idx;
	}
	public void setBci_idx(String bci_idx) {
		this.bci_idx = bci_idx;
	}
	public String getWrite_date() {
		return write_date;
	}
	public void setWrite_date(String write_date) {
		this.write_date = write_date;
	}
	public String getBci_nm() {
		return bci_nm;
	}
	public void setBci_nm(String bci_nm) {
		this.bci_nm = bci_nm;
	}
	public String getUrl_user_photo() {
		return url_user_photo;
	}
	public void setUrl_user_photo(String url_user_photo) {
		this.url_user_photo = url_user_photo;
	}
	public String getUser_nm() {
		return user_nm;
	}
	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}
	public String getUser_idx() {
		return user_idx;
	}
	public void setUser_idx(String user_idx) {
		this.user_idx = user_idx;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getNumLike() {
		return numLike;
	}
	public void setNumLike(int numLike) {
		this.numLike = numLike;
	}
	public int getNumComment() {
		return numComment;
	}
	public void setNumComment(int numComment) {
		this.numComment = numComment;
	}
	public int getNumScrap() {
		return numScrap;
	}
	public void setNumScrap(int numScrap) {
		this.numScrap = numScrap;
	}
	public String getCall() {
		return call;
	}
	public void setCall(String call) {
		this.call = call;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public boolean isLike() {
		return isLike;
	}
	public void setLike(boolean isLike) {
		this.isLike = isLike;
	}
	public int getNumImage() {
		return numImage;
	}
	public void setNumImage(int numImage) {
		this.numImage = numImage;
	}
	public String getImage_idx() {
		return image_idx;
	}
	public void setImage_idx(String image_idx) {
		this.image_idx = image_idx;
	}
	public String getUrl_image() {
		return url_image;
	}
	public void setUrl_image(String url_image) {
		this.url_image = url_image;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public boolean isScrap() {
		return isScrap;
	}
	public void setScrap(boolean isScrap) {
		this.isScrap = isScrap;
	}
	
	
}
