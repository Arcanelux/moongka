/**
 * CheckInAA2
 *  발자취 더보기 ArrayAdapter
 */
package com.anth.moongka.checkin;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.function.Lhy_Function;

public class CheckInAA2 extends ArrayAdapter<MkCheckIn> {
	private final String TAG = "MK_CheckInAA";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	
	private ArrayList<MkCheckIn> checkInList;
	private OnClickListener clicklistener;
	

	public CheckInAA2(Context context, int textViewResourceId, ArrayList<MkCheckIn> checkInList) {
		super(context, textViewResourceId, checkInList);
		mContext= context;
		this.checkInList = checkInList;
		this.clicklistener = (OnClickListener) mContext;
		mImageDownloader = new ImageDownloader(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.checkin_list_item, null);
		}
		
		MkCheckIn curCheck = checkInList.get(position);
		
		View view2Only = curView.findViewById(R.id.viewCheckInItem2Only);
		view2Only.setVisibility(View.VISIBLE);
		
		TextView tvName = (TextView) curView.findViewById(R.id.tvCheckInItemName);
		TextView tvDate = (TextView) curView.findViewById(R.id.tvCheckInItemDate);
		TextView tvContent = (TextView) curView.findViewById(R.id.tvCheckInItemContent);
		TextView tvNumLike = (TextView) curView.findViewById(R.id.tvCheckInItemLike);
		TextView tvNumComment = (TextView) curView.findViewById(R.id.tvCheckInItemComment);
		
		TextView tvShopType = (TextView) curView.findViewById(R.id.tvCheckInItemTop1);
		TextView tvShopName = (TextView) curView.findViewById(R.id.tvCheckInItemTop2);
		TextView tvShopInfo = (TextView) curView.findViewById(R.id.tvCheckInItemTop3);
		TextView tvShopDistance = (TextView) curView.findViewById(R.id.tvCheckInItemTop4);
		
		tvName.setText(curCheck.getUser_nm());
		tvDate.setText(curCheck.getWrite_date());
		tvContent.setText(curCheck.getContent());
		tvNumLike.setText(curCheck.getNumLike()+"");
		tvNumComment.setText(curCheck.getNumComment()+"");

		
		tvShopType.setText(curCheck.getShopName());
		tvShopName.setText(curCheck.getBci_nm());
//		tvShopInfo.setText(curCheck.get)
		
		ImageButton ibLike = (ImageButton) curView.findViewById(R.id.ibCheckInItemLike);
		ImageButton ibComment = (ImageButton) curView.findViewById(R.id.ibCheckInItemComment);
		ImageView ivPhoto = (ImageView) curView.findViewById(R.id.ibCheckInItemPhoto);
		ImageButton ibScrap = (ImageButton) curView.findViewById(R.id.ibCheckInItemScrap);
		mImageDownloader.download(C.MK_BASE + curCheck.getUrl_user_photo().replaceAll("/data/", "/thumb_data/data/"), ivPhoto);
		
		boolean isLike = curCheck.isLike();
		boolean isScrap = curCheck.isScrap();
		if (isLike) {
			ibLike.setImageResource(R.drawable.bt_like);
		} else {
			ibLike.setImageResource(R.drawable.bt_like_gray);	
		}

		if (isScrap) {
			ibScrap.setImageResource(R.drawable.bt_scrap);
		} else {
			ibScrap.setImageResource(R.drawable.bt_scrap_gray);	
		}
		
		Button btnMore = (Button) curView.findViewById(R.id.btnCheckInItemMore);
		
		View invisibleView = curView.findViewById(R.id.viewCheckIn2Invisible);
		invisibleView.setVisibility(View.GONE);
		
		View[] viewList = { ibLike, ibComment, ivPhoto, ibScrap, btnMore };
		Lhy_Function.setViewTag(viewList, position);
		
		ivPhoto.setOnClickListener(clicklistener);
		ibLike.setOnClickListener(clicklistener);
		ibComment.setOnClickListener(clicklistener);
		ibScrap.setOnClickListener(clicklistener);
		btnMore.setOnClickListener(clicklistener);
		
		btnMore.setVisibility(View.GONE);
		
		/** 리스트 추가 액션 **/
		if(checkInList.size()==position+1){
			if(!CheckInActivity.isAdding2 && CheckInActivity.mode==CheckInActivity.LIST2) new MkCheckInMoreTask(C.mem_idx, CheckInActivity.page_num2, CheckInActivity.bci_id, CheckInActivity.cc_cd, CheckInActivity.cc_name, CheckInActivity.cc_type, CheckInActivity.cc_tel, checkInList, this, "불러오는 중...", false, mContext).execute();
		}
		
		return curView;
	}
	
	
	
	

}
