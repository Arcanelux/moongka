/**
 * MkCheckInTask
 * 발자취 액션
 */
package com.anth.moongka.checkin;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import com.anth.moongka.R;
import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class MkCheckInTask extends MkTask{
	private final String TAG = "MK_MkCheckInTask";
	private Context mContext;
	
	private boolean isCert = false;
	private ArrayList<MkCheckIn> checkInList;
	private ArrayAdapter mAdapter;
	private Activity mActivity;
	
	private String mem_idx;
	private int page_num;

	public MkCheckInTask(String mem_idx, int page_num, ArrayList<MkCheckIn> checkInList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.mem_idx = mem_idx;
		this.page_num = page_num;
		this.checkInList = checkInList;
		this.mAdapter = mAdapter;
		this.mActivity = (Activity)context;
		
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		CheckInActivity.isAdding = true;
		CheckInActivity.page_num += 1;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.checkInList(mem_idx, page_num, checkInList);
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
//		Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		if(isCert){
			/**
			 * 발자취 불러오기가 정상처리되면
			 * 리스트1을 VISIBLE처리, mode를 LIST1로 변경
			 */
			Log.i("checkinin", ""+isCert);
			CheckInActivity.lvCheckIn1.setBackgroundColor(Color.rgb(69, 69, 69));
			CheckInActivity.lvCheckIn1.setVisibility(View.VISIBLE);
			CheckInActivity.lvCheckIn2.setVisibility(View.GONE);
			CheckInActivity.viewCheckIn2Top.setVisibility(View.GONE);
			mAdapter.notifyDataSetChanged();
			
			CheckInActivity.mode = CheckInActivity.LIST1;
		}
		if(checkInList.size()==0){
			Log.i("checkinin2", ""+isCert);
			CheckInActivity.lvCheckIn1.setBackgroundResource(R.drawable.img_data_android);
			
			Log.i("checkinin2", "1"+isCert);
			
			Log.i("checkinin2", "2"+isCert);
//			CheckInActivity.isAdding = false;
		}
		
		CheckInActivity.isAdding = false;
		
	}	
}
