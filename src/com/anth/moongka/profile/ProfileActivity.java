/**
 * ProfileActivity
 *  프로필
 *  최근등록장소가 안나오는 버그있음
 */
package com.anth.moongka.profile;

import java.util.ArrayList;

import org.apache.http.util.EncodingUtils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.album.AlbumActivity;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.menu.MenuLeftToMkCarListProfileTask;
import com.anth.moongka.profile_car_list.MkCarListProfileActivity;
import com.anth.moongka.setting.SettingProfileCreateTask;

public class ProfileActivity extends MkActivity implements OnClickListener {
	private final String TAG = "MK_ProfileActivity";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	public static final int TO_PROFILE_EDIT = 7134;

	/** Top **/
	private View viewTop;
	private TextView tvTitle;
	private ImageButton ibBack;
	private Button btnEdit;

	/** Post List **/
	private MkProfile mProfile = new MkProfile();
	private ListView lvProfilePostList;
	private ArrayList<MkPost> postList = new ArrayList<MkPost>();
	private ProfileMkPostAA mPostAA;

	/** Profile **/
	private ImageView ivProfile;
	private TextView tvProfileName, tvJob, tvArea, tvFriend, tvLike, tvScrap,
			tvIntroduce;
	private ImageView ivCar, ivAlbum;
	private TextView tvCar, tvLocation, tvAlbum;
	private WebView wvLocation;

	private String profile_idx;
	public int page_num = 1;
	public boolean isAdding = false;

	/** Profile Another **/
	private View viewAnother;
	private Button btnFriendRequest, btnSendMessage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile);
		mContext = this;
		mImageDownloader = new ImageDownloader(mContext);

		Intent intent = getIntent();
		profile_idx = intent.getStringExtra("profile_idx");
		C.identify = intent.getIntExtra("identify", 0);
		Log.i("identify", "" + C.identify);

		/** Top View **/
		viewTop = findViewById(R.id.viewTopProfile);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		ibBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnEdit = (Button) viewTop.findViewById(R.id.btnCert2);
		tvTitle.setText("프로필");
		ibBack.setOnClickListener(this);
		btnEdit.setOnClickListener(this);

		/** ListView **/
		lvProfilePostList = (ListView) findViewById(R.id.lvProfilePostList);
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		/** ListView Header & Footer **/
		View footer = inflater.inflate(R.layout.main_list_footer, null);
		lvProfilePostList.addFooterView(footer);
		View header = inflater.inflate(R.layout.profile_header, null);
		lvProfilePostList.addHeaderView(header);

		/** Header UI Connect **/
		ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
		tvProfileName = (TextView) header.findViewById(R.id.tvProfileName);
		tvJob = (TextView) header.findViewById(R.id.tvProfileJob);
		tvArea = (TextView) header.findViewById(R.id.tvProfileArea);
		tvFriend = (TextView) header.findViewById(R.id.tvProfileNumFriend);
		tvLike = (TextView) header.findViewById(R.id.tvProfileNumLike);
		tvScrap = (TextView) header.findViewById(R.id.tvProfileNumScrap);
		tvIntroduce = (TextView) header.findViewById(R.id.tvProfileIntroduce);

		ivCar = (ImageView) header.findViewById(R.id.ivProfilePhotoCar);
		// ivLocation = (ImageView)
		// header.findViewById(R.id.ivProfilePhotoLocation);
		wvLocation = (WebView) header.findViewById(R.id.wvProfileLocation);
		// wvLocation.setBackgroundResource(R.drawable.default_location);
		wvLocation.getSettings().setJavaScriptEnabled(true);

		ivAlbum = (ImageView) header.findViewById(R.id.ivProfilePhotoMore);
		ivCar.setOnClickListener(this);
		wvLocation.setOnClickListener(this);
		ivAlbum.setOnClickListener(this);

		/** Another View **/
		viewAnother = findViewById(R.id.viewProfileAnother);
		btnFriendRequest = (Button) viewAnother
				.findViewById(R.id.btnProfileFriendRequest);
		btnSendMessage = (Button) viewAnother
				.findViewById(R.id.btnProfileSendMessage);
		btnFriendRequest.setOnClickListener(this);
		btnSendMessage.setOnClickListener(this);
		if (profile_idx.equals(C.mem_idx)) {
			viewAnother.setVisibility(View.GONE);
		} else {
			viewAnother.setVisibility(View.VISIBLE);
		}

		new ProfileTask(profile_idx, mProfile, postList, page_num,
				"프로필 정보 로딩중...", true, mContext).execute();

		mPostAA = new ProfileMkPostAA(mContext, R.layout.profile_list_item,
				postList);
		lvProfilePostList.setAdapter(mPostAA);

		// ivCar.post(new Runnable() {
		// @Override
		// public void run() {
		// int width = ivCar.getWidth();
		// ivCar.getLayoutParams().width = width;
		// ivCar.getLayoutParams().height = width;
		// ivAlbum.getLayoutParams().width = width;
		// ivAlbum.getLayoutParams().height = width;
		// wvLocation.getLayoutParams().width = width;
		// wvLocation.getLayoutParams().height = width;
		//
		// }
		// });
		if (profile_idx.equals(C.mem_idx)) {
			btnEdit.setVisibility(View.VISIBLE);
		}
		if (C.identify == 2) {
			btnEdit.setVisibility(View.GONE);
		} else if (C.identify == 1) {
			btnEdit.setVisibility(View.VISIBLE);
		}
	}

	public void notifyPostList() {
		mPostAA.notifyDataSetChanged();
	}

	/**
	 * 리스트 추가 로직 MainPostTask에서 추가시작할때 isAdding을 true로 변환, 추가중일때는 더이상 실행되지않음
	 */
	public void addPost() {
		if (!isAdding)
			new ProfilePostTask(postList, C.mem_idx, profile_idx, page_num,
					"포스트 로딩중...", false, mContext).execute();
	}

	public void setProfile(MkProfile mProfile) {
		tvProfileName.setText(mProfile.getUser_name());
		tvJob.setText(mProfile.getUser_job());
		tvArea.setText(mProfile.getUser_area()+" "+mProfile.getUser_city());
		tvFriend.setText(mProfile.getUser_numFriend());
		tvLike.setText(mProfile.getUser_numLike());
		tvScrap.setText(mProfile.getUser_numScrap());
		tvIntroduce.setText(mProfile.getUser_introduce());

		double latX = mProfile.getLatX();
		double lonY = mProfile.getLonY();
		String postData = "userLat=" + latX + "&userLng=" + lonY;
		Log.d(TAG, "postData : " + postData);

		// postData = "userLat=37.5592798&userLng=126.9226244";
		wvLocation.getSettings().setJavaScriptEnabled(true);
		if(latX==0.0&&lonY==0.0){
			wvLocation.setBackgroundColor(0);
			
			wvLocation.setBackgroundResource(R.drawable.default_location);	
		}else{
			wvLocation.postUrl(C.API_PLACE_MY,
					EncodingUtils.getBytes(postData, "BASE64"));		
		}
//		
//		wvLocation.postUrl(C.API_PLACE_MY,
//				EncodingUtils.getBytes(postData, "BASE64"));
		// wvLocation.loadUrl("http://www.naver.com/");

		String urlPhotoAlbum = C.MK_BASE + mProfile.getUser_image_photo();
		String urlPhotoProfile = C.MK_BASE + mProfile.getUser_profile_photo();
		String urlPhotoCar = C.MK_BASE + mProfile.getUser_car_photo();
		mImageDownloader.download(
				urlPhotoProfile.replaceAll("/data/", "/thumb_data/data/"),
				ivProfile);
		mImageDownloader.download(
				urlPhotoAlbum.replaceAll("/data/", "/thumb_data/data/"),
				ivAlbum);
		mImageDownloader.download(
				urlPhotoCar.replaceAll("/data/", "/thumb_data/data/"), ivCar);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		/** Top View **/
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnCert2:
			new SettingProfileCreateTask("프로필 로딩중...", true, mContext)
					.execute();
			break;

		/** Another View **/
		case R.id.btnProfileFriendRequest:
			new ProfileFriendReqTask(profile_idx, "친구 신청중...", true, mContext)
					.execute();
			break;
		case R.id.btnProfileSendMessage:
			intent = new Intent(ProfileActivity.this, ProfileImeActivity.class);
			intent.putExtra("opp_idx", profile_idx);
			startActivity(intent);
			break;

		case R.id.ivProfilePhotoCar:
			new MenuLeftToMkCarListProfileTask(
					MkCarListProfileActivity.MAINCAR, profile_idx,
					"Loading...", true, mContext).execute();
			break;
		case R.id.ivProfilePhotoMore:
			intent = new Intent(ProfileActivity.this, AlbumActivity.class);
			intent.putExtra("mem_idx", profile_idx);
			startActivity(intent);
			break;

		case R.id.wvProfileLocation:
			Log.d(TAG, "wvProfileLocation");
			break;
		}

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == TO_PROFILE_EDIT) {
				new ProfileTask(profile_idx, mProfile, postList, page_num,
						"프로필 정보 로딩중...", true, mContext).execute();
			}
		}
	}

}
