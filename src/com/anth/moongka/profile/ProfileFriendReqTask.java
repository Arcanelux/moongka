/**
 * ProfileFriendReqTask
 *  프로필 - 친구요청 액션
 */
package com.anth.moongka.profile;

import android.content.Context;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class ProfileFriendReqTask extends MkTask{
	private final String TAG = "MK_ProfileFriendReqTask";
	private Context mContext;
	
	private String frd_idx;
	private boolean isCert = false;

	public ProfileFriendReqTask(String frd_idx, String msg, boolean isProgressDialog,
			Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.frd_idx = frd_idx;
	}


	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.friendRequest(frd_idx);
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
	}


}
