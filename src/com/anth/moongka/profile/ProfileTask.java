/**
 * ProfileTask
 *  프로필 불러오기 액션
 */
package com.anth.moongka.profile;

import java.util.ArrayList;

import android.content.Context;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class ProfileTask extends MkTask{
	private final String TAG = "MK_ProfileTask";
	private Context mContext;
	private ProfileActivity mActivity;
	
	private MkProfile mProfile;
	private String profile_idx;
	
	private ArrayList<MkPost> postList;
	private int page_num;
	
	private boolean isCert = false;

	public ProfileTask(String profile_idx, MkProfile mProfile, ArrayList<MkPost> postList, int page_num, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (ProfileActivity) mContext;
		this.profile_idx = profile_idx;
		this.mProfile = mProfile;
		this.postList = postList;
		this.page_num = page_num;
	}


	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.getProfile(profile_idx, mProfile);
		return super.doInBackground(params);
	}	
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			mActivity.setProfile(mProfile);
			new ProfilePostTask(postList, C.mem_idx, profile_idx, page_num, "작성한 글 로딩중...", true, mContext).execute();
		} else{
			Toast.makeText(mContext, "프로필 로딩 실패", Toast.LENGTH_SHORT).show();
		}
	}
}
