/**
 * ProfileMsgAddTask
 *  프로필 쪽지 보내기 액션
 */
package com.anth.moongka.profile;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class ProfileMsgAddTask extends MkTask {
	private final String TAG = "MK_MkMsgDetailAddTask";
	private Context mContext;
	private boolean isCert = false;
	
	private String own_idx, opp_idx;
	private String content;

	public ProfileMsgAddTask(String own_idx, String opp_idx, String content, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.own_idx = own_idx;
		this.opp_idx = opp_idx;
		this.content = content;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.messageWriteProfile(own_idx, opp_idx, content);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
//		Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		Toast.makeText(mContext, "쪽지 전송완료", Toast.LENGTH_SHORT).show();
		((Activity) mContext).finish();
	}
}
