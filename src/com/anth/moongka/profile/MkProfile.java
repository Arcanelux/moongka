/**
 * MkProfile
 *  프로필 정보
 */
package com.anth.moongka.profile;

public class MkProfile {
	private String user_idx;
	private String user_id;
	private String user_name;
	private String user_profile_photo;
	private String user_job;
	private String user_area;
	private String user_city;
	private String user_introduce;
	private String user_numFriend;
	private String user_numLike;
	private String user_numShare;
	private String user_numScrap;
	private String user_image_photo;
	private String user_car_photo;
	private String user_car_name;
	private double latX, lonY;

	public MkProfile(){

	}


	public MkProfile(String user_idx, String user_id, String user_name,
			String user_profile_photo, String user_job, String user_area, String user_city,
			String user_introduce, String user_numFriend, String user_numLike,
			String user_numShare, String user_numScrap,
			String user_image_photo, String user_car_photo,
			String user_car_name, double latX, double lonY) {
		this.user_idx = user_idx;
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_profile_photo = user_profile_photo;
		this.user_job = user_job;
		this.user_area = user_area;
		this.user_city = user_city;
		this.user_introduce = user_introduce;
		this.user_numFriend = user_numFriend;
		this.user_numLike = user_numLike;
		this.user_numShare = user_numShare;
		this.user_numScrap = user_numScrap;
		this.user_image_photo = user_image_photo;
		this.user_car_photo = user_car_photo;
		this.user_car_name = user_car_name;
		this.latX = latX;
		this.lonY = lonY;
	}

	public void init(String user_idx, String user_id, String user_name,
			String user_profile_photo, String user_job, String user_area, String user_city,
			String user_introduce, String user_numFriend, String user_numLike,
			String user_numShare, String user_numScrap,
			String user_image_photo, String user_car_photo,
			String user_car_name, double latX, double lonY) {
		this.user_idx = user_idx;
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_profile_photo = user_profile_photo;
		this.user_job = user_job;
		this.user_area = user_area;
		this.user_city = user_city;
		this.user_introduce = user_introduce;
		this.user_numFriend = user_numFriend;
		this.user_numLike = user_numLike;
		this.user_numShare = user_numShare;
		this.user_numScrap = user_numScrap;
		this.user_image_photo = user_image_photo;
		this.user_car_photo = user_car_photo;
		this.user_car_name = user_car_name;
		this.latX = latX;
		this.lonY = lonY;
	}


	public String getUser_idx() {
		return user_idx;
	}

	public void setUser_idx(String user_idx) {
		this.user_idx = user_idx;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_profile_photo() {
		return user_profile_photo;
	}

	public void setUser_profile_photo(String user_profile_photo) {
		this.user_profile_photo = user_profile_photo;
	}

	public String getUser_job() {
		return user_job;
	}

	public void setUser_job(String user_job) {
		this.user_job = user_job;
	}

	public String getUser_area() {
		return user_area;
	}

	public void setUser_area(String user_area) {
		this.user_area = user_area;
	}

	public String getUser_introduce() {
		return user_introduce;
	}

	public void setUser_introduce(String user_introduce) {
		this.user_introduce = user_introduce;
	}

	public String getUser_numFriend() {
		return user_numFriend;
	}

	public void setUser_numFriend(String user_numFriend) {
		this.user_numFriend = user_numFriend;
	}

	public String getUser_numLike() {
		return user_numLike;
	}

	public void setUser_numLike(String user_numLike) {
		this.user_numLike = user_numLike;
	}

	public String getUser_numShare() {
		return user_numShare;
	}

	public void setUser_numShare(String user_numShare) {
		this.user_numShare = user_numShare;
	}

	public String getUser_numScrap() {
		return user_numScrap;
	}

	public void setUser_numScrap(String user_numScrap) {
		this.user_numScrap = user_numScrap;
	}

	public String getUser_image_photo() {
		return user_image_photo;
	}

	public void setUser_image_photo(String user_image_photo) {
		this.user_image_photo = user_image_photo;
	}
	public String getUser_car_photo() {
		return user_car_photo;
	}
	public void setUser_car_photo(String user_car_photo) {
		this.user_car_photo = user_car_photo;
	}
	public String getUser_car_name() {
		return user_car_name;
	}
	public void setUser_car_name(String user_car_name) {
		this.user_car_name = user_car_name;
	}


	public double getLatX() {
		return latX;
	}


	public void setLatX(double latX) {
		this.latX = latX;
	}


	public double getLonY() {
		return lonY;
	}


	public void setLonY(double lonY) {
		this.lonY = lonY;
	}


	public String getUser_city() {
		return user_city;
	}


	public void setUser_city(String user_city) {
		this.user_city = user_city;
	}
	


}
