/**
 * ProfileImeActivity
 *  프로필 쪽지보내기 클릭시 입력창
 */
package com.anth.moongka.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;

public class ProfileImeActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_MessageImeActivity";
	private Context mContext;
	
	private View viewBlank;
	private EditText etIME;
	private Button btnIME;
	private String own_idx, opp_idx;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_ime);
		mContext = this;
		
		Intent intent = getIntent();
		// 여기 수정해야됨
//		own_idx = intent.getStringExtra("own_idx");
		own_idx = C.mem_idx;
		opp_idx = intent.getStringExtra("opp_idx");
		
		viewBlank = findViewById(R.id.viewMessageImeBlank);
		viewBlank.setOnClickListener(this);
		etIME = (EditText) findViewById(R.id.etMessageIme);
		btnIME = (Button) findViewById(R.id.btnMessageImeSend);
		btnIME.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.viewMessageImeBlank:
			onBackPressed();
			break;
		case R.id.btnMessageImeSend:
			String content = etIME.getText().toString();
			if(content.length()>0){
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(etIME.getWindowToken(), 0);
				new ProfileMsgAddTask(own_idx, opp_idx, content, "쪽지 작성중...", true, mContext).execute();
			}
			break;
		}
	}

	@Override
	public void onBackPressed() {
		Lhy_Function.forceHideKeyboard(mContext, etIME);
		finish();
	}

}
