/**
 * ProfilePostTask
 *  프로필 글 목록 액션
 */
package com.anth.moongka.profile;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.MkApi;

public class ProfilePostTask extends MkTask{
	private final String TAG = "MK_MainPostTask";
	private Context mContext;
	private ProfileActivity mActivity;
	
	private ArrayList<MkPost> postList;
	private String mem_idx, writer_idx;
	private int page_num;
	private boolean isCert = false;
	private boolean isRefresh = false;
	
	public ProfilePostTask(ArrayList<MkPost> postList, String mem_idx, String writer_idx, int page_num, String msg, boolean isPdialog, Context context) {
		super(msg, isPdialog, context);
		mContext = context;
		this.postList = postList;
		this.mem_idx = mem_idx;
		this.writer_idx = writer_idx;
		this.page_num = page_num;
		mActivity = (ProfileActivity) mContext;
	}
	
	public ProfilePostTask(ArrayList<MkPost> postList, String mem_idx, String writer_idx, int page_num, String msg, boolean isPdialog, boolean isRefresh, Context context) {
		super(msg, isPdialog, context);
		mContext = context;
		this.postList = postList;
		this.mem_idx = mem_idx;
		this.writer_idx = writer_idx;
		this.page_num = page_num;
		mActivity = (ProfileActivity) mContext;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mActivity.isAdding = true;
		Log.d(TAG, "MainPost Add Start. page_num : " + mActivity.page_num);
	}

	
	@Override
	protected Void doInBackground(Void... params) {
		Log.d(TAG, "PageNum : " + page_num);
		isCert = MkApi.postList(postList, mem_idx, "date", writer_idx, page_num);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mActivity.notifyPostList();
		mActivity.page_num += 1;
		mActivity.isAdding = false;
		Log.d(TAG, "MainPost Add End. page_num : " + mActivity.page_num);
	}

	

}
