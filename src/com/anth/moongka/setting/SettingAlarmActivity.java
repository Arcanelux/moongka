/**
 * SettingAlarmActivity
 *  알림설정
 */
package com.anth.moongka.setting;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkDialog;
import com.anth.moongka.function.MkPref;
import com.anth.moongka.function.SavedValue;

public class SettingAlarmActivity extends MkActivity implements OnCheckedChangeListener, OnClickListener{
	private final String TAG = "MK_SettingAlarmActivity";
	private Context mContext;
	private MkDialog mDialog;
	
	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;
	
	private ToggleButton tb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.setting_alarm);
		
		viewTop = findViewById(R.id.viewSettingAlarmTop);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("알림 설정");
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		
		tb = (ToggleButton) findViewById(R.id.tbSettingAlarm);
		tb.setOnCheckedChangeListener(this);
		
		SavedValue sValue = MkPref.getSavedValue(mContext);
		boolean isAlarm = sValue.isAlarm();
		tb.setChecked(isAlarm);
	}
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(isChecked){
			MkPref.setAlarm(mContext, true);
//			new SettingAlarmTask(mDialog,"푸시 셋팅중...",true,mContext).execute();
		} else{
			MkPref.setAlarm(mContext, false);
//			new SettingAlarmTask(mDialog,"푸시 셋팅중...",true,mContext).execute();
		}
	}
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		}
	}

}
