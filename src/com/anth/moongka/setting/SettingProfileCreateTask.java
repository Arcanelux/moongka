/**
 * SettingProfileCreateTask
 * 프로필 수정 - 프로필 불러오기 액션
 */
package com.anth.moongka.setting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.profile.ProfileActivity;

public class SettingProfileCreateTask extends MkTask {
	private final String TAG = "MK_SettingProfileCreateTask";
	private Context mContext;
	private Activity mActivity;
	
	private SettingProfile mProfile = new SettingProfile();
	private boolean isCert = false;

	public SettingProfileCreateTask(String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext= context;
		mActivity = (Activity) mContext;
	}


	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.getSettingProfile(C.mem_idx, mProfile);
		return super.doInBackground(params);
	}
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Intent intent = new Intent(mActivity, SettingProfileActivity.class);
			intent.putExtra("mProfile", mProfile);
			mActivity.startActivityForResult(intent, ProfileActivity.TO_PROFILE_EDIT);
		} else{
			Toast.makeText(mContext, "프로필 로딩에 실패했습니다", Toast.LENGTH_SHORT).show();
		}
	}

}
