/**
 * SettingActivity
 *  공지사항
 */
package com.anth.moongka.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkDialog;

public class SettingActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_SettingActivity";
	private Context mContext;
	
	private MkDialog mDialog;
	
	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;
	private Button btnInfo, btnNotice, btnProfile, btnPW, btnMyCar, btnPublic, btnNotify, btnGPS, btnAutoLogin, btnExit, btnAbout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		mContext = this;
		
		viewTop = findViewById(R.id.viewTopSetting);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("설정");
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		
		btnInfo = (Button) findViewById(R.id.btnSettingInfo);
		btnNotice = (Button) findViewById(R.id.btnSettingNotice);
		btnProfile = (Button) findViewById(R.id.btnSettingProfile);
		btnPW = (Button) findViewById(R.id.btnSettingPW);
		btnMyCar = (Button) findViewById(R.id.btnSettingMyCar);
		btnPublic = (Button) findViewById(R.id.btnSettingPublic);
		btnNotify = (Button) findViewById(R.id.btnSettingNotify);
		btnGPS = (Button) findViewById(R.id.btnSettingGPS);
		btnAutoLogin = (Button) findViewById(R.id.btnSettingAutoLogin);
		btnExit = (Button) findViewById(R.id.btnSettingExit);
		btnAbout = (Button) findViewById(R.id.btnSettingAbout);
		
		btnInfo.setOnClickListener(this);
		btnNotice.setOnClickListener(this);
		btnProfile.setOnClickListener(this);
		btnPW.setOnClickListener(this);
		btnMyCar.setOnClickListener(this);
		btnPublic.setOnClickListener(this);
		btnNotify.setOnClickListener(this);
		btnGPS.setOnClickListener(this);
		btnAutoLogin.setOnClickListener(this);
		btnExit.setOnClickListener(this);
		btnAbout.setOnClickListener(this);
	}


	@Override
	public void onClick(View v) {
		Intent intent;
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnSettingInfo:
			intent = new Intent(SettingActivity.this, SettingAutoLoginActivity.class);
			startActivity(intent);
			break;
		case R.id.btnSettingNotice:
//			intent = new Intent(SettingActivity.this, SettingNoticeActivity.class);
//			startActivity(intent);
			new SettingNoticeCreateTask(1, "공지사항 로딩중...", true, mContext).execute();
			break;
		case R.id.btnSettingProfile:
//			intent = new Intent(SettingActivity.this, SettingProfileActivity.class);
//			startActivity(intent);
			new SettingProfileCreateTask("프로필 로딩중...", true, mContext).execute();
			break;
		case R.id.btnSettingPW:
			intent = new Intent(SettingActivity.this, SettingChangePwActivity.class);
			startActivity(intent);
			break;
		case R.id.btnSettingMyCar:
//			intent = new Intent(SettingActivity.this, SettingCarActivity.class);
//			startActivity(intent);
			new SettingCarCreateTask("차량 프로필 로딩중...", true, mContext).execute();
			break;
		case R.id.btnSettingPublic:
			break;
		case R.id.btnSettingNotify:
			intent = new Intent(SettingActivity.this, SettingAlarmActivity.class);
			startActivity(intent);
			break;
		case R.id.btnSettingGPS:
			break;
		case R.id.btnSettingAutoLogin:
			intent = new Intent(SettingActivity.this, SettingAutoLoginActivity.class);
			startActivity(intent);
			break;
		case R.id.btnSettingExit:
			mDialog = new MkDialog(mContext);
			mDialog.setContent("탈퇴하시면 모든 정보가 지워지게됩니다. 정말로 탈퇴하시겠습니까?");
			mDialog.setBtn1("확인");
			mDialog.setBtn1ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					new SettingExitTask(mDialog, "회원탈퇴중...", true, mContext).execute();
				}
			});
			mDialog.setBtn2("취소");
			mDialog.setBtn2ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();
				}
			});
			mDialog.show();
			break;
		case R.id.btnSettingAbout:
//			intent = new Intent(android.content.Intent.ACTION_SEND);
//			intent.setType("plain/text");
//			intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"이메일주소"});
//			intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "제목");
//			intent.putExtra(android.content.Intent.EXTRA_TEXT, "내용");
//			startActivity(Intent.createChooser(intent, "이메일 전송"));
			intent = new Intent(SettingActivity.this, SettingAboutActivity.class);
			startActivity(intent);
			break;
		}
		
	}

}
