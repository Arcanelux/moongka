/**
 * SettingNoticeCreateTask
 * 공지사항 불러오기 액션
 */
package com.anth.moongka.setting;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class SettingNoticeCreateTask extends MkTask{
	private final String TAG = "MK_SettingNoticeTask";
	private Context mContext;
	private Activity mActivity;
	
	private ArrayList<MkNotice> noticeList = new ArrayList<MkNotice>();
	private int page_num;
	private boolean isCert = false;

	public SettingNoticeCreateTask(int page_num, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext= context;
		mActivity = (Activity) mContext;
		this.page_num = page_num;
	}


	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.noticeList(noticeList, page_num);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Intent intent = new Intent(mActivity, SettingNoticeActivity.class);
			intent.putExtra("noticeList", noticeList);
			mActivity.startActivity(intent);
		} else{
			Toast.makeText(mContext, "공지사항을 불러오는데 실패했습니다", Toast.LENGTH_SHORT).show();
		}
	}
}
