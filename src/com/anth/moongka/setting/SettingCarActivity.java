/**
 * SettingCarActivity
 *  차량 정보 수정(안씀)
 */
package com.anth.moongka.setting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_arrayadapter.MkCodeSpAA;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.function.BitmapEdit;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.network.HttpPostMultiPart;
import com.anth.moongka.network.ImageData;
import com.anth.moongka.network.StringData;
import com.anth.moongka.profile_car_list.MkCarListProfileEdit;

public class SettingCarActivity extends MkActivity implements OnClickListener, OnItemSelectedListener, OnCheckedChangeListener{
	private final String TAG = "MK_SettingCarActivity";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	private final int TO_SETTING_CAR_PHOTO = 3432;
	private boolean isSpEnable = false;

	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;

	private Spinner spMaker, spName;
	public static MkCodeSpAA spMakerAA, spNameAA;
	private int curMakerPos, curNamePos;
	private EditText etOld, etNick;
	private ToggleButton tbType1, tbType2, tbType3;
	private ToggleButton[] tbList;
	private String curType;

	private Button btnFindPhoto;
	private ImageView ivPhoto;
	private Button btnOK;

	private ArrayList<MkCar> mkCarList;
	private ArrayList<MkCode> makerList = new ArrayList<MkCode>();
	private ArrayList<MkCode> nameList = new ArrayList<MkCode>();

	private Uri imageUri;

	private MkCarListProfileEdit mCar;
	private int firstNamePos;
	private boolean isFirst = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_car_edit);
		mContext = this;
		mImageDownloader = new ImageDownloader(mContext);

		Intent intent = getIntent();
		// 사용자 자동차 정보
		mCar = (MkCarListProfileEdit) intent.getSerializableExtra("mCar");

		// 전체 자동차 정보
		mkCarList = C.LIST_MKCAR;
		for(MkCar curMkCar : mkCarList){
			// 메이커 리스트 생성
			makerList.add(curMkCar.getMaker());
			// 사용자 자동차의 제조사와 일치하는 배열에서
			if(mCar.getMakerCode().equals(curMkCar.getMaker().getCode())){
				// nameList를 뽑는다
				nameList = curMkCar.getCar();
			}
		}

		// Spinner에 값을 할당하기 위해 position을 찾는다
		// 메이커 위치
		int makerPos = C.findMkCodePosition(mCar.getMakerName(), makerList);
		// 차량이름 위치
		final int namePos = C.findMkCodePosition(mCar.getNameName(), nameList);
		firstNamePos = namePos;

		/** Top VIew **/
		viewTop = findViewById(R.id.viewSettingCarTop);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("마이카 프로필 수정");
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);

		/** Spinner **/
		spMaker = (Spinner) findViewById(R.id.spSettingCarMaker);
		spName = (Spinner) findViewById(R.id.spSettingCarName);
		spMaker.setOnItemSelectedListener(this);
		spName.setOnItemSelectedListener(this);
		spMakerAA = new MkCodeSpAA(mContext, android.R.layout.simple_list_item_1, makerList);
		spNameAA = new MkCodeSpAA(mContext, android.R.layout.simple_list_item_1, nameList);
		spMaker.setAdapter(spMakerAA);
		spName.setAdapter(spNameAA);
		
		
		spMaker.setSelection(makerPos);
		

		/** Layout **/
		etOld = (EditText) findViewById(R.id.etSettingCarOld);
		etNick = (EditText) findViewById(R.id.etSettingCarNickName);
		
		etOld.setText(mCar.getYear());
		etNick.setText(mCar.getNick());

		tbType1 = (ToggleButton) findViewById(R.id.tbSettingCarType1);
		tbType2 = (ToggleButton) findViewById(R.id.tbSettingCarType2);
		tbType3 = (ToggleButton) findViewById(R.id.tbSettingCarType3);
		tbType1.setOnCheckedChangeListener(this);
		tbType2.setOnCheckedChangeListener(this);
		tbType3.setOnCheckedChangeListener(this);
		
		
		tbList = new ToggleButton[] { tbType1, tbType2, tbType3 };
		String curType = mCar.getTypeName();
		Log.d(TAG, "curType : " + curType);
		if(curType.equals("마이카")){
			tbType1.setChecked(true);
		} else if(curType.equals("드림카")){
			tbType2.setChecked(true);
		} else{
			tbType3.setChecked(true);	
		}

		btnFindPhoto = (Button) findViewById(R.id.btnSettingCarFindPhoto);
		btnOK = (Button) findViewById(R.id.btnSettingCarOK);
		btnFindPhoto.setOnClickListener(this);
		btnOK.setOnClickListener(this);

		ivPhoto = (ImageView) findViewById(R.id.ivSettingCarPhoto);
		mImageDownloader.download(C.MK_BASE + mCar.getUrlPhoto().replaceAll("/data/", "/thumb_data/data/"), ivPhoto);

//		isSpEnable = true;
		spName.post(new Runnable() {
			@Override
			public void run() {
				spName.setSelection(namePos);
			}
		});
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;

		case R.id.btnSettingCarFindPhoto:
			intent = new Intent(Intent.ACTION_PICK);
			intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
			intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI); // images on the SD card.
			startActivityForResult(intent, TO_SETTING_CAR_PHOTO);
			break;
		case R.id.btnSettingCarOK:
			HttpPostMultiPart mHttpPostMultiPart = new HttpPostMultiPart(C.API_MKCAR_EDIT, mContext);
			if(imageUri!=null){
				mHttpPostMultiPart.addImageData(new ImageData("settingCarPhoto.jpg", "car_photo[0]", imageUri));
			}
			mHttpPostMultiPart.addStringData(new StringData("mem_idx", C.mem_idx));
			mHttpPostMultiPart.addStringData(new StringData("mem_id", C.mem_id));
			mHttpPostMultiPart.addStringData(new StringData("car_company", nameList.get(curNamePos).getCode()));
			mHttpPostMultiPart.addStringData(new StringData("car_year", etOld.getText().toString()));
			mHttpPostMultiPart.addStringData(new StringData("car_type", curType));
			mHttpPostMultiPart.addStringData(new StringData("car_nick", etNick.getText().toString()));
			new SettingCarEditTask(mHttpPostMultiPart, "프로필 수정중...", true, mContext).execute();
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		if(isSpEnable){
			switch(parent.getId()){
			case R.id.spSettingCarMaker:
				Log.d(TAG, "MakerSpinner " + position);
				curMakerPos = position;
				nameList.clear();
				nameList.addAll(mkCarList.get(position).getCar());

				spNameAA.notifyDataSetChanged();
				break;
			case R.id.spSettingCarName:
				curNamePos = position;
				break;
			}
		} else if(!isSpEnable && !isFirst){
			isSpEnable = true;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode==RESULT_OK){
			switch(requestCode){
			case TO_SETTING_CAR_PHOTO :
				Cursor c = getContentResolver().query(data.getData(), null, null, null, null);
				c.moveToNext();
				String path = c.getString(c.getColumnIndex(MediaStore.MediaColumns.DATA));
				Uri uri = Uri.fromFile(new File(path));
				Log.d(TAG, "Uri : " + uri);
//				Toast.makeText(mContext, uri.toString(), Toast.LENGTH_SHORT).show();

				imageUri = uri;
				setImageThumbnail(imageUri, ivPhoto);
				break;
			}
		}
	}

	/** 썸네일 삽입, 정사각형에 250픽셀로 리사이즈하여 삽입한다 **/
	private void setImageThumbnail(Uri uri, ImageView iv){
		try {
			Bitmap curBitmap = Images.Media.getBitmap(getContentResolver(), uri);
			// BitmapEdit클래스에서 비트맵을 리사이즈하여 돌려줌
			Bitmap editBitmap = BitmapEdit.resizeBitmapSquare(curBitmap, 250);
			iv.setImageBitmap(editBitmap);
		} catch (FileNotFoundException e) { e.printStackTrace(); }
		catch (IOException e) { e.printStackTrace(); }
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	private void setToggleOffOther(int tbId){
		for(ToggleButton curTb : tbList){
			if(!(curTb.getId()==tbId)) { curTb.setChecked(false); }
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		for(ToggleButton curTb : tbList){
			if(curTb.getId()==buttonView.getId()){
				if(isChecked) setToggleOffOther(buttonView.getId());
			}
		}
		if(isChecked){
			switch(buttonView.getId()){
			case R.id.tbSettingCarType1:
//				curType = "마이카";
				curType = "T101";
				break;
			case R.id.tbSettingCarType2:
//				curType = "드림카";
				curType = "T102";
				break;
			case R.id.tbSettingCarType3:
//				curType = "없음";
				curType = "T103";
				break;
			}
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if(hasFocus && isFirst){
			spName.setSelection(firstNamePos);
			isFirst = false;
		} 
	}


}
