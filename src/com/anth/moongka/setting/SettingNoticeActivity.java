/**
 * SettingNoticeActivity
 * 공지사항
 */
package com.anth.moongka.setting;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;

public class SettingNoticeActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_SettingNoticeActivity";
	private Context mContext;

	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;

	private ArrayList<MkNotice> noticeList = new ArrayList<MkNotice>();
	private ListView lvNotice;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_notice);
		mContext = this;

		Intent intent = getIntent();
		noticeList = (ArrayList<MkNotice>) intent.getSerializableExtra("noticeList");

		viewTop = findViewById(R.id.viewSettingNoticeTop);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("공지사항");
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);

		lvNotice = (ListView) findViewById(R.id.lvSettingNotice);
		lvNotice.setAdapter(new ArrayAdapter<MkNotice>(mContext, android.R.layout.simple_list_item_1, noticeList){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View curView = convertView;
				if(curView==null){
					LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					curView = inflater.inflate(R.layout.setting_notice_list_item, null);
				}

				MkNotice curNotice = noticeList.get(position);
				String noticeTitle = curNotice.getTitle();
				TextView tvName = (TextView) curView.findViewById(R.id.tvSettingNoticeItem);
				tvName.setText(noticeTitle);

				return curView;
			}
		});

		lvNotice.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
				Intent intent = new Intent(SettingNoticeActivity.this, SettingNoticeDetailActivity.class);
				intent.putExtra("mNotice", noticeList.get(position));
				startActivity(intent);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		}
	}
}