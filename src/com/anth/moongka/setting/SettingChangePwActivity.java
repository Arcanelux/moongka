/**
 * SettingChangePwActivity
 *  비밀번호 변경
 */
package com.anth.moongka.setting;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;

public class SettingChangePwActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_SettingChangePwActivity";
	private Context mContext;

	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;

	private EditText etOri, etNew1, etNew2;
	private Button btnOK;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_change_pw);
		mContext = this;

		/** Top View **/
		viewTop = findViewById(R.id.viewSettingChangePwTop);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("비밀번호 변경");
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);

		etOri = (EditText) findViewById(R.id.etSettingChangePwOri);
		etNew1 = (EditText) findViewById(R.id.etSettingChangePwNew1);
		etNew2 = (EditText) findViewById(R.id.etSettingChangePwNew2);

		btnOK = (Button) findViewById(R.id.btnSettingChangePwOk);
		btnOK.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnSettingChangePwOk:
			String oriPw = etOri.getText().toString();
			String newPw1 = etNew1.getText().toString();
			String newPw2 = etNew2.getText().toString();

			if(oriPw.equals("")){
				Toast.makeText(mContext, "기존 비밀번호를 입력해주세요", Toast.LENGTH_SHORT).show();
			} else if(newPw1.equals("")){
				Toast.makeText(mContext, "새 비밀번호를 입력해주세요", Toast.LENGTH_SHORT).show();
			} else if(newPw2.equals("")){
				Toast.makeText(mContext, "새 비밀번호를 입력해주세요", Toast.LENGTH_SHORT).show();
			} else if(!newPw1.equals(newPw2)){
				Toast.makeText(mContext, "새 비밀번호와 확인 비밀번호가 다릅니다", Toast.LENGTH_SHORT).show();
			} else{
				new SettingChangePwTask(oriPw, newPw1, "비밀번호 변경중...", true, mContext).execute();
			}
			break;
		}
	}

}
