/**
 * SettingProfileTask
 * 프로필 수정 액션
 */
package com.anth.moongka.setting;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.menu.MenuLeft;
import com.anth.moongka.network.HttpPostMultiPart;
import com.anth.moongka.network.ImageData;
import com.anth.moongka.network.StringData;

public class SettingProfileTask extends MkTask{
	private final String TAG = "MK_SettingProfileTask";
	private Context mContext;
	private Activity mActivity;
	private HttpPostMultiPart mHttpPostMultiPart;

	private String mem_idx;
	private String mem_id;
	private String code_job, code_area, code_city, code_question;
	private String answer;
	private Uri uri_photo;
	private String introduce;
	private String call;

	public SettingProfileTask(String mem_idx, String mem_id, 
			String code_job, String code_area, String code_city, String code_question, String answer, 
			Uri uri_photo, String introduce, String call, 
			String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity) mContext;
		mHttpPostMultiPart = new HttpPostMultiPart(C.API_SETTING_PROFILE_EDIT, mContext);
		this.mem_idx = mem_idx;
		this.mem_id = mem_id;
		this.code_job = code_job;
		this.code_area = code_area;
		this.code_city = code_city;
		this.code_question = code_question;
		this.answer = answer;
		this.uri_photo = uri_photo;
		this.introduce = introduce;
		this.call = call;

		if(uri_photo!=null){
			Log.d(TAG, "uri_photo : " + uri_photo.toString());
			mHttpPostMultiPart.addImageData(new ImageData("profile.jpg", "mem_profile[0]", uri_photo));
			mHttpPostMultiPart.addStringData(new StringData("imgyn", "android"));
		} else{
			mHttpPostMultiPart.addStringData(new StringData("imgyn", "android"));
		}
		mHttpPostMultiPart.addStringData(new StringData("mem_idx", mem_idx));
		mHttpPostMultiPart.addStringData(new StringData("mem_id", mem_id));
		mHttpPostMultiPart.addStringData(new StringData("mem_job", code_job));
		mHttpPostMultiPart.addStringData(new StringData("mem_area", code_city));
		mHttpPostMultiPart.addStringData(new StringData("mem_question", code_question));
		mHttpPostMultiPart.addStringData(new StringData("mem_question_ans", answer));
		mHttpPostMultiPart.addStringData(new StringData("mem_introduce", introduce));
		mHttpPostMultiPart.addStringData(new StringData("mem_phone", call));
	}

	@Override
	protected Void doInBackground(Void... params) {
		mHttpPostMultiPart.execute();
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
//		Toast.makeText(mContext, mHttpPostMultiPart.getResult(), Toast.LENGTH_SHORT).show();
		try {
			JSONObject jObjSettingProfile = new JSONObject(mHttpPostMultiPart.getResult());
			String returnStatus = jObjSettingProfile.getString("returnStatus");
			String returnMessage = jObjSettingProfile.getString("returnMessage");
			String url_profile_photo = jObjSettingProfile.getString("MM_PROFILE_PIC_PATH");
			// 자신의 프로필사진 주소를 바꾼다
			C.mem_profile = C.MK_BASE + url_profile_photo;
			
			if(returnStatus.equals("Y")){
				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT).show();
				// 결과가 Y일 경우, 좌측메뉴의 프로필사진을 다시 다운로드해준다
				MenuLeft.notifyPhotoChanged();
				mActivity.setResult(Activity.RESULT_OK);
				mActivity.finish();
			} else{
				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(mContext, "프로필 수정중에 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
		}
		
	}


}
