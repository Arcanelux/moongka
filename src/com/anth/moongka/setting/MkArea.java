/**
 * MkCar
 *  아마도 이제 안이용
 */
package com.anth.moongka.setting;

import java.io.Serializable;
import java.util.ArrayList;

import com.anth.moongka.fix_value.MkCode;

public class MkArea implements Serializable{
	MkCode area;
	ArrayList<MkCode> city;
	
	
	public MkArea(MkCode area, ArrayList<MkCode> city) {
		this.area = area;
		this.city = city;
	}
	
	public MkCode getArea() {
		return area;
	}

	public void setArea(MkCode area) {
		this.area = area;
	}

	public ArrayList<MkCode> getCity() {
		return city;
	}

	public void setCity(ArrayList<MkCode> city) {
		this.city = city;
	}
}