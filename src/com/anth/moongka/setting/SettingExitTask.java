/**
 * SettingExitTask
 * 회원탈퇴 액션
 */
package com.anth.moongka.setting;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.function.MkPref;

public class SettingExitTask extends MkTask{
	private final String TAG = "MK_SettingExitTask";
	private Context mContext;
	private Activity mActivity;
	
	private boolean isCert = false;
	private Dialog mDialog;

	public SettingExitTask(Dialog mDialog, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity)mContext;
		this.mDialog = mDialog;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.exit();
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		Intent intent;
		super.onPostExecute(result);
		if(isCert){
			mDialog.dismiss();
			MkPref.removeSettingData(mContext);
			Toast.makeText(mContext, "회원탈퇴가 완료되었습니다. 사용자 정보가 삭제되었습니다", Toast.LENGTH_LONG).show();
			mActivity.setResult(4);
			mActivity.finish();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

}
