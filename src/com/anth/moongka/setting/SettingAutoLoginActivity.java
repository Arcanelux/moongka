/**
 * SettingAutoLoginActivity
 *  자동로그인 설정
 */
package com.anth.moongka.setting;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.MkPref;
import com.anth.moongka.function.SavedValue;

public class SettingAutoLoginActivity extends MkActivity implements OnCheckedChangeListener, OnClickListener{
	private final String TAG = "MK_SettingAutoLoginActivity";
	private Context mContext;
	
	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;
	
	private ToggleButton tb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_autologin);
		mContext = this;
		
		viewTop = findViewById(R.id.viewSettingAutoLoginTop);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("자동로그인 설정");
		
		tb = (ToggleButton) findViewById(R.id.tbSettingAutoLogin);
		tb.setOnCheckedChangeListener(this);
		
		SavedValue sValue = MkPref.getSavedValue(mContext);
		boolean isAutoLogin = sValue.isAutoLogin();
		tb.setChecked(isAutoLogin);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(isChecked){
			MkPref.setAutoLogin(mContext, true);
		} else{
			MkPref.setAutoLogin(mContext, false);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		}
	}

}
