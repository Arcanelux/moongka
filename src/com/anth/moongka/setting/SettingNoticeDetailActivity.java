/**
 * SettingNoticeDetailActivity
 * 공지사항 보기
 */
package com.anth.moongka.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;

public class SettingNoticeDetailActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_SettingNoticeDetailActivity";
	private Context mContext;
	
	private View viewTop;
	private TextView tvTitle;
	private ImageButton ibBack;
	
	private MkNotice mNotice;
	private TextView tvContent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_notice_detail);
		
		Intent intent = getIntent();
		mNotice = (MkNotice) intent.getSerializableExtra("mNotice");
		
		viewTop = findViewById(R.id.viewTopSettingNoticeDetail);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		ibBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		
		tvTitle.setText(mNotice.getTitle());
		ibBack.setOnClickListener(this);
		
		tvContent = (TextView) findViewById(R.id.tvSettingNoticeDetail);
		tvContent.setText(mNotice.getContent());
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		}
	}

}
