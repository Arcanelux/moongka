package com.anth.moongka.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;

public class SettingAboutActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_AboutActivity";
	private Context mContext;
	
	private View viewTop;
	private ImageView ivSendMail;
	private ImageButton btnBack;
	private TextView cvTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_about);
		
		viewTop = findViewById(R.id.viewSettingAboutTop);
		ivSendMail = (ImageView) findViewById(R.id.ivSettingAboutSendMail);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		cvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		
		cvTitle.setText("About");
		
		btnBack.setOnClickListener(this);
		ivSendMail.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.ivSettingAboutSendMail:
			Intent intent = new Intent(android.content.Intent.ACTION_SEND);
			intent.setType("plain/text");
			intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"moongka@moongka.com"});
			intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
			intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
			startActivity(Intent.createChooser(intent, "이메일 전송"));
			break;
		case R.id.btnCert1 :
			finish();
			break;
		}
	}

}
