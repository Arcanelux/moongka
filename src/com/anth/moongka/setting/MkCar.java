/**
 * MkCar
 *  아마도 이제 안이용
 */
package com.anth.moongka.setting;

import java.io.Serializable;
import java.util.ArrayList;

import com.anth.moongka.fix_value.MkCode;

public class MkCar implements Serializable{
	MkCode maker;
	ArrayList<MkCode> car;
	ArrayList<ArrayList<MkCode>> model;
	
//	public MkCar(MkCode maker, ArrayList<MkCode> car) {
//		this.maker = maker;
//		this.car = car;
//	}
	public MkCar(MkCode maker, ArrayList<MkCode> car, ArrayList<ArrayList<MkCode>> model) {
		this.maker = maker;
		this.car = car;
		this.model = model;
	}

	public MkCode getMaker() {
		return maker;
	}

	public void setMaker(MkCode maker) {
		this.maker = maker;
	}

	public ArrayList<MkCode> getCar() {
		return car;
	}

	public void setCar(ArrayList<MkCode> car) {
		this.car = car;
	}

	public ArrayList<ArrayList<MkCode>> getModel() {
		return model;
	}

	public void setModel(ArrayList<ArrayList<MkCode>> model) {
		this.model = model;
	}
	
	
}
