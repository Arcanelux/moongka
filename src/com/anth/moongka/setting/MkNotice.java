/**
 * MkNotice
 *  공지사항 형식
 */
package com.anth.moongka.setting;

import java.io.Serializable;

public class MkNotice implements Serializable{
	private String idx;
	private String title;
	private String content;
	private int hits;
	private String writer_idx;
	private String strDate;
	
	public MkNotice(String idx, String title, String content, int hits,
			String writer_idx, String strDate) {
		this.idx = idx;
		this.title = title;
		this.content = content;
		this.hits = hits;
		this.writer_idx = writer_idx;
		this.strDate = strDate;
	}

	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public String getWriter_idx() {
		return writer_idx;
	}

	public void setWriter_idx(String writer_idx) {
		this.writer_idx = writer_idx;
	}

	public String getStrDate() {
		return strDate;
	}

	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}
	
	
}
