/**
 * SettingCar
 *  �Ⱦ�
 */
package com.anth.moongka.setting;

import java.io.Serializable;

public class SettingCar implements Serializable {
	private String makerCode, makerName;
	private String typeCode, typeName;
	private String nameCode, nameName;
	private String urlPhoto;
	private String nick;
	private String year;
	
	public SettingCar(){
		
	}
	public SettingCar(String makerCode, String makerName, String typeCode,
			String typeName, String nameCode, String nameName, String urlPhoto,
			String nick, String year) {
		this.makerCode = makerCode;
		this.makerName = makerName;
		this.typeCode = typeCode;
		this.typeName = typeName;
		this.nameCode = nameCode;
		this.nameName = nameName;
		this.urlPhoto = urlPhoto;
		this.nick = nick;
		this.year = year;
	}
	
	public void init(String makerCode, String makerName, String typeCode,
			String typeName, String nameCode, String nameName, String urlPhoto,
			String nick, String year) {
		this.makerCode = makerCode;
		this.makerName = makerName;
		this.typeCode = typeCode;
		this.typeName = typeName;
		this.nameCode = nameCode;
		this.nameName = nameName;
		this.urlPhoto = urlPhoto;
		this.nick = nick;
		this.year = year;
	}

	public String getMakerCode() {
		return makerCode;
	}

	public void setMakerCode(String makerCode) {
		this.makerCode = makerCode;
	}

	public String getMakerName() {
		return makerName;
	}

	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getNameCode() {
		return nameCode;
	}

	public void setNameCode(String nameCode) {
		this.nameCode = nameCode;
	}

	public String getNameName() {
		return nameName;
	}

	public void setNameName(String nameName) {
		this.nameName = nameName;
	}

	public String getUrlPhoto() {
		return urlPhoto;
	}

	public void setUrlPhoto(String urlPhoto) {
		this.urlPhoto = urlPhoto;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
}
