/**
 * SettingProfileActivity
 * 프로필 수정
 */
package com.anth.moongka.setting;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_arrayadapter.MkCodeSpAA;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.function.Lhy_Function;

public class SettingProfileActivity extends MkActivity implements
		OnClickListener, OnItemSelectedListener {
	private final String TAG = "MK_SettingProfileActivity";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	private final int TO_SETTING_PROFILE_EDIT_PHOTO = 1251;

	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;

	private Spinner spJob, spArea, spCity, spQuestion;
	private MkCodeSpAA spJobAA, spAreaAA, spCityAA, spQuesAA;
	private ArrayList<MkCode> jobList, areaList, quesList;
	private ArrayList<MkCode> cityList = new ArrayList<MkCode>();
	private ArrayList<MkArea> MKAreaList;
	private int curJobPos, curAreaPos, curCityPos, curQuesPos;

	private EditText etAnswer, etIntroduce, etCall;
	private Button btnFindPhoto, btnOK;
	private ImageView ivPhoto;

	private Uri imageUri;

	private SettingProfile mProfile;
	private boolean isSpEnable = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_profile_edit);
		mContext = this;
		mImageDownloader = new ImageDownloader(mContext);

		Intent intent = getIntent();
		mProfile = (SettingProfile) intent.getSerializableExtra("mProfile");

		/** Top View **/
		viewTop = findViewById(R.id.viewSettingProfileTop);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("프로필 수정");
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);

		/** Spinner **/
		jobList = C.LIST_JOB;
		// cityList = C.LIST_CITY;
		quesList = C.LIST_QUESTION;
		MKAreaList = C.LIST_AREA;

		areaList = new ArrayList<MkCode>();
		areaList.add(new MkCode("선택해주세요", "None"));
		cityList = new ArrayList<MkCode>();
		cityList.add(new MkCode("선택해주세요", "None"));
		int jobEndList = 0;
		int areaEndList = 0;
		int quesEndList = 0;
		if (jobList != null && areaList != null && quesList != null) {
			jobEndList = jobList.size();
			areaEndList = areaList.size();
			quesEndList = quesList.size();
			jobList.remove(jobEndList - 1);
			//areaList.remove(areaEndList-1);
			quesList.remove(quesEndList - 1);
		}

		for (MkArea curMKArea : MKAreaList) {
			areaList.add(curMKArea.getArea());
			Log.i(TAG, "" + curMKArea.getArea());
		}

		spJob = (Spinner) findViewById(R.id.spSettingProfileJob);
		spArea = (Spinner) findViewById(R.id.spSettingProfileArea);
		spCity = (Spinner) findViewById(R.id.spSettingProfileCity);
		spQuestion = (Spinner) findViewById(R.id.spSettingProfileQuestion);

		spJobAA = new MkCodeSpAA(mContext, android.R.layout.simple_list_item_1,
				jobList);
		spAreaAA = new MkCodeSpAA(mContext,
				android.R.layout.simple_list_item_1, areaList);
		spCityAA = new MkCodeSpAA(mContext,
				android.R.layout.simple_list_item_1, cityList);
		spQuesAA = new MkCodeSpAA(mContext,
				android.R.layout.simple_list_item_1, quesList);

		spJob.setAdapter(spJobAA);
		spArea.setAdapter(spAreaAA);
		spCity.setAdapter(spCityAA);
		spQuestion.setAdapter(spQuesAA);

		spJob.setOnItemSelectedListener(this);
		spArea.setOnItemSelectedListener(this);
		spCity.setOnItemSelectedListener(this);
		spQuestion.setOnItemSelectedListener(this);

		/** Layout UI **/
		etAnswer = (EditText) findViewById(R.id.etSettingProfileAnswer);
		etIntroduce = (EditText) findViewById(R.id.etSettingProfileIntroduce);
		etCall = (EditText) findViewById(R.id.etSettingProfileCall);
		ivPhoto = (ImageView) findViewById(R.id.ivSettingProfilePhoto);
		btnFindPhoto = (Button) findViewById(R.id.btnSettingProfileFindPhoto);
		btnOK = (Button) findViewById(R.id.btnSettingProfileOK);

		btnFindPhoto.setOnClickListener(this);
		btnOK.setOnClickListener(this);

		/** 서버에서 받아온 내용 세팅 **/
		int jobPos = C.findMkCodePosition(mProfile.getJob(), C.LIST_JOB);
		int areaPos = C.findMkCodePosition(mProfile.getArea(), areaList);
		cityList.addAll(MKAreaList.get(areaPos-1).getCity());
		int cityPos = C.findMkCodePosition(mProfile.getCity(), cityList);
		int quesPos = C.findMkCodePosition(mProfile.getQuestion(),
				C.LIST_QUESTION);

		
		spJob.setSelection(jobPos);
		spArea.setSelection(areaPos);
		spCity.setSelection(cityPos);
		spQuestion.setSelection(quesPos);
		Log.d(TAG, "pos) Job " + jobPos + ", Area " + areaPos + ", Question "
				+ quesPos);

		etAnswer.setText(mProfile.getAnswer());
		etIntroduce.setText(mProfile.getIntroduce());
		etCall.setText(mProfile.getCall());

		mImageDownloader.download(C.MK_BASE + mProfile.getUrl_photo(), ivPhoto);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnSettingProfileFindPhoto:
			intent = new Intent(Intent.ACTION_PICK);
			intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
			intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI); // images
																							// on
																							// the
																							// SD
																							// card.
			startActivityForResult(intent, TO_SETTING_PROFILE_EDIT_PHOTO);
			break;
		case R.id.btnSettingProfileOK:

			String codeJob = jobList.get(curJobPos).getCode();
			String codeQues = quesList.get(curQuesPos).getCode();
			String codeArea = areaList.get(curAreaPos).getCode();
			String codeCity = cityList.get(curCityPos).getCode();
			String answer = etAnswer.getText().toString();
			String introduce = etIntroduce.getText().toString();
			String call = etCall.getText().toString();

			new SettingProfileTask(C.mem_idx, C.mem_id, codeJob, codeArea,
					codeCity, codeQues, answer, imageUri, introduce, call,
					"프로필 수정중...", true, mContext).execute();
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position,
			long id) {
		if (parent.getId() == R.id.spSettingProfileArea) {
			isSpEnable = true;
		}

		switch (parent.getId()) {
		case R.id.spSettingProfileJob:
			curJobPos = position;
			Log.d(TAG, "curJobPos : " + position);
			break;
		case R.id.spSettingProfileQuestion:
			curQuesPos = position;
			Log.d(TAG, "curQuesPos : " + position);
			break;
		}
		if (isSpEnable) {
			switch (parent.getId()) {
			case R.id.spSettingProfileArea:
				curAreaPos = position;
				Log.d(TAG, "curAreaPos : " + position);
				if (position != 0) {
					cityList.clear();
					cityList.add(new MkCode("선택해주세요", "None"));
					Log.i("settingarea", ""+MKAreaList.size());
					cityList.addAll(MKAreaList.get(position - 1).getCity());
					spCityAA.notifyDataSetChanged();
				} else {
					cityList.clear();
					spCityAA.notifyDataSetChanged();
				}

				break;
			case R.id.spSettingProfileCity:
				curCityPos = position;
				break;
			default:
				break;
			}
		}
		// case R.id.spSettingProfileArea:

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case TO_SETTING_PROFILE_EDIT_PHOTO:
			Cursor c = getContentResolver().query(data.getData(), null, null,
					null, null);
			c.moveToNext();
			String path = c.getString(c
					.getColumnIndex(MediaStore.MediaColumns.DATA));
			Uri uri = Uri.fromFile(new File(path));
			Log.d(TAG, "Uri : " + uri);
			// Toast.makeText(mContext, uri.toString(),
			// Toast.LENGTH_SHORT).show();

			imageUri = uri;
			setImageThumbnail(imageUri, ivPhoto);
			Log.d(TAG, "imageUri : " + imageUri);
			break;
		}
	}

	/** 썸네일 삽입, 정사각형에 250픽셀로 리사이즈하여 삽입한다 **/
	private void setImageThumbnail(Uri uri, ImageView iv) {
		// try {
		// // Bitmap curBitmap = Images.Media.getBitmap(getContentResolver(),
		// uri);
		// // Bitmap editBitmap = BitmapEdit.resizeBitmapSquare(curBitmap, 250);
		// // Log.d(TAG, "editBitmap : " + editBitmap.getWidth() + ", " +
		// editBitmap.getHeight());
		// // iv.setImageBitmap(editBitmap);
		// C.setImageThumbnail(uri, iv);
		// } catch (FileNotFoundException e) { e.printStackTrace(); }
		// catch (IOException e) { e.printStackTrace(); }
		C.setImageThumbnail(uri, iv, 4);
	}

	@Override
	public void finish() {
		super.finish();
		Lhy_Function.forceHideKeyboard(mContext, etAnswer);
	}

}
