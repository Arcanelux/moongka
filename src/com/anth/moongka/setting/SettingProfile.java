/**
 * SettingProfile
 * 프로필 정보
 */
package com.anth.moongka.setting;

import java.io.Serializable;

public class SettingProfile implements Serializable{
	private String url_photo;
	private String introduce;
	private String answer;
	private String question;
	private String job;
	private String area;
	private String city;
	private String call;
	
	
	public void init(String url_photo, String introduce, String answer,
			String question, String job, String area, String city, String call) {
		this.url_photo = url_photo;
		this.introduce = introduce;
		this.answer = answer;
		this.question = question;
		this.job = job;
		this.area = area;
		this.city = city;
		this.call = call;
	}
	
	public SettingProfile(){
		
		
	}
	public SettingProfile(String url_photo, String introduce, String answer,
			String question, String job, String area, String city, String call) {
		this.url_photo = url_photo;
		this.introduce = introduce;
		this.answer = answer;
		this.question = question;
		this.job = job;
		this.area = area;
		this.city = city;
		this.call = call;
	}

	public String getUrl_photo() {
		return url_photo;
	}

	public void setUrl_photo(String url_photo) {
		this.url_photo = url_photo;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCall() {
		return call;
	}

	public void setCall(String call) {
		this.call = call;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	
	
}
