package com.anth.moongka.setting;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class SettingAlarmTask extends MkTask{
	private final String TAG = "MK_SettingPushTask";
	private Context mContext;
	private Activity mActivity;
	
	//private boolean isCert = false;
	private String cert;
	private Dialog mDialog;

	public SettingAlarmTask(Dialog mDialog, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity)mContext;
		this.mDialog = mDialog;
	}

	@Override
	protected Void doInBackground(Void... params) {
		
		cert = MkApi.settingPush();
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		Intent intent;
		super.onPostExecute(result);
		if(cert.equals("ON")){
			mDialog.dismiss();
			Toast.makeText(mContext,  MkApi.returnMsg, Toast.LENGTH_LONG).show();
		} else if(cert.equals("OFF")){
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
			
	}

}