/**
 * SettingChangePwTask
 * 비밀번호 변경 액션
 */
package com.anth.moongka.setting;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class SettingChangePwTask extends MkTask {
	private final String TAG = "MK_SettingChangePwTask";
	private Context mContext;
	
	private String oriPw, newPw;
	private boolean isCert = false;

	public SettingChangePwTask(String one_pass, String new_pass, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		oriPw = one_pass;
		newPw = new_pass;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.changePw(oriPw, newPw);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
			((Activity)mContext).finish();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

	
}
