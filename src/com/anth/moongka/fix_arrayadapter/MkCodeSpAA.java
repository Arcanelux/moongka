/**
 * MkCodeSpAA
 *  MkCode의 Spinner ArrayAdapter
 *  모든 MkCode의 Spinner에서 사용가능
 */
package com.anth.moongka.fix_arrayadapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkCode;

public class MkCodeSpAA extends ArrayAdapter<MkCode>{
	public Context mContext;
	public ArrayList<MkCode> codeList;
	public int itemRes = R.layout.spinner_item;

	public MkCodeSpAA(Context context, int textViewResourceId, ArrayList<MkCode> codeList) {
		super(context, textViewResourceId, codeList);
		this.mContext = context;
		this.codeList = codeList;
		
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		MkCode curJoinJob = codeList.get(position);

		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(mContext);
			convertView = inflater.inflate(itemRes, parent, false);
		}
		TextView tv = (TextView) convertView.findViewById(R.id.tvSpItem);
		tv.setText(curJoinJob.getName());
		
		return convertView;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MkCode curCode = codeList.get(position);
		LayoutInflater inflater = LayoutInflater.from(mContext);
		convertView = inflater.inflate(itemRes, parent, false);
		if(convertView == null){
			
		}
		
		TextView tv = (TextView) convertView.findViewById(R.id.tvSpItem);
		tv.setText(curCode.getName());
		return convertView;
	}

}