/**
 * SettingCarEditTask
 *  차량 정보 수정 액션
 */
package com.anth.moongka.profile_car;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.network.HttpPostMultiPart;

public class MkCarProfileEditTask extends MkTask{
	private final String TAG = "MK_SettingCarEditTask";
	private Context mContext;
	
	private HttpPostMultiPart mHttpPostMultiPart;

	public MkCarProfileEditTask(HttpPostMultiPart mHttpPostMultiPart, String msg, boolean isProgressDialog,
			Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.mHttpPostMultiPart = mHttpPostMultiPart;
	}


	@Override
	protected Void doInBackground(Void... params) {
		mHttpPostMultiPart.execute();
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		try {
			JSONObject jObjWrite = new JSONObject(mHttpPostMultiPart.getResult());
			String returnStatus = jObjWrite.getString("returnStatus");
			String returnMessage = jObjWrite.getString("returnMessage");
			
			if(returnStatus.equals("Y")){
				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT).show();
				((Activity)mContext).finish();
			} else{
				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}


}
