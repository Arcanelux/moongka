package com.anth.moongka.profile_car;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.profile_car_list.MkCarListProfileAddActivity;
import com.anth.moongka.profile_car_list.MkCarListProfileToEditMenuLeftCarProfileTask;

public class MKCarAddInformationActivity extends Activity implements OnClickListener{

	private final String TAG = "MK_MKCarAddInformationActivity";
	
	public static Context mContext;
	
	private ImageDownloader mImageDownloader;
	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;
	
	private Button btnConfirm;
	
	private boolean isApplyEditProfile = false;
	
	public static final int NORMAL = 92387;
	public static final int DREAMCAR1 = 92388;
	public static final int DREAMCAR2 = 92389;
	public static final int MAINCAR = 92390;
	
	private String you_idx;
	private int posStart = 0;
	private int posMainCar = 0;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.mkcaraddinformation);
	
	    viewTop = findViewById(R.id.viewCarAddTop);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("뭉카 프로필 안내");
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		
		btnConfirm = (Button) findViewById(R.id.mkcaraddinformation_confirm_btn);
		btnConfirm.setOnClickListener(this);
		
		mContext = this;
		mImageDownloader = new ImageDownloader(mContext);

		
		Intent intent = getIntent();
		you_idx = intent.getStringExtra("you_idx");
		int where = intent.getIntExtra("where", 0);
		
		switch(where){
		case NORMAL:
			posStart = 0;
			break;
		case DREAMCAR1:
			posStart = C.LIST_MK_CAR_PROFILE.size()-2;
			break;
		case DREAMCAR2:
			posStart = C.LIST_MK_CAR_PROFILE.size()-1;
			break;
		case MAINCAR:
			posStart = posMainCar;
			break;
		}
	    // TODO Auto-generated method stub
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnCert1:
			finish();
			break;
		case R.id.mkcaraddinformation_confirm_btn :
			Intent intent = new Intent(getApplicationContext(),MkCarListProfileAddActivity.class);
			startActivityForResult(intent, 0);
			
			break;
		default:
			break;
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		//this.setResult(Activity.RESULT_OK);
		finish();
		super.onActivityResult(requestCode, resultCode, data);
	}
	@Override
	public void finish() {
		
		if(you_idx.equals(C.mem_idx) && !isApplyEditProfile){
//			new MkCarListProfileToEditMenuLeftCarProfileTask("프로필 변경사항 적용중...", true, mContext).execute();
			new MkCarListProfileToEditMenuLeftCarProfileTask("프로필 변경사항 적용중...", false, mContext).execute();
			isApplyEditProfile = true;
		} else{
			super.finish();
		}
	}

}
