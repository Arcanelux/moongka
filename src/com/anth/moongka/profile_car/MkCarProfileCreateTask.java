/**
 * SettingCarCreateTask
 *  차량정보 수정 - 정보 불러오기 액션 (안씀)
 */
package com.anth.moongka.profile_car;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.setting.MkCar;

public class MkCarProfileCreateTask extends MkTask {
	private final String TAG = "MK_SettingCarCreateTask";
	private Context mContext;
	private Activity mActivity;
	
	private MkCarProfile mCar = new MkCarProfile();
	private ArrayList<MkCar> mkCarList = new ArrayList<MkCar>();
	private boolean isCert;

	public MkCarProfileCreateTask(String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity) mContext;
	}

	@Override
	protected Void doInBackground(Void... params) {
//		isCert = MkApi.carInfo(C.mem_idx, mCar);
		isCert = MkApi.getMkCarList();
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Intent intent = new Intent(mActivity, MkCarProfileActivity.class);
			intent.putExtra("mCar", mCar);
//			intent.putExtra("mkCarList", mkCarList);
			C.LIST_MKCAR = mkCarList;
			mActivity.startActivity(intent);
		} else{
			Toast.makeText(mContext, "차량 프로필 로딩에 실패했습니다", Toast.LENGTH_SHORT).show();
		}
	}

}
