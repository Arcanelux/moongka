/**
 * Join1Params
 *  회원가입정보 1
 */
package com.anth.moongka.join;

import java.io.Serializable;
import java.util.Calendar;

import com.anth.moongka.fix_value.MkCode;


public class Join1Params implements Serializable{
	private String id;
	private String pw1;
	private String pw2;
	private String name;
	private Calendar birthday;
	private boolean agree1;
	private boolean agree2;
	private String gender;
	private MkCode questionCode;
	private String answer;
	private String call;
	private String deviceType = "android";
	
	public Join1Params(){
		
	}
	public Join1Params(String id, String pw1, String pw2, String name,
			Calendar birthday, boolean agree1, boolean agree2, String gender,
			MkCode questionCode, String answer, String call, String deviceType) {
		super();
		this.id = id;
		this.pw1 = pw1;
		this.pw2 = pw2;
		this.name = name;
		this.birthday = birthday;
		this.agree1 = agree1;
		this.agree2 = agree2;
		this.gender = gender;
		this.questionCode = questionCode;
		this.answer = answer;
		this.call = call;
		this.deviceType = deviceType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPw1() {
		return pw1;
	}

	public void setPw1(String pw1) {
		this.pw1 = pw1;
	}

	public String getPw2() {
		return pw2;
	}

	public void setPw2(String pw2) {
		this.pw2 = pw2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Calendar getBirthday() {
		return birthday;
	}

	public void setBirthday(Calendar birthday) {
		this.birthday = birthday;
	}

	public boolean isAgree1() {
		return agree1;
	}

	public void setAgree1(boolean agree1) {
		this.agree1 = agree1;
	}

	public boolean isAgree2() {
		return agree2;
	}

	public void setAgree2(boolean agree2) {
		this.agree2 = agree2;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public MkCode getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(MkCode questionCode) {
		this.questionCode = questionCode;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getCall() {
		return call;
	}

	public void setCall(String call) {
		this.call = call;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	
}
