/**
 * JoinTermActivity
 * '자세히'버튼 눌렀을때의 액티비티
 */
package com.anth.moongka.join;

import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.Lhy_Function;

public class JoinTermActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_JoinTermActivity";
	private Context mContext;
	
	private View viewTop;
	private TextView tvTitle;
	private ImageButton btnBack;
	
	private String type;
	private TextView tvContent;
	private String content;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.join_term);
		mContext = this;
		
		viewTop = findViewById(R.id.viewTopJoinTerm);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		
		Intent intent = getIntent();
		type = intent.getStringExtra("type");
		
		tvContent = (TextView) findViewById(R.id.tvJoinTerm);
		try {
			if(type.equals("개인정보")){
				content = Lhy_Function.getStringAssetText("gaein.txt", mContext);
				tvTitle.setText("개인정보 취급방침");
			} else if(type.equals("이용약관")){
				content = Lhy_Function.getStringAssetText("service.txt", mContext);
				content += Lhy_Function.getStringAssetText("wechi.txt", mContext);
				tvTitle.setText("서비스&위치정보 이용약관");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		tvContent.setText(content);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		}
	}
}
