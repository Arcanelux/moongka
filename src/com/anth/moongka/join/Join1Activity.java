/**
 * Join1Activity
 *  회원가입 1
 *  입력한정보를 다음버튼 누를시 Join1Params에 저장 후, C.java 에 넘겨주고 Join2Activity로 이동
 *  Join2Activity에서 resultCode가 RESULT_OK 로 떨어질경우 finish()시킴
 */
package com.anth.moongka.join;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_arrayadapter.MkCodeSpAA;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;

public class Join1Activity extends MkActivity implements OnClickListener, OnItemSelectedListener{
	private final String TAG = "MK_Join";
	private Context mContext;
	private final int TO_JOIN2 = 238493;

	/** TopMenu **/
	private View viewTop;
	private ImageButton btnBack, btnClose;
	private TextView tvTitle;

	/** Layout **/
	// 아이디, 비밀번호, 비밀번호확인, 이름, 중복확인버튼, 생년월일버튼
	private View viewJoin1;
	private EditText etID, etPW1, etPW2, etName;
	private Button btnIdCheckOverlap;
	private Button btnNickNameOverlap;
	private Button btnBirthday;
	
	// 성별버튼, 본인확인질문, 본인확인답, 연락처
	private Button btnGenderMale, btnGenderFemale;
	private String curGender = "";
	private Spinner spJob, spResidence, spQuestion;
	private EditText etAnswer, etCall;
	
	// 자세히버튼, 체크박스, 다음버튼
	private Button btnDetail1, btnDetail2;
	private CheckBox cb1, cb2;
	private Button btnNext;

	/** DatePicker **/
	private Calendar mCalendar;
	private DatePickerDialog mDatePickerDialog;
	private DatePickerDialog.OnDateSetListener mOnDateSetListener;
	private boolean isDatePicked = false;
	private int thisYear = 0;

	/** Spinner Adapter **/
	//	private ArrayList<MkCode> joinJobList = new ArrayList<MkCode>();
	//	private ArrayList<MkCode> joinResidenceList = new ArrayList<MkCode>();
	private ArrayList<MkCode> joinQuestionList = new ArrayList<MkCode>();
	private MkCodeSpAA mQuestionSpAA;

	/** Selected Variable **/
	private Join1Params mJoin1Params;
	private int joinQuestionPos = 0;
	private JoinCertTask mJoinTask;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.join1_new);
		mContext = this;

		/** View Connect **/
		viewTop = findViewById(R.id.viewTopJoin1);
		
		/** UI Connect **/
		// viewTop
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnClose = (ImageButton) viewTop.findViewById(R.id.btnCert2);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("회원가입");

		// Layout
		etID = (EditText) findViewById(R.id.etJoin1ID);
		btnIdCheckOverlap = (Button) findViewById(R.id.btnJoin1IdOverlapCheck);
		btnNickNameOverlap = (Button) findViewById(R.id.btnJoin1NickNameOverlapCheck);
		etPW1 = (EditText) findViewById(R.id.etJoin1PW1);
		etPW2 = (EditText) findViewById(R.id.etJoin1PW2);
		etName = (EditText) findViewById(R.id.etJoin1Name);
		btnBirthday = (Button) findViewById(R.id.btnJoin1Birthday);
		
		btnGenderMale = (Button) findViewById(R.id.btnJoin2GenderMale);
		btnGenderFemale = (Button) findViewById(R.id.btnJoin2GenderFemale);
//		spJob = (Spinner) findViewById(R.id.spJoin1Job);
//		spResidence = (Spinner) findViewById(R.id.spJoin1Residence);
		spQuestion = (Spinner) findViewById(R.id.spJoin1Question);
		etAnswer = (EditText) findViewById(R.id.etJoin1Answer);
		etCall = (EditText) findViewById(R.id.etJoin1Call);
		
		btnDetail1 = (Button) findViewById(R.id.btnJoin1Detail1);
		btnDetail2 = (Button) findViewById(R.id.btnJoin1Detail2);
		cb1 = (CheckBox) findViewById(R.id.cbJoin1_1);
		cb2 = (CheckBox) findViewById(R.id.cbJoin1_2);
		btnNext = (Button) findViewById(R.id.btnJoin1Next);

		/** UI Btn ClickListener **/
		btnBack.setOnClickListener(this);
		btnClose.setOnClickListener(this);

		btnIdCheckOverlap.setOnClickListener(this);
		btnNickNameOverlap.setOnClickListener(this);
		btnBirthday.setOnClickListener(this);
		btnDetail1.setOnClickListener(this);
		btnDetail2.setOnClickListener(this);
		btnNext.setOnClickListener(this);

		btnGenderMale.setOnClickListener(this);
		btnGenderFemale.setOnClickListener(this);

		/** Spinner ClickListener **/
//		spJob.setOnItemSelectedListener(this);
		spQuestion.setOnItemSelectedListener(this);

		/** DatePicker Dialog **/
		mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				isDatePicked = true;
				mCalendar.set(year, monthOfYear, dayOfMonth);
				setBtnBirthdayText();
			}
		};

		/** Initializing1 **/
		joinQuestionList = C.LIST_QUESTION;
	
		int joinQuestionEndList = 0;
		if (joinQuestionList != null) {
			
			joinQuestionEndList = joinQuestionList.size();
			joinQuestionList.remove(joinQuestionEndList-1);
			
		}
		
		/** Spinner Connect **/
		mQuestionSpAA = new MkCodeSpAA(mContext, android.R.layout.simple_list_item_1, joinQuestionList);
		spQuestion.setAdapter(mQuestionSpAA);
		spQuestion.setOnItemSelectedListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch(v.getId()){
		case R.id.btnCert1:
			/** Top View **/
			finish();
			break;
		case R.id.btnJoin1IdOverlapCheck:
			/** 중복체크 **/
			String curId = etID.getText().toString();
			if(curId.length() == 0){
				Lhy_Function.makeToast("아이디를 입력해주세요", mContext);
			} else if(curId.length() < 3){
				Lhy_Function.makeToast("아이디가 너무 짧습니다", mContext);
			} else if(!Lhy_Function.isValidEmail(curId)){
				Toast.makeText(mContext, "이메일 형식이 아닙니다", Toast.LENGTH_SHORT).show();
			} else{
				new JoinCheckIdTask(curId, mContext).execute();
			}
			break;
		case R.id.btnJoin1NickNameOverlapCheck :
			String nickName = etName.getText().toString();
			new NickNameCheckTask(nickName, mContext).execute();
			break;
		case R.id.btnJoin1Birthday:
			/** 생년월일 버튼 **/
			mCalendar = Calendar.getInstance();
			if(thisYear==0) thisYear = mCalendar.get(Calendar.YEAR);
			mDatePickerDialog = new DatePickerDialog(mContext, mOnDateSetListener, 
					mCalendar.get(Calendar.YEAR),
					mCalendar.get(Calendar.MONTH),
					mCalendar.get(Calendar.DAY_OF_MONTH)
					);
			mDatePickerDialog.show();
			break;
		case R.id.btnJoin1Detail1:
			intent = new Intent(Join1Activity.this, JoinTermActivity.class);
			intent.putExtra("type", "이용약관");
			startActivity(intent);
			break;
		case R.id.btnJoin1Detail2:
			intent = new Intent(Join1Activity.this, JoinTermActivity.class);
			intent.putExtra("type", "개인정보");
			startActivity(intent);
			break;
		case R.id.btnJoin1Next:
			String id = etID.getText().toString();
			String pw1 = etPW1.getText().toString();
			String pw2 = etPW2.getText().toString();
			String name = etName.getText().toString();
			String answer = etAnswer.getText().toString();
			String call = etCall.getText().toString();
			Calendar birthday = mCalendar;
			boolean agree1 = cb1.isChecked();
			boolean agree2 = cb2.isChecked();
			MkCode questionCode = joinQuestionList.get(joinQuestionPos);

			String msgCert = "";

			if(id==null || id.equals("")){
				msgCert = "ID를 입력해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(!Lhy_Function.isValidEmail(id)){
				msgCert = "ID가 이메일 형식이 아닙니다";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(pw1==null || pw1.equals("")){
				msgCert = "비밀번호를 입력해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(pw2==null || pw2.equals("")){
				msgCert = "비밀번호 확인란을 입력해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(pw1.length()<4||pw1.length()>12)
				{
				//pw 4~12 아닐경우
					msgCert = "비밀번호는 4~12자리 까지 입니다. 다시 설정해 주세요.";
					etPW1.setText("");
					etPW2.setText("");
					etPW1.findFocus();
					Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
					return;
				}
			else if(!pw1.equals(pw2)){
				msgCert = "비밀번호와 비밀번호 확인란의 내용이 서로 다릅니다";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(name==null || name.equals("")){
				msgCert = "이름을 입력해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
//			else if(birthday==null){
//				msgCert = "생년월일을 입력해주세요";
//				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
//			}
			else if(isDatePicked==false){
				msgCert = "생년월일을 입력해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();				
			}
			else if(birthday.get(Calendar.YEAR) > thisYear || birthday.get(Calendar.YEAR) < 1920){
				msgCert = "생년월일의 연도를 확인해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(curGender.equals("") || curGender==null){
				msgCert = "성별을 선택해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(joinQuestionPos==0){
				msgCert = "본인확인질문을 선택해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(answer.equals("") || answer==null){
				msgCert = "본인확인질문 답을 입력해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(call.equals("") || call==null){
				msgCert = "연락처를 입력해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(agree1==false){
				msgCert = "이용약관에 동의해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else if(agree2==false){
				msgCert = "개인정보취급방침에 동의해주세요";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			
			else if(!Lhy_Function.isValidCellPhoneNumber(call)&&!Lhy_Function.isValidPhoneNumber(call)){
				msgCert = "휴대폰번호 또는 전화번호 형식이 아닙니다";
				Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			}
			else{
				setJoinParams(id, pw1, pw2, name, birthday, agree1, agree2, questionCode, answer, call);
				C.JOIN1_PARAMS = mJoin1Params;
//				intent = new Intent(Join1Activity.this, Join2Activity.class);
//				startActivityForResult(intent, TO_JOIN2);
				msgCert = "가입정보 검증 완료";
				new JoinCertTask(mJoin1Params, mContext).execute();
			}
			
			break;
			/** join2 layout **/
		case R.id.btnJoin2GenderMale:
			btnGenderFemale.setBackgroundResource(R.drawable.btn_gray_round);
			btnGenderMale.setBackgroundResource(R.drawable.btn_darkgray_round);
			curGender = "M";
			break;
		case R.id.btnJoin2GenderFemale:
			btnGenderMale.setBackgroundResource(R.drawable.btn_gray_round);
			btnGenderFemale.setBackgroundResource(R.drawable.btn_darkgray_round);
			curGender = "F";
			break;
		}
	}
	/** DatePicker의 선택에 따라 btnBirthDay의 Text를 변경해줌 **/
	private void setBtnBirthdayText(){
		btnBirthday.setText(mCalendar.get(Calendar.YEAR) + "년 " + 
				(mCalendar.get(Calendar.MONTH)+1) + "월 " + 
				mCalendar.get(Calendar.DAY_OF_MONTH) + "일");
	}

	
	private void setJoinParams(
			String id,
			String pw1,
			String pw2,
			String name,
			Calendar birthday,
			boolean agree1,
			boolean agree2,
			MkCode questionCode,
			String answer,
			String call
			){
		mJoin1Params = new Join1Params();

		mJoin1Params.setId(id);
		mJoin1Params.setPw1(pw1);
		mJoin1Params.setPw2(pw2);
		mJoin1Params.setName(name);
		mJoin1Params.setGender(curGender);
		mJoin1Params.setBirthday(birthday);
		mJoin1Params.setAgree1(agree1);
		mJoin1Params.setAgree2(agree2);
		mJoin1Params.setQuestionCode(joinQuestionList.get(joinQuestionPos));
		mJoin1Params.setAnswer(answer);
		mJoin1Params.setCall(call);
	}

	/** Spinner Listener **/
	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		switch(parent.getId()){
		case R.id.spJoin1Question:
			joinQuestionPos = position;
			break;
//		case R.id.spJoin1Job:
//			break;
//		case R.id.spJoin1Residence:
//			break;
		}
	}
	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==TO_JOIN2){
			if(resultCode==RESULT_OK){
				finish();
			}
		}
	}
}
