/**
 * JoinCheckIdTask
 * ID 중복체크 액션
 */
package com.anth.moongka.join;

import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkApi;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class JoinCheckIdTask extends AsyncTask<Void, Void, Void>{
	private final String TAG = "MK_JoinCheckIdTask";
	private Context mContext;
	
	private ProgressDialog mProgressDialog;
	private String id;
	private boolean isCertId = false;
	
	public JoinCheckIdTask(String id, Context context) {
		mContext = context;
		this.id = id;
		mProgressDialog = new ProgressDialog(mContext);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog.setMessage("아이디 중복확인중...");
		mProgressDialog.show();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		isCertId = MkApi.checkId(id);
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mProgressDialog.dismiss();
		Lhy_Function.makeToast(MkApi.returnMsg, mContext);
	}

	

}
