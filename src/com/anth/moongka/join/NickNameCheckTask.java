package com.anth.moongka.join;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkApi;

public class NickNameCheckTask extends AsyncTask<Void, Void, Void>{
	private final String TAG = "MK_JoinCheckIdTask";
	private Context mContext;
	
	private ProgressDialog mProgressDialog;
	private String nickname;
	private boolean isCertId = false;
	
	public NickNameCheckTask(String nickname, Context context) {
		mContext = context;
		this.nickname = nickname;
		mProgressDialog = new ProgressDialog(mContext);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog.setMessage("닉네임 중복확인중...");
		mProgressDialog.show();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		isCertId = MkApi.checkNickName(nickname);
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mProgressDialog.dismiss();
		Lhy_Function.makeToast(MkApi.returnMsg, mContext);
	}

	

}
