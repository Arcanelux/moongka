/**
 * Join2Activity
 *  회원가입2
 *  완료버튼 누를 시 모든 정보를 Join2Params 에 저장,
 *  Join1Params와 Join2Params를 JoinCertTask에 전달, 실행
 */
package com.anth.moongka.join;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_arrayadapter.MkCodeSpAA;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.setting.MkCar;

public class Join2Activity extends MkActivity implements OnClickListener, OnItemSelectedListener {
	private final String TAG = "MK_Join2Activity";
	private Context mContext;

	private boolean dream_identify=false;
	private boolean mycar_identify=false;
	/** Top View **/
	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;

	/** Layout **/
	private ImageView ivCarImage;
	private Uri uriCarImage;
	private EditText etNick, etYear;
	private Button btnMyCar, btnDreamCar, btnOldMyCar;
	private String curCarType;
	private Button btnPeriod1, btnPeriod2;
	private Button btnFindCarImage, btnComplete;
	private View viewPeriod;

	/** Spinner **/
	private Spinner spMaker, spCarName;
	private MkCodeSpAA spMakerAA, spCarNameAA;
	private ArrayList<MkCode> makerList = new ArrayList<MkCode>();
	private ArrayList<MkCode> carNameList = new ArrayList<MkCode>();
	private int curMakerPos = 0;
	private int curCarNamePos = 0;

	/** DatePicker **/
	private Calendar mCalendar1, mCalendar2;
	private DatePickerDialog mDatePickerDialog;
	private DatePickerDialog.OnDateSetListener mOnDateSetListener1, mOnDateSetListener2;
	private boolean isDatePicked = false;
	private int thisYear = 0;

	/** variable **/
	private final int TO_JOIN2_CAR_PHOTO = 83893;
	private Join1Params mJoin1Params;
	private Join2Params mJoin2Params;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.join2_new);
		mContext = this;

		//		/** 가입정보1 전달받음 **/
		//		Intent intent = getIntent();
		//		mJoin1Params = (Join1Params) intent.getSerializableExtra("mJoin1Params");
		mJoin1Params = C.JOIN1_PARAMS;

		/** View Connect **/
		viewTop = findViewById(R.id.viewTopJoin2);

		/** UI Connect **/
		// viewTop
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("회원가입");

		//layout
		ivCarImage = (ImageView) findViewById(R.id.ivJoin2CarImage);
		btnFindCarImage = (Button) findViewById(R.id.btnJoin2FindCarImage);
		btnFindCarImage.setOnClickListener(this);
		etNick = (EditText) findViewById(R.id.etJoin2Nick);
		etYear = (EditText) findViewById(R.id.etJoin2Year);
		btnPeriod1 = (Button) findViewById(R.id.btnJoin2Period1);
		btnPeriod2 = (Button) findViewById(R.id.btnJoin2Period2);
		btnPeriod1.setOnClickListener(this);
		btnPeriod2.setOnClickListener(this);
		viewPeriod = findViewById(R.id.viewJoin2Period);
		btnMyCar = (Button) findViewById(R.id.btnJoin2TypeMyCar);
		btnDreamCar = (Button) findViewById(R.id.btnJoin2TypeDreamCar);
		btnOldMyCar = (Button) findViewById(R.id.btnJoin2TypeOldMyCar);
		btnMyCar.setOnClickListener(this);
		btnDreamCar.setOnClickListener(this);
		btnOldMyCar.setOnClickListener(this);
		btnComplete = (Button) findViewById(R.id.btnJoin2Complete);
		btnComplete.setOnClickListener(this);

		
		ivCarImage.setImageResource(R.drawable.default_car);
		btnMyCar.setBackgroundResource(R.drawable.btn_join_darkgray_round);
		curCarType = "T101";
		btnPeriod1.setText("");
		btnPeriod2.setText("현재");
		btnPeriod1.setClickable(true);
		btnPeriod2.setClickable(false);

		/** Spinner **/
		spMaker = (Spinner) findViewById(R.id.spJoin2Maker);
		spCarName = (Spinner) findViewById(R.id.spJoin2CarName);
		spMaker.setOnItemSelectedListener(this);
		spCarName.setOnItemSelectedListener(this);

		// 이중배열에서 제조사코드를 뽑아 makerList에 삽입
		makerList.add(new MkCode("선택해주세요", "None"));
		for(MkCar curMkCar : C.LIST_MKCAR){
			makerList.add(curMkCar.getMaker());
		}
		// 이중배열의 첫번째부분에서 getCar를 하여 첫번째 자동차이름 목록을 carNameList에 삽입
		carNameList.add(new MkCode("선택해주세요", "None"));
		carNameList.addAll(C.LIST_MKCAR.get(0).getCar());
		spMakerAA = new MkCodeSpAA(mContext, android.R.layout.simple_list_item_1, makerList);
		spCarNameAA = new MkCodeSpAA(mContext, android.R.layout.simple_list_item_1, carNameList);
		spMaker.setAdapter(spMakerAA);
		spCarName.setAdapter(spCarNameAA);

		/** DatePicker Dialog **/
		mOnDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				isDatePicked = true;
				mCalendar1.set(year, monthOfYear, dayOfMonth);
				btnPeriod1.setText(year + "-" + (monthOfYear+1) + "-" + dayOfMonth);
			}
		};
		mOnDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				isDatePicked = true;
				mCalendar2.set(year, monthOfYear, dayOfMonth);
				btnPeriod2.setText(year + "-" + (monthOfYear+1) + "-" + dayOfMonth);
			}
		};
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch(v.getId()){
		case R.id.btnCert1:
			/** 뒤로가기 **/
			finish();
			break;
		case R.id.btnJoin2FindCarImage:
			/** 자동차 이미지 찾기 **/
			intent = new Intent(Intent.ACTION_PICK);
			intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
			intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI); // images on the SD card.
			startActivityForResult(intent, TO_JOIN2_CAR_PHOTO);
			break;
		case R.id.btnJoin2TypeMyCar:
			btnMyCar.setBackgroundResource(R.drawable.btn_join_emerald_round);
			btnDreamCar.setBackgroundResource(R.drawable.btn_join_darkgray_round);
			btnOldMyCar.setBackgroundResource(R.drawable.btn_join_darkgray_round);
			viewPeriod.setVisibility(View.VISIBLE);
			btnPeriod1.setText("");
			btnPeriod2.setText("현재");
			btnPeriod1.setClickable(true);
			btnPeriod2.setClickable(false);
			curCarType = "T101";
			dream_identify=false;
			mycar_identify=true;
			break;
		case R.id.btnJoin2TypeDreamCar:
			btnMyCar.setBackgroundResource(R.drawable.btn_join_darkgray_round);
			btnDreamCar.setBackgroundResource(R.drawable.btn_join_emerald_round);
			btnOldMyCar.setBackgroundResource(R.drawable.btn_join_darkgray_round);
			viewPeriod.setVisibility(View.GONE);
			btnPeriod1.setText("");
			btnPeriod2.setText("");
			btnPeriod1.setClickable(true);
			btnPeriod2.setClickable(true);
			curCarType = "T102";
			dream_identify=true;
			mycar_identify=false;
			break;
		case R.id.btnJoin2TypeOldMyCar:
			btnMyCar.setBackgroundResource(R.drawable.btn_join_darkgray_round);
			btnDreamCar.setBackgroundResource(R.drawable.btn_join_darkgray_round);
			btnOldMyCar.setBackgroundResource(R.drawable.btn_join_emerald_round);
			viewPeriod.setVisibility(View.VISIBLE);
			btnPeriod1.setText("");
			btnPeriod2.setText("");
			btnPeriod1.setClickable(true);
			btnPeriod2.setClickable(true);
			curCarType = "T103";
			dream_identify=false;
			mycar_identify=false;
			break;
		case R.id.btnJoin2Period1:
			/** 보유기간1 **/
			mCalendar1 = Calendar.getInstance();
			if(thisYear==0) thisYear = mCalendar1.get(Calendar.YEAR);
			mDatePickerDialog = new DatePickerDialog(mContext, mOnDateSetListener1, 
					mCalendar1.get(Calendar.YEAR),
					mCalendar1.get(Calendar.MONTH),
					mCalendar1.get(Calendar.DAY_OF_MONTH)
					);
			Log.i("mCalendar", ""+mCalendar1);
			mDatePickerDialog.show();
			break;
		case R.id.btnJoin2Period2:
			/** 보유기간 2 **/
			mCalendar2 = Calendar.getInstance();
			if(thisYear==0) thisYear = mCalendar2.get(Calendar.YEAR);
			mDatePickerDialog = new DatePickerDialog(mContext, mOnDateSetListener2, 
					mCalendar2.get(Calendar.YEAR),
					mCalendar2.get(Calendar.MONTH),
					mCalendar2.get(Calendar.DAY_OF_MONTH)
					);
			Log.i("mCalendar", ""+mCalendar2);
			mDatePickerDialog.show();
			break;
		case R.id.btnJoin2Complete:
			/** 완료버튼 **/
			String nick = etNick.getText().toString();
			String makerCode = makerList.get(curMakerPos).getCode();
			String carNameCode = null;
			Log.i("makerCode", makerCode);
			if(!makerCode.equals("None")){
				carNameCode = carNameList.get(curCarNamePos).getCode();
			}
			String year = etYear.getText().toString();
			String period1 = btnPeriod1.getText().toString();
			String period2 = btnPeriod2.getText().toString();
			String msgCert = "";

			
			
			if(uriCarImage==null){
				msgCert = "자동차 이미지를 선택해주세요";
			} else if(nick.equals("") || nick==null){
				msgCert = "자동차 애칭을 입력해주세요";
			} else if(makerCode.equals("None")){
				msgCert = "제조사를 선택해주세요";
			} else if(carNameCode.equals("None")){
				msgCert = "차량명을 선택해주세요";
			} else if(year.equals("") || year==null||!Lhy_Function.isValidYear(year)){
				msgCert = "연식을 정확히 입력해주세요";
			} else if((curCarType.equals("T101") || curCarType.equals("T103")) && (period1.equals("") || period1==null)){
				/** 자동차타입이 마이카 또는 올드마이카 이며, 보유기간1이 비어있을경우 **/
				msgCert = "보유기간을 입력해주세요";
			}
			else if(dream_identify=false){
				if(mycar_identify=false){
					if(mCalendar1.after(mCalendar2)){
						msgCert = "보유기간이 잘못되었습니다";
						dream_identify=false;
					}
				}
			}
			else if(year.length()!=4){
				msgCert = "연식이 잘못되었습니다";
			}
			else{
				msgCert = "가입정보 검증 완료";
				mJoin2Params = new Join2Params(uriCarImage, nick, makerCode, carNameCode, year, curCarType, period1, period2);
				//new JoinCertTask(mJoin1Params, mJoin2Params, mContext).execute();
				break;
			}
			Toast.makeText(mContext, msgCert, Toast.LENGTH_SHORT).show();
			break;
		}
	}

	/** Spinner **/
	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		switch(parent.getId()){
		case R.id.spJoin2Maker:
			/** 제조사 Spinner아이템 선택시 - C.LIST_MKCAR 이중배열의 position번째 배열의 .getCar()로 자동차이름 배열을 생성 **/
			curMakerPos = position;
			if(position!=0){
				carNameList.clear();
				carNameList.add(new MkCode("선택해주세요", "None"));
				carNameList.addAll(C.LIST_MKCAR.get(position-1).getCar());
				spCarNameAA.notifyDataSetChanged();
			} else{
				carNameList.clear();
				spCarNameAA.notifyDataSetChanged();
			}
			break;
		case R.id.spJoin2CarName:
			curCarNamePos = position;
			break;
		}
	}
	@Override public void onNothingSelected(AdapterView<?> arg0) { }

	/** Gallery Photo Pick Result **/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode==RESULT_OK){
			switch(requestCode){
			case TO_JOIN2_CAR_PHOTO:
				Cursor c = getContentResolver().query(data.getData(), null, null, null, null);
				c.moveToNext();
				String path = c.getString(c.getColumnIndex(MediaStore.MediaColumns.DATA));
				Uri uri = Uri.fromFile(new File(path));

				uriCarImage = uri;
				C.setImageThumbnail(uriCarImage, ivCarImage);
				break;
			}
		}
	}


	@Override
	public void finish() {
		super.finish();
	}



}
