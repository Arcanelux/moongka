/**
 * Join2Params
 *  회원가입정보 2
 */
package com.anth.moongka.join;

import android.net.Uri;

public class Join2Params {
	private Uri uriCarImage;
	private String nick;
	private String makerCode;
	private String carNameCode;
	private String year;
	private String carType;
	private String period1, period2;
	public Join2Params(Uri uriCarImage, String nick, String makerCode,
			String carNameCode, String year, String carType, String period1,
			String period2) {
		super();
		this.uriCarImage = uriCarImage;
		this.nick = nick;
		this.makerCode = makerCode;
		this.carNameCode = carNameCode;
		this.year = year;
		this.carType = carType;
		this.period1 = period1;
		this.period2 = period2;
	}
	public Uri getUriCarImage() {
		return uriCarImage;
	}
	public void setUriCarImage(Uri uriCarImage) {
		this.uriCarImage = uriCarImage;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getMakerCode() {
		return makerCode;
	}
	public void setMakerCode(String makerCode) {
		this.makerCode = makerCode;
	}
	public String getCarNameCode() {
		return carNameCode;
	}
	public void setCarNameCode(String carNameCode) {
		this.carNameCode = carNameCode;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	public String getPeriod1() {
		return period1;
	}
	public void setPeriod1(String period1) {
		this.period1 = period1;
	}
	public String getPeriod2() {
		return period2;
	}
	public void setPeriod2(String period2) {
		this.period2 = period2;
	}
	
}