/**
 * JoinCertTask
 *  회원가입 액션
 *  Join1Parmas, Join2Params를 인자로 받아서
 *  유효성검사는 생략한다. (Join1, Join2 Activity에서 실시)
 *  MkApi의 join함수에 인자들을 전달, HttpPostMultiPart 객체를 통해 통신
 *  리턴값을 받아 JSONObject형태로 변환, 통신결과가 맞는지 파악
 */
package com.anth.moongka.join;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.anth.moongka.fix_value.MkDialog;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkPref;
import com.anth.moongka.function.SavedValue;
import com.anth.moongka.network.HttpPostMultiPart;
import com.anth.moongka.network.StringData;

public class JoinCertTask extends AsyncTask<Void, String, Integer>{
	private final String TAG = "MK_JoinAsyncTask";
	private Context mContext;
	private Activity mActivity;
	private boolean TEST_CERTOK = true;

	private final int CERT_OK = 1;
	private final int CERT_FAILED = 2;
	private String msgCert;

	private ProgressDialog mProgressDialog;
	private MkDialog mDialog; 

	private Join1Params mJoin1Params;
	private Join2Params mJoin2Params;

	private boolean isCertJoin = false;
//
//	public JoinCertTask(Join1Params join1Params, Join2Params join2Params, Context context){
//		this.mContext = context;
//		this.mJoin1Params = join1Params;
//		this.mJoin2Params = join2Params;
//		mActivity = (Activity) mContext;
//	}
	public JoinCertTask(Join1Params join1Params, Context context){
		this.mContext = context;
		this.mJoin1Params = join1Params;
		mActivity = (Activity) mContext;
	}
	@Override
	protected void onPreExecute() {
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setMessage("가입 인증 중...");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
		super.onPreExecute();
	}
	@Override
	protected Integer doInBackground(Void... params) {
		makeDelay(1000);
		onProgressUpdate("가입정보 검증 중...");
//		int certResult = certJoinForm();
//
////		if(TEST_CERTOK) certResult = CERT_OK; 
//
//		switch(certResult){
//		case CERT_OK:
//			break;
//		case CERT_FAILED:
//			return CERT_FAILED;
//		}
		
		makeDelay(1000);
		publishProgress("서버와 통신 중...");
		connectServerToJoin();

		/** 서버와의 통신까지 완료되면 OK **/
		if(isCertJoin){
			return CERT_OK;
		} else{
			return CERT_FAILED;
		}
	}
	@Override
	protected void onProgressUpdate(String... values) {
//		super.onProgressUpdate(values);
//		mProgressDialog.setMessage(values[0]);
	}
	@Override
	protected void onPostExecute(Integer result) {
		switch (result) {
		case CERT_OK:
//			msgCert = MkApi.returnMsg;
			makeToast(msgCert);
			mProgressDialog.dismiss();
			mDialog = new MkDialog(mContext);
			mDialog.setContent("정상적으로 가입되었습니다");
			mDialog.setBtn1("확인");
			mDialog.setBtn1ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();
					mActivity.setResult(Activity.RESULT_OK);
					mActivity.finish();
				}
			});
			mDialog.show();
			break;
		case CERT_FAILED:
//			msgCert = MkApi.returnMsg;
			makeToast(msgCert);
			mProgressDialog.dismiss();
			mDialog = new MkDialog(mContext);
//			mDialog.setContent("회원가입시 오류가 발생하였습니다. 다시 입력하여 주십시오");
			mDialog.setContent(msgCert);
			mDialog.setBtn1("확인");
			mDialog.setBtn1ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();
				}
			});
			mDialog.show();
			break;
		}
		super.onPostExecute(result);
	}
	@Override
	protected void onCancelled() {
		mProgressDialog.dismiss();
		super.onCancelled();
	}

//	private int certJoinForm(){
//		String id = mJoin1Params.getId();
//		String pw1 = mJoin1Params.getPw1();
//		String pw2 = mJoin1Params.getPw2();
//		String name = mJoin1Params.getName();
//		Calendar birthday = mJoin1Params.getBirthday();
//		boolean agree1 = mJoin1Params.isAgree1();
//		boolean agree2 = mJoin1Params.isAgree2();
//		String gender = mJoin1Params.getGender();
//		String questionCode = mJoin1Params.getQuestionCode().getCode();
//		String answer = mJoin1Params.getAnswer();
//		String call = mJoin1Params.getCall();
//
//		if(id==null || id.equals("")){
//			msgCert = "ID를 입력해주세요";
//			return CERT_FAILED;
//		}
//		else if(!Lhy_Function.isValidEmail(id)){
//			msgCert = "ID가 이메일 형식이 아닙니다";
//			return CERT_FAILED;
//		}
//		else if(pw1==null || pw1.equals("")){
//			msgCert = "비밀번호를 입력해주세요";
//			return CERT_FAILED;
//		}
//		else if(pw2==null || pw2.equals("")){
//			msgCert = "비밀번호 확인란을 입력해주세요";
//			return CERT_FAILED;
//		}
//		else if(!pw1.equals(pw2)){
//			msgCert = "비밀번호와 비밀번호 확인란의 내용이 서로 다릅니다";
//			return CERT_FAILED;
//		}
//		else if(name==null || name.equals("")){
//			msgCert = "이름을 입력해주세요";
//			return CERT_FAILED;
//		}
//		else if(birthday==null){
//			msgCert = "생년월일을 입력해주세요";
//			return CERT_FAILED;
//		}
//		else if(birthday.get(Calendar.YEAR) > 2015 || birthday.get(Calendar.YEAR) < 1920){
//			msgCert = "생년월일의 연도를 확인해주세요";
//			return CERT_FAILED;
//		}
//		else if(agree1==false){
//			msgCert = "이용약관에 동의해주세요";
//			return CERT_FAILED;
//		}
//		else if(agree2==false){
//			msgCert = "개인정보취급방침에 동의해주세요";
//			return CERT_FAILED;
//		}
//		else if(questionCode.equals("None")){
//			msgCert = "본인확인질문을 선택해주세요";
//			return CERT_FAILED;
//		}
//		else if(answer==null || answer.equals("")){
//			msgCert = "본인확인질문의 답을 입력해주세요";
//			return CERT_FAILED;
//		}
//		else if(call==null || call.equals("")){
//			msgCert = "연락처를 입력해주세요";
//			return CERT_FAILED;
//		}
//		else{
//			msgCert = "기입내용 검증 완료";
//			return CERT_OK;
//		}	
//	}
	
	
	/** 서버와 가입정보 통신구현 **/
	private void connectServerToJoin(){
		String id = mJoin1Params.getId();
		String pw1 = mJoin1Params.getPw1();
		String pw2 = mJoin1Params.getPw2();
		String name = mJoin1Params.getName();
		Calendar birthday = mJoin1Params.getBirthday();
		String strBirthday = birthday.get(Calendar.YEAR) + "-" + (birthday.get(Calendar.MONTH)+1) + "-" + birthday.get(Calendar.DAY_OF_MONTH) + "";
		boolean agree1 = mJoin1Params.isAgree1();
		boolean agree2 = mJoin1Params.isAgree2();
		String gender = mJoin1Params.getGender();
		String questionCode = mJoin1Params.getQuestionCode().getCode();
		String answer = mJoin1Params.getAnswer();
		String call = mJoin1Params.getCall();
		
//		Uri uriCarImage = mJoin2Params.getUriCarImage();
//		String nick = mJoin2Params.getNick();
//		String carType = mJoin2Params.getCarType();
//		String makerCode = mJoin2Params.getMakerCode();
//		String carNameCode = mJoin2Params.getCarNameCode();
//		String year = mJoin2Params.getYear();
//		String period1 = mJoin2Params.getPeriod1();
//		String period2 = mJoin2Params.getPeriod2();
		
		SavedValue saveValue = MkPref.getSavedValue(mContext);
		String deviceKey = saveValue.getRegId();
		String pwMd5 = Lhy_Function.getMD5Hash(pw1);
		
		HttpPostMultiPart mHttpMultiPart = new HttpPostMultiPart(C.API_JOIN, mContext);
	//	mHttpMultiPart.addImageData(new ImageData("photo.jpg", "car_photo[0]", uriCarImage));
		mHttpMultiPart.addStringData(new StringData("mem_id", id));
		mHttpMultiPart.addStringData(new StringData("mem_pass", pwMd5));
		mHttpMultiPart.addStringData(new StringData("mem_name", name));
		mHttpMultiPart.addStringData(new StringData("mem_key", deviceKey));
		mHttpMultiPart.addStringData(new StringData("mem_yymmdd", strBirthday));
		mHttpMultiPart.addStringData(new StringData("mem_sex", gender));
		mHttpMultiPart.addStringData(new StringData("mem_question", questionCode));
		mHttpMultiPart.addStringData(new StringData("mem_answer", answer));
		mHttpMultiPart.addStringData(new StringData("mem_type", "android"));
		mHttpMultiPart.addStringData(new StringData("mem_phone", call));
		
//		mHttpMultiPart.addStringData(new StringData("car_nick", nick));
//		mHttpMultiPart.addStringData(new StringData("car_type", carType));
//		mHttpMultiPart.addStringData(new StringData("car_company", carNameCode));
//		mHttpMultiPart.addStringData(new StringData("car_year", year));
//		mHttpMultiPart.addStringData(new StringData("car_first", period1));
//		mHttpMultiPart.addStringData(new StringData("car_end", period2));
		
		mHttpMultiPart.execute();
		try {
			String result = mHttpMultiPart.getResult();
			JSONObject jObj = new JSONObject(result);
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			msgCert = returnMessage;
			
			if(returnStatus.equals("Y")){
				isCertJoin = true;
			} else{
				isCertJoin = false;
			}
		} catch (JSONException e) {
			isCertJoin = false;
			e.printStackTrace();
		}

//		isCertJoin = MkApi.join(id, pw1, name, strBirthday, gender, questionCode, answer, mContext);
//		msgCert = "가입완료";
	}



	private void makeToast(String msg){
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}
	private void makeDelay(int value){
		try {
			Thread.sleep(value);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
