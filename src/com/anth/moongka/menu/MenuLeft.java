/**
 * MenuLeft
 *  FrameLayout, MkMenuActivity에 동적으로 좌측 메뉴를 생성해준다
 *  메뉴설정과 클릭이벤트는 여기서 설정
 */
package com.anth.moongka.menu;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.album.AlbumActivity;
import com.anth.moongka.checkin.CheckInActivity;
import com.anth.moongka.fix_value.MkDialog2;
import com.anth.moongka.friend.FriendActivity;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.function.MkPref;
import com.anth.moongka.login.LoginActivity;
import com.anth.moongka.main.MainActivity;
import com.anth.moongka.menu.MkProfileMain.DreamCar;
import com.anth.moongka.message.MessageActivity;
import com.anth.moongka.profile.ProfileActivity;
import com.anth.moongka.profile_car_list.MkCarListProfileActivity;
import com.anth.moongka.scrap.ScrapActivity;
import com.anth.moongka.setting.SettingActivity;

public class MenuLeft extends FrameLayout implements OnClickListener,
		OnItemClickListener {
	private final static String TAG = "MK_MenuLeft";
	private static Context mContext;
	private Activity mActivity;
	private static ImageDownloader mImageDownloader;

	private FrameLayout flMenuLeft;

	private LinearLayout llProfile, llProfileMainCar;
	private TextView tvProfile;
	private static TextView tvProfileMainCarName;
	private static TextView tvProfileMainCarNum;
	private static ImageView ivProfile, ivProfileMainCar;

	private View viewHome, viewAlbum, viewCheckIn, viewMessage, viewFriend,
			viewScrap, viewAccount, viewLogout;
	private TextView tvHome, tvAlbum, tvCheckIn, tvMessage, tvFriend, tvScrap,
			tvAccount, tvLogout;

	private ListView lvProfileCar;
	private static MenuLeftCarProfileAdapter mAdapter;
	private MkProfileMain mProfile;
	private ImageButton btnOption;

	private MkDialog2 mDialog;

	public MenuLeft(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		firstInit(context);
	}

	public MenuLeft(Context context, AttributeSet attrs) {
		super(context, attrs);
		firstInit(context);
	}

	public MenuLeft(Context context) {
		super(context);
		firstInit(context);
	}

	private void firstInit(Context context) {
		mContext = context;
		mActivity = (Activity) mContext;
		Log.i("mActivity", mActivity.getLocalClassName());
		mImageDownloader = new ImageDownloader(mContext);

		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		flMenuLeft = (FrameLayout) inflater.inflate(R.layout.menu_left2, null);
		FrameLayout.LayoutParams params = new LayoutParams(
				(int) (C.DISPLAY_WIDTH * 0.8f),
				android.view.ViewGroup.LayoutParams.MATCH_PARENT, Gravity.LEFT);
		addView(flMenuLeft, params);

		/** 개인 프로필 **/
		llProfile = (LinearLayout) flMenuLeft
				.findViewById(R.id.llMenuLeftProfile);
		llProfile.post(new Runnable() {
			@Override
			public void run() {
				llProfile.setLayoutParams(new LinearLayout.LayoutParams(
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						C.TOP_HEIGHT));
			}
		});
		llProfile.setOnClickListener(this);
		llProfileMainCar = (LinearLayout) flMenuLeft
				.findViewById(R.id.llMenuLeftProfileMainCar);
		llProfileMainCar.post(new Runnable() {
			@Override
			public void run() {
				llProfileMainCar.setLayoutParams(new FrameLayout.LayoutParams(
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						C.TOP_HEIGHT));
			}
		});
		tvProfile = (TextView) llProfile
				.findViewById(R.id.tvMenuLeftProfileName);
		tvProfile.setText(C.mem_name);
		ivProfile = (ImageView) llProfile
				.findViewById(R.id.ivMenuLeftProfileImage);
		try {
			mImageDownloader.download(
					C.mem_profile.replaceAll("/data/", "/thumb_data/data/"),
					ivProfile);
		} catch (Exception e) {
			mContext = context;
			mImageDownloader = new ImageDownloader(mContext);
			mImageDownloader.download(
					C.mem_profile.replaceAll("/data/", "/thumb_data/data/"),
					ivProfile);
		}

		/** 메인카 프로필 **/
		llProfileMainCar.setOnClickListener(this);
		ivProfileMainCar = (ImageView) llProfileMainCar
				.findViewById(R.id.ivMenuLeftProfileMainCarImage);
		tvProfileMainCarName = (TextView) llProfileMainCar
				.findViewById(R.id.tvMenuLeftProfileMainCarName);
		tvProfileMainCarNum = (TextView) llProfileMainCar
				.findViewById(R.id.tvMenuLeftProfileMainCarNum);
		tvProfileMainCarName.setText(C.MAIN_CAR_PROFILE.getMMC_CARNICK());
		tvProfileMainCarNum.setText(C.MAIN_CAR_PROFILE.getCar_main_cnt());
		try {
			mImageDownloader.download(
					C.MK_BASE
							+ C.MAIN_CAR_PROFILE.getMMC_CARPIC_URL()
									.replaceAll("/data/", "/thumb_data/data/"),
					ivProfileMainCar);
		} catch (Exception e) {
			mImageDownloader = new ImageDownloader(mContext);
			mImageDownloader.download(
					C.MK_BASE
							+ C.MAIN_CAR_PROFILE.getMMC_CARPIC_URL()
									.replaceAll("/data/", "/thumb_data/data/"),
					ivProfileMainCar);
		}

		/** 드림카 리스트 **/
		lvProfileCar = (ListView) findViewById(R.id.lvMenuLeftProfileCar);
		mAdapter = new MenuLeftCarProfileAdapter(mContext,
				android.R.layout.simple_list_item_1,
				C.MAIN_CAR_PROFILE.getDreamCarList());
		for (int i = 0; i < C.MAIN_CAR_PROFILE.getDreamCarList().size(); i++) {
			String abc = C.MAIN_CAR_PROFILE.getDreamCarList().get(i).CC_NM;
			Log.d(TAG, "abc " + abc);
		}
		lvProfileCar.setAdapter(mAdapter);
		lvProfileCar.setOnItemClickListener(this);

		btnOption = (ImageButton) flMenuLeft
				.findViewById(R.id.btnMenuLeftOption);
		viewHome = flMenuLeft.findViewById(R.id.viewMenuLeftHome);
		viewAlbum = flMenuLeft.findViewById(R.id.viewMenuLeftAlbum);
		viewCheckIn = flMenuLeft.findViewById(R.id.viewMenuLeftCheckIn);
		viewMessage = flMenuLeft.findViewById(R.id.viewMenuLeftMessage);
		viewFriend = flMenuLeft.findViewById(R.id.viewMenuLeftFriend);
		viewScrap = flMenuLeft.findViewById(R.id.viewMenuLeftScrap);
		viewAccount = flMenuLeft.findViewById(R.id.viewMenuLeftAccount);
		viewLogout = flMenuLeft.findViewById(R.id.viewMenuLeftLogout);

		btnOption.setOnClickListener(this);
		viewHome.setOnClickListener(this);
		viewAlbum.setOnClickListener(this);
		viewCheckIn.setOnClickListener(this);
		viewMessage.setOnClickListener(this);
		viewFriend.setOnClickListener(this);
		viewScrap.setOnClickListener(this);
		viewAccount.setOnClickListener(this);
		viewLogout.setOnClickListener(this);

		tvHome = (TextView) viewHome.findViewById(R.id.tvMenuLeftItem);
		tvAlbum = (TextView) viewAlbum.findViewById(R.id.tvMenuLeftItem);
		tvCheckIn = (TextView) viewCheckIn.findViewById(R.id.tvMenuLeftItem);
		tvMessage = (TextView) viewMessage.findViewById(R.id.tvMenuLeftItem);
		tvFriend = (TextView) viewFriend.findViewById(R.id.tvMenuLeftItem);
		tvScrap = (TextView) viewScrap.findViewById(R.id.tvMenuLeftItem);
		tvAccount = (TextView) viewAccount.findViewById(R.id.tvMenuLeftItem);
		tvLogout = (TextView) viewLogout.findViewById(R.id.tvMenuLeftItem);

		tvHome.setText("홈");
		tvAlbum.setText("앨범");
		tvCheckIn.setText("발자취");
		tvMessage.setText("쪽지");
		tvFriend.setText("친구");
		tvScrap.setText("스크랩");
		tvAccount.setText("설정");
		tvLogout.setText("로그아웃");
	}

	private class MenuLeftCarProfileAdapter extends
			ArrayAdapter<MkProfileMain.DreamCar> {
		private ArrayList<DreamCar> dreamCarList;

		public MenuLeftCarProfileAdapter(Context context,
				int textViewResourceId, ArrayList<DreamCar> dreamCarList) {
			super(context, textViewResourceId, dreamCarList);
			this.dreamCarList = dreamCarList;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = inflater.inflate(R.layout.menu_left2_car_item, null);
			}

			ImageView ivCarImage = (ImageView) v
					.findViewById(R.id.ivMenuLeftProfileCarItemImage);
			TextView tvCarName = (TextView) v
					.findViewById(R.id.tvMenuLeftProfileCarItemName);
			tvCarName.setText(dreamCarList.get(position).CC_NM);
			try {
				mImageDownloader.download(
						C.MK_BASE
								+ dreamCarList.get(position).MMC_CARPIC_URL
										.replaceAll("/data/",
												"/thumb_data/data/"),
						ivCarImage);
			} catch (Exception e) {
				mImageDownloader = new ImageDownloader(mContext);
				mImageDownloader.download(
						C.MK_BASE
								+ dreamCarList.get(position).MMC_CARPIC_URL
										.replaceAll("/data/",
												"/thumb_data/data/"),
						ivCarImage);
			}
			return v;
		}
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.btnMenuLeftOption:
			mDialog = new MkDialog2(mContext);
			mDialog.setContent("정렬기준 선택");
			mDialog.setBtn1("최신순");
			mDialog.setBtn2("인기순");
			mDialog.setBtn1ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					MkPref.setSort("date");
					Log.i("sort", C.order);
					MainActivity.resetList();
					mDialog.dismiss();
				}
			});
			mDialog.setBtn2ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					MkPref.setSort("like");
					Log.i("sort", C.order);
					MainActivity.resetList();
					mDialog.dismiss();
				}
			});
			mDialog.show();
			break;
		case R.id.llMenuLeftProfile:
			intent = new Intent(mActivity, ProfileActivity.class);
			intent.putExtra("profile_idx", C.mem_idx);
			intent.putExtra("identify", 1);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.llMenuLeftProfileMainCar:

			new MenuLeftToMkCarListProfileTask(
					MkCarListProfileActivity.MAINCAR, C.mem_idx, "Loading...",
					true, mContext).execute();
			break;

		case R.id.viewMenuLeftHome:
			intent = new Intent(mActivity, MainActivity.class);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.viewMenuLeftAlbum:
			intent = new Intent(mActivity, AlbumActivity.class);
			intent.putExtra("mem_idx", C.mem_idx);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.viewMenuLeftCheckIn:
			intent = new Intent(mActivity, CheckInActivity.class);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.viewMenuLeftMessage:
			intent = new Intent(mActivity, MessageActivity.class);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.viewMenuLeftFriend:
			intent = new Intent(mActivity, FriendActivity.class);
			//intent.putExtra("inviteFriend", mProfile.getInviteFriend());
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.viewMenuLeftScrap:
			intent = new Intent(mActivity, ScrapActivity.class);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.viewMenuLeftAccount:
			intent = new Intent(mActivity, SettingActivity.class);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.viewMenuLeftLogout:
			// intent = new Intent(mActivity, LoginActivity.class);
			new AlertDialog.Builder(mActivity)
					// .setIcon(R.drawable.ic_launcher)
					.setMessage("로그아웃 하시겠습니까?")
					.setCancelable(false)
					.setPositiveButton("아니오", null)
					.setNegativeButton("예",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Intent intent = new Intent(mActivity,
											LoginActivity.class);
									MkPref.removeSettingData(mContext);
									mActivity.startActivity(intent);
									mActivity.finish();
									mActivity.overridePendingTransition(
											R.anim.fade_in, R.anim.fade_out);
								}
							}).show();
			break;
		}

	}

	public static void notifyPhotoChanged() {
		mImageDownloader.download(
				C.mem_profile.replaceAll("/data/", "/thumb_data/data/"),
				ivProfile);
	}

	public static void notifyCarListChanged() {
		tvProfileMainCarName.setText(C.MAIN_CAR_PROFILE.getMMC_CARNICK());
		tvProfileMainCarNum.setText(C.MAIN_CAR_PROFILE.getCar_main_cnt());
		String urlProfileMainCar = C.MK_BASE
				+ C.MAIN_CAR_PROFILE.getMMC_CARPIC_URL().replaceAll("/data/",
						"/thumb_data/data/");
		Log.d(TAG, "urlProfileMainCar : " + urlProfileMainCar);
		try {
			mImageDownloader.download(urlProfileMainCar, ivProfileMainCar);
		} catch (Exception e) {
			mImageDownloader = new ImageDownloader(mContext);
			mImageDownloader.download(urlProfileMainCar, ivProfileMainCar);
		}
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		switch (position) {
		case 0:
			if (C.MAIN_CAR_PROFILE.getDreamCarList().size() == 2) {
				new MenuLeftToMkCarListProfileTask(
						MkCarListProfileActivity.DREAMCAR1, C.mem_idx,
						"Loading...", true, mContext).execute();
			} else {
				new MenuLeftToMkCarListProfileTask(
						MkCarListProfileActivity.DREAMCAR2, C.mem_idx,
						"Loading...", true, mContext).execute();
			}

			break;
		case 1:
			new MenuLeftToMkCarListProfileTask(
					MkCarListProfileActivity.DREAMCAR2, C.mem_idx,
					"Loading...", true, mContext).execute();
			break;
		}
	}

}
