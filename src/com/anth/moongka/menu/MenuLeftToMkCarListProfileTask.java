package com.anth.moongka.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.profile_car.MKCarAddInformationActivity;
import com.anth.moongka.profile_car_list.MkCarListProfileActivity;

public class MenuLeftToMkCarListProfileTask extends MkTask {
	private final String TAG = "MK_MenuLeftToMkCarListProfileTask";
	private Context mContext;
	private Activity mActivity;

	private boolean isCert = false;
	private int where;
	private String you_idx;

	public MenuLeftToMkCarListProfileTask(int where, String you_idx,
			String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity) mContext;
		this.you_idx = you_idx;
		this.where = where;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.getCarProfileList(C.mem_idx, you_idx);
		
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (C.car_cnt.equals("0")) {

			Intent intent = new Intent(mActivity,
					MKCarAddInformationActivity.class);
			intent.putExtra("you_idx", you_idx);
			intent.putExtra("where", where);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

		} else if (isCert) {
			Intent intent = new Intent(mActivity,
					MkCarListProfileActivity.class);
			intent.putExtra("you_idx", you_idx);
			intent.putExtra("where", where);
			mActivity.startActivity(intent);
			mActivity
					.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		} else {
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT)
					.show();
		}
	}

}
