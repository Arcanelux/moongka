/**
 * MenuTop
 *  FrameLayout, MkMenuActivity의 상단부분
 */
package com.anth.moongka.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.anth.moongka.R;
import com.anth.moongka.function.C;

public class MenuTop extends FrameLayout{
	private final String TAG = "MK_MenuRight";
	private Context mContext;

	private FrameLayout flMenuTop;
	
	private ImageButton btnMenu, btnNotify, btnPlace, btnWrite, btnSearch;
	public static ImageView ivNew;
	
	public MenuTop(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		firstInit(context);
	}
	public MenuTop(Context context, AttributeSet attrs) {
		super(context, attrs);
		firstInit(context);
	}
	public MenuTop(Context context) {
		super(context);
		firstInit(context);
	}
	
	private void firstInit(Context context){
		mContext = context;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		flMenuTop = (FrameLayout) inflater.inflate(R.layout.menu_top, null);
		addView(flMenuTop);
		
		btnMenu = (ImageButton) flMenuTop.findViewById(R.id.btnTopMenu);
		btnNotify = (ImageButton) flMenuTop.findViewById(R.id.btnTopNotify);
		btnPlace = (ImageButton) flMenuTop.findViewById(R.id.btnTopPlace);
		btnWrite = (ImageButton) flMenuTop.findViewById(R.id.btnTopWrite);
		btnSearch = (ImageButton) flMenuTop.findViewById(R.id.btnTopSearch);
		
		ivNew = (ImageView) flMenuTop.findViewById(R.id.ivTopNew);
		if(C.isExistNotify){
			ivNew.setVisibility(View.VISIBLE);
		} else{
			ivNew.setVisibility(View.GONE);
		}
	}
	
	public void setBtnMenuOnClickLIstener(OnClickListener listener){
		btnMenu.setOnClickListener(listener);
	}
	public void setBtnNotifyOnClickLIstener(OnClickListener listener){
		btnNotify.setOnClickListener(listener);
	}
	public void setBtnPlaceOnClickLIstener(OnClickListener listener){
		btnPlace.setOnClickListener(listener);
	}
	public void setBtnWriteOnClickLIstener(OnClickListener listener){
		btnWrite.setOnClickListener(listener);
	}
	public void setBtnSearchOnClickLIstener(OnClickListener listener){
		btnSearch.setOnClickListener(listener);
	}
	public static void startDelayHandler(int value){
		ivNew.post(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				ivNew.setVisibility(View.VISIBLE);
			}
		});
	}
	public static void setNewIconVisible(int value){
		if(value==View.VISIBLE){
			ivNew.setVisibility(View.VISIBLE);
		} else if(value==View.INVISIBLE){
			ivNew.setVisibility(View.INVISIBLE);
		}
	}

}
