/**
 * 좌측메뉴 프로필 정보
 */
package com.anth.moongka.menu;

import java.util.ArrayList;

public class MkProfileMain {
	private String mem_name;
	private String mem_profile;
	private String inviteFriend;
	
	private String MMC_CARPIC_URL;
	private String MMC_CARNICK;
	private String car_main_cnt;
	
	private ArrayList<DreamCar> dreamCarList = new ArrayList<MkProfileMain.DreamCar>();
	
	
	
	public class DreamCar{
		public String MMC_ID;
		public String MMC_CARPIC_URL;
		public String CC_NM;
		public DreamCar(String mMC_ID, String mMC_CARPIC_URL, String cC_NM) {
			super();
			MMC_ID = mMC_ID;
			MMC_CARPIC_URL = mMC_CARPIC_URL;
			CC_NM = cC_NM;
		}
		
		
	}
	
	
	public void addDreamCar(String mMC_ID, String mMC_CARPIC_URL, String cC_NM) {
		DreamCar mCar = new DreamCar(mMC_ID, mMC_CARPIC_URL, cC_NM);
		dreamCarList.add(mCar);
	}
	
	public MkProfileMain(){
		
	}
	public MkProfileMain(String mem_name, String mem_profile,String inviteFriend,
			String mMC_CARPIC_URL, String mMC_CARNICK, String car_main_cnt,
			ArrayList<DreamCar> dreamCarList) {
		this.mem_name = mem_name;
		this.mem_profile = mem_profile;
		MMC_CARPIC_URL = mMC_CARPIC_URL;
		MMC_CARNICK = mMC_CARNICK;
		this.car_main_cnt = car_main_cnt;
		this.dreamCarList = dreamCarList;
		this.inviteFriend = inviteFriend;
	}

	public String getMem_name() {
		return mem_name;
	}

	public void setMem_name(String mem_name) {
		this.mem_name = mem_name;
	}

	public String getMem_profile() {
		return mem_profile;
	}

	public void setMem_profile(String mem_profile) {
		this.mem_profile = mem_profile;
	}

	public String getMMC_CARPIC_URL() {
		return MMC_CARPIC_URL;
	}

	public void setMMC_CARPIC_URL(String mMC_CARPIC_URL) {
		MMC_CARPIC_URL = mMC_CARPIC_URL;
	}

	public String getMMC_CARNICK() {
		return MMC_CARNICK;
	}

	public void setMMC_CARNICK(String mMC_CARNICK) {
		MMC_CARNICK = mMC_CARNICK;
	}

	public String getCar_main_cnt() {
		return car_main_cnt;
	}

	public void setCar_main_cnt(String car_main_cnt) {
		this.car_main_cnt = car_main_cnt;
	}

	public ArrayList<DreamCar> getDreamCarList() {
		return dreamCarList;
	}

	public void setDreamCarList(ArrayList<DreamCar> dreamCarList) {
		this.dreamCarList = dreamCarList;
	}

	public String getInviteFriend() {
		return inviteFriend;
	}

	public void setInviteFriend(String inviteFriend) {
		this.inviteFriend = inviteFriend;
	}
	
	
}
