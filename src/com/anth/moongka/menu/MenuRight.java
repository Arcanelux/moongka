/**
 * MenuRight
 *  FrameLayout, MkMenuActivity의 우측 메뉴
 *  현재 작동하지않음
 */
package com.anth.moongka.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.function.C;

public class MenuRight extends FrameLayout implements OnClickListener{
	private final String TAG = "MK_MenuRight";
	private Context mContext;

	private FrameLayout flMenuRight;
	
	private Button btn1, btn2, btn3;
	
	public MenuRight(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		firstInit(context);
	}
	public MenuRight(Context context, AttributeSet attrs) {
		super(context, attrs);
		firstInit(context);
	}
	public MenuRight(Context context) {
		super(context);
		firstInit(context);
	}
	
	private void firstInit(Context context){
		mContext = context;
		
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		flMenuRight = (FrameLayout) inflater.inflate(R.layout.menu_right, null);
		FrameLayout.LayoutParams params = new LayoutParams((int)(C.DISPLAY_WIDTH*0.8f), android.view.ViewGroup.LayoutParams.MATCH_PARENT, Gravity.RIGHT);
		addView(flMenuRight, params);
//		flMenuRight.offsetLeftAndRight(ConstValue.DISPLAY_WIDTH);
		btn1 = (Button) flMenuRight.findViewById(R.id.button1);
		btn2 = (Button) flMenuRight.findViewById(R.id.button2);
		btn3 = (Button) flMenuRight.findViewById(R.id.button3);
		
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.button1:
			Toast.makeText(mContext, v.getId(), Toast.LENGTH_SHORT).show();
			break;
		case R.id.button2:
			Toast.makeText(mContext, v.getId(), Toast.LENGTH_SHORT).show();
			break;
		case R.id.button3:
			Toast.makeText(mContext, v.getId(), Toast.LENGTH_SHORT).show();
			break;
		}
	}
	
	

}
