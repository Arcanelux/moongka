/**
 * MkMsgListTask
 *  쪽지 리스트 액션
 */
package com.anth.moongka.message;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkMsg;
import com.anth.moongka.function.MkApi;

public class MkMsgGCMListTask extends MkTask{
	private final String TAG = "MK_MkMsgTask";
	private Context mContext;
	
	private String oth_idx;
	private ArrayList<MkMsg> msgList;
	private ArrayAdapter mAdapter;
	private boolean isCert = false;
	
	public MkMsgGCMListTask(String oth_idx, ArrayList<MkMsg> msgList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.oth_idx = oth_idx;
		this.msgList = msgList;
		this.mAdapter = mAdapter;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.messageGCMList(oth_idx, msgList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mAdapter.notifyDataSetChanged();
		if(isCert){
			int position=0;
			MkMsg curMsg = msgList.get(position);
			Intent intent = new Intent(mContext, MessageDetailActivity.class);
			intent.putExtra("opp_name", curMsg.getOpp_user_name());
			intent.putExtra("own_idx", curMsg.getOwn_user_idx());
			intent.putExtra("opp_idx", curMsg.getOpp_user_idx());
			((Activity)mContext).startActivity(intent);
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
		
	}	
}