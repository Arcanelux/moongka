/**
 * MkMsgDetailAddTask
 * 쪽지 쓰기
 * 전송 이후에 특별한 액션 없음. Refresh는 MkMsgDetailTask가 주기적으로 실행되면서 처리해준다
 * 전송이 실패했을경우 예외처리 되어있지않음. (실패하면 다시보낸다던가, 카톡처럼 x표시로 다시 보낼수있는 로직 필요)
 */
package com.anth.moongka.message;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class MkMsgDeleteTask extends MkTask {
	private final String TAG = "MK_MkMsgDetailAddTask";
	private Context mContext;
	private boolean isCert = false;
	private ArrayAdapter mAdapter;
	private String cmc_idx;
	

	public MkMsgDeleteTask(String cmc_idx,ArrayAdapter mAdapter,boolean isProgressDialog, Context context) {
		super("삭제중....", isProgressDialog, context);
		mContext = context;
		this.cmc_idx = cmc_idx;
		this.mAdapter = mAdapter;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.messageDelete(cmc_idx);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mAdapter.notifyDataSetChanged();
		if(isCert){
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}
}
