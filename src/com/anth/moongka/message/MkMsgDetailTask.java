/**
 * MkMsgDetailTask
 *  쪽지 보기 액션
 *  MessageDetailActivity에서 2초마다 실행하며, 모든 쪽지를 2초마다 다시 받아온다
 *  서비스가 커지거나 느리다는 컴플레인 들어오면 해결할것
 */
package com.anth.moongka.message;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkMsg;
import com.anth.moongka.function.MkApi;

public class MkMsgDetailTask extends MkTask{
	private final String TAG = "MK_MkMsgDetailTask";
	private Context mContext;
	private boolean isCert = false;

	private ArrayList<MkMsg> msgList;
	private ArrayAdapter mAdapter;
	private String opp_idx, own_idx;
	private int start_num;
	private long c_time;
	private long old_time;
	private float p_time;
	private float t_time;

	public static boolean canRun = true; 

	public MkMsgDetailTask(String own_idx, String opp_idx, int start_num, ArrayList<MkMsg> msgList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.opp_idx = opp_idx;
		this.own_idx = own_idx;
		this.start_num = start_num;
		this.msgList = msgList;
		this.mAdapter = mAdapter;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		boolean isWait = true;
		boolean isFirst = true;
		long oriTime = 0;
		while(canRun){
			if(!isWait){
				oriTime = System.currentTimeMillis();
				isWait = true;
			} else{	//wait 중
				if(System.currentTimeMillis() - oriTime > 2000 || isFirst){
					isWait = false;
					isFirst = false;
					//Log.i(TAG, own_idx);
					isCert = MkApi.messageDetail(own_idx, opp_idx, start_num, msgList);
					publishProgress();
				}
			}
//			c_time=System.currentTimeMillis();
//			if(old_time==0)
//				old_time=c_time;
//			p_time=c_time-old_time;
//			old_time=c_time;
//			t_time+=p_time;//변화된 시간을 저장.
//			if(t_time>5){
//				isCert = MkApi.messageDetail(opp_idx, start_num, msgList);
//				publishProgress();
//				t_time=0;
//			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
		MessageDetailActivity.notifyList();
	}
}
