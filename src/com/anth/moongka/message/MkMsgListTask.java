/**
 * MkMsgListTask
 *  쪽지 리스트 액션
 */
package com.anth.moongka.message;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkMsg;
import com.anth.moongka.function.MkApi;

public class MkMsgListTask extends MkTask{
	private final String TAG = "MK_MkMsgTask";
	private Context mContext;
	
	private ArrayList<MkMsg> msgList;
	private ArrayAdapter mAdapter;
	private boolean isCert = false;
	private Activity mActivity;
	
	public MkMsgListTask(ArrayList<MkMsg> msgList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.msgList = msgList;
		this.mAdapter = mAdapter;
		this.mActivity = (Activity)mContext;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.messageList(msgList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mAdapter.notifyDataSetChanged();
		if(msgList.size()==0){
			LinearLayout mainBackground = (LinearLayout) mActivity.findViewById(R.id.messageMain);
			mainBackground.setBackgroundResource(R.drawable.img_data_android);
		}
		if(isCert){
			
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
		
	}	
}