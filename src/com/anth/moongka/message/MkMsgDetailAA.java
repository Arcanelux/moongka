/**
 * MkMsgDetailAA
 *  쪽지보기 ArrayAdapter
 *  좌, 우 판단
 */
package com.anth.moongka.message;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkMsg;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;

public class MkMsgDetailAA extends ArrayAdapter<MkMsg> {
	private final String TAG = "MK_MkMsgAA";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	
	private ArrayList<MkMsg> msgList;

	public MkMsgDetailAA(Context context, int textViewResourceId, ArrayList<MkMsg> msgList) {
		super(context, textViewResourceId, msgList);
		mContext = context;
		mImageDownloader = new ImageDownloader(mContext);
		this.msgList = msgList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MkMsg curMsg = msgList.get(position);
		
		View curView = convertView;
		if(true){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if(curMsg.getType().equals("R")){
				curView = inflater.inflate(R.layout.message_detail_list_item1, null);	
			} else{
				curView = inflater.inflate(R.layout.message_detail_list_item2, null);
			}
		}
		
		TextView tvName = (TextView) curView.findViewById(R.id.tvMessageDetailItemName);
		TextView tvContent = (TextView) curView.findViewById(R.id.tvMessageDetailItemContent);
		TextView tvDate = (TextView) curView.findViewById(R.id.tvMessageDetailItemDate);
		ImageView ivPhoto = (ImageView) curView.findViewById(R.id.ivMessageDetailItemThumbnail);
		
		tvName.setText(curMsg.getOpp_user_name());
		tvContent.setText(curMsg.getContent());
		tvDate.setText(curMsg.getWriteTime());
		
		if(curMsg.getType().equals("R")){
			mImageDownloader.download(C.MK_BASE + curMsg.getOpp_user_url_photo().replaceAll("/data/", "/thumb_data/data/"), ivPhoto);
		} else{
			mImageDownloader.download(C.mem_profile.replaceAll("/data/", "/thumb_data/data/"), ivPhoto);
		}
		
		return curView;
	}

	
}
