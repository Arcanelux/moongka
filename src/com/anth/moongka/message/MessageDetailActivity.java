/**
 * MessageDetailActivity
 *  쪽지 보기
 *  MkMsgDetailAddTask와 겹칠 경우 에러발생하기때문에 MkMsgDetailTask를 2초마다 병렬로 동작시킴 
 */
package com.anth.moongka.message;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkMsg;
import com.anth.moongka.function.Lhy_Function;

public class MessageDetailActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_MessageDetailActivity";
	private Context mContext;
	
	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;

	private static ListView lvMessageDetail;
	private static MkMsgDetailAA mAdapter;
	private static ArrayList<MkMsg> msgList = new ArrayList<MkMsg>();
	private String opp_name, opp_idx, own_idx;
	private MkMsgDetailTask mTask;

	private EditText etMsgDetail;
	private Button btnMsgDetailSend;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.message_detail);
		
		viewTop = findViewById(R.id.viewTopMessageDetail);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("쪽지");

		Intent intent = getIntent();
//		MkMsg curMsg = (MkMsg) intent.getSerializableExtra("curMsg");
		opp_name = intent.getStringExtra("opp_name");
		//Log.i("opp_name", opp_name);
		opp_idx = intent.getStringExtra("opp_idx");
		own_idx = intent.getStringExtra("own_idx");
		tvTitle.setText(opp_name + " 님과의 대화");
			
		Log.d(TAG, "opp : " + opp_idx + ", " + "own : " + own_idx);

		lvMessageDetail = (ListView) findViewById(R.id.lvMessageDetail);
		mAdapter = new MkMsgDetailAA(mContext, android.R.layout.simple_list_item_1, msgList);
		lvMessageDetail.setAdapter(mAdapter);

		mTask = new MkMsgDetailTask(own_idx, opp_idx, 0, msgList, mAdapter, "쪽지 불러오는 중...", false, mContext);
		MkMsgDetailTask.canRun = true;

		etMsgDetail = (EditText) findViewById(R.id.etMessageDetail);
		btnMsgDetailSend = (Button) findViewById(R.id.btnMessageDetailSend);
		btnMsgDetailSend.setOnClickListener(this);
		
		mTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnMessageDetailSend:
			String content = etMsgDetail.getText().toString();
			if(content.length()>0){
				new MkMsgDetailAddTask(own_idx, opp_idx, content, "메세지 전송중", false, mContext).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
				etMsgDetail.setText("");
			}
			break;
		}
	}

	@Override
	public void finish() {
		super.finish();
		msgList.clear();
		MkMsgDetailTask.canRun = false;
		Lhy_Function.forceHideKeyboard(mContext, etMsgDetail);
	}
	
	public static void notifyList(){
		mAdapter.notifyDataSetChanged();
		lvMessageDetail.setSelection(msgList.size()-1);
	}


	
}
