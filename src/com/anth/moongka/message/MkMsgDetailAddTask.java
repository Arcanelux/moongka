/**
 * MkMsgDetailAddTask
 * 쪽지 쓰기
 * 전송 이후에 특별한 액션 없음. Refresh는 MkMsgDetailTask가 주기적으로 실행되면서 처리해준다
 * 전송이 실패했을경우 예외처리 되어있지않음. (실패하면 다시보낸다던가, 카톡처럼 x표시로 다시 보낼수있는 로직 필요)
 */
package com.anth.moongka.message;

import android.content.Context;
import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class MkMsgDetailAddTask extends MkTask {
	private final String TAG = "MK_MkMsgDetailAddTask";
	private Context mContext;
	private boolean isCert = false;
	
	private String own_idx, opp_idx;
	private String content;

	public MkMsgDetailAddTask(String own_idx, String opp_idx, String content, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.own_idx = own_idx;
		this.opp_idx = opp_idx;
		this.content = content;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.messageWrite(own_idx, opp_idx, content);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
//		Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
//		Toast.makeText(mContext, "쪽지 전송완료", Toast.LENGTH_SHORT).show();
	}
}
