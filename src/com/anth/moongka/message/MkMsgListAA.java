/**
 * MkMsgListAA
 *  MessageActivity�� ����Ʈ ArrayAdapter
 */
package com.anth.moongka.message;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkMsg;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;

public class MkMsgListAA extends ArrayAdapter<MkMsg> {
	private final String TAG = "MK_MkMsgAA";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	
	private Activity mActivity;
	private ArrayList<MkMsg> msgList;

	public MkMsgListAA(Context context, int textViewResourceId, ArrayList<MkMsg> msgList) {
		super(context, textViewResourceId, msgList);
		mContext = context;
		mActivity = (Activity)mContext;
		mImageDownloader = new ImageDownloader(mContext);
		this.msgList = msgList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.message_list_item, null);
		}
		
		
		
		MkMsg curMsg = msgList.get(position);
		TextView tvName = (TextView) curView.findViewById(R.id.tvMessageItemName);
		TextView tvContent = (TextView) curView.findViewById(R.id.tvMessageItemContent);
		ImageView ivPhoto = (ImageView) curView.findViewById(R.id.ivMessageItemThumbnail);
		
		
		
		tvName.setText(curMsg.getOpp_user_name());
		tvContent.setText(curMsg.getContent());
		mImageDownloader.download(C.MK_BASE + curMsg.getOpp_user_url_photo().replaceAll("/data/", "/thumb_data/data/"), ivPhoto);
		
		return curView;
	}

	
}
