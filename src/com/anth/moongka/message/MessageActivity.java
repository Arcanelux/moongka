/**
 * MessageActivity
 *  쪽지 목록
 */
package com.anth.moongka.message;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkMsg;

public class MessageActivity extends MkActivity implements OnItemClickListener,OnItemLongClickListener, OnClickListener{
	private final String TAG = "MK_MessageActivity";
	private Context mContext;
	
	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;
	private LinearLayout mainBackground;
	
	private ListView lvMessage;
	private MkMsgListAA mAdapter;
	private ArrayList<MkMsg> msgList = new ArrayList<MkMsg>();
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.message);
		
		viewTop = findViewById(R.id.viewTopMessage);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("쪽지");
		
		mainBackground = (LinearLayout) findViewById(R.id.messageMain);
			
		Intent intent = getIntent();
		String oth_idx = intent.getStringExtra("oth_idx");
		boolean isFromGCM = intent.getBooleanExtra("isFromGCM", false);
	
		lvMessage = (ListView) findViewById(R.id.lvMessage);
		mAdapter = new MkMsgListAA(mContext, android.R.layout.simple_list_item_1, msgList);
		lvMessage.setAdapter(mAdapter);
		lvMessage.setOnItemClickListener(this);
		lvMessage.setOnItemLongClickListener(this);
		if(isFromGCM){
			new MkMsgGCMListTask(oth_idx, msgList, mAdapter, "쪽지 목록을 받아오는중입니다...", true, mContext).execute();
		}else{
			new MkMsgListTask(msgList, mAdapter, "쪽지 목록을 받아오는 중입니다...", true, mContext).execute();
			Log.i("msg1", ""+msgList.size());
//			if(msgList.size()==0){
//				mainBackground.setBackgroundResource(R.drawable.img_data_android);
//			}
		}
		
		
		
	}


	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		MkMsg curMsg = msgList.get(position);
		Intent intent = new Intent(MessageActivity.this, MessageDetailActivity.class);
		intent.putExtra("opp_name", curMsg.getOpp_user_name());
		intent.putExtra("own_idx", curMsg.getOwn_user_idx());
		intent.putExtra("opp_idx", curMsg.getOpp_user_idx());
		startActivity(intent);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View v, int position,
			long id) {
		final int itemPosition = position;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("삭제하시겠습니까?")
				.setCancelable(false)
				.setPositiveButton("아니오",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {

							}
						})
				.setNegativeButton("예", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						new MkMsgDeleteTask(msgList.get(itemPosition).getIdx(),mAdapter,true,mContext).execute();
						Log.i("delete", "");
						msgList.clear();
						new MkMsgListTask(msgList, mAdapter, "쪽지 목록을 받아오는 중입니다...", true, mContext).execute();
						Log.i("msg", ""+msgList.size());
//						if(msgList.size()==0){
//							mainBackground.setBackgroundResource(R.drawable.img_data_android);
//						}
						mAdapter = new MkMsgListAA(mContext, android.R.layout.simple_list_item_1, msgList);
						lvMessage.setAdapter(mAdapter);
						mAdapter.notifyDataSetChanged();
					}
				});
		builder.create().show();
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		}
	}




}
