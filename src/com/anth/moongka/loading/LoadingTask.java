/**
 * LoadingTask
 * 로딩 액션
 *  LocationManager할당(어디서든 쓸 수 있도록 로딩단계에서 미리 초기화)
 */
package com.anth.moongka.loading;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.login.LoginActivity;

public class LoadingTask extends AsyncTask<Void, Integer, Void>{
	private final String TAG = "MK_LoadingTask";
	private Context mContext;
	private Activity mActivity;

	private ProgressDialog mProgressDialog;

	private boolean isCertCode = false;

	public LoadingTask(Context context){
		mContext = context;
		mActivity = (Activity)mContext;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		//		mProgressDialog = new ProgressDialog(mContext);
		//		mProgressDialog.setMessage("Loading...");
		//		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		//		mProgressDialog.show();
	}
	@Override
	protected Void doInBackground(Void... params) {
		/** GPS 세팅 **/
		// LocationListener 핸들
		C.locManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);


		/** Display 정보를 받아온다 **/
		getDisplayLenght();
		/** Code 정보를 받아온다 **/
		isCertCode = MkApi.getCodes();
		MkApi.getAreaCodes();
		return null;
	}
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);


		/** GPS 세팅 **/
		try{
			// GPS로 부터 위치 정보를 업데이트 요청
			C.locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, C.locationListener);
			// 기지국으로부터 위치 정보를 업데이트 요청
			C.locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, C.locationListener);
		} catch(Exception e){
			e.printStackTrace();
			Log.d(TAG, "Location Search Failed");
		}
		Log.d(TAG, "DWidth : " + C.DISPLAY_WIDTH);
		Log.d(TAG, "DHeight : " + C.DISPLAY_HEIGHT);

		//		mProgressDialog.dismiss();

		if(isCertCode){
			Intent intent = new Intent(mActivity, LoginActivity.class);
			mActivity.startActivity(intent);
			mActivity.finish();
		} else{
			Lhy_Function.makeToast(MkApi.returnMsg, mContext);
		}
	}
	@Override
	protected void onCancelled() {
		super.onCancelled();
	}

	private void getDisplayLenght(){
		Display display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		C.DISPLAY_WIDTH = display.getWidth();
		C.DISPLAY_HEIGHT = display.getHeight();
	}

	private void getJsonData(){

	}



}
