/**
 * LoadingActivity
 * 로딩화면
 *  Push를 통해 왔을 경우, 전역변수에 Push로 옴을 알려줌
 */
package com.anth.moongka.loading;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.anth.moongka.GCMIntentService;
import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkPref;
import com.google.android.gcm.GCMRegistrar;

public class LoadingActivity extends MkActivity{
	private final String TAG = "MK_LoadingActivity";
	private Context mContext;

	public static final Handler mHandler = new Handler();

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loading);
		mContext = this;
		/** 
		 * Push로 왔을경우
		 *  전역변수에 GCM에서 왔으며, 어느액티비티로 가야하는지 저장
		 *  LoginActivity에서 받아서 맞는 액티비티로 이동
		 */
		Intent intent = getIntent();
		boolean isGCM = intent.getBooleanExtra("fromGCM", false);
		if(isGCM){
			C.isFromGCM = true;
			C.GCM_CURRENT = intent.getIntExtra("toIntent", 0);
			C.GCM_KEY_ID = intent.getStringExtra("keyId");
		}

		registGCM();
		new LoadingTask(mContext).execute();
	}

	private void registGCM() {
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);

		final String regId = GCMRegistrar.getRegistrationId(this);
		Log.d(TAG, "regId : " + regId);
		MkPref.saveGCM(mContext, regId);

		if("".equals(regId) || regId==null){   //구글 가이드에는 regId.equals("")로 되어 있는데 Exception을 피하기 위해 수정
			GCMRegistrar.register(this, GCMIntentService.SEND_ID);
			Log.d(TAG, "Register");
			//			tvTest.setText("Register Success. regId : " + regId);
		}

		else{
			Log.d(TAG, "Alreay Registered : " + regId);
			//			tvTest.setText("Already Registered. regId : " + regId);
		}
	}

}
