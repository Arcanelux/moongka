/**
 * LoginParams
 * 로그인정보
 */
package com.anth.moongka.loading;

public class LoginParams {
	public String id;
	public String pw;
	public boolean isAutoLogin;
	
}
