/**
 * MkActivity
 * 뭉카 기본 액티비티
 *  - 타이틀 바 없애기
 *  - 액티비티 전환효과
 *  - 앱 종료 코드 삽입
 */
package com.anth.moongka.fix_activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.anth.moongka.R;

public class MkActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

	
	@Override
	public void startActivity(Intent intent) {
		super.startActivityForResult(intent, 0);
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(resultCode){
		case 4:
			finish();
		}
	}
	
	
}
