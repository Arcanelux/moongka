/**
 * MkMenuActivity
 *  뭉카 메뉴 액티비티
 *  가장 기초가 되는 액티비티
 *  TopMenu, Left&Right Menu, MainLayout을 동적으로 생성해준다
 *  실제 사용시에는 상속받은 후 일반 Activity 처럼 사용하면 된다
 */
package com.anth.moongka.fix_activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.anth.moongka.R;
import com.anth.moongka.function.C;
import com.anth.moongka.main.SearchNewActivity;
import com.anth.moongka.menu.MenuLeft;
import com.anth.moongka.menu.MenuRight;
import com.anth.moongka.menu.MenuTop;
import com.anth.moongka.notify.NotifyActivity;
import com.anth.moongka.place.PlaceActivity;
import com.anth.moongka.write.WriteActivity;

public class MkMenuActivity extends Activity implements OnClickListener {
	private final String TAG = "MK_IncludeMenuActivity";
	private Context mContext;
	private Activity mActivity;
	public static final int TO_SEARCH = 6324;

	/** 메뉴표출시 터치영역 **/
	private View viewLeftTouch, viewRightTouch;

	/** TopMenu **/
	private MenuTop flMenuTop;
	private OnClickListener listenerBtnMenu;
	private OnClickListener listenerBtnNotify;
	private OnClickListener listenerBtnPlace;
	private OnClickListener listenerBtnWrite;
	private OnClickListener listenerBtnSearch;
	private boolean isLeftMenuOpen = false;
	private boolean isRightMenuOpen = false;

	/** Left & Right Menu **/
	private MenuLeft flMenuLeft;
	private MenuRight flMenuRight;

	/** MainLayout **/
	private FrameLayout flFullLayout; // 가장 상위 레이아웃. 와 MainContentFeed를 포함한다
	private FrameLayout flMainLayout;
	private LinearLayout llMainLayout; // 좌/우 메뉴를 제외한 Top메뉴와 Content부분을 포함한 레이아웃
	private AnimationSet aniRight, aniLeft;
	private int endX;
	private int duration = 300;

	/** Extends IncludeMenuActivity의 Activity에서 메뉴를 제외한 사용할 부분 **/
	public FrameLayout flContent; // Extends한 Activity에서 내용을 넣을때 사용
	private ImageView ivShadow;

	/** View ID **/
	private final int ID_MainLayout = 50;
	private final int ID_LeftTouch = 51;
	private final int ID_RightTouch = 52;

	/** Display Width **/
	private int dWidth = C.DISPLAY_WIDTH;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContext = this;
		mActivity = (Activity) mContext;

		/** Animation & TopMenu ClickListener Setting **/
		setAnimation();
		setMenuTopListener();

		/** TopMenu Variable Connect **/
		flMenuTop = new MenuTop(mContext); // TopMenu(FrameLayout) 동적생성
		flMenuTop.setBtnMenuOnClickLIstener(listenerBtnMenu); // TopMenu의 버튼리스너
																// 할당
		flMenuTop.setBtnSearchOnClickLIstener(listenerBtnSearch);
		flMenuTop.setBtnPlaceOnClickLIstener(listenerBtnPlace);
		flMenuTop.setBtnNotifyOnClickLIstener(listenerBtnNotify);
		flMenuTop.setBtnWriteOnClickLIstener(listenerBtnWrite);

		/** Content View(사용자 설정하게될 View) **/
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		flContent = (FrameLayout) inflater.inflate(R.layout.main, null);
		ivShadow = (ImageView) flContent.findViewById(R.id.ivMainShadow);

		/** FullLayout, MainLayout Connect **/
		flFullLayout = new FrameLayout(mContext);
		flFullLayout.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT));
		flMainLayout = new FrameLayout(mContext);
		flMainLayout.setLayoutParams(new LayoutParams(C.DISPLAY_WIDTH,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT));

		llMainLayout = new LinearLayout(mContext);
		llMainLayout.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.MATCH_PARENT));
		// llMainLayout.setLayoutParams(new
		// LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.MATCH_PARENT, 1280.0f));
		llMainLayout.setBackgroundColor(Color.WHITE);
		// llMainLayout.setOnClickListener(this); // MainLayout의 클릭이벤트(좌/우메뉴
		// 슬라이딩) 할당
		llMainLayout.setId(ID_MainLayout);
		llMainLayout.setOrientation(LinearLayout.VERTICAL);

		/** MainLayout에 TopMenu와 Content View를 삽입 **/
		// 타이틀바 : 58 (xhdpi)
		flMainLayout.addView(llMainLayout);
		llMainLayout.addView(flMenuTop, new LinearLayout.LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 100.0f)); // MainLayout에
																				// TopMenu
																				// View를
																				// 삽입
		llMainLayout.addView(flContent, new LinearLayout.LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 1122.0f));

		C.TOP_HEIGHT = flMenuTop.getHeight();
		Log.d(TAG, "topHeight1 : " + C.TOP_HEIGHT);

		flMainLayout.post(new Runnable() {
			@Override
			public void run() {
				C.TOP_HEIGHT = flMenuTop.getHeight();
				flMenuTop
						.setLayoutParams(new android.widget.LinearLayout.LayoutParams(
								android.view.ViewGroup.LayoutParams.MATCH_PARENT,
								C.TOP_HEIGHT));
				Log.d(TAG, "topHeight2 : " + C.TOP_HEIGHT);
			}
		});

		/** Left & Right Menu Connect **/
		flMenuLeft = new MenuLeft(mContext); // 좌/우메뉴 동적생성
		flMenuRight = new MenuRight(mContext);

		flFullLayout.addView(flMenuLeft);
		flFullLayout.addView(flMenuRight);
		flFullLayout.addView(flMainLayout);

		/** 양쪽 터치영역 View **/
		viewLeftTouch = new View(mContext);
		viewLeftTouch
				.setLayoutParams(new FrameLayout.LayoutParams(
						(int) (dWidth * 0.2f),
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						Gravity.LEFT));
		viewLeftTouch.setOnClickListener(this);
		viewLeftTouch.setId(ID_LeftTouch);

		viewRightTouch = new View(mContext);
		viewRightTouch
				.setLayoutParams(new FrameLayout.LayoutParams(
						(int) (dWidth * 0.2f),
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						Gravity.RIGHT));
		viewRightTouch.setOnClickListener(this);
		viewRightTouch.setId(ID_RightTouch);

		// 나머지
		flMainLayout.bringToFront(); // 모든 View추가 후 MainLayout을 가장 위로 올려줌
		flFullLayout.addView(viewLeftTouch); // 터치영역 View들은 가장 상단에
												// (View.visibility 를 통해 터치가능여부를
												// 설정
		flFullLayout.addView(viewRightTouch);
		flMainLayoutEnable();

		setContentView(flFullLayout);

		flMainLayout.post(new Runnable() {
			@Override
			public void run() {
				C.TOP_HEIGHT = flMenuTop.getHeight();
				Log.d(TAG, "topHeight3 : " + C.TOP_HEIGHT);
			}
		});
	}

	private void setAnimation() {
		aniRight = new AnimationSet(false);

		Animation translate = null;
		endX = (int) (dWidth * 0.8f);
		translate = new TranslateAnimation(0, endX, 0, 0);
		translate.setDuration(duration);
		// translate.setInterpolator(AnimationUtils.loadInterpolator(mContext,
		// android.R.anim.anticipate_interpolator));

		aniRight.setFillAfter(false);
		aniRight.addAnimation(translate);
		aniRight.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				Handler mHandler = new Handler();
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						/**
						 * 메인 레이아웃은 오른쪽으로 이동한다(좌측 메뉴가 나타난다 -> 결과는
						 * isLeftMenuOpen=true
						 **/
						moveMainLayout(endX);
						/** 만약 메인레이아웃이 왼쪽에 있었다면(오른쪽 메뉴가 열린상태) **/
						if (isRightMenuOpen) {
							/** 오른쪽으로 레이아웃이 이동하여 터치영역은 없어지며 메인레이아웃 활성화 **/
							flMainLayoutEnable();
							isRightMenuOpen = false;
							/** 메뉴가 오픈된 상태가 아니었다면 **/
						} else {
							/** 오른쪽으로 레이아웃이 이동하여(왼쪽메뉴가 열리며) 오른쪽여백 활성화 **/
							rightTouchEnable();
							isLeftMenuOpen = true;
						}
					}
				}, 10);
			}
		});

		/** aniLeft **/
		aniLeft = new AnimationSet(false);

		endX = (int) (dWidth * 0.8f);
		translate = new TranslateAnimation(0, -endX, 0, 0);
		translate.setDuration(duration);

		aniLeft.setFillAfter(false);
		aniLeft.addAnimation(translate);
		aniLeft.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				Handler mHandler = new Handler();
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						/**
						 * 메인레이아웃은 왼쪽으로 이동한다(우측 메뉴가 나타난다 -> 결과는
						 * isRightMenuOpen=true
						 **/
						moveMainLayout(-endX);
						/** 만약 메인레이아웃이 오른쪽에 있었다면(왼쪽메뉴가 열린상태) **/
						if (isLeftMenuOpen) {
							/**
							 * 왼쪽으로 레이아웃이 이동하여 터치영역은 없어지며 메인레이아웃이 활성화,
							 * isLeftMenuOpen은 false
							 **/
							flMainLayoutEnable();
							isLeftMenuOpen = false;
							/** 메뉴가 오픈된 상태가 아니었다면 **/
						} else {
							/**
							 * 왼쪽으로 레이아웃이 이동하여(오른쪽메뉴가 열리며) 왼쪽여백 터치를 Enable,
							 * isRightMenuOpen은 true
							 **/
							leftTouchEnable();
							isRightMenuOpen = true;
						}
					}
				}, 10);
			}
		});

	}

	private void moveMainLayout(int value) {
		// llMainLayout.offsetLeftAndRight(value);
		FrameLayout.LayoutParams params = (LayoutParams) flMainLayout
				.getLayoutParams();
		int ori = params.leftMargin;
		params.setMargins(ori + value, 0, 0, 0);
		// params.leftMargin = value;
		params.gravity = Gravity.TOP;
		flMainLayout.setLayoutParams(params);
		Log.d(TAG, "ori : " + ori);
		Log.d(TAG, "value : " + value);
	}

	private void setMenuTopListener() {
		listenerBtnMenu = new OnClickListener() {
			@Override
			public void onClick(View v) {
				/** 어느 한쪽도 열린 상태가 아닐경우에 작동 **/
				if (!isRightMenuOpen && !isLeftMenuOpen) {
					flMenuLeft.setVisibility(View.VISIBLE);
					flMenuRight.setVisibility(View.GONE);
					/** 오른쪽으로 열리는 애니메이션 **/
					flMainLayout.startAnimation(aniRight);
					// isLeftOpen = true;
				}
			}
		};
		listenerBtnNotify = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, NotifyActivity.class);
				mActivity.startActivity(intent);
			}
		};
		listenerBtnPlace = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, PlaceActivity.class);
				mActivity.startActivity(intent);
			}
		};
		listenerBtnWrite = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, WriteActivity.class);
				mActivity.startActivity(intent);
			}
		};
		// listenerBtnSearch = new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// /** 어느 한쪽도 열린 상태가 아닐경우에 작동 **/
		// if(!isRightMenuOpen && !isLeftMenuOpen){
		// flMenuLeft.setVisibility(View.GONE);
		// flMenuRight.setVisibility(View.VISIBLE);
		// /** 왼쪽으로 열리는 애니메이션 **/
		// flMainLayout.startAnimation(aniLeft);
		// // isRightOpen = true;
		// }
		// }
		// };
		listenerBtnSearch = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MkMenuActivity.this,
						SearchNewActivity.class);
				startActivityForResult(intent, TO_SEARCH);
			}
		};

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case ID_LeftTouch:
			flMainLayout.startAnimation(aniRight);
			break;
		case ID_RightTouch:
			flMainLayout.startAnimation(aniLeft);
			break;
		}
	}

	private void leftTouchEnable() {
		viewLeftTouch.setVisibility(View.VISIBLE);
	}

	private void rightTouchEnable() {
		viewRightTouch.setVisibility(View.VISIBLE);
	}

	private void flMainLayoutEnable() {
		viewLeftTouch.setVisibility(View.GONE);
		viewRightTouch.setVisibility(View.GONE);
	}

	/** flContent 에 넣을 xml 을 setContentView에서 설정 **/
	@Override
	public void setContentView(int layoutResID) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View setView = inflater.inflate(layoutResID, null);
		flContent.addView(setView);
		ivShadow.bringToFront();
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

	@Override
	public void startActivity(Intent intent) {
		super.startActivityForResult(intent, 0);
		closeMenu();
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

	@Override
	public void onBackPressed() {
		if (isRightMenuOpen) {
			flMainLayout.startAnimation(aniRight);
		} else if (isLeftMenuOpen) {
			flMainLayout.startAnimation(aniLeft);
		} else {
			super.onBackPressed();
		}
	}

	public void closeMenu() {
		if (isRightMenuOpen) {
			flMainLayout.startAnimation(aniRight);
		} else if (isLeftMenuOpen) {
			flMainLayout.startAnimation(aniLeft);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 4:
			finish();
		}
	}

}
