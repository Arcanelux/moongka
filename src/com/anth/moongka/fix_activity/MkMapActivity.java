/**
 * MkMapActivity
 *  MkActivity의 MapActivity버전
 *  MkActivity가 변경되면 여기에도 반영해주어야함
 */
package com.anth.moongka.fix_activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.anth.moongka.R;
import com.google.android.maps.MapActivity;

public class MkMapActivity extends MapActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

	
	@Override
	public void startActivity(Intent intent) {
		super.startActivityForResult(intent, 0);
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(resultCode){
		case 4:
			finish();
		}
	}


	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
