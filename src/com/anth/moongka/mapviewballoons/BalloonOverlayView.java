/***
 * Copyright (c) 2010 readyState Software Ltd
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.anth.moongka.mapviewballoons;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anth.moongka.R;
import com.google.android.maps.OverlayItem;

/**
 * A view representing a MapView marker information balloon.
 * <p>
 * This class has a number of Android resource dependencies:
 * <ul>
 * <li>drawable/balloon_overlay_bg_selector.xml</li>
 * <li>drawable/balloon_overlay_close.png</li>
 * <li>drawable/balloon_overlay_focused.9.png</li>
 * <li>drawable/balloon_overlay_unfocused.9.png</li>
 * <li>layout/balloon_map_overlay.xml</li>
 * </ul>
 * </p>
 * 
 * @author Jeff Gilfelt
 *
 */
public class BalloonOverlayView extends FrameLayout {
	private final String TAG = "MK_BalloonOverlayView";
	private Context mContext;

	private LinearLayout layout;
	private TextView title;
	private TextView snippet;

	/**
	 * Create a new BalloonOverlayView.
	 * 
	 * @param context - The activity context.
	 * @param balloonBottomOffset - The bottom padding (in pixels) to be applied
	 * when rendering this view.
	 */
	/** 
	 * BallonOverlayView 생성자는 2개
	 *  아래여백만 주는것과 전체여백 설정하는것
	 *  오른쪽이나 왼쪽으로 치우친 View를 위해 구현
	 * @param context
	 * @param balloonBottomOffset
	 */
	public BalloonOverlayView(Context context, int balloonBottomOffset) {
		super(context);
		firstInit(context);
		createViewByPadding(10, 0, 10, balloonBottomOffset);
	}
	public BalloonOverlayView(Context context, int left, int top, int right, int bottom){
		super(context);
		firstInit(context);
		createViewByPadding(left, top, right, bottom);
	}


	private void firstInit(Context context){
		mContext = context;
	}

	/**
	 * BalloonOverlayView의 padding값을 설정.
	 *  기본생성자의 경우 10, 0, 10, Bottom
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 */
	private void createViewByPadding(int left, int top, int right, int bottom){
		//		setPadding(10, 0, 10, balloonBottomOffset);
		setPadding(left, top, right, bottom);
		layout = new LinearLayout(mContext);
		layout.setVisibility(VISIBLE);

		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.balloon_overlay, layout);
		title = (TextView) v.findViewById(R.id.balloon_item_title);
		snippet = (TextView) v.findViewById(R.id.balloon_item_snippet);

		ImageView close = (ImageView) v.findViewById(R.id.close_img_button);
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				layout.setVisibility(GONE);
			}
		});

		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.NO_GRAVITY;

//		layout.setVisibility(View.INVISIBLE);
		addView(layout, params);
		
		/** 
		 * 혹시나 BalloonView를 왼쪽으로 옮길때 재사용
		 * 오른쪽으로 옮기는건 BalloonItemizedOverlay에서 Gravity를 Bottom_Center -> Bottom으로 바꾸는것으로 가능
		 */
//		Log.d(TAG, "layout width : " + layout.getWidth() + ", layout height : " + layout.getHeight());
//		/** View.post 상태에서만 길이 판단 가능 **/
//		layout.post(new Runnable() {
//			@Override
//			public void run() {
//				Log.d(TAG, "layout width : " + layout.getWidth() + ", layout height : " + layout.getHeight());
//				layout.setPadding(layout.getWidth()/2, 0, 10, layout.getPaddingBottom());
//
//				Handler mHandler = new Handler();
//				mHandler.postDelayed(new Runnable() {
//					public void run(){
//						layout.setVisibility(View.VISIBLE);
//					}
//				}, 10);
//			}
//		});

	
	}

	/**
	 * Sets the view data from a given overlay item.
	 * 
	 * @param item - The overlay item containing the relevant view data 
	 * (title and snippet). 
	 */
	public void setData(OverlayItem item) {

		layout.setVisibility(VISIBLE);
		if (item.getTitle() != null) {
			title.setVisibility(VISIBLE);
			title.setText(item.getTitle());
		} else {
			title.setVisibility(GONE);
		}
		if (item.getSnippet() != null) {
			snippet.setVisibility(VISIBLE);
			snippet.setText(item.getSnippet());
		} else {
			snippet.setVisibility(GONE);
		}

	}

}
