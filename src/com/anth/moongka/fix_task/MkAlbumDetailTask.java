/**
 * MkAlbumDetailTask
 *  �ٹ� ��� �׼�
 */
package com.anth.moongka.fix_task;

import java.util.ArrayList;

import lhy.library.imageviewerpager.Image;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.album.Album;
import com.anth.moongka.album.AlbumDetailActivity;
import com.anth.moongka.function.MkApi;

public class MkAlbumDetailTask extends MkTask {
	private final String TAG = "MK_MainAlbumDetailTask";
	private Context mContext;
	private Activity mActivity;
	
	private boolean isCert = false;
	private String bod_idx;
	
	private ArrayList<Album> albumList = new ArrayList<Album>();

	public MkAlbumDetailTask(String bod_idx, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity) mContext;
		this.bod_idx = bod_idx;
		
	}
	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.albumInPost(bod_idx, albumList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			ArrayList<Image> imageList = new ArrayList<Image>();
			for(int i=0; i<albumList.size(); i++){
				Album curAlbum = albumList.get(i);
				Image curImage = new Image(curAlbum.getUrlOri());
				imageList.add(curImage);
			}
			Intent intent = new Intent(mActivity, AlbumDetailActivity.class);
			intent.putExtra("albumList", albumList);
			intent.putExtra("imageList", imageList);
			intent.putExtra("position", 0);
			mActivity.startActivity(intent);
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

}
