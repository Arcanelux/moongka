/**
 * MkLikeTask
 *  좋아요 액션
 *  param : mem_idx, bod_idx, bod_type, ArrayList<MkPost>, ArrayAdapter
 *  mem_idx는 C.mem_idx로 고정
 *  bod_type은 task호출시 지정, 변동값은 bod_idx
 *  통신 후 ArrayList를 수정해주며, ArrayAdapter에서 notifyDataSetChanged를 호출, Toast도 표시
 */
package com.anth.moongka.fix_task;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class MkLikeTask extends MkTask{
	private final String TAG = "MK_MainLikeTask";
	private Context mContext;
	
	private ArrayList<MkPost> postList;
	private ArrayAdapter mAdapter;
	private int position;
	private boolean isCert = false;
	
	private String mem_idx, bod_idx, bod_type;
	

	public MkLikeTask(String bod_idx, String bod_type, ArrayList<MkPost> postList, ArrayAdapter mAdapter, int position, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.mAdapter = mAdapter;
		this.position = position;
		this.postList = postList;
		this.bod_idx = bod_idx;
		this.bod_type = bod_type;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.likeToggle(C.mem_idx, bod_idx, bod_type);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			int oriNumLike = postList.get(position).getNumLike();
			boolean oriIsLike = postList.get(position).isLike();
			if(oriIsLike){
				postList.get(position).setNumLike(oriNumLike-1);
				postList.get(position).setLike(false);
			} else{
				postList.get(position).setNumLike(oriNumLike+1);
				postList.get(position).setLike(true);
			}
			mAdapter.notifyDataSetChanged();
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

}
