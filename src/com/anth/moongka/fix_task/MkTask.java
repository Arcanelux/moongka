/**
 * MkTask
 *  뭉카 기본 AsyncTask
 *  ProgressDialog 설정을 여기서해준다
 *  param : String msg, boolean isProgressDialog, Context context
 *   msg : ProgressDialog의 Message
 *   isProgressDialog : ProgressDialog의 표출 여부
 */
package com.anth.moongka.fix_task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class MkTask extends AsyncTask<Void, Void, Void>{
	private final String TAG = "MK_MkTask";
	private Context mContext;

	private ProgressDialog mProgressDialog;
	private boolean isProgressDialog = true;
	private String msg;

	public MkTask(String msg, boolean isProgressDialog, Context context){
		mContext= context;
		this.msg = msg;
		this.isProgressDialog = isProgressDialog;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if(isProgressDialog){
			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog.setMessage(msg);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isProgressDialog) mProgressDialog.dismiss();
		//mProgressDialog.dismiss();
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		if(isProgressDialog) mProgressDialog.dismiss();
	}

	@Override
	protected Void doInBackground(Void... params) {
		return null;
	}


}
