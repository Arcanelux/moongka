/**
 * MkScrapTask
 *  스크랩 액션
 *  param : mem_idx, bod_idx
 *  스크랩은 자신만이 하게되므로, mem_idx는  C.mem_idx로 고정
 *  bod_idx만 전달하여 통신 후, 결과값 Toast로 표시
 */
package com.anth.moongka.fix_task;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.MkApi;

public class MkScrapTask extends MkTask{
	private final String TAG = "MK_MkScrapTask";
	private Context mContext;
	
	private boolean isCert = false;
	private String bod_idx;
	private ArrayList<MkPost> postList;
	private ArrayAdapter mAdapter;
	private int position;

	public MkScrapTask(String bod_idx,ArrayList<MkPost> postList,int position, String msg, boolean isProgressDialog, Context context, ArrayAdapter mAdapter) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.mAdapter = mAdapter;
		this.postList = postList;
		this.bod_idx = bod_idx;
		this.position = position;
	}

	public MkScrapTask(String bod_idx, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.bod_idx = bod_idx;
	}
	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.scrap(bod_idx);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			//int oriNumLike = postList.get(position).getNumLike();
			boolean oriISScrap = postList.get(position).isScrap();
			if(oriISScrap){
				//postList.get(position).setNumLike(oriNumLike-1);
				postList.get(position).setScrap(false);
			} else{
				//postList.get(position).setNumLike(oriNumLike+1);
				postList.get(position).setScrap(true);
			}
			mAdapter.notifyDataSetChanged();
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}
}
