/**
 * FindActivity
 *  아이디/비밀번호 찾기 메인
 */
package com.anth.moongka.find;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;

public class FindActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_FindActivity";
	private Context mContext;
	
	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;
	
	private Button btnId, btnPw;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_main);
		
		/** Top View **/
		viewTop = findViewById(R.id.viewTopFindMain);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		
		tvTitle.setText("아이디/비밀번호 찾기");
		btnBack.setOnClickListener(this);
		
		/** Layout **/
		btnId = (Button) findViewById(R.id.btnFindMainID);
		btnPw = (Button) findViewById(R.id.btnFindMainPW);
		btnId.setOnClickListener(this);
		btnPw.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnFindMainID:
			intent = new Intent(FindActivity.this, FindIdActivity.class);
			startActivity(intent);
			break;
		case R.id.btnFindMainPW:
			intent = new Intent(FindActivity.this, FindPwActivity.class);
			startActivity(intent);
			break;
		}
	}

}
