/**
 * FindIdTask
 *  아이디 찾기 액션
 *   결과에 따라 MkDialog 생성
 */
package com.anth.moongka.find;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkDialog;
import com.anth.moongka.function.MkApi;

public class FindIdTask extends MkTask{
	private final String TAG = "MK_FindIdTask";
	private Context mContext;
	private Activity mActivity;
	private boolean isCert = false;
	private MkDialog mDialog;
	
	private String mem_name, mem_question, mem_answer;

	public FindIdTask(String mem_name, String mem_question, String mem_answer, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity) mContext;
		this.mem_name = mem_name;
		this.mem_question = mem_question;
		this.mem_answer = mem_answer;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.findId(mem_name, mem_question, mem_answer);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			mDialog = new MkDialog(mContext);
			mDialog.setTitle("알림");
			mDialog.setContent("회원님의 아이디는\n" + MkApi.returnValue + "\n입니다");
			mDialog.setBtn1("확인");
			mDialog.setBtn1ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();
					mActivity.finish();
				}
			});
			mDialog.setBtn2("비밀번호 찾기");
			mDialog.setBtn2ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();
					Intent intent = new Intent(mActivity, FindPwActivity.class);
					mActivity.startActivity(intent);
					mActivity.finish();
				}
			});
			mDialog.show();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}
	
}
