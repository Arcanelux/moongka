/**
 * FindPwActivity
 *  비밀번호 찾기
 */
package com.anth.moongka.find;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_arrayadapter.MkCodeSpAA;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.function.C;

public class FindPwActivity extends MkActivity implements OnClickListener, OnItemSelectedListener{
	private final String TAG = "MK_FindPwActivity";
	private Context mContext;

	/** Top&Layout View **/
	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;
	private Button btnOK;

	/** EditText & Spinner **/
	private EditText etEmail, etName, etAnswer;
	private Spinner spQuestion;
	private ArrayList<MkCode> questionList;
	private int posQuestion;
	private MkCodeSpAA mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_pw);
		mContext = this;

		/** Top View **/
		viewTop = findViewById(R.id.viewTopFindPw);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);

		tvTitle.setText("비밀번호 찾기");
		btnBack.setOnClickListener(this);

		/** Layout **/
		etEmail = (EditText) findViewById(R.id.etFindPwEmail);
		etName = (EditText) findViewById(R.id.etFindPwName);
		etAnswer = (EditText) findViewById(R.id.etFindPwAnswer);
		btnOK = (Button) findViewById(R.id.btnFindPw);
		btnOK.setOnClickListener(this);

		/** Spinner **/
		spQuestion = (Spinner) findViewById(R.id.spFindPwQuestion);
		spQuestion.setOnItemSelectedListener(this);
		questionList = C.LIST_QUESTION;
		mAdapter = new MkCodeSpAA(mContext, android.R.layout.simple_list_item_1, questionList);
		spQuestion.setAdapter(mAdapter);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnFindPw:
			String email = etEmail.getText().toString();
			String name = etName.getText().toString();
			String answer = etAnswer.getText().toString();
			String questionCode = questionList.get(posQuestion).getCode();
			if(email.length()<=0){
				Toast.makeText(mContext, "이메일을 입력해주세요", Toast.LENGTH_SHORT).show();
			}else if(name.length()<=0){
				Toast.makeText(mContext, "이름을 입력해주세요", Toast.LENGTH_SHORT).show();
			} else if(answer.length()<=0){
				Toast.makeText(mContext, "본인확인 답을 입력해주세요", Toast.LENGTH_SHORT).show();
			} else{
				new FindPwTask(email, name, questionCode, answer, "비밀번호 찾는 중...", true, mContext).execute();
			}
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		switch(parent.getId()){
		case R.id.spFindPwQuestion:
			posQuestion = position;
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) { }


}
