/**
 * CommentActivity
 *  모든 글(컨텐츠피츠, 프로필뷰, 스크랩, 앨범, 앨범뷰...등등)의 댓글 화면은 동일하다고 가정
 *  시작시 이 액티비티를 호출한 곳에서 Intent를 통해 bod_idx, bod_type, position을 받아옴
 *  받아온 정보로 CommentTask(댓글 목록 액션)을 실행, 리스트에 넣어준다
 *  
 *  댓글 등록버튼을 누르면 CommentAddTask(댓글 등록 액션)으로 이동
 *  앨범, 컨텐츠 피드 등, 댓글을 보고 나왔을 때 댓글 수가 달라질 수 있는 액티비티를 위해
 *  finish()에서 setResult에 댓글수와 position을 되돌려준다
 *   댓글 수 : 댓글 수가 추가되었을 때 사용
 *   position : 댓글이 삭제되었을 때 위치를 알려주기위해 사용 
 */
package com.anth.moongka.comment;

import java.util.ArrayList;

import lhy.library.imageviewerpager.Image;
import lhy.library.imageviewerpager.Lhy_Function;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.main.MainMkSinglePostAA;

public class CommentActivity extends MkActivity implements OnClickListener {
	private final String TAG = "MK_CommentActivity";
	private Context mContext;

	private View viewTop;
	private View viewMain;
	private ImageButton btnBack;
	private TextView tvTitle;

	private ListView lvComment;
	private CommentAA mCommentAA;
	private ArrayList<MkComment> commentList = new ArrayList<MkComment>();

	private Button btnSubmit;
	private EditText etComment;

	private String post_idx, post_type;
	private int postPosition;

	private ListView lvMain;
	private static MainMkSinglePostAA mPostAA;
	private static ArrayList<MkPost> postList = new ArrayList<MkPost>();

	private CommentNewAdapter commentNew;
	private ImageDownloader mImageDownloader;
	
	private View view;//상단 피드 내용.
	
	//이미지 예외 변수
	private boolean check_Pic=false;
	private ArrayList<Image> imageList;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comment);
		mContext = this;

		// 탑 셋팅
		viewTop = findViewById(R.id.viewTopComment);

		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("댓글 목록");

		// LayoutInflater inflater =
		// (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		view = getLayoutInflater().inflate(R.layout.main_list_item, null,
				false);
		Intent intent = getIntent();
		post_idx = intent.getStringExtra("post_idx");
		post_type = intent.getStringExtra("post_type");
		postPosition = intent.getIntExtra("postPosition", 0);
		mImageDownloader = new ImageDownloader(mContext);
		//이미지 정보 받아오기.
		
		if(post_type.equals("CPR_ID")){
			check_Pic=true;
			imageList = (ArrayList<Image>) intent.getSerializableExtra("imageList");
			onSetImageSettingView();
		}
		if(!check_Pic){
		new CommentFinalTask(postList, "댓글 로딩중...", true, mContext, post_idx,
				post_type).execute();
		}

		Log.d(TAG, "post_idx : " + post_idx);
		lvComment = (ListView) findViewById(R.id.lvComment);
		mCommentAA = new CommentAA(mContext, R.layout.comment_list_item,
				commentList);

		lvComment.addHeaderView(view);
		lvComment.setAdapter(mCommentAA);
		
		new CommentTask(commentList, mCommentAA, post_idx, post_type,
				"댓글 불러오는 중...", true, mContext).execute();

		btnSubmit = (Button) findViewById(R.id.btnCommentSubmit);
		etComment = (EditText) findViewById(R.id.etComment);

		btnSubmit.setOnClickListener(this);
	}
	public void onSetImageSettingView(){
		ImageView thumbnail = (ImageView) view
				.findViewById(R.id.ivMainItemThumbnail);
		TextView nickName = (TextView) view.findViewById(R.id.tvMainItemName);
		TextView time = (TextView) view.findViewById(R.id.tvMainItemDate);
		TextView content = (TextView) view.findViewById(R.id.tvMainItemContent);
		ImageView iv_content = (ImageView) view
				.findViewById(R.id.ivMainItemPhoto);
		TextView tvTag = (TextView) view.findViewById(R.id.tvMainItemTag);
		TextView tvLike = (TextView) view.findViewById(R.id.tvMainItemLike);
		TextView tvComment = (TextView) view
				.findViewById(R.id.tvMainItemComment);
		ImageView ivTag = (ImageView) view.findViewById(R.id.ibmainTagView);
		ImageView ivTagTitle = (ImageView) view.findViewById(R.id.imageView1);
		ImageButton ibLike = (ImageButton) view.findViewById(R.id.ibMainItemLike);
		ImageButton ibComment = (ImageButton) view.findViewById(R.id.ibMainItemComment);
		ImageButton ibScrap = (ImageButton) view.findViewById(R.id.ibMainItemScrap);
		TextView tvMore = (TextView) view.findViewById(R.id.tvMainMore);
		
		thumbnail.setVisibility(View.GONE);
		nickName.setVisibility(View.GONE);
		time.setVisibility(View.GONE);
		content.setVisibility(View.GONE);
		tvTag.setVisibility(View.GONE);
		tvLike.setVisibility(View.GONE);
		tvComment.setVisibility(View.GONE);
		ivTag.setVisibility(View.GONE);
		ivTagTitle.setVisibility(View.GONE);
		ibLike.setVisibility(View.GONE);
		ibComment.setVisibility(View.GONE);
		ibScrap.setVisibility(View.GONE);
		tvMore.setVisibility(View.GONE);
		
		String urlImg = imageList.get(postPosition).getImageAddress();
		Log.i("CHECKIMAGE",urlImg);
		if (urlImg == null || urlImg.equals("")) {
			Log.d(TAG, "NoImage!");
			iv_content.setVisibility(View.GONE);
		} else {
			iv_content.setVisibility(View.VISIBLE);
			Log.d(TAG, "setting!");
			mImageDownloader.download(
					urlImg.replaceAll("/data/", "/thumb_data/data/"),
					iv_content);
		}

	}
	public void onCreateCommentView(){

		Log.i(TAG,"CREATEVIEW LISTSIZE="+postList.size());
		
		// View v = (View)inflater.inflate(R.layout.list_view_row_mypage, null);
				ImageView thumbnail = (ImageView) view
						.findViewById(R.id.ivMainItemThumbnail);
				TextView nickName = (TextView) view.findViewById(R.id.tvMainItemName);
				TextView time = (TextView) view.findViewById(R.id.tvMainItemDate);
				TextView content = (TextView) view.findViewById(R.id.tvMainItemContent);
				ImageView iv_content = (ImageView) view
						.findViewById(R.id.ivMainItemPhoto);
				TextView tvTag = (TextView) view.findViewById(R.id.tvMainItemTag);
				TextView tvLike = (TextView) view.findViewById(R.id.tvMainItemLike);
				TextView tvComment = (TextView) view
						.findViewById(R.id.tvMainItemComment);
				ImageView ivTag = (ImageView) view.findViewById(R.id.ibmainTagView);
				TextView tvMore = (TextView) view.findViewById(R.id.tvMainMore);
				
				Intent intent = getIntent();
				post_idx = intent.getStringExtra("post_idx");

				post_type = intent.getStringExtra("post_type");
				postPosition = intent.getIntExtra("postPosition", 0);
				int iden = intent.getIntExtra("iden", 0);
				
				if(iden==99){
					
				}else{
					
				}
				
				MkPost curPost = postList.get(0);
				
				String name = curPost.getWriter_name();
				String date = curPost.getStrDate();
				String subContent = curPost.getContent();

				String tag = curPost.getTag();
				int numLike = curPost.getNumLike();
				int numComment = curPost.getNumComment();
				boolean checkLike = curPost.isLike();
				boolean checkScrap = curPost.isScrap();
				String categoryTag = curPost.getCategory_cd();

				if (categoryTag.equals("일반")) {
					ivTag.setImageResource(R.drawable.category_normal);
				}else if(categoryTag.equals("튜닝")){
					ivTag.setImageResource(R.drawable.category_tuning);
				}else if(categoryTag.equals("여행")){
					ivTag.setImageResource(R.drawable.category_travel);
				}else if(categoryTag.equals("구매/시승")){
					ivTag.setImageResource(R.drawable.category_purchase);
				}else if(categoryTag.equals("기타")){
					ivTag.setImageResource(R.drawable.category_etc);
				}
				String urlImg = curPost.getPhoto_url();
				String urlThumbnail = curPost.getPhoto_url_thumbnail();
				Log.i(TAG, "CHECKCEHCK="+urlThumbnail);
				nickName.setText(name);
				time.setText(date);
				content.setText(subContent);

				tvTag.setText(tag);
				tvLike.setText(numLike + "");
				tvComment.setText(numComment + "");

				if (urlImg == null || urlImg.equals("")) {
					Log.d(TAG, "NoImage!");
					iv_content.setVisibility(View.GONE);
				} else {
					iv_content.setVisibility(View.VISIBLE);
					urlImg = C.MK_BASE + urlImg;
					mImageDownloader.download(
							urlImg.replaceAll("/data/", "/thumb_data/data/"),
							iv_content);
				}

				/** 썸네일 이미지 다운로드 **/
				if (urlThumbnail == null || urlThumbnail.equals("")) {
					Log.d(TAG, "NoThumbImage!");
				} else {
					urlThumbnail = C.MK_BASE + urlThumbnail;
					mImageDownloader.download(
							urlThumbnail.replaceAll("/data/", "/thumb_data/data/"),
							thumbnail);
				}
				tvMore.setVisibility(View.GONE);
				ImageButton ibLike = (ImageButton) view
						.findViewById(R.id.ibMainItemLike);
				ImageButton ibComment = (ImageButton) view
						.findViewById(R.id.ibMainItemComment);
				ImageButton ibScrap = (ImageButton) view
						.findViewById(R.id.ibMainItemScrap);
				ImageButton ibDel = (ImageButton) view.findViewById(R.id.ibMainitemDel);
				View[] viewList = { iv_content, thumbnail, nickName, time, ibLike,
						ibComment, ibScrap, iv_content, ibDel };
				Lhy_Function.setViewTag(viewList, 0);
				if (checkLike) {
					ibLike.setImageResource(R.drawable.bt_like);

				} else {
					ibLike.setImageResource(R.drawable.bt_like_gray);
				}
				if (checkScrap) {
					ibScrap.setImageResource(R.drawable.bt_scrap);
				} else {
					ibScrap.setImageResource(R.drawable.bt_scrap_gray);
				}
				
		}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnCommentSubmit:
			String content = etComment.getText().toString();
			new CommentAddTask(post_idx, post_type, content, commentList,
					mCommentAA, etComment, "댓글 작성중...", true, mContext)
					.execute();
			break;
		}
	}

	@Override
	public void finish() {
		Intent intent = new Intent();
		intent.putExtra("commentNum", commentList.size());
		intent.putExtra("postPosition", postPosition);
		setResult(RESULT_OK, intent);
		super.finish();
	}
}
