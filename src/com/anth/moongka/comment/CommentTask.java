/**
 * CommentTask
 *  댓글 불러오기 액션
 *  param : bod_idx, bod_type, ArrayList<MkComment>, ArrayAdapter
 *   통신결과를 ArrayList에 넣어준 후, ArrayAdapter를 통해 notifyDataSetChanged() 호출 
 */
package com.anth.moongka.comment;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.function.MkApi;

public class CommentTask extends MkTask{
	private final String TAG = "MK_MainCommentTask";
	private Context mContext;
	private CommentActivity mActivity;
	private ArrayList<MkComment> commentList;
	private ArrayAdapter mAdapter;

	private String post_idx, post_type;
	private boolean isCert = false;

	public CommentTask(ArrayList<MkComment> commentList, ArrayAdapter mAdapter, String post_idx, String post_type, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (CommentActivity) mContext;
		this.commentList = commentList;
		this.post_idx = post_idx;
		this.post_type = post_type;
		this.mAdapter = mAdapter;
	}

	@Override
	protected Void doInBackground(Void... params) {
		commentList.clear();
		isCert = MkApi.commentList(commentList, post_idx, post_type);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			mAdapter.notifyDataSetChanged();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

}
