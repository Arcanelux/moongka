/**
 * CommentAddTask
 *  댓글 추가 액션
 */
package com.anth.moongka.comment;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkApi;

public class CommentAddTask extends MkTask{
	private final String TAG = "MK_CommentAddTask";
	private Context mContext;
	private Activity mActivity;
	private ArrayList<MkComment> commentList;
	private ArrayAdapter mAdapter;
	private boolean isCert = false;
	
	private EditText etComment;
	
	private String post_idx, post_type, content;

	public CommentAddTask(String post_idx, String post_type, String content, ArrayList<MkComment> commentList, ArrayAdapter mAdapter, EditText etComment, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext= context;
		mActivity = (Activity) mContext;
		this.post_idx = post_idx;
		this.post_type = post_type;
		this.content = content;
		this.commentList = commentList;
		this.mAdapter = mAdapter;
		this.etComment = etComment;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.commentAdd(C.mem_idx, post_idx, post_type, content);
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			new CommentTask(commentList, mAdapter, post_idx, post_type, "댓글 새로고침 중...", true, mContext).execute();
			etComment.setText("");
			Lhy_Function.forceHideKeyboard(mContext, etComment);
		} else{
			Toast.makeText(mContext, "댓글 등록에 실패했습니다", Toast.LENGTH_SHORT).show();
		}
	}
}
