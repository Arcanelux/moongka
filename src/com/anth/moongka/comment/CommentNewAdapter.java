package com.anth.moongka.comment;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.function.Lhy_Function;

public class CommentNewAdapter extends ArrayAdapter<MkPost> implements
		OnClickListener {

	private Context mContext;
	private Activity mActivity;
	private ImageDownloader mImageDownloader;
	
	int id1,id2;

	private ArrayList<MkPost> postList;
	private ArrayList<MkComment> commentList;

	public CommentNewAdapter(Context context, int textViewResourceId1,
			int textViewResourceId2, ArrayList<MkPost> postList, ArrayList<MkComment> commentList) {
		super(context, textViewResourceId1, postList);
		this.postList = postList;
		this.commentList = commentList;
		this.id1 = textViewResourceId1;
		this.id2 = textViewResourceId2;
		mContext = context;
		mActivity = (Activity) mContext;
		mImageDownloader = new ImageDownloader(mContext);
	}

	@Override
	public View getView(int position, View convertView,ViewGroup parent) {
		// TODO Auto-generated method stub
		Log.i("getView", "getview");
		Log.i("getview", ""+position);
		View curView1 = convertView;
		View curView2 = convertView;
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
//		curView1 = inflater.inflate(R.layout.main_list_item, parent);
//		curView2 = inflater.inflate(R.layout.comment_list_item, parent);
		
		curView1 = inflater.inflate(id1, null);
		curView2 = inflater.inflate(id2, null);
		
		if (position == 0) {
			MkPost curPost = postList.get(position);
			if (curPost != null) {
				TextView tvName = (TextView) curView1
						.findViewById(R.id.tvMainItemName);
				TextView tvDate = (TextView) curView1
						.findViewById(R.id.tvMainItemDate);
				TextView tvContent = (TextView) curView1
						.findViewById(R.id.tvMainItemContent);
				TextView tvTag = (TextView) curView1
						.findViewById(R.id.tvMainItemTag);
				TextView tvLike = (TextView) curView1
						.findViewById(R.id.tvMainItemLike);
				TextView tvComment = (TextView) curView1
						.findViewById(R.id.tvMainItemComment);
				ImageView ivPhoto = (ImageView) curView1
						.findViewById(R.id.ivMainItemPhoto);
				ImageView ivThumbnail = (ImageView) curView1
						.findViewById(R.id.ivMainItemThumbnail);

				String name = curPost.getWriter_name();
				String date = curPost.getStrDate();
				String content = curPost.getContent();

				String tag = curPost.getTag();
				int numLike = curPost.getNumLike();
				int numComment = curPost.getNumComment();
				boolean checkLike = curPost.isLike();
				boolean checkScrap = curPost.isScrap();

				String urlImg = curPost.getPhoto_url();
				// Log.d(TAG, "urlImg : " + urlImg);
				String urlThumbnail = curPost.getPhoto_url_thumbnail();
				// Log.d(TAG, "urlThumbnail : " + urlThumbnail);

				tvName.setText(name);
				tvDate.setText(date);
				tvContent.setText(content);

				tvTag.setText(tag);
				tvLike.setText(numLike + "");
				tvComment.setText(numComment + "");

				/** 게시물 이미지 다운로드 **/
				if (urlImg == null || urlImg.equals("")) {

					ivPhoto.setVisibility(View.GONE);
				} else {
					ivPhoto.setVisibility(View.VISIBLE);
					urlImg = C.MK_BASE + urlImg;
					mImageDownloader.download(
							urlImg.replaceAll("/data/", "/thumb_data/data/"),
							ivPhoto);
				}

				/** 썸네일 이미지 다운로드 **/
				if (urlThumbnail == null || urlThumbnail.equals("")) {

				} else {
					urlThumbnail = C.MK_BASE + urlThumbnail;
					mImageDownloader.download(urlThumbnail.replaceAll("/data/",
							"/thumb_data/data/"), ivThumbnail);
				}

				ImageButton ibLike = (ImageButton) curView1
						.findViewById(R.id.ibMainItemLike);
				ImageButton ibComment = (ImageButton) curView1
						.findViewById(R.id.ibMainItemComment);
				ImageButton ibScrap = (ImageButton) curView1
						.findViewById(R.id.ibMainItemScrap);
				View[] viewList = { ivPhoto, ivThumbnail, tvName, tvDate,
						ibLike, ibComment, ibScrap, ivPhoto };
				Lhy_Function.setViewTag(viewList, position);
				if (checkLike) {
					ibLike.setImageResource(R.drawable.bt_like);

				} else {
					ibLike.setImageResource(R.drawable.bt_like_gray);
				}

				if (checkScrap) {
					ibScrap.setImageResource(R.drawable.bt_scrap);
				} else {
					ibScrap.setImageResource(R.drawable.bt_scrap_gray);
				}
				ivPhoto.setOnClickListener(this);
				ivThumbnail.setOnClickListener(this);
				tvName.setOnClickListener(this);
				tvDate.setOnClickListener(this);

				ibLike.setOnClickListener(this);
				ibComment.setOnClickListener(this);
				ibScrap.setOnClickListener(this);
				//parent.addView(curView1);
				return curView1;
			}
		}else if(position>0){
			Log.i("getview", "position>0");
			MkComment curComment = commentList.get(position);
			ImageButton ibPhoto = (ImageButton) curView2.findViewById(R.id.ibCommentItemPhoto);
			
			
			ImageButton ibDel = (ImageButton) curView2.findViewById(R.id.ibCommentItemDel);
			ibDel.setOnClickListener(this);
			/** 자신의 댓글일경우, 삭제버튼 표출 **/
			if(commentList.get(position).getWriter_idx().equals(C.mem_idx)){
				ibDel.setVisibility(View.VISIBLE);
			}
			
			TextView tvName = (TextView) curView2.findViewById(R.id.tvCommentItemName);
			TextView tvContent = (TextView) curView2.findViewById(R.id.tvCommentItemContent);
			TextView tvDate = (TextView) curView2.findViewById(R.id.tvCommentItemDate);
			
			String name = curComment.getWriter_name();
			String content = curComment.getContent();
			String strDate = curComment.getStrDate();
			String photo_url_thumbnail = C.MK_BASE + curComment.getPhoto_url_thumbnail();
			
			mImageDownloader.download(photo_url_thumbnail.replaceAll("/data/", "/thumb_data/data/"), ibPhoto);
			tvName.setText(name);
			tvContent.setText(content);
			tvDate.setText(strDate);
			
			View viewList[] = new View[] { ibPhoto, ibDel, tvName, tvContent };
			Lhy_Function.setViewTag(viewList, position);
			//parent.addView(curView2);
			return curView2;	
			
		}
		//return curView1;
		return curView2;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return super.getCount();
	}

}
