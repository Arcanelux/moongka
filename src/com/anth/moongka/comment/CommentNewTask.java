package com.anth.moongka.comment;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.MkApi;

public class CommentNewTask extends MkTask {

	private Context mContext;
	private CommentActivity mActivity;
	private ArrayList<MkPost> postList;
	private ArrayList<MkComment> commentList;
	private ArrayAdapter mAdapter;

	private String post_idx, post_type, keyid;
	private boolean isCert = false;
	private boolean isCert2 = false;

	public CommentNewTask(ArrayList<MkPost> postList,
			ArrayList<MkComment> commentList, ArrayAdapter mAdapter,
			String msg, boolean isPdialog, Context context, String post_idx,
			String post_type) {
		super(msg, isPdialog, context);
		mContext = context;
		mActivity = (CommentActivity) mContext;
		this.postList = postList;
		this.commentList = commentList;
		this.post_idx = post_idx;
		this.post_type = post_type;
		this.mAdapter = mAdapter;
		this.keyid = post_idx;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
	}

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		isCert = MkApi.postGCMList(keyid, postList);
		isCert2 = MkApi.commentList(commentList, post_idx, post_type);

		return super.doInBackground(params);
	}

}