package com.anth.moongka.comment;

import java.util.ArrayList;

import android.content.Context;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.MkApi;

public class CommentFinalTask extends MkTask {

	private Context mContext;
	private CommentActivity mActivity;
	private ArrayList<MkPost> postList;
	private ArrayList<MkComment> commentList;

	private String post_idx, post_type, keyid;
	private boolean isCert = false;
	private boolean isCert2 = false;
	
	public CommentFinalTask(ArrayList<MkPost> postList,
			 String msg, boolean isPdialog, Context context, String post_idx,
			String post_type) {
		super(msg, isPdialog, context);
		mContext = context;
		mActivity = (CommentActivity) mContext;
		this.postList = postList;

		this.post_idx = post_idx;
		this.post_type = post_type;
		this.keyid = post_idx;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		mActivity.onCreateCommentView();
		super.onPostExecute(result);
	}

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		isCert = MkApi.postGCMList(keyid, postList);
		return super.doInBackground(params);
	}

}