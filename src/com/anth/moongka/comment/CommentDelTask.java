/**
 * CommentTask
 *  댓글 삭제 액션
 *  param : re_idx(댓글 번호), bod_idx(글 번호), bod_type(글 타입), ArrayList<MkComment>, position, ArrayAdapter
 *   통신이 이루어지면 ArrayList에서 호출한 댓글을 삭제해준 후, ArrayAdapter를 통해 notifyDataSetChanged() 호출 
 */
package com.anth.moongka.comment;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.function.MkApi;

public class CommentDelTask extends MkTask{
	private final String TAG = "MK_CommentDelTask";
	private Context mContext;
	private CommentActivity mActivity;

	private ArrayList<MkComment> commentList;
	private ArrayAdapter mAdapter;
	private boolean isCert = false;
	
	private String re_idx, bod_idx, bod_type;
	private int position;

	public CommentDelTask(String re_idx, String bod_idx, String bod_type, ArrayList<MkComment> commentList, int position, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (CommentActivity) mContext;
		this.commentList = commentList;
		this.re_idx = re_idx;
		this.bod_idx = bod_idx;
		this.bod_type = bod_type;
		this.position = position;
		this.mAdapter = mAdapter;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.commentDelete(re_idx, bod_idx, bod_type);
		return super.doInBackground(params);
	}
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
			commentList.remove(position);
			mAdapter.notifyDataSetChanged();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}



}
