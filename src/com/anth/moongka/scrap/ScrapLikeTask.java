/**
 * MainLikeTask
 *  스크랩 좋아요 액션
 *  스크랩 전체는 Main을 Copy&Paste한 형태
 *  시간날때 MainActivity에 분기로 포함시켜야함
 */
package com.anth.moongka.scrap;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class ScrapLikeTask extends MkTask{
	private final String TAG = "MK_MainLikeTask";
	private Context mContext;
	private ScrapActivity mActivity;
	
	private ArrayList<MkPost> postList;
	private ArrayAdapter mAdapter;
	private int position;
	private boolean isCert = false;
	
	private String mem_idx, bod_idx, bod_type;
	

	public ScrapLikeTask(String bod_idx, String bod_type, ArrayList<MkPost> postList, ArrayAdapter mAdapter, int position, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (ScrapActivity) mContext;
		this.position = position;
		this.postList = postList;
		this.mAdapter = mAdapter;
		this.bod_idx = bod_idx;
		this.bod_type = bod_type;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.likeToggle(C.mem_idx, bod_idx, bod_type);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			int oriNumLike = postList.get(position).getNumLike();
			boolean oriIsLike = postList.get(position).isLike();
			if(oriIsLike){
				postList.get(position).setNumLike(oriNumLike-1);
				postList.get(position).setLike(false);
			} else{
				postList.get(position).setNumLike(oriNumLike+1);
				postList.get(position).setLike(true);
			}
			mAdapter.notifyDataSetChanged();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

}
