/**
 * Author : Lee HanYeong
 * File Name : CommentAA.java
 * Created Date : 2012. 12. 2.
 * Description
 */
package com.anth.moongka.scrap;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.comment.CommentDelTask;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.ImageDownloader;

public class ScrapMkCommentAA extends ArrayAdapter<MkComment> implements OnClickListener{
	private final String TAG = "MK_CommentArrayAdapter";
	private Context mContext;
	private ArrayList<MkComment> commentList;
	private ImageDownloader mImageDownloader;

	public ScrapMkCommentAA(Context context, int textViewResourceId, ArrayList<MkComment> commentList) {
		super(context, textViewResourceId, commentList);
		mContext = context;
		this.commentList = commentList;
		mImageDownloader = new ImageDownloader(mContext);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.comment_list_item, null);
		}
		
		MkComment curComment = commentList.get(position);
		ImageButton ibPhoto = (ImageButton) curView.findViewById(R.id.ibCommentItemPhoto);
		
		
		ImageButton ibDel = (ImageButton) curView.findViewById(R.id.ibCommentItemDel);
		ibDel.setOnClickListener(this);
		if(commentList.get(position).getWriter_idx().equals(C.mem_idx)){
			ibDel.setVisibility(View.VISIBLE);
		}
		
		TextView tvName = (TextView) curView.findViewById(R.id.tvCommentItemName);
		TextView tvContent = (TextView) curView.findViewById(R.id.tvCommentItemContent);
		
		String name = curComment.getWriter_name();
		String content = curComment.getContent();
		String photo_url_thumbnail = C.MK_BASE + curComment.getPhoto_url_thumbnail();
		
		mImageDownloader.download(photo_url_thumbnail.replaceAll("/data/", "/thumb_data/data/"), ibPhoto);
		tvName.setText(name);
		tvContent.setText(content);
		
		View viewList[] = new View[] { ibPhoto, ibDel, tvName, tvContent };
		Lhy_Function.setViewTag(viewList, position);
		
		return curView;
	}

	@Override
	public void onClick(View v) {
		int position;
		switch(v.getId()){
		case R.id.ibCommentItemDel:
			position = (Integer) v.getTag();
			MkComment curComment = commentList.get(position);
			String re_idx = curComment.getComment_idx();
			String bod_idx = curComment.getPost_idx();
			String bod_type = "CFB_ID";
			new CommentDelTask(re_idx, bod_idx, bod_type, commentList, position, this, "��� ������...", true, mContext).execute();
			break;
		}
	}

}
