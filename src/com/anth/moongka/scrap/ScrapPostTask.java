package com.anth.moongka.scrap;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.MkApi;

public class ScrapPostTask extends MkTask{
	private final String TAG = "MK_MainPostTask";
	private Context mContext;
	private ScrapActivity mActivity;

	private ArrayList<MkPost> postList;
	private ArrayAdapter mAdapter;
	private String mem_idx, writer_idx;
	private int page_num;
	private boolean isCert = false;
	private boolean isRefresh = false;

	public ScrapPostTask(ArrayList<MkPost> postList, ArrayAdapter mAdapter, String mem_idx, String writer_idx, int page_num, String msg, boolean isPdialog, Context context) {
		super(msg, isPdialog, context);
		mContext = context;
		this.postList = postList;
		this.mAdapter = mAdapter;
		this.mem_idx = mem_idx;
		this.writer_idx = writer_idx;
		this.page_num = page_num;
		mActivity = (ScrapActivity) mContext;
	}

	public ScrapPostTask(ArrayList<MkPost> postList, ArrayAdapter mAdapter, String mem_idx, String writer_idx, int page_num, String msg, boolean isPdialog, boolean isRefresh, Context context) {
		super(msg, isPdialog, context);
		mContext = context;
		this.postList = postList;
		this.mAdapter = mAdapter;
		this.mem_idx = mem_idx;
		this.writer_idx = writer_idx;
		this.page_num = page_num;
		this.isRefresh = isRefresh;
		mActivity = (ScrapActivity) mContext;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mActivity.isAdding = true;
		Log.d(TAG, "MainPost Add Start. page_num : " + mActivity.page_num);
	}


	@Override
	protected Void doInBackground(Void... params) {
		Log.d(TAG, "PageNum : " + page_num);
		isCert = MkApi.scrapList(postList, mem_idx, writer_idx, page_num);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mActivity.page_num += 1;
		mActivity.isAdding = false;
		if(isCert){
			mAdapter.notifyDataSetChanged();
			if(isRefresh) mActivity.completeRefresh();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
			LinearLayout scrapMain = (LinearLayout) mActivity.findViewById(R.id.scrapMain);
			scrapMain.setBackgroundResource(R.drawable.img_data_android);
		}
	}



}
