/**
 * Author : Lee HanYeong
 * File Name : MainActivity.java
 * Created Date : 2012. 11. 23.
 * Description
 */
package com.anth.moongka.scrap;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.PullToRefreshView;

public class ScrapActivity extends MkActivity implements OnItemClickListener, OnClickListener{
	private final String TAG = "MK_MainActivity";
	private Context mContext;

	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;

	private ListView lvMain;
	private PullToRefreshView pullView = null;
	private ArrayList<MkPost> postList = new ArrayList<MkPost>();
	private ScrapMkPostAA mPostAA;

	public int page_num = 1;
	public boolean isAdding = false;

	/** ContentFeed UI **/
	private ImageButton ibLike;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.scrap);

		viewTop = findViewById(R.id.viewTopScrap);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("스크랩"); 

		lvMain = (ListView) findViewById(R.id.lvMain);
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View footer = inflater.inflate(R.layout.main_list_footer, null);
		lvMain.addFooterView(footer);

		mPostAA = new ScrapMkPostAA(mContext, R.layout.main_list_item, postList);
		lvMain.setAdapter(mPostAA);
		
		new ScrapPostTask(postList, mPostAA, C.mem_idx, "", 1, "포스트 로딩중..", true, mContext).execute();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

	}


	public void addPost(){
		if(!isAdding) new ScrapPostTask(postList, mPostAA, C.mem_idx, "", page_num, "포스트 로딩중..", false, mContext).execute();
	}

	public void completeRefresh(){
		pullView.completeRefresh();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Lhy_Function.clearCache(mContext);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode){
		case 2:
			int commentNum = data.getIntExtra("commentNum", 0);
			int postPosition = data.getIntExtra("postPosition", 0);
			postList.get(postPosition).setNumComment(commentNum);
			mPostAA.notifyDataSetChanged();
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;	
		}

	}



}
