/**
 * WritePlaceActivity
 * 글쓰기 - 장소선택
 */
package com.anth.moongka.write;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkPlaceNew;
import com.anth.moongka.function.C;

public class WritePlaceActivity extends MkActivity implements OnClickListener, OnItemClickListener{
	private final String TAG = "MK_WritePlaceActivity";
	private Context mContext;

	private ListView lvPlace;
	private ArrayList<MkPlaceNew> placeList = new ArrayList<MkPlaceNew>();
	private WriteMkPlaceNewAA mAdapter;

	/** WritePopupTop **/
	private View viewTop;
	private TextView tvTitle;
	private Button btnClose, btnSelect;

	private EditText etSearch;
	private ImageButton btnSearch;
	private WebView wvWritePlace;

	private static boolean isAfterSearch = false;
	private String strFinalSearch = "";
	private boolean isSearched=false;


	/** Location **/
	//	private boolean isFirst = true;
	//	LocationManager locManager;
	//	Geocoder geoCoder;
	//	Location myLocation;
	//	double latPoint;
	//	double lngPoint;
	//	float speed;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write_place);
		mContext = this;

		wvWritePlace = (WebView) findViewById(R.id.wvWritePlace);

		//		// LocationListener 핸들
		//		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		//		// GPS로 부터 위치 정보를 업데이트 요청
		//		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		//		// 기지국으로부터 위치 정보를 업데이트 요청
		//		locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
		//		// 주소를 가져오기 위해 설정 - KOREA, KOREAN 모두 가능 
		//		geoCoder = new Geocoder(this, Locale.KOREA);

		/** PopupTop View **/
		viewTop = findViewById(R.id.viewWritePlaceTop);
		btnClose = (Button) viewTop.findViewById(R.id.btnWrite1);
		btnSelect = (Button) viewTop.findViewById(R.id.btnWrite2);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvWriteTitle);
		tvTitle.setText("장소");
		btnClose.setOnClickListener(this);
		btnSelect.setVisibility(View.GONE);

		lvPlace = (ListView) findViewById(R.id.lvWritePlace);
		mAdapter = new WriteMkPlaceNewAA(mContext, android.R.layout.simple_list_item_1, placeList);
		lvPlace.setAdapter(mAdapter);
		lvPlace.setOnItemClickListener(this);

		etSearch = (EditText) findViewById(R.id.etWritePlaceSearch);
		btnSearch = (ImageButton) findViewById(R.id.btnWritePlaceSearch);
		btnSearch.setOnClickListener(this);

		new WritePlaceNewTask("", C.myLocation.getLatitude(), C.myLocation.getLongitude(), placeList, mAdapter, wvWritePlace, "장소정보 불러오는 중...", true, mContext).execute();


	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnWrite1:
			finish();
			break;
		case R.id.btnWritePlaceSearch:
			String search = etSearch.getText().toString();
			strFinalSearch = search;
			isSearched=true;
			if(search.equals("") || search==null){
				Toast.makeText(mContext, "검색어를 입력해주세요", Toast.LENGTH_SHORT).show();
			} else{
				new WritePlaceNewSearchTask(search, C.myLocation.getLatitude(), C.myLocation.getLongitude(), placeList, mAdapter, wvWritePlace, etSearch, "검색중...", true, mContext).execute();
			}
			break;
		}
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		switch(parent.getId()){
		case R.id.lvWritePlace:
			if((position==placeList.size()-1)&&isSearched){
				new WritePlaceAddTask(strFinalSearch, "장소 등록중...", true, mContext).execute();
			} else{
				Intent intent = new Intent();
				intent.putExtra("bpi_nm", placeList.get(position).getBPI_NM());
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}

	public static void setSearchAfter(){
		isAfterSearch = true;
	}

	//	@Override
	//	public void onLocationChanged(Location location) {
	////		C.myLocation = location;
	////		if(isFirst){
	////			
	////			isFirst = false;
	////		}
	//	}
	//
	//	@Override
	//	public void onProviderDisabled(String provider) {
	//		// TODO Auto-generated method stub
	//		
	//	}
	//
	//	@Override
	//	public void onProviderEnabled(String provider) {
	//		// TODO Auto-generated method stub
	//		
	//	}
	//
	//	@Override
	//	public void onStatusChanged(String provider, int status, Bundle extras) {
	//		// TODO Auto-generated method stub
	//		
	//	}


}
