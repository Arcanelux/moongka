/**
 * WritePublicActivity
 * 글쓰기 - 정보공개설정
 */
package com.anth.moongka.write;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.function.C;

public class WritePublicActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_WritePublicActivity";
	private Context mContext;
	
	private View viewTop, viewBtn1, viewBtn2, viewBtn3;
	private ImageButton btnBack;
	private TextView tvTitle, tvView1, tvView2, tvView3;
	private ArrayList<MkCode> publicList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write_public);
		mContext = this;
		publicList = C.LIST_ACCESS;
		
		
		viewTop = findViewById(R.id.viewTopWritePublic);
		viewBtn1 = findViewById(R.id.viewWritePublicBtn1);
		viewBtn2 = findViewById(R.id.viewWritePublicBtn2);
		viewBtn3 = findViewById(R.id.viewWritePublicBtn3);
		
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
//		btnCancel.setOnClickListener(this);
//		btnPost.setOnClickListener(this);
		
		tvView1 = (TextView) viewBtn1.findViewById(R.id.tvWritePublicItem);
		tvView2 = (TextView) viewBtn2.findViewById(R.id.tvWritePublicItem);
		tvView3 = (TextView) viewBtn3.findViewById(R.id.tvWritePublicItem);
		
		tvTitle.setText("공개여부 선택");
		tvView1.setText(publicList.get(0).getName());
		tvView2.setText(publicList.get(1).getName());
		tvView3.setText(publicList.get(2).getName());
		
		viewBtn1.setOnClickListener(this);
		viewBtn2.setOnClickListener(this);
		viewBtn3.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		String msg = "";
		switch(v.getId()){
//		case R.id.btnWrite1:
//			finish();
//			break;
//		case R.id.btnWrite2:
//			
//			break;
		case R.id.btnCert1:
			finish();
			break;
		case R.id.viewWritePublicBtn1:
//			C.CUR_PUBLIC = publicList.get(0);
			C.CUR_PUBLIC = new MkCode("전체공개", "O101");
			msg = publicList.get(0).getName();
			Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
			finish();
			break;
		case R.id.viewWritePublicBtn2:
//			C.CUR_PUBLIC = publicList.get(1);
			C.CUR_PUBLIC = new MkCode("abc", "O102");
			msg = publicList.get(1).getName();
			Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
			finish();
			break;
		case R.id.viewWritePublicBtn3:
//			C.CUR_PUBLIC = publicList.get(2);
			C.CUR_PUBLIC = new MkCode("abc", "O103");
			msg = publicList.get(2).getName();
			Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
			finish();
			break;
		}
	}

}
