/**
 * WriteFriendActivity
 * 글쓰기 - 친구선택
 */
package com.anth.moongka.write;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.function.C;

public class WriteFriendActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_WriteFriendActivity";
	private Context mContext;

	/** WritePopupTop **/
	private View viewTop;
	private TextView tvTitle;
	private Button btnClose;
	private Button btnApply;

	private ListView lvFriend;
	private WriteFriendArrayAdapter mFriendAA;

	private ArrayList<MkFriend> friendList;
	private WriteFriendArrayAdapter mAdapter;
	private OnClickListener acceptBtnListener;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write_friend);
		mContext = this;

		/** PopupTop View **/
		viewTop = findViewById(R.id.viewWritePopupTop);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvWriteTitle);
		btnClose = (Button) viewTop.findViewById(R.id.btnWrite1);
		btnApply = (Button) viewTop.findViewById(R.id.btnWrite2);
		btnApply.setText("적용");

		tvTitle.setText("친구 선택");
		btnClose.setOnClickListener(this);
		btnApply.setOnClickListener(this);


		Intent intent = getIntent();
		try{
			friendList = (ArrayList<MkFriend>) intent.getSerializableExtra("friendList");
		} catch(Exception e){
			e.printStackTrace();
		}
		if(friendList==null || friendList.size()==0){
			friendList = new ArrayList<MkFriend>();
		}

		lvFriend = (ListView) findViewById(R.id.lvWriteFriend);
		mFriendAA = new WriteFriendArrayAdapter(mContext, R.layout.write_friend_list_item, friendList);
		lvFriend.setAdapter(mFriendAA);
		
		new WriteFriendTask(C.mem_idx, friendList, mFriendAA, "친구목록 로딩중...", true, mContext).execute();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnWrite1:
			finish();
			break;
		case R.id.btnWrite2:
			Intent intent = getIntent();
			intent.putExtra("friendList", friendList);
			setResult(RESULT_OK, intent);
			finish();
			break;
		}
	}
}
