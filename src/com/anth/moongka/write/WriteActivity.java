/**
 * WriteActivity
 * 글쓰기
 */
package com.anth.moongka.write;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.fix_value.MkDialog2;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.network.HttpPostMultiPart;
import com.anth.moongka.network.ImageData;
import com.anth.moongka.network.StringData;

public class WriteActivity extends MkActivity implements OnClickListener,
		OnCheckedChangeListener {
	private final String TAG = "MK_WriteActivity";
	private Context mContext;
	private final int TO_WRITE_PHOTO_CAM = 1099;
	private final int TO_WRITE_PHOTO_GAL = 1100;
	private final int TO_WRITE_FRIEND = 1101;
	private final int TO_WRITE_PLACE = 1102;

	private View viewTop;
	private TextView tvTitle;
	private Button btnCancel, btnPost;
	private Intent intentPhoto;
	private MkDialog2 mDialog;

	private LinearLayout llKeyboardZone;
	private View viewKeyboardZone;
	private ToggleButton tbNormal, tbTuning, tbTravel, tbReview, tbInsurance,
			tbOther;
	private ToggleButton[] tbList;
	private Button btnFriend, btnPhoto, btnPlace, btnPublic;
	private EditText etContent, etTag;
	private ImageView ivThumbnail1, ivThumbnail2, ivThumbnail3, ivThumbnail4,
			ivThumbnail5;
	private TextView tvAddFriend, tvAddPlace;

	private static ArrayList<Uri> imagePathList = new ArrayList<Uri>();
	private HttpPostMultiPart mHttpPostMultiPart;
	private MkCode curCategory;
	private Uri curUri; // 각 ImageView에 넣을 Uri
	private static Uri uriTemp; // 카메라에서 받아올 파일 임시 Uri
	private static String strPlace;
	private static String strFriend;

	/** FriendList **/
	private ArrayList<MkFriend> friendList;

	/** Place **/
	private String bpi_nm;
	private String place_nm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write);
		mContext = this;
		mHttpPostMultiPart = new HttpPostMultiPart(C.API_WRITE, mContext);

		/** Top View **/
		viewTop = findViewById(R.id.viewTopWrite);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvWriteTitle);
		btnCancel = (Button) viewTop.findViewById(R.id.btnWrite1);
		btnPost = (Button) viewTop.findViewById(R.id.btnWrite2);

		tvTitle.setText("글 작성");
		btnCancel.setOnClickListener(this);
		btnPost.setOnClickListener(this);

		/** 키보드나오는 영역, EditText, Thumbnail **/
		// llKeyboardZone = (LinearLayout)
		// findViewById(R.id.llWriteKeyboardZone);
		// llKeyboardZone.setOnClickListener(this);
		viewKeyboardZone = findViewById(R.id.viewWriteKeyboardZone);
		viewKeyboardZone.setOnClickListener(this);
		etContent = (EditText) findViewById(R.id.etWriteContent);
		etTag = (EditText) findViewById(R.id.etWriteTag);
		ivThumbnail1 = (ImageView) findViewById(R.id.ivWriteImage1);
		ivThumbnail2 = (ImageView) findViewById(R.id.ivWriteImage2);
		ivThumbnail3 = (ImageView) findViewById(R.id.ivWriteImage3);
		ivThumbnail4 = (ImageView) findViewById(R.id.ivWriteImage4);
		ivThumbnail5 = (ImageView) findViewById(R.id.ivWriteImage5);
		tvAddFriend = (TextView) findViewById(R.id.tvWriteAddFriendList);
		tvAddPlace = (TextView) findViewById(R.id.tvWriteAddPlace);

		/** Btn&TglBtn Connect **/
		btnFriend = (Button) findViewById(R.id.btnWriteFriend);
		btnPhoto = (Button) findViewById(R.id.btnWritePhoto);
		btnPlace = (Button) findViewById(R.id.btnWritePlace);
		btnPublic = (Button) findViewById(R.id.btnWritePublic);

		tbNormal = (ToggleButton) findViewById(R.id.tbWriteNormal);
		tbTuning = (ToggleButton) findViewById(R.id.tbWriteTuning);
		tbTravel = (ToggleButton) findViewById(R.id.tbWriteTravel);
		tbReview = (ToggleButton) findViewById(R.id.tbWriteReview);
		tbInsurance = (ToggleButton) findViewById(R.id.tbWriteInsurance);
		tbOther = (ToggleButton) findViewById(R.id.tbWriteOther);

		/** Btn setOnclickListener **/
		btnFriend.setOnClickListener(this);
		btnPhoto.setOnClickListener(this);
		btnPlace.setOnClickListener(this);
		btnPublic.setOnClickListener(this);

		tbNormal.setOnCheckedChangeListener(this);
		tbTuning.setOnCheckedChangeListener(this);
		tbTravel.setOnCheckedChangeListener(this);
		tbReview.setOnCheckedChangeListener(this);
		tbInsurance.setOnCheckedChangeListener(this);
		tbOther.setOnCheckedChangeListener(this);

		tbList = new ToggleButton[] { tbNormal, tbTuning, tbTravel, tbReview,
				tbInsurance, tbOther };
		tbNormal.setChecked(true);

		ArrayList<ImageView> ivList = new ArrayList<ImageView>();
		ivList.add(ivThumbnail1);
		ivList.add(ivThumbnail2);
		ivList.add(ivThumbnail3);
		ivList.add(ivThumbnail4);
		ivList.add(ivThumbnail5);

		// for(ImageView curiv : ivList){
		// curiv.setVisibility(View.VISIBLE);
		// curiv.setBackgroundColor(Color.RED);
		// }

		// for(ImageView curiv : ivList){
		// curiv.setVisibility(View.VISIBLE);
		// curiv.setBackgroundColor(Color.RED);
		// }

		switch (imagePathList.size()) {
		case 5:
			curUri = imagePathList.get(4);
			Log.d(TAG, "curUri : " + curUri);
			C.setImageThumbnail(curUri, ivThumbnail5);
			ivThumbnail5.setVisibility(View.VISIBLE);
		case 4:
			curUri = imagePathList.get(3);
			Log.d(TAG, "curUri : " + curUri);
			C.setImageThumbnail(curUri, ivThumbnail4);
			ivThumbnail4.setVisibility(View.VISIBLE);
		case 3:
			curUri = imagePathList.get(2);
			Log.d(TAG, "curUri : " + curUri);
			C.setImageThumbnail(curUri, ivThumbnail3);
			ivThumbnail3.setVisibility(View.VISIBLE);
		case 2:
			curUri = imagePathList.get(1);
			Log.d(TAG, "curUri : " + curUri);
			C.setImageThumbnail(curUri, ivThumbnail2);
			ivThumbnail2.setVisibility(View.VISIBLE);
		case 1:
			curUri = imagePathList.get(0);
			Log.d(TAG, "curUri : " + curUri);
			C.setImageThumbnail(curUri, ivThumbnail1);
			ivThumbnail1.setVisibility(View.VISIBLE);
		}

		tvAddPlace.setText(strPlace);
		tvAddFriend.setText(strFriend);
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		/** Top View **/
		case R.id.btnWrite1:
			Lhy_Function.forceHideKeyboard(mContext, etContent);
			finish();
			break;
		case R.id.btnWrite2:
			/** 이미지 추가 (ImageData) **/
			Lhy_Function.forceHideKeyboard(mContext, etContent);
			for (int i = 0; i < imagePathList.size(); i++) {
				Uri curImageUri = imagePathList.get(i);
				String imageParamName = "photo[" + i + "]";
				ImageData curImageData = new ImageData("photo" + i + ".jpg",
						imageParamName, curImageUri);
				mHttpPostMultiPart.addImageData(curImageData);
			}
			if (C.CUR_PUBLIC.getCode().equals("C101")) {
				C.CUR_PUBLIC = new MkCode("전체공개", "O101");
			}
			if (curCategory == null) {
				curCategory = new MkCode("일반", "C101");
			}

			String content = etContent.getText().toString() + "\n";
			if (content.equals("" + "\n")) {
				Toast.makeText(getApplicationContext(), "글이 작성되지 않았습니다.",
						Toast.LENGTH_SHORT).show();
				break;
			}
			if (tvAddFriend.getText().toString().length() > 0)
				content += tvAddFriend.getText().toString() + "\n";
			if (tvAddPlace.getText().toString().length() > 0)
				content += tvAddPlace.getText().toString();
			mHttpPostMultiPart
					.addStringData(new StringData("bod_txt", content));
			mHttpPostMultiPart.addStringData(new StringData("mem_idx",
					C.mem_idx));
			mHttpPostMultiPart
					.addStringData(new StringData("mem_id", C.mem_id));
			mHttpPostMultiPart.addStringData(new StringData("bod_pub_cd",
					C.CUR_PUBLIC.getCode()));
			mHttpPostMultiPart.addStringData(new StringData("bod_ctgy_cd",
					curCategory.getCode()));
			if (!("".equals(bpi_nm)) && bpi_nm != null) {
				mHttpPostMultiPart.addStringData(new StringData("bpi_nm",
						bpi_nm));
			}

			String tag = etTag.getText().toString();
			if (!tag.equals("")) {
				mHttpPostMultiPart.addStringData(new StringData("tag", tag));
			}

			new WriteTask(mHttpPostMultiPart, mContext).execute();

			break;

		/** Layout **/
		case R.id.btnWriteFriend:
			/** 글에 친구 추가 **/
			Lhy_Function.forceHideKeyboard(mContext, etContent);
			intent = new Intent(WriteActivity.this, WriteFriendActivity.class);
			if (friendList != null) {
				intent.putExtra("friendList", friendList);
			}
			startActivityForResult(intent, TO_WRITE_FRIEND);
			break;
		case R.id.btnWritePhoto:
			/** 글에 사진 추가 **/
			mDialog = new MkDialog2(mContext);
			mDialog.setContent("사진등록");
			mDialog.setBtn1("카메라에서 찍기");
			mDialog.setBtn1ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Lhy_Function.forceHideKeyboard(mContext, etContent);
					// 임시로 사용할 파일 생성
					// int curListSize = imagePathList.size();
					// String tempFileName = "mkTemp" + curListSize + ".jpg";
					// File tempFile = new
					// File(Environment.getExternalStorageDirectory(),
					// tempFileName);
					// uriTemp = Uri.fromFile(tempFile);
					//
					// // 카메라를 호출합니다.
					// intentPhoto = new
					// Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					// intentPhoto.putExtra(MediaStore.EXTRA_OUTPUT, uriTemp);
					// startActivityForResult(intentPhoto,TO_WRITE_PHOTO_CAM);

					intentPhoto = new Intent(WriteActivity.this,
							WriteGetPhotoActivity.class);
					intentPhoto.putExtra("curListSize", imagePathList.size());
					startActivityForResult(intentPhoto, TO_WRITE_PHOTO_CAM);
					mDialog.dismiss();
				}
			});
			mDialog.setBtn2("갤러리에서 가져오기");
			mDialog.setBtn2ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Lhy_Function.forceHideKeyboard(mContext, etContent);
					WriteGetPhotoActivity.isFirst = true;

					intentPhoto = new Intent(Intent.ACTION_PICK);
					intentPhoto
							.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
					intentPhoto
							.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI); // images
																										// on
																										// the
																										// SD
																										// card.
					startActivityForResult(intentPhoto, TO_WRITE_PHOTO_GAL);
					mDialog.dismiss();
				}
			});
			mDialog.show();
			// Lhy_Function.forceHideKeyboard(mContext, etContent);
			// intentPhoto = new Intent(Intent.ACTION_PICK);
			// intentPhoto.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
			// intentPhoto.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			// // images on the SD card.
			// startActivityForResult(intentPhoto, TO_WRITE_PHOTO_GAL);
			break;
		case R.id.btnWritePlace:
			/** 글에 장소 추가 **/
			Lhy_Function.forceHideKeyboard(mContext, etContent);
			intent = new Intent(WriteActivity.this, WritePlaceActivity.class);
			startActivityForResult(intent, TO_WRITE_PLACE);
			break;
		case R.id.btnWritePublic:
			/** 정보공개 설정 **/
			Lhy_Function.forceHideKeyboard(mContext, etContent);
			intent = new Intent(WriteActivity.this, WritePublicActivity.class);
			startActivity(intent);
			break;
		case R.id.llWriteKeyboardZone:
		case R.id.viewWriteKeyboardZone:
			/** 키보드 나올 영역 터치시 **/
			Lhy_Function.forceShowKeyboard(mContext, etContent);
			etContent.requestFocus();
			break;

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case TO_WRITE_PHOTO_CAM:
				if (imagePathList.size() < 5) {
					String strUri = data.getStringExtra("strUri");
					Uri uriTemp = Uri.parse(strUri);
					Log.d(TAG, "uriTemp : " + uriTemp);
					Log.d(TAG, "uriPathListSize : " + imagePathList.size());
					imagePathList.add(uriTemp);
					/** 리스트 길이로 이미지 개수 확인, 썸네일 위치 지정 **/
					switch (imagePathList.size()) {
					case 5:
						curUri = imagePathList.get(4);
						Log.d(TAG, "curUri : " + curUri);
						C.setImageThumbnail(curUri, ivThumbnail5);
						ivThumbnail5.setVisibility(View.VISIBLE);
					case 4:
						curUri = imagePathList.get(3);
						Log.d(TAG, "curUri : " + curUri);
						C.setImageThumbnail(curUri, ivThumbnail4);
						ivThumbnail4.setVisibility(View.VISIBLE);
					case 3:
						curUri = imagePathList.get(2);
						Log.d(TAG, "curUri : " + curUri);
						C.setImageThumbnail(curUri, ivThumbnail3);
						ivThumbnail3.setVisibility(View.VISIBLE);
					case 2:
						curUri = imagePathList.get(1);
						Log.d(TAG, "curUri : " + curUri);
						C.setImageThumbnail(curUri, ivThumbnail2);
						ivThumbnail2.setVisibility(View.VISIBLE);
					case 1:
						curUri = imagePathList.get(0);
						Log.d(TAG, "curUri : " + curUri);
						C.setImageThumbnail(curUri, ivThumbnail1);
						ivThumbnail1.setVisibility(View.VISIBLE);
					}
				} else {
					Toast.makeText(mContext, "사진은 5개까지 등록 가능합니다",
							Toast.LENGTH_SHORT).show();
				}
				break;
			case TO_WRITE_PHOTO_GAL:
				/** 앨범에서 사진 골라온 후 **/
				if (imagePathList.size() < 5) {
					Cursor c = getContentResolver().query(data.getData(), null,
							null, null, null);
					c.moveToNext();
					String path = c.getString(c
							.getColumnIndex(MediaStore.MediaColumns.DATA));
					Uri uri = Uri.fromFile(new File(path));
					Log.d(TAG, "Uri : " + uri);
					// Toast.makeText(mContext, uri.toString(),
					// Toast.LENGTH_SHORT).show();
					imagePathList.add(uri);

					/** 리스트 길이로 이미지 개수 확인, 썸네일 위치 지정 **/
					switch (imagePathList.size()) {
					case 1:
						C.setImageThumbnail(uri, ivThumbnail1);
						ivThumbnail1.setVisibility(View.VISIBLE);
						break;
					case 2:
						C.setImageThumbnail(uri, ivThumbnail2);
						ivThumbnail2.setVisibility(View.VISIBLE);
						break;
					case 3:
						C.setImageThumbnail(uri, ivThumbnail3);
						ivThumbnail3.setVisibility(View.VISIBLE);
						break;
					case 4:
						C.setImageThumbnail(uri, ivThumbnail4);
						ivThumbnail4.setVisibility(View.VISIBLE);
						break;
					case 5:
						C.setImageThumbnail(uri, ivThumbnail5);
						ivThumbnail5.setVisibility(View.VISIBLE);
						break;
					}
					c.close();
				} else {
					Toast.makeText(mContext, "사진은 5개까지 등록 가능합니다",
							Toast.LENGTH_SHORT).show();
				}
				break;
			case TO_WRITE_FRIEND:
				/** 친구목록에서 돌아온 후 **/
				tvAddFriend.setText("");
				friendList = (ArrayList<MkFriend>) data
						.getSerializableExtra("friendList");

				if (friendList.size() != 0) {
					ArrayList<String> addFriendList = new ArrayList<String>();
					for (int i = 0; i < friendList.size(); i++) {
						if (friendList.get(i).isChecked()) {
							addFriendList.add(friendList.get(i).getFriend_nm());
						}
					}
					String addString = "";
					if (addFriendList.size() != 0) {
						addString += " - ";
					}
					for (int j = 0; j < addFriendList.size(); j++) {
						if (j == (addFriendList.size() - 1)) {
							addString += addFriendList.get(j);
						} else {
							addString += addFriendList.get(j) + ", ";
						}
					}
					if (addFriendList.size() != 0) {
						addString += " 님과 함께";
					}
					strFriend = addString;
					tvAddFriend.setText(addString);
				}

				break;
			case TO_WRITE_PLACE:
				/** 장소목록에서 돌아온 후 **/
				String bpi_nm = data.getStringExtra("bpi_nm");
				this.bpi_nm = bpi_nm;

				if (!bpi_nm.equals("")) {
					String addString = "- " + bpi_nm + " 에서";
					strPlace = addString;
					tvAddPlace.setText(addString);
				}
				break;
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		/** 토글버튼 리스트중에서 **/
		for (ToggleButton curTb : tbList) {
			/** Changed 된 버튼이 속할경우 **/
			if (curTb.getId() == buttonView.getId()) {
				/** 나머지 토글버튼을 전부 off **/
				if (isChecked)
					setToggleOffOther(buttonView.getId());
			}
		}

		if (isChecked) {
			switch (buttonView.getId()) {
			case R.id.tbWriteNormal:
				curCategory = C.findMkCode("일반", C.LIST_CONTENT);
				Log.i("code", "" + curCategory.getCode());
				break;
			case R.id.tbWriteTuning:
				curCategory = C.findMkCode("튜닝", C.LIST_CONTENT);
				break;
			case R.id.tbWriteTravel:
				curCategory = C.findMkCode("여행", C.LIST_CONTENT);
				break;
			case R.id.tbWriteReview:
				curCategory = C.findMkCode("구매", C.LIST_CONTENT);
				break;
			case R.id.tbWriteInsurance:
				curCategory = C.findMkCode("보험", C.LIST_CONTENT);
				break;
			case R.id.tbWriteOther:
				curCategory = C.findMkCode("기타", C.LIST_CONTENT);
				break;
			}
		}
	}

	private void setToggleOffOther(int tbId) {
		for (ToggleButton curTb : tbList) {
			if (!(curTb.getId() == tbId)) {
				curTb.setChecked(false);
			}
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Log.d(TAG, "ConfiguredChange");
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			Log.d(TAG, "Orientation : LandScape");
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			Log.d(TAG, "Orientation : Portrait");
		}
	}

	@Override
	public void finish() {
		imagePathList.clear();
		strPlace = "";
		strFriend = "";
		super.finish();
	}
}