/**
 * WriteFriendTask
 * 글쓰기 - 친구선택 - 친구목록 액션
 */
package com.anth.moongka.write;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.function.MkApi;

public class WriteFriendTask extends MkTask{
	private final String TAG = "MK_WriteFriendTask";
	private Context mContext;
	private WriteFriendActivity mActivity;
	private ArrayAdapter mAdapter;
	private boolean isCert = false;
	
	private ArrayList<MkFriend> friendList;
	private String mem_idx;

	public WriteFriendTask(String mem_idx, ArrayList<MkFriend> friendList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (WriteFriendActivity) mContext;
		this.mem_idx = mem_idx;
		this.friendList = friendList;
		this.mAdapter = mAdapter;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.friendList(mem_idx, friendList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
			mAdapter.notifyDataSetChanged();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

	
}
