/**
 * WriteMkPlaceAA
 * 글쓰기 - 장소선택 목록 ArrayAdapter
 */
package com.anth.moongka.write;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkPlaceNew;

public class WriteMkPlaceNewAA extends ArrayAdapter<MkPlaceNew> {
	private final String TAG = "MK_MkPlaceNewAA";
	private Context mContext;

	private ArrayList<MkPlaceNew> placeList;

	public WriteMkPlaceNewAA(Context context, int textViewResourceId, ArrayList<MkPlaceNew> placeList) {
		super(context, textViewResourceId, placeList);
		mContext = context;
		this.placeList = placeList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.write_place_list_item, null);
		}

		MkPlaceNew curPlace = placeList.get(position);
		TextView tvName = (TextView) curView.findViewById(R.id.tvWritePlaceItemName);
		TextView tvDistance = (TextView) curView.findViewById(R.id.tvWritePlaceItemDistance);
		CheckBox cb = (CheckBox) curView.findViewById(R.id.cbWritePlaceitem);
		cb.setFocusable(false);

		tvName.setText(curPlace.getBPI_NM());
		tvDistance.setText(curPlace.getDistance());
//		cb.setOnCheckedChangeListener(this);

//		Lhy_Function.setViewTag(cb, position);

		return curView;
	}

//	@Override
//	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//		int position = (Integer) buttonView.getTag();
//		placeList.get(position).setCheck(isChecked);
//	}



}
