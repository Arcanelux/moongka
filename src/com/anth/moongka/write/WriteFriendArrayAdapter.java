/**
 * WriteFriendArrayAdapter
 * 글쓰기 - 친구선택 ArrayAdapter
 */
package com.anth.moongka.write;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.ImageDownloader;

public class WriteFriendArrayAdapter extends ArrayAdapter<MkFriend> implements OnCheckedChangeListener{
	private final String TAG = "MK_WriteFriendArrayAdapter";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	
	private ArrayList<MkFriend> friendList;
	
	public WriteFriendArrayAdapter(Context context, int textViewResourceId, ArrayList<MkFriend> friendList
			) {
		super(context, textViewResourceId, friendList);
		mContext = context;
		mImageDownloader = new ImageDownloader(mContext);
		this.friendList = friendList;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.write_friend_list_item, null);
		}
		
		MkFriend curFriend = friendList.get(position);
		TextView tvName = (TextView) curView.findViewById(R.id.tvWriteFriendItemName);
		CheckBox cbCheck = (CheckBox) curView.findViewById(R.id.cbWriteFriendItem);
		ImageView ivPhoto = (ImageView) curView.findViewById(R.id.ivWriteFriendItemThumbnail);
//		mImageDownloader.download(C.MK_BASE + curFriend.getFriend_url_photo().replaceAll("/data/", "/thumb_data/data/"), ivPhoto);
		mImageDownloader.download(C.MK_BASE + curFriend.getFriend_url_photo(), ivPhoto);
		cbCheck.setChecked(curFriend.isChecked());
		cbCheck.setOnCheckedChangeListener(this);
		Lhy_Function.setViewTag(cbCheck, position);
		
		tvName.setText(curFriend.getFriend_nm());
		return curView;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		int position;
		switch(buttonView.getId()){
		case R.id.cbWriteFriendItem:
			position = (Integer) buttonView.getTag();
			friendList.get(position).setChecked(isChecked);
			break;
		}
	}

}
