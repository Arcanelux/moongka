package com.anth.moongka.write;

import java.io.File;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.anth.moongka.fix_activity.MkActivity;

public class WriteGetPhotoActivity extends MkActivity{
	private final String TAG = "MK_WriteGetPhotoActivity";
	private final int TO_WRITE_PHOTO_CAM = 8923;

	private int curListSize;
	public static boolean isFirst = true;
	private static Uri uriTemp;
	private Intent intentPhoto;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(isFirst){
			Intent intent = getIntent();
			curListSize = intent.getIntExtra("curListSize", 0);

			String tempFileName = "mkTemp" + curListSize + ".jpg";
			File tempFile = new File(Environment.getExternalStorageDirectory(), tempFileName);
			uriTemp = Uri.fromFile(tempFile);

			intentPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intentPhoto.putExtra(MediaStore.EXTRA_OUTPUT, uriTemp);
			isFirst = false;
			startActivityForResult(intentPhoto,TO_WRITE_PHOTO_CAM);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode==RESULT_OK){
			switch(requestCode){
			case TO_WRITE_PHOTO_CAM:
				Uri returnUri = uriTemp;
				String returnPath = returnUri.getPath();
				Log.d(TAG, "returnUri : " + uriTemp);
				Intent intent = new Intent();
				intent.putExtra("strUri", returnPath);
				setResult(RESULT_OK, intent);
				isFirst = true;
				finish();
			}
		} else{
			finish();
		}
	}
}
