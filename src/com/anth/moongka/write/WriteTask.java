/**
 * WriteTask
 * 글쓰기 액션
 */
package com.anth.moongka.write;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.anth.moongka.function.C;
import com.anth.moongka.main.MainActivity;
import com.anth.moongka.network.HttpPostMultiPart;

public class WriteTask extends AsyncTask<Void, Void, Void>{
	private final String TAG = "MK_WriteTask";
	private Context mContext;
	private Activity mActivity;
	private ProgressDialog mProgressDialog;
	
	private HttpPostMultiPart mHttpPost;
	
	public WriteTask(HttpPostMultiPart httpPost, Context context){
		mContext = context;
		mActivity = (Activity) mContext;
		mHttpPost = httpPost;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage("잠시만 기다려주세요...");
		mProgressDialog.show();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		mHttpPost.execute();
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mProgressDialog.dismiss();
		try {
			JSONObject jObjWrite = new JSONObject(mHttpPost.getResult());
			String returnStatus = jObjWrite.getString("returnStatus");
			String returnMessage = jObjWrite.getString("returnMessage");
			
			if(returnStatus.equals("Y")){
				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT).show();
				MainActivity.resetList();
				C.CUR_PUBLIC = C.LIST_ACCESS.get(0);
				mActivity.finish();
			} else{
				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(mContext, "글 등록중에 오류가 발생했습니다", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		mProgressDialog.dismiss();
	}



}
