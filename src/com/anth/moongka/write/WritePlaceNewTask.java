/**
 * WritePlaceTask
 * 장소목록 액션
 */
package com.anth.moongka.write;

import java.util.ArrayList;

import org.apache.http.util.EncodingUtils;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPlaceNew;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class WritePlaceNewTask extends MkTask{
	private final String TAG = "MK_Place1WebViewTask";
	private Context mContext;
	
	private boolean isCert = false;
	private String search;
	private double lat, lon;
	private ArrayList<MkPlaceNew> placeList;
	private ArrayAdapter mAdapter;
	private WebView wvWritePlace;

	public WritePlaceNewTask(String search, double d, double e, ArrayList<MkPlaceNew> placeList, ArrayAdapter mAdapter, WebView wvWritePlace, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.search = search;
		this.lat = d;
		this.lon = e;
		this.placeList = placeList;
		this.mAdapter = mAdapter;
		this.wvWritePlace = wvWritePlace;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.getWritePlaceList(search, lat, lon, placeList);
		return super.doInBackground(params);
	}
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		if(isCert){
			mAdapter.notifyDataSetChanged();
			wvWritePlace.getSettings().setJavaScriptEnabled(true);
			String postData = "userLat=" + lat + "&userLng=" + lon;
//			for(int i=0; i<placeList.size(); i++){
//				postData += "&check" + "[" + i + "]" + "=" + placeList.get(i).getIdx();
//			}
			Log.d(TAG, "PostData : " + postData);
			wvWritePlace.postUrl(C.API_PLACE_MY, EncodingUtils.getBytes(postData, "BASE64"));
		}
		
	}


	
}
