package com.anth.moongka.write;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class WritePlaceAddTask extends MkTask{
	private final String TAG = "MK_WritePlaceAddTask";
	private Context mContext;
	private Activity mActivity;
	
	private boolean isCert = false;
	private String placeName;
	
	public WritePlaceAddTask(String placeName, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		this.placeName = placeName;
		mContext = context;
		mActivity = (Activity) mContext;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.addWritePlace(placeName, C.myLocation.getLatitude(), C.myLocation.getLongitude());
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		if(isCert){
			Intent intent = new Intent();
			intent.putExtra("bpi_nm", placeName);
			mActivity.setResult(Activity.RESULT_OK, intent);
			mActivity.finish();
		}
	}
}
