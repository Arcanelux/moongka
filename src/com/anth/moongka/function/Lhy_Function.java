/**
 * Lhy_Function
 *  자잘한 함수입니다
 */
package com.anth.moongka.function;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Lhy_Function {
	private static final String TAG = "lhy_Function";
	public static final int REQ_UPDATE = 1009;

	public static String getImageName(String imageUrl) {
		String url = imageUrl.replaceAll(".*/", "");
		Log.d(TAG, "getImageName : " + url);
		return url;
	}

	public static String getImageNameExceptSlash(String imageUrl) {
		String url = imageUrl.replaceAll("/", "");
		Log.d(TAG, "getImageNameExceptSlash : " + url);
		return url;
	}

	public static void setViewTag(View[] viewList, int position) {
		for (View curView : viewList) {
			curView.setTag(Integer.valueOf(position));
		}
	}

	public static void setViewTag(View view, int position) {
		view.setTag(Integer.valueOf(position));
	}

	/** 여러개의 뷰에 같은 클릭리스너 할당 **/
	public static void setViewClickListener(View[] viewList,
			OnClickListener listener) {
		for (View curView : viewList) {
			curView.setOnClickListener(listener);
		}
	}

	/** 여러개의 뷰에 같은 토글리스너 할당 **/
	public static void setViewCheckListener(View[] viewList,
			OnCheckedChangeListener listener) {
		for (View curView : viewList) {
			((ToggleButton) curView).setOnCheckedChangeListener(listener);
		}
	}

	public static void makeToast(String msg, Context context) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}

	/** 이메일 정규식 확인 **/
	public static boolean isValidEmail(String email) {
		boolean returnValue = false;
		// String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
		String regex = "^[_0-9a-zA-Z-.]+@[0-9a-zA-Z]+.[a-zA-Z]{2,4}+$";

		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(email);
		if (m.matches()) {
			returnValue = true;
		}
		return returnValue;
	}

	public static boolean isValidYear(String year) {
		boolean returnValue = false;
		// String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
		String regex = "^[0-9]*$";

		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(year);
		if (m.matches()) {
			returnValue = true;
		}
		return returnValue;
	}
	/**
	 * 
	 * @param 휴대폰번호
	 *            유효성 검사
	 * @return
	 */
	public static boolean isValidCellPhoneNumber(String cellphoneNumber) {
		boolean returnValue = false;
		Log.i("cell", cellphoneNumber);
		String regex = "^\\s*(010|011|012|013|014|015|016|017|018|019)(-|\\)|\\s)*(\\d{3,4})(-|\\s)*(\\d{4})\\s*$";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(cellphoneNumber);
		if (m.matches()) {
			returnValue = true;
		}
		return returnValue;
	}
	/**
	 * 
	 * @param 걍
	 *            전화번호 유효성 검사
	 * @return
	 */
	public static boolean isValidPhoneNumber(String phoneNumber) {
		boolean returnValue = false;
		String regex = "^\\s*(02|031|032|033|041|042|043|051|052|053|054|055|061|062|063|064|070)?(-|\\)|\\s)*(\\d{3,4})(-|\\s)*(\\d{4})\\s*$";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(phoneNumber);
		if (m.matches()) {
			returnValue = true;
		}
		return returnValue;
	}
	/** MD5 String을 돌려준다 **/
	public static String getMD5Hash(String s) {
		MessageDigest m = null;
		String hash = null;
		try {
			m = MessageDigest.getInstance("MD5");
			m.update(s.getBytes(), 0, s.length());
			hash = new BigInteger(1, m.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return hash;
	}

	/** Asset폴더의 File이름을 인자로 전달, String을 반환한다 **/
	public static String getStringAssetText(String file, Context context)
			throws IOException {
		InputStream is = context.getAssets().open(file);

		int size = is.available();
		byte[] buffer = new byte[size];
		is.read(buffer);
		is.close();

		String text = new String(buffer);

		return text;
	}

	/** Cache 삭제 **/
	public static void clearCache(Context context) {
		final File cacheDirFile = context.getCacheDir();
		if (null != cacheDirFile && cacheDirFile.isDirectory()) {
			clearSubCacheFiles(cacheDirFile);
		}
	}

	private static void clearSubCacheFiles(File cacheDirFile) {
		if (null == cacheDirFile || cacheDirFile.isFile()) {
			return;
		}
		for (File cacheFile : cacheDirFile.listFiles()) {
			if (cacheFile.isFile()) {
				if (cacheFile.exists()) {
					cacheFile.delete();
				}
			} else {
				clearSubCacheFiles(cacheFile);
			}
		}
	}

	/** 현재 앱 버전 확인 **/
	public static int checkAppVersion(Context context) {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
			return info.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return 0;
		}
	}

	/** HTML페이지에서 버전 확인하기 **/
	public static int checkLhyVersion(Context context) {
		String result = "";
		try {
			String strUrl = "http://kkoksara.com/def/"
					+ context.getPackageName();
			URL url = new URL(strUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			if (conn != null) {
				conn.setConnectTimeout(10000);
				conn.setUseCaches(false);
				if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
					BufferedReader br = new BufferedReader(
							new InputStreamReader(conn.getInputStream()));
					String line = br.readLine();
					result = line;
					br.close();
				}
				conn.disconnect();
			}
		} catch (Exception ex) {
			;
		}

		return Integer.parseInt(result);
	}

	/** 업데이트 Dialog **/
	public static Dialog updateDialog(int id, final Context context) {
		Dialog dialog;
		AlertDialog.Builder builder;
		switch (id) {
		case REQ_UPDATE:
			builder = new AlertDialog.Builder(context);
			builder.setMessage("마켓에 새로운 버전이 있습니다. 업데이트를 해주세요")
			.setCancelable(false)
			.setPositiveButton("업데이트하기",
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int id) {
					String pkgName = context.getPackageName();
					Intent intent = new Intent(
							Intent.ACTION_VIEW,
							Uri.parse("market://details?id="
									+ pkgName));
					((Activity) context).startActivity(intent);
					((Activity) context).finish();
				}
			})
			.setNegativeButton("종료",
					new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
						int id) {
					((Activity) context).finish();
				}
			});
			dialog = builder.create();
			break;
		default:
			dialog = null;
		}
		return dialog;
	}

	/** 자동 업데이트 Dialog **/
	public static void showUpdateDialog(Context context) {
		int curVersion = checkAppVersion(context);
		int lhyVersion = checkLhyVersion(context);
		if (curVersion < lhyVersion) {
			updateDialog(REQ_UPDATE, context).show();
		}
	}

	/** 자동 업데이트 Dialog, 미리 버전을 알고있을 때 **/
	public static void showUpdateDialog(int curVersion, int lhyVersion,
			Context context) {
		if (curVersion < lhyVersion) {
			updateDialog(REQ_UPDATE, context).show();
		}
	}

	/** 강제 키보드 내리기(임의의 EditText 필요) **/
	public static void forceHideKeyboard(Context context, EditText editText) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		// 키보드를 띄운다.
		imm.showSoftInput(editText, 0);
		// 키보드를 없앤다.
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	/** 강제 키보드 보이기 **/
	public static void forceShowKeyboard(Context context, EditText editText){
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
	}

	/** 카메라 캡쳐 Uri 받아오기 **/
	public static Uri getLastCaptureImageUri(Context context){
		Uri uri =null;
		String[] IMAGE_PROJECTION = {
				MediaColumns.DATA, 
				BaseColumns._ID,
		};

		try {
			Cursor cursorImages = context.getContentResolver().query(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
					IMAGE_PROJECTION, null, null,null);
			if (cursorImages != null && cursorImages.moveToLast()) {
				uri = Uri.parse(cursorImages.getString(0)); //경로
				//id = cursorImages.getInt(1); //아이디    
				cursorImages.close(); // 커서 사용이 끝나면 꼭 닫아준다.
			}
		} catch(Exception e) {
			e.printStackTrace();
		}        
		return uri;  
	}
}
