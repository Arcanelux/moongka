/**
 * BitmapEdit
 *  비트맵 처리 클래스
 */
package com.anth.moongka.function;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class BitmapEdit {
	/**
	 * 카메라에서 찍은 사진의 비트맵을 인자로받아
	 * 90도 회전시키고 짧은 축 기준으로 정사각형이 되도록 자른 후,
	 * 다시 비트맵을 반환
	 * @param originalBitmap	원본사진의 비트맵
	 * @return				편집된사진의 비트맵
	 */
	public static Bitmap resizeBitmapSquare(Bitmap originalBitmap, int length){
		int width = originalBitmap.getWidth();
		int height = originalBitmap.getHeight();
		int narrowLength = width > height ? height : width;
		boolean isWide = width > height ? true : false;

		Matrix matrix = new Matrix();
		//matrix.postRotate(90);
		// 긴 축에 맞추어서 정사각형으로 자름
		if(isWide)	originalBitmap = Bitmap.createBitmap(originalBitmap, (width-narrowLength)/4, 0, narrowLength, narrowLength, matrix, true);
		else		originalBitmap = Bitmap.createBitmap(originalBitmap, 0, (height-narrowLength)/4, narrowLength, narrowLength, matrix, true);

		// 짧은축이 800px이상일 경우 리사이즈
		if(narrowLength > length){
			narrowLength = length;
			originalBitmap = Bitmap.createScaledBitmap(originalBitmap, narrowLength, narrowLength, true);
		}

		return originalBitmap;
	}
	public static Bitmap resizeBitmapSquareRotate(Bitmap originalBitmap, int length){
		int width = originalBitmap.getWidth();
		int height = originalBitmap.getHeight();
		int narrowLength = width > height ? height : width;
		boolean isWide = width > height ? true : false;

		Matrix matrix = new Matrix();
		matrix.postRotate(90);
		// 긴 축에 맞추어서 정사각형으로 자름
		if(isWide)	originalBitmap = Bitmap.createBitmap(originalBitmap, (width-narrowLength)/4, 0, narrowLength, narrowLength, matrix, true);
		else		originalBitmap = Bitmap.createBitmap(originalBitmap, 0, (height-narrowLength)/4, narrowLength, narrowLength, matrix, true);

		// 짧은축이 800px이상일 경우 리사이즈
		if(narrowLength > length){
			narrowLength = length;
			originalBitmap = Bitmap.createScaledBitmap(originalBitmap, narrowLength, narrowLength, true);
		}

		return originalBitmap;
	}
	/**
	 * 비트맵과 resizeSize를 인자로받아 세로길이를 resizeSize값에 맞추어 리사이즈한 비트맵을 반환
	 * 지정한 resizeSize값보다 이미지의 세로가 작을경우 원 이미지 그대로 반환
	 * 비율변화 없음
	 * @param oriBitmap
	 * @param resizeSize
	 * @return
	 */
	public static Bitmap makeResizeBitmap(Bitmap oriBitmap, int resizeSize){
		int imgHeight = oriBitmap.getHeight();
		int imgWidth = oriBitmap.getWidth();

		if(imgHeight < resizeSize) { return oriBitmap; }
		while(imgHeight > resizeSize){
			oriBitmap = Bitmap.createScaledBitmap(oriBitmap, imgWidth*resizeSize/imgHeight, resizeSize, true);
			imgWidth = oriBitmap.getWidth();
			imgHeight = oriBitmap.getHeight();
		}

		return oriBitmap;
	}	
}
