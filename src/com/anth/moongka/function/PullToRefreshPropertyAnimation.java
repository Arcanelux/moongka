/**
* 프로그래스바 애니메이션 class
*/

package com.anth.moongka.function;

import android.view.animation.Animation;
import android.view.animation.Transformation;

public class PullToRefreshPropertyAnimation extends Animation {
	public static interface PropertyListener {
		public void onPropertyChanged(PullToRefreshPropertyAnimation animation,float now);
	}
	private PropertyListener listener = null;
	private float start = 0;
	private float end = 0;
	private float now = 0;
	public PullToRefreshPropertyAnimation(float start,float end,PropertyListener listener) {
		this.start = start;
		this.end = end;
		this.listener = listener;
	}
	public void setStart(float start) {
		this.start = start;
	}
	public float getStart() {
		return start;
	}
	public void setEnd(float end) {
		this.end = end;
	}
	public float getEnd() {
		return end;
	}
	public float getNow() {
		return now;
	}
	public void setListener(PropertyListener listener) {
		this.listener = listener;
	}
	/**
	 * @param interpolatedTime
	 * @param t
	 * @see android.view.animation.ScaleAnimation#applyTransformation(float, android.view.animation.Transformation)
	 */
	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		super.applyTransformation(interpolatedTime, t);
		float gap = end - start;
		now = start + (interpolatedTime * gap);
		if (listener != null) {
			listener.onPropertyChanged(this, now);
		}
	}
}
