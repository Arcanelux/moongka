/**
 * MkPref
 *  SharedPreferences 클래스
 *  자동로그인, 알림, GCM ID를 저장
 *  다른곳에서 getSavedValue 함수로 저장된 값을 객체형태로 꺼내어 사용
 */
package com.anth.moongka.function;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class MkPref {
	private static final String TAG = "MK_Pref";
	
	/** 자동로그인 설정 **/
	public static void setAutoLogin(Context context, Boolean value){
		SharedPreferences pref = context.getSharedPreferences("Setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean("autoLogin", value);
		editor.commit();
	}
	/** 자동로그인을 위한 ID 저장 **/
	public static void saveID(Context context, String id){
		SharedPreferences pref = context.getSharedPreferences("Setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("id", id);
		editor.commit();
	}
	/** 자동로그인을 위한 PW 저장 **/
	public static void savePW(Context context, String pw){
		SharedPreferences pref = context.getSharedPreferences("Setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("pw", pw);
		editor.commit();
	}
	/** 알림 설정 **/
	public static void setAlarm(Context context, Boolean value){
		SharedPreferences pref = context.getSharedPreferences("Setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean("alarm", value);
		editor.commit();
	}
	
	/** GCM RegID 저장 **/
	public static void saveGCM(Context context, String regId){
		SharedPreferences pref = context.getSharedPreferences("Setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("regId", regId);
		editor.commit();
		Log.d(TAG, "SavedGCM RegId : " + regId);
	}
	
	public static SavedValue getSavedValue(Context context){
		SharedPreferences pref = context.getSharedPreferences("Setting", Context.MODE_PRIVATE);
		
		String id = pref.getString("id", null);
		String pw = pref.getString("pw", null);
		boolean isAutoLogin = pref.getBoolean("autoLogin", false);
		boolean isAlarm = pref.getBoolean("alarm", true);
		String regId = pref.getString("regId", "");
		
		SavedValue curSValue = new SavedValue();
		curSValue.setId(id);
		curSValue.setPw(pw);
		curSValue.setAutoLogin(isAutoLogin);
		curSValue.setRegId(regId);
		curSValue.setAlarm(isAlarm);
		
		return curSValue;
	}
	
	public static void removeSettingData(Context context){
		SharedPreferences pref = context.getSharedPreferences("Setting", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.remove("id");
		editor.remove("pw");
		editor.remove("autoLogin");
		editor.remove("alarm");
		//editor.clear();
		editor.commit();
	}
	public static void setSort(String order){
		C.order = order;		
	}
	public static String getSort(){
		String order = C.order;
		return order;
	}
}
