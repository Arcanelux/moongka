/**
 * C
 *  각종 static 변수 모음
 */
package com.anth.moongka.function;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.join.Join1Params;
import com.anth.moongka.join.Join2Params;
import com.anth.moongka.menu.MkProfileMain;
import com.anth.moongka.profile_car_list.MkCarListProfile;
import com.anth.moongka.setting.MkArea;
import com.anth.moongka.setting.MkCar;

public class C {
	private static final String TAG = "MK_C";
	public static final int LOGIN_FAILED = 10;
	public static final int LOGIN_SUCCESS = 11;

	public static int DISPLAY_WIDTH, DISPLAY_HEIGHT;
	public static int identify = 0;

	/** From GCM **/
	public static int GCM_CURRENT = 0;
	public static final int GCM_TO_FRIEND = 40;
	public static final int GCM_TO_MESSAGE = 41;
	public static final int GCM_TO_COMMENT_TXT = 42;
	public static final int GCM_TO_COMMENT_IMAGE = 43;
	public static final int GCM_TO_POST = 44;
	public static String GCM_KEY_ID;
	public static boolean isFromGCM = false;

	public static boolean isFromNotify = false;

	// true일 경우 new아이콘 표시
	public static boolean isExistNotify = false;

	/** API **/
	public static boolean API_OK = true;
	public static boolean API_FAILED = false;

	public static String MK_BASE = "http://api.moongka.com";
	private static String API_BASE = "http://api.moongka.com/api/";
//	public static String MK_BASE = "http://dev-moongka.anth.co.kr";
//	private static String API_BASE = "http://dev-moongka.anth.co.kr/api/";
	
	public static String API_LOGIN = API_BASE + "memberLogin.php";
	public static String API_FIND_ID = API_BASE + "memberIdSearch.php";
	public static String API_FIND_PW = API_BASE + "memberPasswdSearch.php";
	public static String API_JOIN = API_BASE + "memberInput.php";
	public static String API_CHECK_ID = API_BASE + "memberIdchk.php";
	public static String API_CHECK_NICKNAME = API_BASE + "nameChk.php";
	public static String API_PROFILE = API_BASE + "newProfileView.php";
	public static String API_PROFILE_MAIN = API_BASE + "newMemberProfile.php";
	public static String API_SETTING_PROFILE_GET = API_BASE
			+ "newMemberUpdateView.php";
	public static String API_SETTING_PROFILE_EDIT = API_BASE
			+ "memberUpdate.php";
	public static String API_FRIEND_LIST = API_BASE + "friendList.php";
	public static String API_FRIEND_LIST_REQ = API_BASE + "friendApplyList.php";
	public static String API_FRIEND_DELETE = API_BASE + "friendErase.php";
	public static String API_FRIEND_ACCEPT = API_BASE + "friendApplyChk.php";
	public static String API_FRIEND_REQUEST = API_BASE + "friendApply.php";
	public static String API_NOTICE_LIST = API_BASE + "noticeList.php";
	public static String API_SETTING_CHANGE_PW = API_BASE + "passUpdate.php";
	public static String API_SCRAP = API_BASE + "boardScrap.php";
	public static String API_SCRAP_LIST = API_BASE + "boardScrapList.php";
	public static String API_EXIT = API_BASE + "memberErase.php";
	public static String API_WRITE = API_BASE + "boardWrite.php";
	public static String API_WRITE_PLACE = API_BASE + "traceList.php";
	public static String API_WRITE_PLACE_ADD = API_BASE + "traceWrite.php";
	public static String API_POSTLIST = API_BASE + "boardList.php";
	public static String API_POST_DEL = API_BASE + "boardErase.php";
	public static String API_POST_SEARCH_LIST = API_BASE + "searchList.php";
	public static String API_COMMENT_LIST = API_BASE + "replyBdList.php";
	public static String API_COMMENT_ADD = API_BASE + "replyBdWrite.php";
	public static String API_COMMENT_DEL = API_BASE + "replyBdErase.php";
	public static String API_LIKE_TOGGLE = API_BASE + "likeChk.php";
	public static String API_PLACE = API_BASE + "mobileLocation.php";
	public static String API_PLACE_MY = API_BASE + "myLocation.php";
	public static String API_LBS = API_BASE + "LBS.php";
	public static String API_ALBUM = API_BASE + "albumList.php";
	public static String API_ALBUM_IN_POST = API_BASE + "albumView.php";

	public static String API_CAR = API_BASE + "NewcarUpdateView.php";
	// public static String API_MKCAR = API_BASE + "carCodeAnd.php";
	public static String API_MKCAR = API_BASE + "car_code.php";
	public static String API_MKCAR_EDIT = API_BASE + "newCarUpdate.php";
	public static String API_MKCAR_ADD = API_BASE + "carInsert.php";
	// public static String API_CAR_MAKER = API_BASE + "carCodeFirst.php";
	// public static String API_CAR_NAME = API_BASE + "carCodeSecond.php";
	public static String API_MESSAGE_LIST = API_BASE + "newMessageList.php";
	public static String API_MESSAGE_DETAIL = API_BASE + "messageView.php";
	// public static String API_MESSAGE_WRITE = API_BASE + "messageWrite.php";
	public static String API_MESSAGE_WRITE = API_BASE
			+ "newMessageWrite_android.php";
	public static String API_MESSAGE_DELETE = API_BASE + "newMessageErase.php";

	public static String API_ALARM_LIST = API_BASE + "alarmList.php";
	public static String API_ALARM_CHECK = API_BASE + "alarmChk.php";
	public static String API_CHECKIN_LIST = API_BASE + "placeList.php";
	public static String API_CHECKIN_DETAIL_LIST = API_BASE
			+ "placeListView.php";

	public static String API_PROFILE_CAR_LIST = API_BASE + "newCarYear.php";
	public static String API_PUSH_CHECK = API_BASE + "pushChk.php";

	/** Codes **/
	public static String API_CODE = API_BASE + "code.php";
	public static String API_REGION_CD = API_BASE + "locationCode_android.php";
	// private static String CODE_BASE = "http://dev-moongka.anth.co.kr/api/";
	// public static String CODE_ACCESS = CODE_BASE + "memberAccess.php";
	// public static String CODE_AREA = CODE_BASE + "memberArea.php";
	// public static String CODE_COMPANY = CODE_BASE + "memberCompany.php";
	// public static String CODE_CONTENT = CODE_BASE + "memberContent.php";
	// public static String CODE_JOB = CODE_BASE + "memberJob.php";
	// public static String CODE_QUESTION = CODE_BASE + "memberQuestion.php";

	/** MK DateFormat **/
	// public static SimpleDateFormat mkDateFormat = new SimpleDateFormat
	// ("yyyy.MM.dd G 'at' hh:mm:ss a zzz");
	public static SimpleDateFormat mkDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");

	/** GPS **/
	public static LocationManager locManager;
	public static Geocoder geoCoder;
	public static Location myLocation;
	public static double latPoint;
	public static double lngPoint;
	public static float speed;
	public static LocationListener locationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			C.myLocation = location;
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	};

	/** SavedCodes **/
	public static ArrayList<MkCode> LIST_ACCESS = new ArrayList<MkCode>();
	
	public static ArrayList<MkCode> LIST_CITY = new ArrayList<MkCode>();
	public static ArrayList<MkCode> LIST_CARTYPE = new ArrayList<MkCode>();
	public static ArrayList<MkCode> LIST_COMPANY = new ArrayList<MkCode>();
	public static ArrayList<MkCode> LIST_CONTENT = new ArrayList<MkCode>();
	public static ArrayList<MkCode> LIST_JOB = new ArrayList<MkCode>();
	public static ArrayList<MkCode> LIST_QUESTION = new ArrayList<MkCode>();

	public static ArrayList<MkCar> LIST_MKCAR = new ArrayList<MkCar>();;
	public static ArrayList<MkArea> LIST_AREA = new ArrayList<MkArea>();
	public static ArrayList<MkCarListProfile> LIST_MK_CAR_PROFILE = new ArrayList<MkCarListProfile>();
	public static MkProfileMain MAIN_CAR_PROFILE = new MkProfileMain();

	/** MkCode 찾는 함수 **/
	public static MkCode findMkCode(String findStr, ArrayList<MkCode> mkCodeList) {
		MkCode returnCode = null;
		for (MkCode curCode : mkCodeList) {
			if (curCode.getName().contains(findStr)) {
				// Log.d(TAG, "FindMkCode : " + curCode.getCode() + ", " +
				// curCode.getName());
				return curCode;
			}
		}
		return returnCode;
	}

	/** MkCode position 찾는 함수 **/
	public static int findMkCodePosition(String findStr,
			ArrayList<MkCode> mkcodeList) {
		for (int i = 0; i < mkcodeList.size(); i++) {
			MkCode curCode = mkcodeList.get(i);
			if (curCode.getName().equals(findStr)) {
				return i;
			}
		}
		return 0;
	}

	/** Static Code **/
	// 공개여부 선택시
	public static MkCode CUR_PUBLIC;

	/** Login Info **/
	public static String mem_idx;
	public static String mem_id;
	public static String mem_name;
	public static String mem_profile;

	/** Join **/
	public static Join1Params JOIN1_PARAMS;
	public static Join2Params JOIN2_PARAMS;

	/** HttpClient **/
	public static HttpClient httpClient = new DefaultHttpClient();

	/** MenuLeft **/
	public static int TOP_HEIGHT;
	public static final int MENULEFT_ITEM1 = 20;
	public static final int MENULEFT_ITEM2 = 21;
	public static String order = "date";
	public static String car_cnt;
	public static String frd_invite;

	/** Notify **/
	public static final int NOTIFY_1 = 30;

	/** Message **/
	public static final int MESSAGE_LEFT = 30;
	public static final int MESSAGE_RIGHT = 31;

	/** 썸네일 삽입, 정사각형에 250픽셀로 리사이즈하여 삽입한다. **/
	public static void setImageThumbnail(Uri uri, ImageView iv) {
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 2;
			options.inPreferredConfig = Config.RGB_565;
			Bitmap curBitmap = BitmapFactory.decodeFile(uri.getPath(), options);

			// 이미지를 상황에 맞게 회전시킨다
			ExifInterface exif = new ExifInterface(uri.getPath());
			int exifOrientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			int exifDegree = 0;
			switch (exifOrientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				exifDegree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				exifDegree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				exifDegree = 270;
				break;
			default:
				exifDegree = 0;
				break;
			}
			if (exifDegree != 0) {
				Matrix m = new Matrix();
				m.setRotate(exifDegree, (float) curBitmap.getWidth() / 2,
						(float) curBitmap.getHeight() / 2);
				try {
					Bitmap converted = Bitmap.createBitmap(curBitmap, 0, 0,
							curBitmap.getWidth(), curBitmap.getHeight(), m,
							true);
					if (curBitmap != converted) {
						curBitmap.recycle();
						curBitmap = converted;
					}
				} catch (OutOfMemoryError ex) {
					ex.printStackTrace();
				}
			}
			Bitmap editBitmap = BitmapEdit.resizeBitmapSquare(curBitmap, 250);
			iv.setImageBitmap(editBitmap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** 썸네일 삽입, 정사각형에 250픽셀로 리사이즈하여 삽입한다. **/
	public static void setImageThumbnail(Uri uri, ImageView iv, int sampleSize) {
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = sampleSize;
			options.inPreferredConfig = Config.RGB_565;
			Bitmap curBitmap = BitmapFactory.decodeFile(uri.getPath(), options);

			// 이미지를 상황에 맞게 회전시킨다
			ExifInterface exif = new ExifInterface(uri.getPath());
			int exifOrientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			int exifDegree = 0;
			switch (exifOrientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				exifDegree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				exifDegree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				exifDegree = 270;
				break;
			default:
				exifDegree = 0;
				break;
			}
			if (exifDegree != 0) {
				Matrix m = new Matrix();
				m.setRotate(exifDegree, (float) curBitmap.getWidth() / 2,
						(float) curBitmap.getHeight() / 2);
				try {
					Bitmap converted = Bitmap.createBitmap(curBitmap, 0, 0,
							curBitmap.getWidth(), curBitmap.getHeight(), m,
							true);
					if (curBitmap != converted) {
						curBitmap.recycle();
						curBitmap = converted;
					}
				} catch (OutOfMemoryError ex) {
					ex.printStackTrace();
				}
			}

			Bitmap editBitmap = BitmapEdit.resizeBitmapSquare(curBitmap, 250);
			iv.setImageBitmap(editBitmap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
