package com.anth.moongka.function;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.anth.moongka.album.Album;
import com.anth.moongka.checkin.MkCheckIn;
import com.anth.moongka.fix_value.MkAlarm;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.fix_value.MkComment;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.fix_value.MkMsg;
import com.anth.moongka.fix_value.MkPlace;
import com.anth.moongka.fix_value.MkPlaceNew;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.menu.MkProfileMain;
import com.anth.moongka.profile.MkProfile;
import com.anth.moongka.profile_car_list.MkCarListProfile;
import com.anth.moongka.profile_car_list.MkCarListProfileEdit;
import com.anth.moongka.setting.MkArea;
import com.anth.moongka.setting.MkCar;
import com.anth.moongka.setting.MkNotice;
import com.anth.moongka.setting.SettingProfile;

public class MkApi {
	private final static String TAG = "MK_Api";
	private final static String TAG_CODE = "CodeDetail";
	public static String returnMsg = "";
	public static String returnValue = "";

	private static boolean TEST = false;

	/**
	 * LOGIN mem_id mem_pass
	 */
	public static boolean login(String id, String pw, Context context) {
		SavedValue saveValue = MkPref.getSavedValue(context);
		String deviceKey = saveValue.getRegId();
		Log.i("MK_Api_", deviceKey);

		String pwMd5 = Lhy_Function.getMD5Hash(pw);

		Log.d(TAG, "pwMD5(Login) : " + pwMd5);

		/** List **/
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_id", id));
		nameValuePairs.add(new BasicNameValuePair("mem_pass", pwMd5));
		nameValuePairs.add(new BasicNameValuePair("mem_key", deviceKey));
		nameValuePairs.add(new BasicNameValuePair("mem_type", "android"));

		JSONObject jObjLogin = getJsonObj(C.API_LOGIN, nameValuePairs);

		/** returnStatus가 Y일 경우 true, 그 외엔 false **/
		try {
			String returnStatus = jObjLogin.getString("returnStatus");
			String returnMessage = jObjLogin.getString("returnMessage");
			String mem_idx = jObjLogin.getString("mem_idx");
			String mem_id = jObjLogin.getString("mem_id");
			String mem_name = jObjLogin.getString("mem_name");
			String mem_profile = jObjLogin.getString("mem_profile");

			/** 아이디와 고유번호를 ConstValue에 삽입 **/
			C.mem_id = mem_id;
			C.mem_idx = mem_idx;
			C.mem_name = mem_name;
			C.mem_profile = C.MK_BASE + mem_profile;

			String result = "Login returnStatus : " + returnStatus
					+ "\nreturnMessage : " + returnMessage;
			returnMsg = returnMessage;
			Log.d(TAG, result);
			if (returnStatus.equals("Y")) {
				return true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			if (TEST) {
				return true;
			} else {
				return false;
			}
		}
		if (TEST) {
			return true;
		} else {
			return false;
		}
	}

	/** FindId **/
	public static boolean findId(String mem_name, String mem_question,
			String mem_answer) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		// nameValuePairs.add(new BasicNameValuePair("mem_email", mem_email));
		nameValuePairs.add(new BasicNameValuePair("mem_name", mem_name));
		nameValuePairs
				.add(new BasicNameValuePair("mem_question", mem_question));
		nameValuePairs.add(new BasicNameValuePair("mem_answer", mem_answer));
		JSONObject jObj = getJsonObj(C.API_FIND_ID, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				returnValue = jObj.getString("mem_id");
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** FindPw **/
	public static boolean findPw(String mem_email, String mem_name,
			String mem_question, String mem_answer) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_email", mem_email));
		nameValuePairs.add(new BasicNameValuePair("mem_name", mem_name));
		nameValuePairs
				.add(new BasicNameValuePair("mem_question", mem_question));
		nameValuePairs.add(new BasicNameValuePair("mem_answer", mem_answer));
		JSONObject jObj = getJsonObj(C.API_FIND_PW, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * JOIN
	 */
	public static boolean join(String id, String pw, String name,
			String birthday, String gender, String questionCode, String answer,
			Context context) {
		SavedValue saveValue = MkPref.getSavedValue(context);
		String deviceKey = saveValue.getRegId();
		String pwMd5 = Lhy_Function.getMD5Hash(pw);

		/** list **/
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_id", id));
		nameValuePairs.add(new BasicNameValuePair("mem_pass", pwMd5));
		nameValuePairs.add(new BasicNameValuePair("mem_name", name));
		nameValuePairs.add(new BasicNameValuePair("mem_key", deviceKey));
		nameValuePairs.add(new BasicNameValuePair("mem_yymmdd", birthday));
		nameValuePairs.add(new BasicNameValuePair("mem_sex", gender));
		nameValuePairs.add(new BasicNameValuePair("mem_question", ""
				+ questionCode));
		nameValuePairs.add(new BasicNameValuePair("mem_answer", answer));
		nameValuePairs.add(new BasicNameValuePair("mem_type", "android"));

		// nameValuePairs.add(new BasicNameValuePair("mem_id",
		// URLEncoder.encode(id, "utf-8")));
		// nameValuePairs.add(new BasicNameValuePair("mem_pass",
		// URLEncoder.encode(pwMd5, "utf-8")));
		// nameValuePairs.add(new BasicNameValuePair("mem_name",
		// URLEncoder.encode(name, "utf-8")));
		// nameValuePairs.add(new BasicNameValuePair("mem_key",
		// URLEncoder.encode(deviceKey, "utf-8")));
		// nameValuePairs.add(new BasicNameValuePair("mem_yymmdd",
		// URLEncoder.encode(birthday, "utf-8")));
		// nameValuePairs.add(new BasicNameValuePair("mem_sex",
		// URLEncoder.encode(gender, "utf-8")));
		// nameValuePairs.add(new BasicNameValuePair("mem_question", "" +
		// URLEncoder.encode(questionCode, "utf-8")));
		// nameValuePairs.add(new BasicNameValuePair("mem_answer",
		// URLEncoder.encode(answer, "utf-8")));
		// nameValuePairs.add(new BasicNameValuePair("mem_type",
		// URLEncoder.encode("android", "utf-8")));
		//
		// // nameValuePairs.add(new BasicNameValuePair("mem_id",
		// URLEncoder.encode(id, "utf-8")));
		// // nameValuePairs.add(new BasicNameValuePair("mem_pass",
		// URLEncoder.encode(pwMd5, "utf-8")));
		// // nameValuePairs.add(new BasicNameValuePair("mem_name",
		// URLEncoder.encode(name, "utf-8")));
		// // nameValuePairs.add(new BasicNameValuePair("mem_key",
		// URLEncoder.encode(deviceKey, "utf-8")));
		// // nameValuePairs.add(new BasicNameValuePair("mem_yymmdd",
		// URLEncoder.encode(birthday, "utf-8")));
		// // nameValuePairs.add(new BasicNameValuePair("mem_sex",
		// URLEncoder.encode(gender, "utf-8")));
		// // nameValuePairs.add(new BasicNameValuePair("mem_question", "" +
		// URLEncoder.encode(questionCode, "utf-8")));
		// // nameValuePairs.add(new BasicNameValuePair("mem_answer",
		// URLEncoder.encode(answer, "utf-8")));
		// // nameValuePairs.add(new BasicNameValuePair("mem_type",
		// URLEncoder.encode("android", "utf-8")));

		Log.d(TAG, "-Join Start -");
		Log.d(TAG, "id : " + id);
		Log.d(TAG, "pw : " + pw);
		Log.d(TAG, "pwMD5(Join) : " + pwMd5);
		Log.d(TAG, "name : " + name);
		Log.d(TAG, "deviceKey : " + deviceKey);
		Log.d(TAG, "birthday : " + birthday);
		Log.d(TAG, "gender : " + gender);
		Log.d(TAG, "questionCode : " + questionCode);
		Log.d(TAG, "answer : " + answer);
		Log.d(TAG, "-Join End -");

		JSONObject jObjJoin = getJsonObj(C.API_JOIN, nameValuePairs);

		/** returnStatus가 Y일 경우 true, 그 외엔 false **/
		try {
			String returnStatus = jObjJoin.getString("returnStatus");
			String returnMessage = jObjJoin.getString("returnMessage");
			returnMsg = returnMessage;

			String result = "Join returnStatus : " + returnStatus
					+ "\nreturnMessage : " + returnMessage;
			Log.d(TAG, result);
			if (returnStatus.equals("Y")) {
				return true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}

		return false;
	}

	/**
	 * CheckId
	 */
	public static boolean checkId(String id) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_id", id));
		JSONObject jObjJoin = getJsonObj(C.API_CHECK_ID, nameValuePairs);

		try {
			String returnStatus = jObjJoin.getString("returnStatus");
			String returnMessage = jObjJoin.getString("returnMessage");

			String result = "Join returnStatus : " + returnStatus
					+ "\nreturnMessage : " + returnMessage;
			returnMsg = returnMessage;
			Log.d(TAG, result);
			if (returnStatus.equals("Y")) {
				return true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	public static boolean checkNickName(String nickname) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_name", nickname));
		JSONObject jObjJoin = getJsonObj(C.API_CHECK_NICKNAME, nameValuePairs);

		try {
			String returnStatus = jObjJoin.getString("returnStatus");
			String returnMessage = jObjJoin.getString("returnMessage");

			String result = "NickName returnStatus : " + returnStatus
					+ "\nreturnMessage : " + returnMessage;
			returnMsg = returnMessage;
			Log.d(TAG, result);
			if (returnStatus.equals("Y")) {
				return true;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/** 메인프로필정보불러오기 **/
	public static boolean getProfileMain(String mem_idx, MkProfileMain mProfile) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		JSONObject jObj = getJsonObj(C.API_PROFILE_MAIN, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			String mem_name = jObj.getString("mem_name");
			String mem_profile = jObj.getString("mem_profile");
			String inviteFriend = jObj.getString("frd_apply_cnt");

			Log.i("frd", inviteFriend);
			mProfile.setMem_name(mem_name);
			mProfile.setMem_profile(mem_profile);
			C.frd_invite = inviteFriend;

			// MainCar
			JSONObject jObjMainCar = jObj.getJSONObject("car_main");
			String MMC_CARNICK = jObjMainCar.getString("MMC_CARNICK");
			String MMC_CARPIC_URL = jObjMainCar.getString("MMC_CARPIC_URL");
			C.car_cnt = jObjMainCar.getString("CAR_CNT");
			Log.i("cnt", C.car_cnt);
			mProfile.setMMC_CARNICK(MMC_CARNICK);
			mProfile.setMMC_CARPIC_URL(MMC_CARPIC_URL);

			String car_main_cnt = jObj.getString("car_main_cnt");
			mProfile.setCar_main_cnt(car_main_cnt);

			// DreamCar
			try {
				JSONArray jAryDreamCar = jObj.getJSONArray("car_dream_list");
				mProfile.getDreamCarList().clear(); // 초기화
				for (int i = 0; i < jAryDreamCar.length(); i++) {
					JSONObject jObjCurDreamCar = jAryDreamCar.getJSONObject(i);
					String MMC_ID = jObjCurDreamCar.getString("MMC_ID");
					String MMC_CARPIC_URL_DREAM = jObjCurDreamCar
							.getString("MMC_CARPIC_URL");
					String CC_NM = jObjCurDreamCar.getString("CC_NM");

					mProfile.addDreamCar(MMC_ID, MMC_CARPIC_URL_DREAM, CC_NM);
					Log.d(TAG, "AddDreamCAR : " + CC_NM);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** ProfileView (ProfileActivity) **/
	public static boolean getProfile(String profile_idx, MkProfile mProfile) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("profile_idx", profile_idx));
		JSONObject jObjProfile = getJsonObj(C.API_PROFILE, nameValuePairs);

		try {
			// JSONArray jAryProfile = jObjProfile.getJSONArray("list");
			// JSONObject curJobjProfile = jAryProfile.getJSONObject(0);
			JSONObject curJobjProfile = jObjProfile.getJSONObject("list");
			String user_idx = curJobjProfile.getString("MM_ID");
			String user_id = curJobjProfile.getString("MM_USERID");
			String user_name = curJobjProfile.getString("MM_NM");
			String user_profile_photo = curJobjProfile
					.getString("MM_PROFILE_PIC_PATH");
			String user_job = curJobjProfile.getString("MM_JOB_NAME");
			String user_area = curJobjProfile.getString("MM_RGN_NAME");
			String user_city = curJobjProfile.getString("MM_RGN_NAME2");
			Log.i("json", "area - " + user_area + ", city - " + user_city);
			String user_introduce = curJobjProfile.getString("MM_INTRODUCE");
			String user_numFriend = curJobjProfile.getString("FRIEND_CNT");
			String user_numLike = curJobjProfile.getString("LIKE_CNT");
			String user_numShare = curJobjProfile.getString("SHARE_CNT");
			String user_numScrap = curJobjProfile.getString("SCRAP_CNT");
			String user_image_photo = curJobjProfile.getString("IMG_URL");
			String user_car_photo = curJobjProfile.getString("MM_CAR_URL");
			String user_car_name = curJobjProfile.getString("MM_CAR_NICK");
			double latX = 0;
			double lonY = 0;
			try {
				latX = Double.parseDouble(curJobjProfile
						.getString("MM_PLACE_X"));
				Log.i("lat", "" + latX);
				lonY = Double.parseDouble(curJobjProfile
						.getString("MM_PLACE_Y"));
				Log.i("lat", "" + lonY);
			} catch (Exception e) {

			}

			mProfile.init(user_idx, user_id, user_name, user_profile_photo,
					user_job, user_area, user_city,user_introduce, user_numFriend,
					user_numLike, user_numShare, user_numScrap,
					user_image_photo, user_car_photo, user_car_name, latX, lonY);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** FriendList **/
	public static boolean friendList(String mem_idx,
			ArrayList<MkFriend> friendList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		JSONObject jObj = getJsonObj(C.API_FRIEND_LIST, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			String frd_invite = jObj.getString("frd_apply_cnt");
			C.frd_invite = frd_invite;
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			friendList.clear();
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String friend_id = curJobj.getString("MM_USERID");
				String friend_nm = curJobj.getString("MM_NM");
				String friend_url_photo = curJobj
						.getString("MM_PROFILE_PIC_PATH");
				String friend_idx = curJobj.getString("MM_ID");

				friendList.add(new MkFriend(friend_idx, friend_id, friend_nm,
						friend_url_photo));
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** FriendReqList **/
	public static boolean friendReqList(String mem_idx,
			ArrayList<MkFriend> friendList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		JSONObject jObj = getJsonObj(C.API_FRIEND_LIST_REQ, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			String frd_invite = jObj.getString("frd_apply_cnt");
			C.frd_invite = frd_invite;
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			friendList.clear();
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String friend_id = curJobj.getString("MM_USERID");
				String friend_nm = curJobj.getString("MM_NM");
				String friend_url_photo = curJobj
						.getString("MM_PROFILE_PIC_PATH");
				String friend_idx = curJobj.getString("MF_REQ_MM_ID");

				friendList.add(new MkFriend(friend_idx, friend_id, friend_nm,
						friend_url_photo));
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** 친구요청수락 **/
	public static boolean friendAccept(String frd_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("frd_idx", frd_idx));
		nameValuePairs.add(new BasicNameValuePair("con", "okok"));
		JSONObject jObj = getJsonObj(C.API_FRIEND_ACCEPT, nameValuePairs);

		try {

			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			String frd_invite = jObj.getString("frd_apply_cnt");
			C.frd_invite = frd_invite;
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** 친구요청거절 **/
	public static boolean friendDeny(String frd_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("frd_idx", frd_idx));
		nameValuePairs.add(new BasicNameValuePair("con", "deny"));
		JSONObject jObj = getJsonObj(C.API_FRIEND_ACCEPT, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			String frd_invite = jObj.getString("frd_apply_cnt");
			C.frd_invite = frd_invite;
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** FriendDelete **/
	public static boolean friendDelete(String frd_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("frd_idx", frd_idx));
		JSONObject jObj = getJsonObj(C.API_FRIEND_DELETE, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** FriendRequest **/
	public static boolean friendRequest(String frd_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("frd_idx", frd_idx));
		JSONObject jObjFriendReq = getJsonObj(C.API_FRIEND_REQUEST,
				nameValuePairs);

		try {
			String returnStatus = jObjFriendReq.getString("returnStatus");
			String returnMessage = jObjFriendReq.getString("returnMessage");

			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** ProfileEdit (SettingProfileActivity) **/
	public static boolean editProfile() {

		return false;
	}

	/** 게시글목록 **/
	public static boolean postList(ArrayList<MkPost> postList, String mem_idx,
			String order, String writer_idx, int page_num) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		nameValuePairs.add(new BasicNameValuePair("order", order));
		nameValuePairs.add(new BasicNameValuePair("writer_idx", writer_idx));
		nameValuePairs.add(new BasicNameValuePair("page_num", page_num + ""));
		JSONObject jObjPost = getJsonObj(C.API_POSTLIST, nameValuePairs);

		if (page_num == 1) {
			postList.clear();
		}
		try {
			JSONArray jAryPostList = jObjPost.getJSONArray("list");
			for (int i = 0; i < jAryPostList.length(); i++) {
				JSONObject curJobjPost = jAryPostList.getJSONObject(i);

				String post_idx = curJobjPost.getString("CFB_ID");
				String writer_idx_post = curJobjPost.getString("CFB_MM_ID");
				String writer_id = curJobjPost.getString("CFB_MM_USERID");
				String category_cd = curJobjPost.getString("CFB_CTGY_CD");
				String content = curJobjPost.getString("CFB_DTL_TXT");
				int numLike = curJobjPost.getInt("CFB_LIKE_CNT");
				int numComment = curJobjPost.getInt("CFB_REPLY_CNT");
				// int num = curJobjPost.getString("CFB__CNT");
				String strDate = curJobjPost.getString("WRTDATE");
				String photo_idx = curJobjPost.getString("CPR_ID");
				String photo_url = curJobjPost.getString("CPR_PIC_URL");
				String photo_url_thumbnail = curJobjPost
						.getString("MM_PROFILE_PIC_PATH");
				String writer_name = curJobjPost.getString("MM_NM");
				String tag = curJobjPost.getString("TAG");
				String strIsLike = curJobjPost.getString("ISLIKE");
				String strIsScrap = curJobjPost.getString("ISSCRAP");
				String strImgCount = curJobjPost.getString("IMG_CNT");
				boolean isLike = false;
				boolean isScrap = false;
				Log.i("category", "" + category_cd);
				if (strIsLike.equals("Y")) {
					isLike = true;
				}
				if (strIsScrap.equals("Y")) {
					isScrap = true;
				}

				MkPost curPost = new MkPost(post_idx, writer_idx_post,
						writer_id, category_cd, content, numLike, numComment,
						strDate, photo_idx, photo_url, photo_url_thumbnail,
						writer_name, tag, isLike, isScrap, strImgCount);
				postList.add(curPost);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** 게시글삭제 **/
	public static boolean postDel(String bod_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_idx", bod_idx));
		JSONObject jObj = getJsonObj(C.API_POST_DEL, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** PostGCMList **/
	public static boolean postGCMList(String keyid, ArrayList<MkPost> postList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("alarm", keyid));
		// nameValuePairs.add(new BasicNameValuePair("alarm_idx", keyId));
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		JSONObject jObjPost = getJsonObj(C.API_POSTLIST, nameValuePairs);

		postList.clear();
		try {
			JSONArray jAryPostList = jObjPost.getJSONArray("list");
			for (int i = 0; i < jAryPostList.length(); i++) {
				JSONObject curJobjPost = jAryPostList.getJSONObject(i);

				String post_idx = curJobjPost.getString("CFB_ID");
				String writer_idx_post = curJobjPost.getString("CFB_MM_ID");
				String writer_id = curJobjPost.getString("CFB_MM_USERID");
				String category_cd = curJobjPost.getString("CFB_CTGY_CD");
				String content = curJobjPost.getString("CFB_DTL_TXT");
				int numLike = curJobjPost.getInt("CFB_LIKE_CNT");
				int numComment = curJobjPost.getInt("CFB_REPLY_CNT");

				Log.i("category", "" + category_cd);
				String strDate = curJobjPost.getString("WRTDATE");
				String photo_idx = curJobjPost.getString("CPR_ID");
				String photo_url = curJobjPost.getString("CPR_PIC_URL");
				String photo_url_thumbnail = curJobjPost
						.getString("MM_PROFILE_PIC_PATH");
				String writer_name = curJobjPost.getString("MM_NM");
				String tag = curJobjPost.getString("TAG");
				String strIsLike = curJobjPost.getString("ISLIKE");
				String strIsScrap = curJobjPost.getString("ISSCRAP");
				boolean isLike = false;
				boolean isScrap = false;
				if (strIsLike.equals("Y")) {
					isLike = true;
				}
				if (strIsScrap.equals("Y")) {
					isScrap = true;
				}

				MkPost curPost = new MkPost(post_idx, writer_idx_post,
						writer_id, category_cd, content, numLike, numComment,
						strDate, photo_idx, photo_url, photo_url_thumbnail,
						writer_name, tag, isLike, isScrap);
				postList.add(curPost);
				Log.i("size3", "" + postList.size());
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** PostSearchList **/
	public static boolean postSearchList(ArrayList<MkPost> postList,
			String mem_idx, int page_num, String type, String txt, String sort) {
		String type1 = type;
		String txt1 = txt;
		String sort1 = sort;
		Log.d(TAG, type + txt + sort);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		nameValuePairs.add(new BasicNameValuePair("page_num", page_num + ""));
		nameValuePairs.add(new BasicNameValuePair("type", type1));
		nameValuePairs.add(new BasicNameValuePair("txt", txt1));
		nameValuePairs.add(new BasicNameValuePair("sort", sort1));
		JSONObject jObjPost = getJsonObj(C.API_POST_SEARCH_LIST, nameValuePairs);

		if (page_num == 1) {
			postList.clear();
		}
		try {
			JSONArray jAryPostList = jObjPost.getJSONArray("list");
			for (int i = 0; i < jAryPostList.length(); i++) {
				JSONObject curJobjPost = jAryPostList.getJSONObject(i);

				String post_idx = curJobjPost.getString("CFB_ID");
				String writer_idx_post = curJobjPost.getString("CFB_MM_ID");
				String writer_id = curJobjPost.getString("CFB_MM_USERID");
				String category_cd = curJobjPost.getString("CFB_CTGY_CD");
				String content = curJobjPost.getString("CFB_DTL_TXT");
				String imgCNT = curJobjPost.getString("IMG_CNT");
				int numLike = curJobjPost.getInt("CFB_LIKE_CNT");
				int numComment = curJobjPost.getInt("CFB_REPLY_CNT");
				
				// int num = curJobjPost.getString("CFB__CNT");
				String strDate = curJobjPost.getString("WRTDATE");
				String photo_idx = curJobjPost.getString("CPR_ID");
				String photo_url = curJobjPost.getString("CPR_PIC_URL");
				String photo_url_thumbnail = curJobjPost
						.getString("MM_PROFILE_PIC_PATH");
				String writer_name = curJobjPost.getString("MM_NM");
				String tag = curJobjPost.getString("TAG");
				String strIsLike = curJobjPost.getString("ISLIKE");
				String strIsScrap = curJobjPost.getString("ISSCRAP");
				boolean isLike = false;
				boolean isScrap = false;
				if (strIsLike.equals("Y")) {
					isLike = true;
				}
				if (strIsScrap.equals("Y")) {
					isScrap = true;
				}

				MkPost curPost = new MkPost(post_idx, writer_idx_post,
						writer_id, category_cd, content, numLike, numComment,
						strDate, photo_idx, photo_url, photo_url_thumbnail,
						writer_name, tag, isLike, isScrap,imgCNT);
				postList.add(curPost);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** CommentList **/
	public static boolean commentList(ArrayList<MkComment> commentList,
			String post_idx, String post_type) {
		Log.i("getview", "getview");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("bod_idx", post_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_type", post_type));
		JSONObject jObjComment = getJsonObj(C.API_COMMENT_LIST, nameValuePairs);

		try {
			String returnStatus = jObjComment.getString("returnStatus");
			String returnMessage = jObjComment.getString("returnMessage");
			returnMsg = returnMessage;
			JSONArray jAryCommentList = jObjComment.getJSONArray("list");

			// 댓글 없을시 이부분 catch로 넘어감
			for (int i = 0; i < jAryCommentList.length(); i++) {
				JSONObject curJobjComment = jAryCommentList.getJSONObject(i);
				Log.d(TAG, "CommentList pos " + i + " : " + returnStatus + ", "
						+ returnMessage);
				String comment_idx = curJobjComment.getString("re_idx");
				// String post_idx // 인수임
				String writer_idx = curJobjComment.getString("useridx");
				String writer_id = curJobjComment.getString("userid");
				String writer_name = curJobjComment.getString("username");
				String photo_url_thumbnail = curJobjComment.getString("pic");
				String content = curJobjComment.getString("txt");
				String strDate = curJobjComment.getString("date");

				MkComment curComment = new MkComment(comment_idx, post_idx,
						writer_idx, writer_id, writer_name,
						photo_url_thumbnail, content, strDate);
				commentList.add(curComment);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** CommentAdd **/
	public static boolean commentAdd(String writer_idx, String post_idx,
			String post_type, String content) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_idx", post_idx));
		nameValuePairs.add(new BasicNameValuePair("reply_txt", content));
		nameValuePairs.add(new BasicNameValuePair("bod_type", post_type));
		JSONObject jObjCommentAdd = getJsonObj(C.API_COMMENT_ADD,
				nameValuePairs);

		try {
			String returnStatus = jObjCommentAdd.getString("returnStatus");
			String returnMessage = jObjCommentAdd.getString("returnMessage");

			returnMsg = returnMessage;
			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** CommentDelete **/
	public static boolean commentDelete(String re_idx, String bod_idx,
			String bod_type) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("re_idx", re_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_idx", bod_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_type", bod_type));
		JSONObject jObj = getJsonObj(C.API_COMMENT_DEL, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** LikeToggle **/
	public static boolean likeToggle(String mem_idx, String bod_idx,
			String bod_type) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_idx", bod_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_type", bod_type));
		JSONObject jObjLike = getJsonObj(C.API_LIKE_TOGGLE, nameValuePairs);

		try {
			String returnStatus = jObjLike.getString("returnStatus");
			String returnMessage = jObjLike.getString("returnMessage");

			returnMsg = returnMessage;
			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** NoticeList **/
	public static boolean noticeList(ArrayList<MkNotice> noticeList,
			int page_num) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("page_num", page_num + ""));
		JSONObject jObjNoticeList = getJsonObj(C.API_NOTICE_LIST);

		try {
			String returnStatus = jObjNoticeList.getString("returnStatus");
			String returnMessage = jObjNoticeList.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAryList = jObjNoticeList.getJSONArray("list");
			for (int i = 0; i < jAryList.length(); i++) {
				JSONObject jObjCurNotice = jAryList.getJSONObject(i);

				String idx = jObjCurNotice.getString("BN_ID");
				String title = jObjCurNotice.getString("BN_TITLE_TXT");
				String content = jObjCurNotice.getString("BN_DETAIL_TXT");
				// int hits = jObjCurNotice.getInt("BN_READ_CNT");
				// String writer_idx = jObjCurNotice.getString("WUSERID");
				String strDate = jObjCurNotice.getString("WRTDATE");

				int hits = 0;
				String writer_idx = "";

				noticeList.add(new MkNotice(idx, title, content, hits,
						writer_idx, strDate));
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** ProfileEditView **/
	public static boolean getSettingProfile(String mem_idx,
			SettingProfile mProfile) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		JSONObject jObjGetProfile = getJsonObj(C.API_SETTING_PROFILE_GET,
				nameValuePairs);

		try {
			String returnStatus = jObjGetProfile.getString("returnStatus");
			String returnMessage = jObjGetProfile.getString("returnMessage");

			returnMsg = returnMessage;

			String url_photo = jObjGetProfile.getString("MM_PROFILE_PIC_PATH");
			String introduce = jObjGetProfile.getString("MM_INTRODUCE");
			String answer = jObjGetProfile.getString("MM_QUESTION_ANS");
			String question = jObjGetProfile.getString("MM_QUESTION_NAME");
			String job = jObjGetProfile.getString("MM_JOB_NAME");
			String area = jObjGetProfile.getString("MM_RGN_NAME");
			String city = jObjGetProfile.getString("MM_RGN_NAME2");
			
			String call = jObjGetProfile.getString("MM_TEL_NUM");

			mProfile.init(url_photo, introduce, answer, question, job, area, city,call);

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** Album **/
	public static boolean album(String mem_idx, ArrayList<Album> albumList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		JSONObject jObjAlbum = getJsonObj(C.API_ALBUM, nameValuePairs);

		try {
			String returnStatus = jObjAlbum.getString("returnStatus");
			String returnMessage = jObjAlbum.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAryAlbum = jObjAlbum.getJSONArray("list");
			for (int i = 0; i < jAryAlbum.length(); i++) {
				JSONObject curJobjAlbum = jAryAlbum.getJSONObject(i);

				String idx = curJobjAlbum.getString("CPR_ID");
				String urlPhotoOri = curJobjAlbum.getString("CPR_PIC_URL");
				int numLike = curJobjAlbum.getInt("CPR_LIKE_CNT");
				int numScrap = curJobjAlbum.getInt("CPR_SHARE_CNT");
				int numComment = curJobjAlbum.getInt("CPR_REPLY_CNT");
				String strIsLike = curJobjAlbum.getString("ISLIKE");
				boolean isLike = false;
				if (strIsLike.equals("Y")) {
					isLike = true;
				}

				String urlOri = C.MK_BASE + urlPhotoOri;
				String urlThumbnail = urlOri;

				albumList.add(new Album(idx, urlThumbnail, urlOri, numLike,
						numComment, numScrap, isLike));
			}
			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** AlbumInPost **/
	public static boolean albumInPost(String bod_idx, ArrayList<Album> albumList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_idx", bod_idx));
		JSONObject jObjAlbum = getJsonObj(C.API_ALBUM_IN_POST, nameValuePairs);

		try {
			String returnStatus = jObjAlbum.getString("returnStatus");
			String returnMessage = jObjAlbum.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAryAlbum = jObjAlbum.getJSONArray("list");
			for (int i = 0; i < jAryAlbum.length(); i++) {
				JSONObject curJobjAlbum = jAryAlbum.getJSONObject(i);

				String idx = curJobjAlbum.getString("CPR_ID");
				String urlPhotoOri = curJobjAlbum.getString("CPR_PIC_URL");
				int numLike = curJobjAlbum.getInt("CPR_LIKE_CNT");
				int numShare = curJobjAlbum.getInt("CPR_SHARE_CNT");
				int numComment = curJobjAlbum.getInt("CPR_REPLY_CNT");
				// String strIsLike = curJobjAlbum.getString("ISLIKE");
				// boolean isLike = false;
				// if(strIsLike.equals("Y")){
				// isLike = true;
				// }
				boolean isLike = false;

				String urlOri = C.MK_BASE + urlPhotoOri;
				String urlThumbnail = urlOri;

				albumList.add(new Album(idx, urlThumbnail, urlOri, numLike,
						numComment, numShare, isLike));
			}
			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** SettingChangePw **/
	public static boolean changePw(String oriPw, String newPw) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		String oriPwMD5 = Lhy_Function.getMD5Hash(oriPw);
		String newPwMD5 = Lhy_Function.getMD5Hash(newPw);
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("one_pass", oriPwMD5));
		nameValuePairs.add(new BasicNameValuePair("new_pass", newPwMD5));
		JSONObject jObj = getJsonObj(C.API_SETTING_CHANGE_PW, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** ScrapList **/
	public static boolean scrapList(ArrayList<MkPost> postList, String mem_idx,
			String writer_idx, int page_num) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		// nameValuePairs.add(new BasicNameValuePair("writer_idx", writer_idx));
		nameValuePairs.add(new BasicNameValuePair("page_num", page_num + ""));
		JSONObject jObjPost = getJsonObj(C.API_SCRAP_LIST, nameValuePairs);

		if (page_num == 1) {
			postList.clear();
		}
		try {
			String returnStatus = jObjPost.getString("returnStatus");
			String returnMessage = jObjPost.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAryPostList = jObjPost.getJSONArray("list");
			for (int i = 0; i < jAryPostList.length(); i++) {
				JSONObject curJobjPost = jAryPostList.getJSONObject(i);

				String post_idx = curJobjPost.getString("CFB_ID");
				String writer_idx_post = curJobjPost.getString("CFB_MM_ID");
				String writer_id = curJobjPost.getString("CFB_MM_USERID");
				String category_cd = curJobjPost.getString("CFB_CTGY_CD");
				String content = curJobjPost.getString("CFB_DTL_TXT");
				int numLike = curJobjPost.getInt("CFB_LIKE_CNT");
				int numComment = curJobjPost.getInt("CFB_REPLY_CNT");
				// int num = curJobjPost.getString("CFB__CNT");
				String strDate = curJobjPost.getString("WRTDATE");
				String photo_idx = curJobjPost.getString("CPR_ID");
				String photo_url = curJobjPost.getString("CPR_PIC_URL");
				String photo_url_thumbnail = curJobjPost
						.getString("MM_PROFILE_PIC_PATH");
				String writer_name = curJobjPost.getString("MM_NM");
				String tag = curJobjPost.getString("TAG");
				String strIsLike = curJobjPost.getString("ISLIKE");
				String strIsScrap = curJobjPost.getString("ISSCRAP");
				boolean isLike = false;
				boolean isScrap = false;
				if (strIsLike.equals("Y")) {
					isLike = true;
				}
				if (strIsScrap.equals("Y")) {
					isScrap = true;
				}
				MkPost curPost = new MkPost(post_idx, writer_idx_post,
						writer_id, category_cd, content, numLike, numComment,
						strDate, photo_idx, photo_url, photo_url_thumbnail,
						writer_name, tag, isLike, isScrap);
				postList.add(curPost);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** Scrap **/
	public static boolean scrap(String bod_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("bod_idx", bod_idx));
		JSONObject jObj = getJsonObj(C.API_SCRAP, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** MessageList **/
	public static boolean messageList(ArrayList<MkMsg> msgList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		JSONObject jObj = getJsonObj(C.API_MESSAGE_LIST, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String idx = curJobj.getString("CMC_ROOM");
				String own_user_idx = curJobj.getString("CMC_OWN_MM_ID");
				String opp_user_idx = curJobj.getString("CMC_RECP_MM_ID");
				Log.d(TAG, "MessageList opp(recv)_user : " + opp_user_idx);
				String content = curJobj.getString("CMC_MSG_TXT");
				String type = curJobj.getString("CMC_TYPE");
				String sendTime = curJobj.getString("CMC_SND_DTHMS");
				String receiveTime = curJobj.getString("CMC_RCV_DTHMS");
				String writeTime = curJobj.getString("WRTDATE");
				String opp_user_name = curJobj.getString("MM_NM");
				String opp_user_url_photo = curJobj
						.getString("MM_PROFILE_PIC_PATH");

				MkMsg curMsg = new MkMsg(idx, own_user_idx, opp_user_idx,
						content, type, sendTime, receiveTime, writeTime,
						opp_user_name, opp_user_url_photo);
				msgList.add(curMsg);
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean messageGCMList(String oth_idx,
			ArrayList<MkMsg> msgList) {
		// oth_idx
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("oth_idx", oth_idx));
		JSONObject jObj = getJsonObj(C.API_MESSAGE_LIST, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String idx = curJobj.getString("CMC_ROOM");
				String own_user_idx = curJobj.getString("CMC_OWN_MM_ID");
				String opp_user_idx = curJobj.getString("CMC_RECP_MM_ID");
				Log.d(TAG, "MessageList opp(recv)_user : " + opp_user_idx);
				String content = curJobj.getString("CMC_MSG_TXT");
				String type = curJobj.getString("CMC_TYPE");
				String sendTime = curJobj.getString("CMC_SND_DTHMS");
				String receiveTime = curJobj.getString("CMC_RCV_DTHMS");
				String writeTime = curJobj.getString("WRTDATE");
				String opp_user_name = curJobj.getString("MM_NM");
				String opp_user_url_photo = curJobj
						.getString("MM_PROFILE_PIC_PATH");

				MkMsg curMsg = new MkMsg(idx, own_user_idx, opp_user_idx,
						content, type, sendTime, receiveTime, writeTime,
						opp_user_name, opp_user_url_photo);
				msgList.add(curMsg);
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** MessageView **/
	public static boolean messageDetail(String own_idx, String opp_idx,
			int start_num, ArrayList<MkMsg> msgList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("own_idx", own_idx));
		nameValuePairs.add(new BasicNameValuePair("rec_idx", opp_idx));
		nameValuePairs.add(new BasicNameValuePair("start_num", start_num + ""));
		Log.d(TAG, "Detail : " + "mem : " + C.mem_idx + ", own : " + own_idx
				+ ", rec : " + opp_idx);
		JSONObject jObj = getJsonObj(C.API_MESSAGE_DETAIL, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			msgList.clear();
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String idx = curJobj.getString("CMC_ROOM");
				String own_user_idx = curJobj.getString("CMC_OWN_MM_ID");
				String opp_user_idx = curJobj.getString("CMC_RECP_MM_ID");
				String content = curJobj.getString("CMC_MSG_TXT");
				String type = curJobj.getString("CMC_ME_TYPE");
				String sendTime = curJobj.getString("CMC_SND_DTHMS");
				String receiveTime = curJobj.getString("CMC_RCV_DTHMS");
				String writeTime = curJobj.getString("WRTDATE");
				String opp_user_name = curJobj.getString("MM_NM");
				String opp_user_url_photo = curJobj
						.getString("MM_PROFILE_PIC_PATH");

				MkMsg curMsg = new MkMsg(idx, own_user_idx, opp_user_idx,
						content, type, sendTime, receiveTime, writeTime,
						opp_user_name, opp_user_url_photo);
				msgList.add(curMsg);
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** MessageWrite **/
	public static boolean messageWrite(String own_idx, String opp_idx,
			String content) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("own_idx", own_idx));
		nameValuePairs.add(new BasicNameValuePair("rec_idx", opp_idx));
		Log.d(TAG, "Write : " + "mem : " + C.mem_idx + ", own : " + own_idx
				+ ", rec : " + opp_idx);
		nameValuePairs.add(new BasicNameValuePair("txt", content));
		JSONObject jObj = getJsonObj(C.API_MESSAGE_WRITE, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** MessageDelete **/
	public static boolean messageDelete(String cmc_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("cmc_idx", cmc_idx));

		Log.d("delete", "mem_idx : " + "mem : " + C.mem_idx + ", cmc_idx : "
				+ cmc_idx);
		JSONObject jObj = getJsonObj(C.API_MESSAGE_DELETE, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			String sql = jObj.getString("sql");
			returnMsg = returnMessage;

			Log.i("delete", "returnstatus - " + returnStatus);
			Log.i("delete", "returnmessage - " + returnMessage);
			Log.i("delete", "sql - " + sql);
			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** 프로필메세지보내기 **/
	public static boolean messageWriteProfile(String own_idx, String opp_idx,
			String content) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("own_idx", own_idx));
		nameValuePairs.add(new BasicNameValuePair("rec_idx", opp_idx));
		nameValuePairs.add(new BasicNameValuePair("profile_send", "1"));
		Log.d(TAG, "Write : " + "mem : " + C.mem_idx + ", own : " + own_idx
				+ ", rec : " + opp_idx);
		nameValuePairs.add(new BasicNameValuePair("txt", content));
		JSONObject jObj = getJsonObj(C.API_MESSAGE_WRITE, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** LBS **/
	public static boolean lbs(String type, double lat, double lon,
			ArrayList<MkPlace> placeList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("type", type));
		nameValuePairs.add(new BasicNameValuePair("lat", lat + ""));
		nameValuePairs.add(new BasicNameValuePair("lon", lon + ""));
		JSONObject jObj = getJsonObj(C.API_LBS, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			placeList.clear();
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String name = curJobj.getString("name");
				String idx = curJobj.getString("BCI_ID");
				String latX = curJobj.getString("BCI_GPS_X");
				String lonY = curJobj.getString("BCI_GPS_Y");
				String distance = curJobj.getString("distance_real");
				boolean isCheck = false;

				MkPlace curPlace = new MkPlace(name, idx, latX, lonY, distance,
						isCheck);
				placeList.add(curPlace);
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** 글쓰기장소목록 **/
	public static boolean getWritePlaceList(String search, double lat,
			double lon, ArrayList<MkPlaceNew> placeList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("search", search));
		nameValuePairs.add(new BasicNameValuePair("lat", lat + ""));
		nameValuePairs.add(new BasicNameValuePair("lon", lon + ""));
		JSONObject jObj = getJsonObj(C.API_WRITE_PLACE, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			placeList.clear();
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String BPI_NM = curJobj.getString("BPI_NM");
				String distance = curJobj.getString("distance_real");

				MkPlaceNew curPlace = new MkPlaceNew(BPI_NM, distance);
				placeList.add(curPlace);
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** 글쓰기장소등록 **/
	public static boolean addWritePlace(String name, double lat, double lon) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("name", name));
		nameValuePairs.add(new BasicNameValuePair("lat", lat + ""));
		nameValuePairs.add(new BasicNameValuePair("lon", lon + ""));
		JSONObject jObj = getJsonObj(C.API_WRITE_PLACE_ADD, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** AlarmList(알림목록) **/
	public static boolean alarmList(ArrayList<MkAlarm> alarmList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		JSONObject jObj = getJsonObj(C.API_ALARM_LIST, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			alarmList.clear();
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String idx = curJobj.getString("NN_ID");
				String content = curJobj.getString("NN_NOTE");
				String url_photo = curJobj.getString("MM_PROFILE_PIC_PATH");
				String write_time = curJobj.getString("WRTDATE");
				String kindCode = curJobj.getString("NN_KIND_CD");
				String keyId = curJobj.getString("NN_KEY_ID");
				String opp_name = curJobj.getString("MM_NM");

				MkAlarm curAlarm = new MkAlarm(idx, content, url_photo,
						write_time, kindCode, keyId, opp_name);
				alarmList.add(curAlarm);
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** 알림확인 **/
	public static boolean alarmCheck(String nn_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("nn_idx", nn_idx));
		JSONObject jObj = getJsonObj(C.API_ALARM_CHECK, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** CheckInList **/
	public static boolean checkInList(String mem_idx, int page_num,
			ArrayList<MkCheckIn> checkInList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("page_num", page_num + ""));
		JSONObject jObj = getJsonObj(C.API_CHECKIN_LIST, nameValuePairs);
		Log.d(TAG, "CheckInList Page : " + page_num);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String idx = curJobj.getString("CFB_ID");
				// String bci_idx = curJobj.getString("BPI_NM");
				String bci_idx = "";
				String write_date = curJobj.getString("WRTDATE");
				String bci_nm = curJobj.getString("BPI_NM");
				String url_user_photo = curJobj
						.getString("MM_PROFILE_PIC_PATH");
				String user_nm = curJobj.getString("MM_NM");
				String user_idx = curJobj.getString("MM_ID");
				String content = curJobj.getString("CFB_DTL_TXT");
				int numLike = curJobj.getInt("CFB_LIKE_CNT");
				int numComment = curJobj.getInt("CFB_REPLY_CNT");
				int numScrap = curJobj.getInt("CFB_SCRAP_CNT");
				// String call = curJobj.getString("BCI_CONTACT_NUM1");
				String call = "";
				// String shopName = curJobj.getString("CC_NM");
				String shopName = "";
				// String shopCode = curJobj.getString("CC_CD");
				String shopCode = "";
				String strIsLike = curJobj.getString("ISLIKE");
				boolean isLike = false;
				if (strIsLike.equals("Y")) {
					isLike = true;
				}

				// int num = curJobj.getInt("CFB__CNT");
				int numImage = Integer.parseInt(curJobj.getString("IMG_CNT"));
				String image_idx = curJobj.getString("CPR_ID");
				String url_image = curJobj.getString("CPR_PIC_URL");
				String tag = curJobj.getString("TAG");

				checkInList.add(new MkCheckIn(idx, bci_idx, write_date, bci_nm,
						url_user_photo, user_nm, user_idx, content, numLike,
						numComment, numScrap, call, shopCode, shopName, isLike,
						false, numImage, image_idx, url_image, tag));
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** CheckInDetailList **/
	public static boolean checkInDetailList(String mem_idx, int page_num,
			String bpi_nm, String cc_cd, String cc_name, String cc_type,
			String cc_tel, ArrayList<MkCheckIn> checkInList) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("page_num", page_num + ""));
		nameValuePairs.add(new BasicNameValuePair("bpi_nm", bpi_nm));
		nameValuePairs.add(new BasicNameValuePair("cc_cd", cc_cd));
		nameValuePairs.add(new BasicNameValuePair("cc_name", cc_name));
		nameValuePairs.add(new BasicNameValuePair("cc_type", cc_type));
		nameValuePairs.add(new BasicNameValuePair("cc_tel", cc_tel));
		JSONObject jObj = getJsonObj(C.API_CHECKIN_DETAIL_LIST, nameValuePairs);

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAry = jObj.getJSONArray("list");
			for (int i = 0; i < jAry.length(); i++) {
				JSONObject curJobj = jAry.getJSONObject(i);
				String idx = curJobj.getString("CFB_ID");
				// String bci_idx = curJobj.getString("BCI_ID");
				String bci_idx = "";
				String write_date = curJobj.getString("WRTDATE");
				// String bci_nm = curJobj.getString("BCI_NM");
				String bci_nm = "";
				String url_user_photo = curJobj
						.getString("MM_PROFILE_PIC_PATH");
				String user_nm = curJobj.getString("MM_NM");
				String user_idx = curJobj.getString("MM_ID");
				String content = curJobj.getString("CFB_DTL_TXT");
				int numLike = curJobj.getInt("CFB_LIKE_CNT");
				int numComment = curJobj.getInt("CFB_REPLY_CNT");
				int numScrap = curJobj.getInt("CFB_SCRAP_CNT");
				// String call = curJobj.getString("BCI_CONTACT_NUM");
				String call = "";
				// String shopName = curJobj.getString("CC_NM");
				String shopName = "";
				// String shopCode = curJobj.getString("CC_CD");
				String shopCode = "";
				String strIsLike = curJobj.getString("ISLIKE");
				boolean isLike = false;
				if (strIsLike.equals("Y")) {
					isLike = true;
				}
				String strIsScrap = curJobj.getString("ISSCRAP");
				boolean isScrap = false;
				if (strIsLike.equals("Y")) {
					isLike = true;
				}
				// int num = curJobj.getInt("CFB__CNT");
				int numImage = Integer.parseInt(curJobj.getString("IMG_CNT"));
				String image_idx = curJobj.getString("CPR_ID");
				String url_image = curJobj.getString("CPR_PIC_URL");
				String tag = curJobj.getString("TAG");

				checkInList.add(new MkCheckIn(idx, bci_idx, write_date, bci_nm,
						url_user_photo, user_nm, user_idx, content, numLike,
						numComment, numScrap, call, shopCode, shopName, isLike,
						isScrap, numImage, image_idx, url_image, tag));
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** Exit (Setting-Exit) **/
	public static boolean exit() {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		nameValuePairs.add(new BasicNameValuePair("mem_cause", "꺼졍"));
		JSONObject jObjExit = getJsonObj(C.API_EXIT, nameValuePairs);

		try {
			String returnStatus = jObjExit.getString("returnStatus");
			String returnMessage = jObjExit.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static String settingPush() {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", C.mem_idx));
		JSONObject jObjExit = getJsonObj(C.API_PUSH_CHECK, nameValuePairs);

		try {
			String returnStatus = jObjExit.getString("returnStatus");
			String returnMessage = jObjExit.getString("returnMessage");
			returnMsg = returnMessage;

			if (returnStatus.equals("ON")) {
				return returnMessage;
			} else if (returnStatus.equals("OFF")) {
				return returnMessage;
			} else {
				return returnMessage;
			}
		} catch (JSONException e) {
			e.printStackTrace();

		}
		return returnMsg;
	}

	/** MyCarProfile(자동차 수정 불러오기) **/
	public static boolean carInfo(String MMC_ID, MkCarListProfileEdit mCar) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("MMC_ID", MMC_ID));
		JSONObject jObjCarInfo = getJsonObj(C.API_CAR, nameValuePairs);

		try {
			String returnStatus = jObjCarInfo.getString("returnStatus");
			String returnMessage = jObjCarInfo.getString("returnMessage");
			returnMsg = returnMessage;

			String idx = jObjCarInfo.getString("MMC_ID");
			String typeCode = jObjCarInfo.getString("CAR_TYPE_CD");
			String typeName = jObjCarInfo.getString("CAR_TYPE_NM");
			String modelCode = jObjCarInfo.getString("CAR_CD");
			String modelName = jObjCarInfo.getString("CAR_NM");
			String makerCode = jObjCarInfo.getString("CAR_COMPANY_CD");
			String makerName = jObjCarInfo.getString("CAR_COMPANY_NM");
			String nameCode = jObjCarInfo.getString("CAR_KIND_CD");
			String nameName = jObjCarInfo.getString("CAR_KIND_NM");
			String urlPhoto = jObjCarInfo.getString("PIC_URL");
			String nick = jObjCarInfo.getString("CAR_NICK");
			String year = jObjCarInfo.getString("CAR_YEAR");
			String strIsMainCar = jObjCarInfo.getString("CAR_MAIN");
			String strIsPublic = jObjCarInfo.getString("CAR_HIDDEN_CD");
			String introduce = jObjCarInfo.getString("CAR_INTRODUCE");
			Log.i("car", "" + modelName);
			Log.i("car", "" + makerName);
			Log.i("car", "" + nameName);
			boolean isMainCar = false;
			if (strIsMainCar.equals("M")) {
				isMainCar = true;
			}
			boolean isPublic = false;
			if (strIsPublic.equals("Y")) {
				isPublic = true;
			}
			String carName = jObjCarInfo.getString("CAR_NAME");
			String period1 = jObjCarInfo.getString("CAR_FIRST_DATE");
			String period2 = jObjCarInfo.getString("CAR_END_DATE");

			mCar.init(idx, makerCode, makerName, typeCode, typeName, nameCode,
					nameName, modelCode, modelName, urlPhoto, nick, year,
					isMainCar, isPublic, carName, period1, period2, introduce);

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** MkCar **/
	// public static boolean getMkCarList(){
	// if(C.LIST_MKCAR.size()>0) {
	// return true;
	// }
	// JSONObject jObj = getJsonObj(C.API_MKCAR);
	// C.LIST_MKCAR = new ArrayList<MkCar>();
	//
	// try{
	// String returnStatus = jObj.getString("returnStatus");
	// String returnMessage = jObj.getString("returnMessage");
	// returnMsg = returnMessage;
	//
	// JSONArray jAryList = jObj.getJSONArray("list");
	// for(int i=0; i<jAryList.length(); i++){
	// JSONObject curJobj = jAryList.getJSONObject(i);
	// String makerCode = curJobj.getString("CC_CD");
	// String makerName = curJobj.getString("CC_NM");
	//
	// // 추가될 임시변수
	// MkCode mkMaker = new MkCode(makerName, makerCode);
	// ArrayList<MkCode> curCarList = new ArrayList<MkCode>();
	// Log.d(TAG, "Maker : " + makerCode + ", " + makerName);
	//
	// JSONArray curJary = curJobj.getJSONArray("list");
	// for(int j=0; j<curJary.length(); j++){
	// JSONObject curCarJobj = curJary.getJSONObject(j);
	// String carCode = curCarJobj.getString("CC_CD");
	// String carName = curCarJobj.getString("CC_NM");
	//
	// MkCode mkCurCar = new MkCode(carName, carCode);
	// curCarList.add(mkCurCar);
	//
	// Log.d(TAG, " Car : " + carCode + ", " + carName);
	// }
	//
	// C.LIST_MKCAR.add(new MkCar(mkMaker, curCarList));
	// Log.d(TAG, "-------------------");
	// }
	//
	// if(returnStatus.equals("Y")){
	// return true;
	// } else{
	// return false;
	// }
	// } catch(Exception e){
	// e.printStackTrace();
	// return false;
	// }
	// }
	public static boolean getMkCarList() {
		if (C.LIST_MKCAR.size() > 0) {
			return true;
		}
		JSONObject jObj = getJsonObj(C.API_MKCAR);
		C.LIST_MKCAR = new ArrayList<MkCar>();

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAryList = jObj.getJSONArray("list");
			for (int i = 0; i < jAryList.length(); i++) {
				JSONObject curJobj = jAryList.getJSONObject(i);
				String makerCode = curJobj.getString("CC_CD");
				String makerName = curJobj.getString("CC_NM");

				ArrayList<ArrayList<MkCode>> arrModel = new ArrayList<ArrayList<MkCode>>();

				// 추가될 임시변수
				MkCode mkMaker = new MkCode(makerName, makerCode);
				ArrayList<MkCode> curCarList = new ArrayList<MkCode>();
				Log.d(TAG, "Maker : " + makerCode + ", " + makerName);

				JSONArray curJary = curJobj.getJSONArray("list");
				for (int j = 0; j < curJary.length(); j++) {
					JSONObject curCarJobj = curJary.getJSONObject(j);
					String carCode = curCarJobj.getString("CC_CD");
					String carName = curCarJobj.getString("CC_NM");

					MkCode mkCurCar = new MkCode(carName, carCode);
					curCarList.add(mkCurCar);

					JSONArray curSub = curCarJobj.getJSONArray("list");
					Log.d(TAG, " Car : " + carCode + ", " + carName);
					ArrayList<MkCode> curCarSubList = new ArrayList<MkCode>();
					for (int k = 0; k < curSub.length(); k++) {
						JSONObject curSubJobj = curSub.getJSONObject(k);
						String subCode = curSubJobj.getString("CC_CD");
						String subName = curSubJobj.getString("CC_NM");

						MkCode mkSubCar = new MkCode(subName, subCode);
						curCarSubList.add(mkSubCar);
						Log.d(TAG + k, " Car : " + subCode + ", " + subName + k);
					}
					arrModel.add(curCarSubList);
				}

				C.LIST_MKCAR.add(new MkCar(mkMaker, curCarList, arrModel));
				Log.d(TAG, "-------------------");
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean getMkCarMainList() {
		if (C.LIST_MKCAR.size() > 0) {
			return true;
		}
		JSONObject jObj = getJsonObj(C.API_MKCAR);
		C.LIST_MKCAR = new ArrayList<MkCar>();

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAryList = jObj.getJSONArray("list");
			for (int i = 0; i < jAryList.length(); i++) {
				JSONObject curJobj = jAryList.getJSONObject(i);
				String makerCode = curJobj.getString("CC_CD");
				String makerName = curJobj.getString("CC_NM");

				// 추가될 임시변수
				MkCode mkMaker = new MkCode(makerName, makerCode);
				ArrayList<MkCode> curCarList = new ArrayList<MkCode>();
				Log.d(TAG, "Maker : " + makerCode + ", " + makerName);

				ArrayList<ArrayList<MkCode>> arrModel = new ArrayList<ArrayList<MkCode>>();

				JSONArray curJary = curJobj.getJSONArray("list");
				for (int j = 0; j < curJary.length(); j++) {
					JSONObject curCarJobj = curJary.getJSONObject(j);
					String carCode = curCarJobj.getString("CC_CD");
					String carName = curCarJobj.getString("CC_NM");

					MkCode mkCurCar = new MkCode(carName, carCode);
					curCarList.add(mkCurCar);

					ArrayList<MkCode> curCarSubList = new ArrayList<MkCode>();

					JSONArray curSub = curCarJobj.getJSONArray("list");
					Log.d(TAG, " Car : " + carCode + ", " + carName);
					for (int k = 0; k < curSub.length(); k++) {
						JSONObject curSubJobj = curSub.getJSONObject(k);
						String subCode = curSubJobj.getString("CC_CD");
						String subName = curSubJobj.getString("CC_NM");

						MkCode mkSubCar = new MkCode(subName, subCode);
						curCarSubList.add(mkSubCar);

						Log.d(TAG + k, " Car : " + subCode + ", " + subName + k);
					}
					arrModel.add(curCarSubList);
				}
				C.LIST_MKCAR.add(new MkCar(mkMaker, curCarList, arrModel));
				Log.d(TAG, "-------------------");
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** CarProfileList(카 프로필 목록) **/
	public static boolean getCarProfileList(String mem_idx, String you_idx) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("mem_idx", mem_idx));
		nameValuePairs.add(new BasicNameValuePair("you_idx", you_idx));
		JSONObject jObj = getJsonObj(C.API_PROFILE_CAR_LIST, nameValuePairs);
		C.LIST_MK_CAR_PROFILE.clear();

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAryList = jObj.getJSONArray("list");
			for (int i = 0; i < jAryList.length(); i++) {
				JSONObject curJObj = jAryList.getJSONObject(i);
				String idx = curJObj.getString("MMC_ID");
				String nick = curJObj.getString("MMC_CARNICK");
				String year = curJObj.getString("MMC_YEAR");
				String indicatorName = curJObj.getString("MMC_YEAR2");
				String type = curJObj.getString("MMC_TYPE");
				String introduce = curJObj.getString("MMC_INTRODUCE");
				String period1 = curJObj.getString("MMC_FIRST_DATE");
				String period2 = curJObj.getString("MMC_END_DATE");
				String strIsMainCar = curJObj.getString("MMC_MAIN");
				boolean isMainCar = false;
				if (strIsMainCar.equals("Y")) {
					isMainCar = true;
				} else if (strIsMainCar.equals("N")) {
					isMainCar = false;
				}
				String carName = curJObj.getString("MMC_NAME");
				String urlCarPhoto = curJObj.getString("MMC_CARPIC_URL");

				C.LIST_MK_CAR_PROFILE.add(new MkCarListProfile(idx, nick, year,
						indicatorName, type, period1, period2, introduce,
						isMainCar, carName, urlCarPhoto));
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** Code **/
	public static boolean getCodes() {
		Log.d(TAG, "code URL : " + C.API_CODE);
		JSONObject jObjCodes = getJsonObj(C.API_CODE);

		/** Get JsonArray **/
		JSONArray jAryAccess = new JSONArray();
		// JSONArray jAryArea = new JSONArray();
		JSONArray jAryCarType = new JSONArray();
		JSONArray jAryCompany = new JSONArray();
		JSONArray jAryContent = new JSONArray();
		JSONArray jAryJob = new JSONArray();
		JSONArray jAryQuestion = new JSONArray();

		try {
			jAryAccess = jObjCodes.getJSONArray("access");
			// jAryArea = jObjCodes.getJSONArray("area");
			jAryCarType = jObjCodes.getJSONArray("car_type");
			jAryCompany = jObjCodes.getJSONArray("company");
			jAryContent = jObjCodes.getJSONArray("content");
			jAryJob = jObjCodes.getJSONArray("job");
			jAryQuestion = jObjCodes.getJSONArray("question");

			/** jAryAccess **/
			Log.d(TAG_CODE, "-Access");
			for (int i = 0; i < jAryAccess.length(); i++) {
				JSONObject curJObj = jAryAccess.getJSONObject(i);
				String code = curJObj.getString("CC_CD");
				String name = curJObj.getString("CC_NM");
				C.LIST_ACCESS.add(new MkCode(name, code));
				Log.d(TAG_CODE, "code : " + code + ", name : " + name);
			}

			/** jAryArea **/
			Log.d(TAG_CODE, "-Area");
			// C.LIST_AREA.add(new MkCode("선택해주세요", "none"));
			// for(int i=0; i<jAryArea.length(); i++){
			// JSONObject curJObj = jAryArea.getJSONObject(i);
			// String code = curJObj.getString("CC_CD");
			// String name = curJObj.getString("CC_NM");
			// C.LIST_AREA.add(new MkCode(name, code));
			// Log.d(TAG_CODE, "code : " + code + ", name : " + name);
			// }

			/** jAryCarType **/
			Log.d(TAG_CODE, "-CarType");
			// C.LIST_CARTYPE.add(new MkCode("선택해주세요", "none"));
			for (int i = 0; i < jAryCarType.length(); i++) {
				JSONObject curJObj = jAryCarType.getJSONObject(i);
				String code = curJObj.getString("CC_CD");
				String name = curJObj.getString("CC_NM");
				C.LIST_CARTYPE.add(new MkCode(name, code));
				Log.d(TAG_CODE, "code : " + code + ", name : " + name);
			}

			/** jAryCompany **/
			Log.d(TAG_CODE, "-Company");
			// C.LIST_COMPANY.add(new MkCode("선택해주세요", "none"));
			for (int i = 0; i < jAryCompany.length(); i++) {
				JSONObject curJObj = jAryCompany.getJSONObject(i);
				String code = curJObj.getString("CC_CD");
				String name = curJObj.getString("CC_NM");
				C.LIST_COMPANY.add(new MkCode(name, code));
				Log.d(TAG_CODE, "code : " + code + ", name : " + name);
			}

			/** jAryContent **/
			Log.d(TAG_CODE, "-Content");
			for (int i = 0; i < jAryContent.length(); i++) {
				JSONObject curJObj = jAryContent.getJSONObject(i);
				String code = curJObj.getString("CC_CD");
				String name = curJObj.getString("CC_NM");
				C.LIST_CONTENT.add(new MkCode(name, code));
				Log.d(TAG_CODE, "code : " + code + ", name : " + name);
				C.CUR_PUBLIC = C.LIST_CONTENT.get(0);
			}

			/** jAryJob **/
			Log.d(TAG_CODE, "-Job");
			// C.LIST_JOB.add(new MkCode("선택해주세요", "none"));
			for (int i = 0; i < jAryJob.length(); i++) {
				JSONObject curJObj = jAryJob.getJSONObject(i);
				String code = curJObj.getString("CC_CD");
				String name = curJObj.getString("CC_NM");
				C.LIST_JOB.add(new MkCode(name, code));
				Log.d(TAG_CODE, "code : " + code + ", name : " + name);
			}

			/** jAryQuestion **/
			Log.d(TAG_CODE, "-Question");
			C.LIST_QUESTION.add(new MkCode("선택해주세요", "none"));
			for (int i = 0; i < jAryQuestion.length(); i++) {
				JSONObject curJObj = jAryQuestion.getJSONObject(i);
				String code = curJObj.getString("CC_CD");
				String name = curJObj.getString("CC_NM");
				C.LIST_QUESTION.add(new MkCode(name, code));
				Log.d(TAG_CODE, "code : " + code + ", name : " + name);
			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
			returnMsg = "서버와의 정보 연동에 실패했습니다";
			return false;
		}

	}

	public static boolean getAreaCodes() {
		if (C.LIST_AREA.size() > 0) {
			return true;
		}
		JSONObject jObj = getJsonObj(C.API_REGION_CD);
		C.LIST_AREA = new ArrayList<MkArea>();

		try {
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			returnMsg = returnMessage;

			JSONArray jAryList = jObj.getJSONArray("list");
			for (int i = 0; i < jAryList.length(); i++) {
				JSONObject curJobj = jAryList.getJSONObject(i);
				String areaCode = curJobj.getString("CC_CD");
				String areaName = curJobj.getString("CC_NM");

				// 추가될 임시변수
				MkCode mkArea = new MkCode(areaName, areaCode);
				ArrayList<MkCode> curAreaList = new ArrayList<MkCode>();
				Log.d("area", "Area : " + areaCode + ", " + areaName);

				JSONArray curJary = curJobj.getJSONArray("list");
				ArrayList<MkCode> curCityList = new ArrayList<MkCode>();
				Log.i("size", "json size = "+curJary.length());
				for (int j = 0; j < curJary.length(); j++) {

					JSONObject curCityJobj = curJary.getJSONObject(j);
					String cityCode = curCityJobj.getString("CC_CD");
					String cityName = curCityJobj.getString("CC_NM");

					MkCode mkCurCity = new MkCode(cityName, cityCode);
					Log.d("area", "Area : " + cityCode + ", " + cityName);
					curCityList.add(mkCurCity);
				}
				C.LIST_AREA.add(new MkArea(mkArea, curCityList));
				Log.d(TAG, "-------------------");
			}

			if (returnStatus.equals("Y")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/** HttpPostGetJSON **/
	private static JSONObject getJsonObj(String url,
			List<NameValuePair> nameValuePairs) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		httppost.addHeader("user-agent", "Android");

		HttpResponse response = null;
		try {
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			response = httpclient.execute(httppost);
		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 통신결과값
		HttpEntity httpEntity = response.getEntity();
		JSONObject jObj = null;
		try {
			String resultString = EntityUtils.toString(httpEntity);
			jObj = new JSONObject(resultString);
		} catch (ParseException e1) {
			e1.printStackTrace();
		} catch (JSONException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return jObj;
	}

	/** HttpPostGetJSON (No NameValuePairs) **/
	private static JSONObject getJsonObj(String url) {
		HttpClient httpclient = C.httpClient;
		HttpPost httppost = new HttpPost(url);
		httppost.addHeader("user-agent", "Android");

		HttpResponse response = null;
		try {
			// httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			response = httpclient.execute(httppost);
		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 통신결과값
		HttpEntity httpEntity = response.getEntity();
		JSONObject jObj = null;
		try {
			String result = EntityUtils.toString(httpEntity);
			// Log.d(TAG, "getJsonResult : " + result);
			jObj = new JSONObject(result);
		} catch (ParseException e1) {
			e1.printStackTrace();
		} catch (JSONException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return jObj;
	}
}
