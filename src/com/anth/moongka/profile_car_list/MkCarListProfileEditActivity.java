package com.anth.moongka.profile_car_list;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.ToggleButton;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_arrayadapter.MkCodeSpAA;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.network.HttpPostMultiPart;
import com.anth.moongka.network.ImageData;
import com.anth.moongka.network.StringData;
import com.anth.moongka.setting.MkCar;

public class MkCarListProfileEditActivity extends MkActivity implements
		OnItemSelectedListener, OnClickListener, OnCheckedChangeListener {
	private final String TAG = "MK_MkCarListProfileEdit&AddActivity";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	private final int TO_MK_CAR_LIST_PROFILE_PHOTO = 34322;
	private boolean isSpEnable = false;
	int j=0;
	/** UI **/
	private ImageButton btnBack;
	private Button btnComplete;
	private ImageView ivCarImage;
	private Button btnFind;
	private EditText etNick, etYear, etIntroduce;
	private Spinner spMaker, spCarName, spCarModel;
	private MkCodeSpAA spMakerAA, spNameAA, spModelAA;
	private int curMakerPos, curNamePos, curModelPos;
	private ToggleButton tbMyCar, tbDreamCar, tbOldMyCar;
	private ToggleButton[] tbList;
	private String curType;
	private View viewType;
	private Button btnPeriod1, btnPeriod2;
	private CheckBox cbMainCar, cbPublic;
	private Button btnPublic;

	private ArrayList<MkCar> mkCarList;
	private ArrayList<MkCode> makerList = new ArrayList<MkCode>();
	private ArrayList<MkCode> nameList = new ArrayList<MkCode>();
	private ArrayList<MkCode> modelList = new ArrayList<MkCode>();
	private Uri uriImage;

	private MkCarListProfileEdit mCar;
	private int firstNamePos;
	private boolean isFirst = true;

	/** DatePicker **/
	private Calendar mCalendar1, mCalendar2;
	private DatePickerDialog mDatePickerDialog;
	private DatePickerDialog.OnDateSetListener mOnDateSetListener1,
			mOnDateSetListener2;
	private boolean isDatePicked = false;
	private int thisYear = 0;

	private String MMC_ID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mkprofile_edit);
		mContext = this;
		mImageDownloader = new ImageDownloader(mContext);

		Intent intent = getIntent();
		// 사용자 자동차 정보
		mCar = (MkCarListProfileEdit) intent.getSerializableExtra("mCar");
		MMC_ID = intent.getStringExtra("MMC_ID");
		Log.i("position_mmc", ""+MMC_ID);

		// 전체 자동차 정보
		mkCarList = C.LIST_MKCAR;
		
		makerList.add(new MkCode("선택해주세요", "None"));
		nameList.add(new MkCode("선택해주세요", "None"));
		modelList.add(new MkCode("선택해주세요", "None"));
		for (MkCar curMkCar : mkCarList) {
			// 메이커 리스트 생성
			makerList.add(curMkCar.getMaker());
//			//makerlist position search
//			
//			// 사용자 자동차의 제조사와 일치하는 배열에서
//			Log.i("position_code1", ""+mCar.getModelCode());
//			Log.i("position_code2",""+curMkCar.getMaker().getCode());
//			if (mCar.getModelCode().equals(curMkCar.getMaker().getCode())) {
//				// nameList를 뽑는다
//				nameList.add(new MkCode("선택해주세요", "None"));
//				nameList = curMkCar.getCar();
//			}
		}
		// Spinner에 값을 할당하기 위해 position을 찾는다
		// 메이커 위치
		int makerPos = C.findMkCodePosition(mCar.getMakerName(), makerList);
		//nameList.clear();
		nameList.addAll(mkCarList.get(makerPos-1).getCar());
		Log.i("position size", ""+nameList.size());
		// 차량이름 위치
		Log.i(TAG, "GETNAME="+mCar.getNameName());
		Log.i(TAG, "GETNAME="+mCar.getCarName());
		int namePos = C.findMkCodePosition(mCar.getNameName(), nameList);
		Log.i("mcar", ""+mCar.getMakerName());
		Log.i("mcar", ""                                                                   +mCar.getNameName());
		Log.i("mcar", ""+mCar.getModelName());
		
		final int firstNamePos = namePos;
		//modelList.clear();
		modelList.addAll(mkCarList.get(makerPos-1).getModel().get(namePos-1));
		Log.i("position", ""+modelList.size());
		int modelPos = C.findMkCodePosition(mCar.getModelName(), modelList);
		Log.i("position", ""+makerPos);
		Log.i("position", ""+namePos);
		Log.i("position", ""+modelPos);
		
		/** Top Btns **/
		btnBack = (ImageButton) findViewById(R.id.btnMkProfileEditBack);
		btnComplete = (Button) findViewById(R.id.btnMkProfileEditComplete);

		/** Spinner **/
		spMaker = (Spinner) findViewById(R.id.spMkProfileEditMaker);
		spCarName = (Spinner) findViewById(R.id.spMkProfileEditName);
		spCarModel = (Spinner) findViewById(R.id.spMkProfileEditModel);
		spMaker.setOnItemSelectedListener(this);
		spCarName.setOnItemSelectedListener(this);
		spCarModel.setOnItemSelectedListener(this);
		spMakerAA = new MkCodeSpAA(mContext,
				android.R.layout.simple_list_item_1, makerList);
		spNameAA = new MkCodeSpAA(mContext,
				android.R.layout.simple_list_item_1, nameList);
		spModelAA = new MkCodeSpAA(mContext,
				android.R.layout.simple_list_item_1, modelList);
		
		spMaker.setAdapter(spMakerAA);
		spCarName.setAdapter(spNameAA);
		spCarModel.setAdapter(spModelAA);
		curMakerPos = makerPos;
		curNamePos = namePos;
		curModelPos = modelPos;
		spMaker.setSelection(makerPos);
		spCarName.setSelection(namePos);
		spCarModel.setSelection(modelPos);
//		spMaker.setSelection(5);
//		spCarName.setSelection(8);
//		spCarModel.setSelection(6);
		/** UI **/
		ivCarImage = (ImageView) findViewById(R.id.ivMkProfileEditPhoto);
		btnFind = (Button) findViewById(R.id.btnMkProfileEditFindPhoto);
		etNick = (EditText) findViewById(R.id.etMkProfileEditNickName);
		etYear = (EditText) findViewById(R.id.etMkProfileEditYear);
		etIntroduce = (EditText) findViewById(R.id.etMkProfileEditIntroduce);
		viewType = findViewById(R.id.viewMkProfileEditType);
		btnPeriod1 = (Button) findViewById(R.id.btnMkProfileEditPeriod1);
		btnPeriod2 = (Button) findViewById(R.id.btnMkProfileEditPeriod2);
		cbMainCar = (CheckBox) findViewById(R.id.cbMkProfileEditCheckMainCar);
		cbPublic = (CheckBox) findViewById(R.id.cbMkProfileEditCheckPublic);
		ivCarImage = (ImageView) findViewById(R.id.ivMkProfileEditPhoto);
		tbMyCar = (ToggleButton) findViewById(R.id.tbMkProfileEditType1);
		tbDreamCar = (ToggleButton) findViewById(R.id.tbMkProfileEditType2);
		tbOldMyCar = (ToggleButton) findViewById(R.id.tbMkProfileEditType3);
		tbMyCar.setOnCheckedChangeListener(this);
		tbDreamCar.setOnCheckedChangeListener(this);
		tbOldMyCar.setOnCheckedChangeListener(this);
		tbList = new ToggleButton[] { tbMyCar, tbDreamCar, tbOldMyCar };
		String curType = mCar.getTypeName();
		Log.d(TAG, "curType : " + curType);
		if (curType.equals("마이카")) {
			tbMyCar.setChecked(true);
		} else if (curType.equals("드림카")) {
			tbDreamCar.setChecked(true);
		} else {
			tbOldMyCar.setChecked(true);
		}

		/** Connect Listener **/
		btnBack.setOnClickListener(this);
		btnComplete.setOnClickListener(this);
		btnFind.setOnClickListener(this);
		btnPeriod1.setOnClickListener(this);
		btnPeriod2.setOnClickListener(this);

		/** DatePicker Dialog **/
		mOnDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				isDatePicked = true;
				mCalendar1.set(year, monthOfYear, dayOfMonth);
				btnPeriod1.setText(year + "-" + (monthOfYear + 1) + "-"
						+ dayOfMonth);
			}
		};
		mOnDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				isDatePicked = true;
				mCalendar2.set(year, monthOfYear, dayOfMonth);
				btnPeriod2.setText(year + "-" + (monthOfYear + 1) + "-"
						+ dayOfMonth);
			}
		};

		/** UI Data Add **/
		mImageDownloader.download(
				C.MK_BASE
						+ mCar.getUrlPhoto().replaceAll("/data/",
								"/thumb_data/data/"), ivCarImage);
		etNick.setText(mCar.getNick());
		etYear.setText(mCar.getYear());
		etIntroduce.setText(mCar.getIntroduce());
		btnPeriod1.setText(mCar.getPeriod1());
		btnPeriod2.setText(mCar.getPeriod2());
		cbMainCar.setChecked(mCar.isMainCar());
		cbPublic.setChecked(mCar.isPublic());
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position,
			long id) {
		
		if(parent.getId()==R.id.spMkProfileEditModel){
			isSpEnable=true;
		}
		if (isSpEnable) {
			Log.i(TAG, "TEST"+position);
			switch (parent.getId()) {
			case R.id.spMkProfileEditMaker:
				curMakerPos = position;
				if (position != 0) {
					nameList.clear();
					nameList.add(new MkCode("선택해주세요", "None"));
					nameList.addAll(mkCarList.get(position - 1).getCar());
					spNameAA.notifyDataSetChanged();
				} else {
					nameList.clear();
					spNameAA.notifyDataSetChanged();
				}
				break;
			case R.id.spMkProfileEditName:
				curNamePos = position;
				if (position != 0) {
					modelList.clear();
					modelList.add(new MkCode("선택해주세요", "None"));
					modelList.addAll(mkCarList.get(curMakerPos-1).getModel().get(position-1));
					spModelAA.notifyDataSetChanged();
				} else {
					modelList.clear();
					spModelAA.notifyDataSetChanged();
				}
				break;
			case R.id.spMkProfileEditModel:
				curModelPos = position;
			}

		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> v) {
		Log.i(TAG, "TEST2");
	}

	private void setToggleOffOther(int tbId) {
		for (ToggleButton curTb : tbList) {
			if (!(curTb.getId() == tbId)) {
				curTb.setChecked(false);
			}
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		for (ToggleButton curTb : tbList) {
			if (curTb.getId() == buttonView.getId()) {
				if (isChecked)
					setToggleOffOther(buttonView.getId());
			}
		}
		if (isChecked) {
			switch (buttonView.getId()) {
			case R.id.tbMkProfileEditType1:
				curType = "T101";
				viewType.setVisibility(View.VISIBLE);
				btnPeriod1.setText("");
				btnPeriod2.setText("현재");
				btnPeriod1.setClickable(true);
				btnPeriod2.setClickable(false);
				break;
			case R.id.tbMkProfileEditType2:
				curType = "T102";
				viewType.setVisibility(View.GONE);
				btnPeriod1.setText("");
				btnPeriod2.setText("");
				btnPeriod1.setClickable(true);
				btnPeriod2.setClickable(true);
				break;
			case R.id.tbMkProfileEditType3:
				curType = "T103";
				viewType.setVisibility(View.VISIBLE);
				btnPeriod1.setText("");
				btnPeriod2.setText("");
				btnPeriod1.setClickable(true);
				btnPeriod2.setClickable(true);

				break;
			}
		}
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.btnMkProfileEditBack:
			finish();
			break;
		case R.id.btnMkProfileEditComplete:
			
			HttpPostMultiPart mHttpPostMultiPart = new HttpPostMultiPart(
					C.API_MKCAR_EDIT, mContext);
			if (uriImage != null) {
				mHttpPostMultiPart.addImageData(new ImageData(
						"settingCarPhoto.jpg", "car_photo[0]", uriImage));
				mHttpPostMultiPart.addStringData(new StringData("imgyn",
						"android"));
			} else {
				mHttpPostMultiPart.addStringData(new StringData("imgyn",
						"android"));
			}
			mHttpPostMultiPart.addStringData(new StringData("mmc_id", mCar
					.getIdx()));
			mHttpPostMultiPart.addStringData(new StringData("mem_idx",
					C.mem_idx));
			mHttpPostMultiPart
					.addStringData(new StringData("mem_id", C.mem_id));
			mHttpPostMultiPart.addStringData(new StringData("car_company",
					modelList.get(curModelPos).getCode()));
			mHttpPostMultiPart.addStringData(new StringData("car_year", etYear
					.getText().toString()));
			mHttpPostMultiPart
					.addStringData(new StringData("car_type", curType));
			mHttpPostMultiPart.addStringData(new StringData("car_nick", etNick
					.getText().toString()));
			mHttpPostMultiPart.addStringData(new StringData("car_first",
					btnPeriod1.getText().toString()));

			mHttpPostMultiPart.addStringData(new StringData("car_introduce",
					etIntroduce.getText().toString()));
			String car_end = "";
			if (curType.equals("T103")) {
				car_end = btnPeriod2.getText().toString();
			}
			Log.i("ear_end", car_end);
			mHttpPostMultiPart
					.addStringData(new StringData("car_end", car_end));
			String car_ms = "";
			if (cbMainCar.isChecked()) {
				car_ms = "M";
			} else {
				car_ms = "S";
			}
			mHttpPostMultiPart.addStringData(new StringData("car_ms", car_ms));
			String car_hd = "";
			if (cbPublic.isChecked()) {
				car_hd = "Y";
			} else {
				car_hd = "N";
			}
			mHttpPostMultiPart.addStringData(new StringData("car_hd", car_hd));
			Lhy_Function.forceHideKeyboard(mContext, etNick);
			new MkCarListProfileEditTask(MMC_ID, mHttpPostMultiPart,
					"차량 프로필 수정중...", true, mContext).execute();
			break;
		case R.id.btnMkProfileEditFindPhoto:
			intent = new Intent(Intent.ACTION_PICK);
			intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
			intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI); // images
																		// card.
			startActivityForResult(intent, TO_MK_CAR_LIST_PROFILE_PHOTO);
			break;
		case R.id.btnMkProfileEditPeriod1:
			/** 보유기간1 **/
			mCalendar1 = Calendar.getInstance();
			if (thisYear == 0)
				thisYear = mCalendar1.get(Calendar.YEAR);
			mDatePickerDialog = new DatePickerDialog(mContext,
					mOnDateSetListener1, mCalendar1.get(Calendar.YEAR),
					mCalendar1.get(Calendar.MONTH),
					mCalendar1.get(Calendar.DAY_OF_MONTH));
			Log.i("mCalendar", "" + mCalendar1);
			mDatePickerDialog.show();
			break;
		case R.id.btnMkProfileEditPeriod2:
			/** 보유기간 2 **/
			mCalendar2 = Calendar.getInstance();
			if (thisYear == 0)
				thisYear = mCalendar2.get(Calendar.YEAR);
			mDatePickerDialog = new DatePickerDialog(mContext,
					mOnDateSetListener2, mCalendar2.get(Calendar.YEAR),
					mCalendar2.get(Calendar.MONTH),
					mCalendar2.get(Calendar.DAY_OF_MONTH));
			Log.i("mCalendar", "" + mCalendar2);
			mDatePickerDialog.show();
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case TO_MK_CAR_LIST_PROFILE_PHOTO:
				Cursor c = getContentResolver().query(data.getData(), null,
						null, null, null);
				c.moveToNext();
				String path = c.getString(c
						.getColumnIndex(MediaStore.MediaColumns.DATA));
				Uri uri = Uri.fromFile(new File(path));
				Log.d(TAG, "Uri : " + uri);

				uriImage = uri;
				C.setImageThumbnail(uriImage, ivCarImage);
				break;
			}
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus && isFirst) {
			//spCarName.setSelection(firstNamePos);
			isFirst = false;
		}
	}

}
