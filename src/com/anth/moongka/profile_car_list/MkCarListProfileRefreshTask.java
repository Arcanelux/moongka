package com.anth.moongka.profile_car_list;

import java.util.ArrayList;

import lhy.library.indicator_scroll.Indicator;
import lhy.library.indicator_scroll.IndicatorItem;
import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.profile_car_list.MkCarListProfileActivity.MkCarListProfilePagerAdapter;

public class MkCarListProfileRefreshTask extends MkTask{
	private final String TAG = "MK_MenuLeftToMkCarListProfileTask";
	private Context mContext;
	private Activity mActivity;
	private MkCarListProfilePagerAdapter mAdapter;
	private ViewPager mPager;

	private ArrayList<IndicatorItem> itemList;
	private Indicator mIndicator;
	private View viewCenter;

	private boolean isCert = false;
	private String you_idx;
	private String MMC_ID;
	
	private int posMove = 0;

	public MkCarListProfileRefreshTask(String MMC_ID, String you_idx, ViewPager mPager, MkCarListProfilePagerAdapter mAdapter, ArrayList<IndicatorItem> itemList, Indicator mIndicator, View viewCenter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext= context;
		mActivity = (Activity) mContext;
		this.mAdapter = mAdapter;
		this.mPager = mPager;
		this.itemList = itemList;
		this.mIndicator = mIndicator;
		this.viewCenter = viewCenter;
		this.MMC_ID = MMC_ID;
		this.you_idx = you_idx;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.getCarProfileList(C.mem_idx, you_idx);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			//			int posMainCar = 0;
			//			for(int i=0; i<C.LIST_MK_CAR_PROFILE.size(); i++){
			//				boolean isMainCar = C.LIST_MK_CAR_PROFILE.get(i).isMainCar();
			//				if(isMainCar==true){
			//					posMainCar = i;
			//					Log.d(TAG, "posMainCar : " + posMainCar);
			//				}
			//			}
			
			Log.i("mmc_idx_setting", MMC_ID);
			for(int i=0; i<C.LIST_MK_CAR_PROFILE.size(); i++){
				if(MMC_ID.equals(C.LIST_MK_CAR_PROFILE.get(i).getIdx())){
					Log.i("mmc_idx_server", ""+C.LIST_MK_CAR_PROFILE.get(i).getIdx());
					Log.i("mmc_idx_i", ""+i);
					posMove = i;
					break;
				}
			}

			// Indicator
			itemList.clear();
			for(int i=0; i<C.LIST_MK_CAR_PROFILE.size(); i++){
				MkCarListProfile mProfile = C.LIST_MK_CAR_PROFILE.get(i);
				itemList.add(new IndicatorItem(mProfile.getIndicatorName()));
			}
			// 리프레쉬 위해서
			mPager.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();

			mIndicator.removeAllItem();
			mIndicator.setItemList(itemList);
			mIndicator.init();
			mPager.post(new Runnable() {
				@Override
				public void run() {
					mPager.setCurrentItem(posMove, false);					
				}
			});

			viewCenter.bringToFront();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}
}
