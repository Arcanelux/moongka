/**
 * SettingCarEditTask
 *  차량 정보 수정 액션
 */
package com.anth.moongka.profile_car_list;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.network.HttpPostMultiPart;

public class MkCarListProfileAddTask extends MkTask {
	private final String TAG = "MK_CarListProfileAddTask";
	private Context mContext;

	private HttpPostMultiPart mHttpPostMultiPart;
	private String MMC_ID;

	public MkCarListProfileAddTask(String MMC_ID,
			HttpPostMultiPart mHttpPostMultiPart, String msg,
			boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.mHttpPostMultiPart = mHttpPostMultiPart;
		this.MMC_ID = MMC_ID;
	}

	@Override
	protected Void doInBackground(Void... params) {
		mHttpPostMultiPart.execute();
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		try {
			String strJsonResult = mHttpPostMultiPart.getResult();
			JSONObject jObj = new JSONObject(strJsonResult);
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			Log.i("return", returnStatus);
			Log.i("return", returnMessage);
			String returnmMC_idx = jObj.getString("mmc_idx");

			Log.i("mmc_idx", "" + returnmMC_idx);
			if (returnStatus.equals("Y")) {

				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT)
						.show();
				Intent intent = new Intent();
				intent.putExtra("MMC_ID", returnmMC_idx);
				
				// mContext = MkCarListProfileActivity.mContext;
				((Activity) mContext).setResult(Activity.RESULT_OK, intent);
				((Activity) mContext).finish();
				
				// ((Activity)mContext).startActivity(intent);
			} else {
				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT)
						.show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT)
					.show();
		}
	}

}
