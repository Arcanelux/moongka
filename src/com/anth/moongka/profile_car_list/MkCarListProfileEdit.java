/**
 * SettingCar
 *  �Ⱦ�
 */
package com.anth.moongka.profile_car_list;

import java.io.Serializable;

public class MkCarListProfileEdit implements Serializable {
	private String idx;
	private String makerCode, makerName;
	private String typeCode, typeName;
	private String nameCode, nameName;
	private String modelCode, modelName;
	private String urlPhoto;
	private String nick;
	private String year;
	private boolean isMainCar = false;
	private boolean isPublic = false;
	private String carName;
	private String period1, period2;
	private String introduce;
	
	public MkCarListProfileEdit(){
		
	}
	
	public MkCarListProfileEdit(String idx, String makerCode, String makerName,
			String typeCode, String typeName, String nameCode, String nameName,
			String modelCode,String modelName,
			String urlPhoto, String nick, String year, boolean isMainCar,
			boolean isPublic, String carName, String period1, String period2,
			String introduce) {
		this.idx = idx;
		this.makerCode = makerCode;
		this.makerName = makerName;
		this.typeCode = typeCode;
		this.typeName = typeName;
		this.nameCode = nameCode;
		this.nameName = nameName;
		this.modelCode = modelCode;
		this.modelName = modelName;
		this.urlPhoto = urlPhoto;
		this.nick = nick;
		this.year = year;
		this.isMainCar = isMainCar;
		this.isPublic = isPublic;
		this.carName = carName;
		this.period1 = period1;
		this.period2 = period2;
		this.introduce = introduce;
	}
	
	public void init(String idx, String makerCode, String makerName,
			String typeCode, String typeName, String nameCode, String nameName,
			String modelCode,String modelName,
			String urlPhoto, String nick, String year, boolean isMainCar,
			boolean isPublic, String carName, String period1, String period2,
			String introduce) {
		this.idx = idx;
		this.makerCode = makerCode;
		this.makerName = makerName;
		this.typeCode = typeCode;
		this.typeName = typeName;
		this.nameCode = nameCode;
		this.nameName = nameName;
		this.modelCode = modelCode;
		this.modelName = modelName;
		this.urlPhoto = urlPhoto;
		this.nick = nick;
		this.year = year;
		this.isMainCar = isMainCar;
		this.isPublic = isPublic;
		this.carName = carName;
		this.period1 = period1;
		this.period2 = period2;
		this.introduce = introduce;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getMakerCode() {
		return makerCode;
	}
	public void setMakerCode(String makerCode) {
		this.makerCode = makerCode;
	}
	public String getMakerName() {
		return makerName;
	}
	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getNameCode() {
		return nameCode;
	}
	public void setNameCode(String nameCode) {
		this.nameCode = nameCode;
	}
	public String getNameName() {
		return nameName;
	}
	public void setNameName(String nameName) {
		this.nameName = nameName;
	}
	public String getUrlPhoto() {
		return urlPhoto;
	}
	public void setUrlPhoto(String urlPhoto) {
		this.urlPhoto = urlPhoto;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public boolean isMainCar() {
		return isMainCar;
	}
	public void setMainCar(boolean isMainCar) {
		this.isMainCar = isMainCar;
	}
	public boolean isPublic() {
		return isPublic;
	}
	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public String getPeriod1() {
		return period1;
	}
	public void setPeriod1(String period1) {
		this.period1 = period1;
	}
	public String getPeriod2() {
		return period2;
	}
	public void setPeriod2(String period2) {
		this.period2 = period2;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getModelCode() {
		return modelCode;
	}

	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	
	
	
	
}