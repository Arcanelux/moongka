package com.anth.moongka.profile_car_list;

import java.util.ArrayList;

import lhy.library.indicator_scroll.Indicator;
import lhy.library.indicator_scroll.IndicatorItem;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;

public class MkCarListProfileActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_MkCarListProfileActivity";
	public static Context mContext;
	private ImageDownloader mImageDownloader;
	public static  final int TO_ADD = 83873;
	public static  final int TO_EDIT = 83874;
	
	public static final int NORMAL = 92383;
	public static final int DREAMCAR1 = 92384;
	public static final int DREAMCAR2 = 92385;
	public static final int MAINCAR = 92386;
	private boolean isApplyEditProfile = false;

	private MkViewPager mPager;
	private MkCarListProfilePagerAdapter mPagerAdapter;
	private Indicator mIndicator;
	//	private ArrayList<MkCarListProfile> carListProfileList;
	private ArrayList<IndicatorItem> itemList = new ArrayList<IndicatorItem>();

	private ImageButton btnBack;
	private Button btnEdit;
	private Button btnIndicatorPrev, btnIndicatorNext, btnIndicatorAdd;

	private View viewCenter;
	private View viewIndicatorControl;

	private String you_idx;
	private int posStart = 0;
	private int posMainCar = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mkprofile_list);
		mContext = this;
		mImageDownloader = new ImageDownloader(mContext);

		for(int i=0; i<C.LIST_MK_CAR_PROFILE.size(); i++){
			MkCarListProfile mProfile = C.LIST_MK_CAR_PROFILE.get(i);
			itemList.add(new IndicatorItem(mProfile.getIndicatorName()));
			if(mProfile.isMainCar()==true){
				posMainCar = i;
			}
		}
		
		Intent intent = getIntent();
		you_idx = intent.getStringExtra("you_idx");
		int where = intent.getIntExtra("where", 0);
		
		switch(where){
		case NORMAL:
			posStart = 0;
			break;
		case DREAMCAR1:
			posStart = C.LIST_MK_CAR_PROFILE.size()-2;
			break;
		case DREAMCAR2:
			posStart = C.LIST_MK_CAR_PROFILE.size()-1;
			break;
		case MAINCAR:
			posStart = posMainCar;
			break;
		}
		Log.d(TAG, "posStart : " + posStart);
		

		// 이전 Task에서 저장한 값 불러옴
		//		carListProfileList = C.LIST_MK_CAR_PROFILE;
		

		mPagerAdapter = new MkCarListProfilePagerAdapter(mContext);
		mPager = (MkViewPager) findViewById(R.id.vpMkProfileList);
		mPager.setPageMargin(50);
		mPager.setAdapter(mPagerAdapter);

		mIndicator = (Indicator) findViewById(R.id.lhy_IndicatorMkProfileList);
		mIndicator.setViewPager(mPager);
		//		mIndicator.setItemResSize(50, 50);
		mIndicator.setItemList(itemList);
		mIndicator.setItemResSelected(R.drawable.indicator_circle);
//		mIndicator.setPosition(posStart);
		mIndicator.init();

		viewCenter = findViewById(R.id.viewMkProfileListIndicatorCenterDot);
		final View viewLine = findViewById(R.id.viewMkProfileListIndicatorLine);
		viewCenter.post(new Runnable() {
			@Override
			public void run() {
				viewCenter.bringToFront();
			}
		});

		btnBack = (ImageButton) findViewById(R.id.btnMkProfileListBack);
		btnEdit = (Button) findViewById(R.id.btnMkProfileListEdit);
		btnBack.setOnClickListener(this);
		btnEdit.setOnClickListener(this);

		btnIndicatorPrev = (Button) findViewById(R.id.btnMkProfileListIndicatorPrev);
		btnIndicatorNext = (Button) findViewById(R.id.btnMkProfileListIndicatorNext);
		btnIndicatorAdd = (Button) findViewById(R.id.btnMkProfileListIndicatorAdd);
		btnIndicatorPrev.setOnClickListener(this);
		btnIndicatorNext.setOnClickListener(this);
		btnIndicatorAdd.setOnClickListener(this);
		
		mPager.post(new Runnable() {
			@Override
			public void run() {
				mPager.setCurrentItem(posStart, false);	
			}
		});
		
		if(you_idx.equals(C.mem_idx)){
			btnEdit.setVisibility(View.VISIBLE);
			
			viewIndicatorControl = findViewById(R.id.viewMkProfileListIndicatorControl);
			viewIndicatorControl.setBackgroundResource(R.drawable.bg_addcar);
		} else{
			btnIndicatorAdd.setClickable(false);
		}
	}

	public class MkCarListProfilePagerAdapter extends PagerAdapter{
		private LayoutInflater mInflater;

		public MkCarListProfilePagerAdapter(Context context){
			mInflater = LayoutInflater.from(mContext);
		}		
		
		@Override
		public int getCount() {
			return C.LIST_MK_CAR_PROFILE.size();
		}
		@Override
		public Object instantiateItem(View pager, int position) {
			View v = mInflater.inflate(R.layout.mkprofile_list_pager, null);
			MkCarListProfile curProfile = C.LIST_MK_CAR_PROFILE.get(position);
			String nick = curProfile.getNick();
			String carName = curProfile.getCarName();
			String year = curProfile.getYear();
			String type = curProfile.getType();
			String period1 = curProfile.getPeriod1();
			String period2 = curProfile.getPeriod2();
			String introduce = curProfile.getIntroduce();
			String urlCarPhoto = C.MK_BASE + curProfile.getUrlCarPhoto();

			TextView tvNick = (TextView) v.findViewById(R.id.tvMkProfileListPagerNick);
			TextView tvCarName = (TextView) v.findViewById(R.id.tvMkProfileListPagerCarName);
			TextView tvYear = (TextView) v.findViewById(R.id.tvMkProfileListPagerYear);
			TextView tvMyCar = (TextView) v.findViewById(R.id.tvMkProfileListMyCar);
			TextView tvDreamCar = (TextView) v.findViewById(R.id.tvMkProfileListDreamCar);
			TextView tvOldMyCar = (TextView) v.findViewById(R.id.tvMkProfileListOldCar);
			TextView tvPeriod1 = (TextView) v.findViewById(R.id.tvMkProfileListPeriod1);
			TextView tvPeriod2 = (TextView) v.findViewById(R.id.tvMkProfileListPeriod2);
			TextView tvIntroduce = (TextView) v.findViewById(R.id.tvMkProfileListIntroduce);
			ImageView ivCarImage = (ImageView) v.findViewById(R.id.ivMkProfileListCarImage);

			tvNick.setText(nick);
			tvCarName.setText(carName);
			tvYear.setText(year);
			if(type.equals("T101")){
				setTextSelected(tvMyCar);
				setTextNotSelected(tvDreamCar);
				setTextNotSelected(tvOldMyCar);
			} else if(type.equals("T102")){
				setTextNotSelected(tvMyCar);
				setTextSelected(tvDreamCar);
				setTextNotSelected(tvOldMyCar);
			} else if(type.equals("T103")){
				setTextNotSelected(tvMyCar);
				setTextNotSelected(tvDreamCar);
				setTextSelected(tvOldMyCar);
			}
			tvPeriod1.setText(period1);
			tvPeriod2.setText(period2);
			tvIntroduce.setText(introduce);
			mImageDownloader.download(urlCarPhoto, ivCarImage);

			((ViewPager)pager).addView(v, 0);
			return v; 
		}
		
		@Override
		public void destroyItem(View pager, int position, Object view) {    
			((ViewPager)pager).removeView((View)view);
		}
		@Override
		public boolean isViewFromObject(View pager, Object obj) {
			return pager == obj;
		}

		private void setTextSelected(TextView tv){
			tv.setTextColor(mContext.getResources().getColor(R.color.mk_color_darkgray));
			tv.setBackgroundColor(mContext.getResources().getColor(R.color.mk_color_emerald));
		}
		private void setTextNotSelected(TextView tv){
			tv.setTextColor(mContext.getResources().getColor(R.color.mk_color_emerald));
			tv.setBackgroundColor(mContext.getResources().getColor(R.color.mk_color_darkgray));
		}

//		@Override
//		public int getItemPosition(Object object) {
//			return POSITION_NONE;
//		}
	}

	@Override
	public void onClick(View v) {
		int pagerPos = mPager.getCurrentItem();
		String MMC_ID = null;
		switch(v.getId()){
		case R.id.btnMkProfileListBack:
			finish();
			break;
		case R.id.btnMkProfileListEdit:
			MMC_ID = C.LIST_MK_CAR_PROFILE.get(pagerPos).getIdx();
			new MkCarListProfileEditCreateTask(MMC_ID, "Loading...", true, mContext).execute();
			break;
		case R.id.btnMkProfileListIndicatorPrev:
			if(pagerPos>0) mPager.setCurrentItem(pagerPos-1, false);
			break;
		case R.id.btnMkProfileListIndicatorNext:
			if(pagerPos < itemList.size()-1) mPager.setCurrentItem(pagerPos+1, false);
			break;
		case R.id.btnMkProfileListIndicatorAdd:
			MMC_ID = C.LIST_MK_CAR_PROFILE.get(pagerPos).getIdx();
			new MkCarListProfileAddCreateTask(MMC_ID, "Loading...", true, mContext).execute();
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK){
			String MMC_ID = null;
			switch(requestCode){
			case TO_ADD:
				MMC_ID = data.getStringExtra("MMC_ID");
				new MkCarListProfileRefreshTask(MMC_ID, you_idx, mPager, mPagerAdapter, itemList, mIndicator, viewCenter, "차량목록 로딩중...", true, mContext).execute();
				break;
			case TO_EDIT:
				MMC_ID = data.getStringExtra("MMC_ID");
				new MkCarListProfileRefreshTask(MMC_ID, you_idx, mPager, mPagerAdapter, itemList, mIndicator, viewCenter, "차량목록 로딩중...", true, mContext).execute();
				break;
			}
		}
	}

	@Override
	public void finish() {
		if(you_idx.equals(C.mem_idx) && !isApplyEditProfile){
//			new MkCarListProfileToEditMenuLeftCarProfileTask("프로필 변경사항 적용중...", true, mContext).execute();
			new MkCarListProfileToEditMenuLeftCarProfileTask("프로필 변경사항 적용중...", false, mContext).execute();
			isApplyEditProfile = true;
		} else{
			super.finish();
		}
	}
}
