/**
 * SettingCarCreateTask
 *  차량정보 수정 - 정보 불러오기 액션 (안씀)
 */
package com.anth.moongka.profile_car_list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class MkCarListProfileEditCreateTask extends MkTask {
	private final String TAG = "MK_SettingCarCreateTask";
	private Context mContext;
	private Activity mActivity;
	
	private String MMC_ID;
	private MkCarListProfileEdit mCar = new MkCarListProfileEdit();
	private boolean isCert;

	public MkCarListProfileEditCreateTask(String MMC_ID, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity) mContext;
		this.MMC_ID = MMC_ID;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.carInfo(MMC_ID, mCar);
		isCert = MkApi.getMkCarList();
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Intent intent = new Intent(mActivity, MkCarListProfileEditActivity.class);
			intent.putExtra("mCar", mCar);
			intent.putExtra("MMC_ID", MMC_ID);
			mActivity.startActivityForResult(intent, MkCarListProfileActivity.TO_EDIT);
		} else{
			Toast.makeText(mContext, "차량 프로필 로딩에 실패했습니다", Toast.LENGTH_SHORT).show();
		}
	}

}
