/**
 * SettingCarEditTask
 *  차량 정보 수정 액션
 */
package com.anth.moongka.profile_car_list;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.network.HttpPostMultiPart;

public class MkCarListProfileEditTask extends MkTask{
	private final String TAG = "MK_CarListProfileEditTask";
	private Context mContext;
	
	private HttpPostMultiPart mHttpPostMultiPart;
	private String MMC_ID;

	public MkCarListProfileEditTask(String mMC_ID, HttpPostMultiPart mHttpPostMultiPart, String msg, boolean isProgressDialog,
			Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.mHttpPostMultiPart = mHttpPostMultiPart;
		this.MMC_ID = mMC_ID;
	}


	@Override
	protected Void doInBackground(Void... params) {
		mHttpPostMultiPart.execute();
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		try {
			JSONObject jObj = new JSONObject(mHttpPostMultiPart.getResult());
			String returnStatus = jObj.getString("returnStatus");
			String returnMessage = jObj.getString("returnMessage");
			String returnmMC_idx = jObj.getString("mmc_idx");
			Log.i("mmc_idx", returnmMC_idx);
			if(returnStatus.equals("Y")){
				Intent intent = new Intent();
				intent.putExtra("MMC_ID", returnmMC_idx);
				((Activity)mContext).setResult(Activity.RESULT_OK, intent);
				((Activity)mContext).finish();
			} else{
				Toast.makeText(mContext, returnMessage, Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


}
