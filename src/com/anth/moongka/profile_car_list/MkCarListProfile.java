package com.anth.moongka.profile_car_list;

public class MkCarListProfile {
	String idx;
	String nick;
	String year;
	String indicatorName;
	String type;
	String period1, period2;
	String introduce;
	boolean isMainCar = false;
	String carName;
	String urlCarPhoto;
	public MkCarListProfile(String idx, String nick, String year,
			String indicatorName, String type, String period1, String period2,
			String introduce, boolean isMainCar, String carName,
			String urlCarPhoto) {
		super();
		this.idx = idx;
		this.nick = nick;
		this.year = year;
		this.indicatorName = indicatorName;
		this.type = type;
		this.period1 = period1;
		this.period2 = period2;
		this.introduce = introduce;
		this.isMainCar = isMainCar;
		this.carName = carName;
		this.urlCarPhoto = urlCarPhoto;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getIndicatorName() {
		return indicatorName;
	}
	public void setIndicatorName(String indicatorName) {
		this.indicatorName = indicatorName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPeriod1() {
		return period1;
	}
	public void setPeriod1(String period1) {
		this.period1 = period1;
	}
	public String getPeriod2() {
		return period2;
	}
	public void setPeriod2(String period2) {
		this.period2 = period2;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public boolean isMainCar() {
		return isMainCar;
	}
	public void setMainCar(boolean isMainCar) {
		this.isMainCar = isMainCar;
	}
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public String getUrlCarPhoto() {
		return urlCarPhoto;
	}
	public void setUrlCarPhoto(String urlCarPhoto) {
		this.urlCarPhoto = urlCarPhoto;
	}
	
	
	
	
	
}
