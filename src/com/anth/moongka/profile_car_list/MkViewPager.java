package com.anth.moongka.profile_car_list;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MkViewPager extends ViewPager{
	private boolean isScrollEnable = false;

	public MkViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MkViewPager(Context context) {
		super(context);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if(this.isScrollEnable){
			return super.onInterceptTouchEvent(event);	
		}
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(this.isScrollEnable){
			return super.onTouchEvent(event);	
		}
		return false;
	}
	
	
}
