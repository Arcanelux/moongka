package com.anth.moongka.profile_car_list;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.menu.MenuLeft;

public class MkCarListProfileToEditMenuLeftCarProfileTask extends MkTask{
	private final String TAG = "MK_MkCarListProfileToEditMenuLeftCarProfileTask";
	private Context mContext;
	
	private boolean isCert = false;
	
	public MkCarListProfileToEditMenuLeftCarProfileTask(String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext= context;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.getProfileMain(C.mem_idx, C.MAIN_CAR_PROFILE);
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			MenuLeft.notifyCarListChanged();
			((Activity)mContext).finish();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}


}
