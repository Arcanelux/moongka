/**
 * HttpPostMultiPart 클래스
 *  파일전송을 편하게 해줌
 *  ImageData와 StringData로 데이터를 보냄
 *  위 두 클래스는 직접만듬. 사용할경우 분석필요
 */
package com.anth.moongka.network;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;


public class HttpPostMultiPart {
	private final String TAG = "lhy_Network_HttpPost";
	private Context mContext;

	private String strUrl;
	private ArrayList<ImageData> imageList = new ArrayList<ImageData>();
	private ArrayList<StringData> stringList = new ArrayList<StringData>();
	private String result;

	public HttpPostMultiPart(String strUrl, Context context){
		this.strUrl = strUrl;
		mContext = context;
	}
	public void addImageData(ImageData data){
		imageList.add(data);
	}
	public void addStringData(StringData data){
		stringList.add(data);
	}
	public String getResult(){
		return result;
	}

	public void execute(){
		if(imageList.size()==0 && stringList.size()==0){
			Toast.makeText(mContext, "전송할 데이터가 없습니다", Toast.LENGTH_SHORT).show();
		} else{
			result = httpPostMultiPartBase2(strUrl, imageList, stringList, mContext);
			//						result = test(strUrl, imageList, stringList, mContext);
		}
	}

	/**
	 * 아파치 오픈소스 사용
	 *  MultipartEntity 폴더의
	 *   apache-mime4j-core
	 *   httpclient
	 *   httpcore
	 *   httpmime
	 *  4개 라이브러리 필요
	 * @param strUrl
	 * @param imageList
	 * @param stringList
	 * @param context
	 * @return
	 */
	private String httpPostMultiPartBase2(String strUrl, ArrayList<ImageData> imageList, ArrayList<StringData> stringList, Context context){
		try{
			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(strUrl);

			// Header
			post.addHeader("Connection", "Keep-Alive");
			//		post.addHeader("Content-Type", "multipart/form-data; boundary="+boundary);
			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);

			// post data Set. UTF-8
			Map mHashMap = new HashMap();
			for(StringData curData : stringList){
				String paramName = curData.getParamName();
				String strData = curData.getDataString();
				mHashMap.put(paramName, strData);
			}
			Iterator<String> keys = mHashMap.keySet().iterator();
			while(keys.hasNext()){
				String key = keys.next();
				entity.addPart(key, new StringBody((String)mHashMap.get(key), "text/plain", Charset.forName("UTF-8")));
			}

			// File
			for(int i=0; i<imageList.size(); i++){
				ImageData curImageData = imageList.get(i);
				String fileName = curImageData.getFileName();
				String paramName = curImageData.getParamName();
				Uri imageUri = curImageData.getImageUri();
				Log.d(TAG, "curUri : " + imageUri);

				// 압축
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				options.inPreferredConfig = Config.RGB_565;
				Bitmap curBitmap = BitmapFactory.decodeFile(imageUri.getPath(), options);

				// 이미지를 상황에 맞게 회전시킨다. curBitmap을 회전
				ExifInterface exif = new ExifInterface(imageUri.getPath());
				int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
				int exifDegree = 0;
				switch(exifOrientation){
				case ExifInterface.ORIENTATION_ROTATE_90:
					exifDegree = 90;
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					exifDegree = 180;
					break;
				case ExifInterface.ORIENTATION_ROTATE_270:
					exifDegree = 270;
					break;
				default:
					exifDegree = 0;
					break;
				}
				if(exifDegree!=0){
					Matrix m = new Matrix();
					m.setRotate(exifDegree, (float)curBitmap.getWidth()/2, (float)curBitmap.getHeight()/2);
					try{
						Bitmap converted = Bitmap.createBitmap(curBitmap, 0, 0, curBitmap.getWidth(), curBitmap.getHeight(), m, true);
						curBitmap.recycle();
						curBitmap = converted;
					} catch(OutOfMemoryError ex){
						ex.printStackTrace();
					}
				}

				// 뭉카 임시폴더생성
				String pathSdCard = Environment.getExternalStorageDirectory().getAbsolutePath();
				File path = new File(pathSdCard + "/Moongka");
				if(! path.isDirectory()) {
					path.mkdirs();
				}

				String pathTemp = pathSdCard + "/Moongka/temp" + i + ".jpg";
				File tempFile = new File(pathTemp);
				Log.d(TAG, "Path : " + pathTemp);
				OutputStream out = null;
				try {
					out = new FileOutputStream(tempFile);
					curBitmap.compress(CompressFormat.JPEG, 70, out);
				} catch (Exception e) {         
					e.printStackTrace();
				} finally {
					try {
						out.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				//	File file = new File(imageUri.getPath());
				//	ContentBody fileBody = new FileBody(file, "image/pjpeg");
				ContentBody fileBody = new FileBody(tempFile, "image/pjpeg");
				entity.addPart(paramName, fileBody);
				entity.addPart(fileName, fileBody);
			}
			post.setEntity(entity);

			HttpResponse response = client.execute(post);
			int status = response.getStatusLine().getStatusCode();

			HttpEntity httpEntity = response.getEntity();
			String result = null;
			try {
				result = EntityUtils.toString(httpEntity);
				Log.d(TAG, "result : " + result);
				return result;
			} catch (ParseException e) { 
				e.printStackTrace();
				return e.toString();
			} catch (IOException e) { 
				e.printStackTrace();
				return e.toString();
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}

	private String httpPostMultiPartBase3(String strUrl, ArrayList<ImageData> imageList, ArrayList<StringData> stringList, Context context){
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "3FS4PKI7YH9S";      // 임의로 설정한다
		// 경계선
		String delimiter = lineEnd + twoHyphens + boundary + lineEnd;

		try{
			StringBuffer postDataBuilder = new StringBuffer();

			postDataBuilder.append(delimiter);
			// key&value
			for(StringData curData : stringList){
				String paramName = curData.getParamName();
				String strData = curData.getDataString();

				postDataBuilder.append("Content-Disposition: form-data; name=\"" + paramName + "\"" + lineEnd + strData);
				postDataBuilder.append(delimiter);
			}

			// ImageData
			for(ImageData curImageData : imageList){
				String fileName = curImageData.getFileName();
				String paramName = curImageData.getParamName();
				Uri imageUri = curImageData.getImageUri();

				// Connection Setting
				HttpURLConnection conn = (HttpURLConnection) new URL(strUrl).openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setUseCaches(false);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

				// File
				postDataBuilder.append("Content-Disposition: form-data; name=\"" + paramName +
						"\"; filename\"" + fileName + "\"" + lineEnd);
				postDataBuilder.append(lineEnd);

				// Transfer Start
				File curFile = new File(imageUri.getPath());
				FileInputStream mFileInputStream = new FileInputStream(curFile);
				DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(conn.getOutputStream()));

				// Send MetaData First
				dos.writeUTF(postDataBuilder.toString());

				// File Copy Task Start
				int maxBufferSize = 1024;
				int bufferSize = Math.min(mFileInputStream.available(), maxBufferSize);
				byte[] buffer = new byte[bufferSize];

				// File to Buffer Bytes
				int byteRead = mFileInputStream.read(buffer, 0, bufferSize);

				// Transfer
				while(byteRead > 0){
					dos.write(buffer);
					bufferSize = Math.min(mFileInputStream.available(), maxBufferSize);
					byteRead = mFileInputStream.read(buffer, 0, bufferSize);
				}

				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
				dos.flush();
				dos.close();
				mFileInputStream.close();

				conn.getInputStream();
				conn.disconnect();
			}

			return "";
		} catch(Exception e){
			e.printStackTrace();
			return "";
		}

	}
	private String httpPostMultiPartBase(String strUrl, ArrayList<ImageData> imageList, ArrayList<StringData> stringList, Context context){
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "3FS4PKI7YH9S";      // 임의로 설정한다

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		FileInputStream mFileInputStream;


		// DataOutputStream에 데이터 삽입
		// 전송데이터가 null일경우 Exception이 발생함
		try{
			// 이미지 파일 등 byte[] 형태의 전송
			// DataOutputStream에 twoHyphens + boundary + lineEnd 로 각 데이터의 시작부분을 알려준다
			for(ImageData curImageData : imageList){
				/** 이미지 데이터 정보 **/
				String fileName = curImageData.getFileName();
				String paramName = curImageData.getParamName();
				Uri imageUri = curImageData.getImageUri();
				Log.d(TAG, fileName + ", " + paramName + ", " + imageUri);
				Log.d(TAG, "imageUri : " + imageUri);

				/** 이미지 절대경로 획득 **/
				String curAbsolutePath = imageUri.getPath();
				Log.d(TAG, "CurAbsolutePath : " + curAbsolutePath);

				/** 절대경로에서 byte[] 구함 **/
				mFileInputStream = new FileInputStream(curAbsolutePath);
				int bytesAvailable = mFileInputStream.available();
				int maxBufferSize = 128;
				int bufferSize = Math.min(bytesAvailable, maxBufferSize);

				byte[] imageBuffer = new byte[bufferSize];
				int bytesRead = mFileInputStream.read(imageBuffer, 0, bufferSize);

				/** 전송데이터1 - 파일형식 **/
				dos.writeBytes(twoHyphens + boundary + lineEnd);	
				dos.writeBytes("Content-Disposition: form-data; " + "filename=\"" + fileName + "\"; name=\"" + paramName + "\"" + lineEnd);
				Log.d(TAG, "Param: " + "Content-Disposition: form-data; " + "filename=\"" + fileName + "\"; name=\"" + paramName + "\"" + lineEnd);
				dos.writeBytes("Content-Type: image/pjpeg " + lineEnd);
				dos.writeBytes(lineEnd);

				/** 전송데이터2 - 이미지byte[] **/
				while (bytesRead > 0) {
					/** 이미지 byte[] 입력 **/
					dos.write(imageBuffer, 0, bufferSize);
					bytesAvailable = mFileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = mFileInputStream.read(imageBuffer, 0, bufferSize);
				}
				dos.writeBytes(lineEnd);
				mFileInputStream.close();

				/** Bitmap 꺼내서 하기 **/
				//				Bitmap fucking = Images.Media.getBitmap(mContext.getContentResolver(), imageUri);
				//				Log.d(TAG, fucking.getWidth() + ", " + fucking.getHeight());
				//				Log.d(TAG, fucking.getByteCount()+"");
				//				byte[] byteImage= null;
				//				try{
				//					ByteArrayOutputStream  byteArray = new ByteArrayOutputStream();
				//					fucking.compress(CompressFormat.JPEG, 50, byteArray);
				//					byteImage = byteArray.toByteArray();
				//				} catch(Exception e){
				//					e.printStackTrace();
				//				}
				//
				//				dos.writeBytes(twoHyphens + boundary + lineEnd); 
				//				dos.writeBytes("Content-Disposition: form-data" + "filename=\"" + fileName + "\"; name=\"" + paramName + "\"" + lineEnd);
				////				dos.writeBytes("Content-Type: image/pjpeg" + lineEnd);
				//
				//				dos.writeBytes(lineEnd);
				//				dos.write(byteImage,0,byteImage.length);
				//				dos.writeBytes(lineEnd);
			}


			// 일반 텍스트 전송
			for(StringData curStringData : stringList){
				String paramName = curStringData.getParamName();
				String dataString = curStringData.getDataString();
				//				dataString = new String(dataString.getBytes("euc-kr"), "utf-8");

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"" + paramName + "\"" + lineEnd);
				//				dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + lineEnd);
				dos.writeBytes(lineEnd);
				dos.writeBytes(dataString + lineEnd);
			}

			// 전송 데이터 끝 표시
			// DataOutputStream에 twoHyphens + boundary + twoHyphens + lineEnd 로 끝을 알려준다
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			dos.flush();
			dos.close();

		} catch(Exception e){
			e.printStackTrace();
		}

		// DataOutputStream으로부터 ByteArrayOutputStream으로 전달된 데이터를 이용하여
		// ByteArrayInputStream객체를 생성, BasicHttpEntity에 전달해준다
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		BasicHttpEntity entity = new BasicHttpEntity();
		//		entity.setContentEncoding("UTF-8");
		entity.setContent(bais);

		// HttpPost 설정
		// "Content-Type"을 "multipart/form-data; boundary=미리 정의한 임의의 boundary" 로 설정한다 
		HttpPost httpPost = new HttpPost(strUrl);
		httpPost.addHeader("Connection", "Keep-Alive");
		httpPost.addHeader("Content-Type", "multipart/form-data; boundary="+boundary);
		//		httpPost.addHeader("Accept-Charset", "UTF-8");
		httpPost.setEntity(entity);

		// HttpClient 할당, 세션 유지를 위해 HttpClient의 재사용을 원한다면
		// 외부 클래스에 static으로 선언하여 전송함수에서 참조한다
		// ex) Val.java파일 - public static HttpClient httpClient = new DefaultHttpClient();
		// --- 함수내부 -       HttpClient httpClient = Val.httpClient;
		HttpClient httpClient = new DefaultHttpClient();

		// 전송, HttpResponse에 결과값 할당
		HttpResponse response = null;
		try {
			response = httpClient.execute(httpPost);
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// 통신결과값 처리
		HttpEntity httpEntity = response.getEntity();
		String result = null;
		try {
			result = EntityUtils.toString(httpEntity);
			Log.d(TAG, "result : " + result);
			return result;
		} catch (ParseException e) { 
			e.printStackTrace();
			return e.toString();
		} catch (IOException e) { 
			e.printStackTrace();
			return e.toString();
		}
	}
}
