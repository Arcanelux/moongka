/**
 * StringData
 *  ImageData와 같은 형식으로 전송
 */
package com.anth.moongka.network;

public class StringData {
	private String paramName;
	private String dataString;
	
	public StringData(String paramName, String dataString) {
		this.paramName = paramName;
		this.dataString = dataString;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}
	
}
