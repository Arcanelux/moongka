/**
 * ImageData
 *  HttpPostMultiPart클래스를 위한 클래스 데이터형식
 *  HttpPost방식에서 이미지를 전송할때의 fileName, parameterName, 그리고 이미지의 Uri를 넣어주면 HttpPostMultiPart클래스에서 받아 전송
 */
package com.anth.moongka.network;

import android.net.Uri;

public class ImageData {
	private String fileName;
	private String paramName;
	private Uri imageUri;
	
	public ImageData(String fileName, String paramName, Uri imageUri) {
		this.fileName = fileName;
		this.paramName = paramName;
		this.imageUri = imageUri;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public Uri getImageUri() {
		return imageUri;
	}

	public void setImageUri(Uri imageUri) {
		this.imageUri = imageUri;
	}
	
	
}
