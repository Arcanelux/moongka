package com.anth.moongka.login;

import java.util.ArrayList;

import android.content.Context;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkAlarm;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class LoginGetNotifyTask extends MkTask{
	private boolean isCert = false;
	private Context mContext;
	private ArrayList<MkAlarm> alarmList = new ArrayList<MkAlarm>();

	public LoginGetNotifyTask(String msg, boolean isProgressDialog,
			Context context) {
		super(msg, isProgressDialog, context);
		mContext= context;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.alarmList(alarmList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		if(isCert){
			if(alarmList.size()>0){
				C.isExistNotify = true;
			}
			new LoginGetProfileTask("프로필 로딩중...", true, mContext).execute();
		} else{
			Toast.makeText(mContext, "알림목록을 불러오는 중 문제가 발생하였습니다", Toast.LENGTH_SHORT).show();
		}
		super.onPostExecute(result);
	}
}
