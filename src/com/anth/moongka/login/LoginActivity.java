/**
 * LoginActivity
 * 로그인
 */
package com.anth.moongka.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.find.FindActivity;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkPref;
import com.anth.moongka.function.SavedValue;

public class LoginActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_LoginActivity";
	private Context mContext;

	/** UI **/
	private ImageView ivLogo;
	private EditText etID, etPW;
	private CheckBox cbAutoLogin;
	private Button btnFind, btnLogin, btnJoin;

	private String id, pw;
	private boolean checkAutoLogin = false;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		mContext = this;

		SavedValue savedValue = MkPref.getSavedValue(mContext);
		checkAutoLogin = savedValue.isAutoLogin();

		/** UI Connect **/
		//		ivLogo = (ImageView) findViewById(R.id.)
		etID = (EditText) findViewById(R.id.etLoginID);
		etPW = (EditText) findViewById(R.id.etLoginPW);
		cbAutoLogin = (CheckBox) findViewById(R.id.cbLoginAutoLogin);
		btnFind = (Button) findViewById(R.id.btnLoginFindIDPW);
		btnLogin = (Button) findViewById(R.id.btnLoginLogin);
		btnJoin = (Button) findViewById(R.id.btnLoginJoin);

		btnFind.setOnClickListener(this);
		btnLogin.setOnClickListener(this);
		btnJoin.setOnClickListener(this);
		cbAutoLogin.setChecked(true);

		
		if(checkAutoLogin){
			id = savedValue.getId();
			pw = savedValue.getPw();
			etID.setText(id);
			etPW.setText(pw);
			//			checkAutoLogin = cbAutoLogin.isChecked();
			new LoginCertTask(id, pw, checkAutoLogin, mContext).execute();
		}
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch(v.getId()){
		case R.id.btnLoginFindIDPW:
			intent = new Intent(LoginActivity.this, FindActivity.class);
			startActivity(intent);
			break;
		case R.id.btnLoginLogin:
			if(etID.getText().toString().equals("")){
				Toast.makeText(mContext, "아이디를 입력해주세요", Toast.LENGTH_SHORT).show();
			} else if(etPW.getText().toString().equals("")){
				Toast.makeText(mContext, "패스워드를 입력해주세요", Toast.LENGTH_SHORT).show();
			} else{
				id = etID.getText().toString();
				pw = etPW.getText().toString();
				checkAutoLogin = cbAutoLogin.isChecked();
				Lhy_Function.forceHideKeyboard(mContext, etID);
				new LoginCertTask(id, pw, checkAutoLogin, mContext).execute();
			}
			break;
		case R.id.btnLoginJoin:
//			intent = new Intent(LoginActivity.this, Join1Activity.class);
//			startActivity(intent);
			new LoginToJoinTask("회원가입화면 로딩중...", true, mContext).execute();
			break;
			
		}
	}

	private void makeToast(String msg){
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == 4) {
			setDialog();
			return false;
		} else
			return super.onKeyDown(keyCode, event);
	}

	void setDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("종료하시겠습니까?")
				.setCancelable(false)
				.setPositiveButton("아니오",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {

							}
						})
				.setNegativeButton("예", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				});
		builder.create().show();
	}
}
