/**
 * LoginCertTask
 * 로그인 액션
 *  자동로그인 처리, Push분기 처리를 해준다
 */
package com.anth.moongka.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;

import com.anth.moongka.fix_value.MkDialog;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.function.MkPref;

public class LoginCertTask extends AsyncTask<Void, Integer, Integer>{
	private final String TAG = "MK_LoginCertTask";
	private Context mContext;
	private Activity mActivity;
	
	private ProgressDialog mProgressDialog;
	private MkDialog mDialog;
	
	private String id, pw;
	private boolean isAutoLogin = false;
	private boolean isCert = false;
	
	public LoginCertTask(String id, String pw, boolean isAutoLogin, Context context){
		mContext = context;
		mActivity = (Activity)mContext;
		this.id = id;
		this.pw = pw;
		this.isAutoLogin = isAutoLogin;
	}

	@Override
	protected void onPreExecute() {
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage("로그인 인증 중...");
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.show();
		super.onPreExecute();
	}
	@Override
	protected Integer doInBackground(Void... params) {
		// Md5는 MkApi내부에서 처리
		isCert = MkApi.login(id, pw, mContext);
//		isCert = MkApi.login("android", "q", mContext);
//		isCert = MoongkaApi.login("android", "q", mContext);
		//MkApi.getMkCarList();
		MkApi.getMkCarMainList();
		if(isCert){
			return C.LOGIN_SUCCESS;
		} else{
			return C.LOGIN_FAILED;
		}
		
		
		
		// Test
//		return C.LOGIN_SUCCESS;
//		return C.LOGIN_FAILED;
	}
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		
//		Toast.makeText(mContext, MoongkarApi.returnMsg, Toast.LENGTH_SHORT).show();
		Intent intent = null;
		switch(result){
		case C.LOGIN_SUCCESS:
			mProgressDialog.dismiss();
			if(isAutoLogin) {
				MkPref.setAutoLogin(mContext, true);
			}
			/** 자동로그인 체크를 하지않아도 일단 저장 **/
			MkPref.saveID(mContext, id);
			MkPref.savePW(mContext, pw);
			
//			new LoginGetProfileTask("프로필 로딩중...", true, mContext).execute();
			new LoginGetNotifyTask("알림목록 로딩중...", true, mContext).execute();
			break;
		case C.LOGIN_FAILED:
			mDialog = new MkDialog(mContext);
			mDialog.setContent("일치하는 아이디가 없거나 비밀번호 오류입니다. 아이디와 비밀번호를 확인하여 주십시오");
			mDialog.setBtn1("확인");
			mDialog.setBtn1ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();
					mProgressDialog.dismiss();
//					Intent intent = new Intent(mActivity, FindActivity.class);
//					mActivity.startActivity(intent);
				}
			});
			mDialog.setBtn2("취소");
			mDialog.setBtn2ClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();
					mProgressDialog.dismiss();
				}
			});
			mDialog.show();
			break;
		}
	}
	@Override
	protected void onCancelled() {
		super.onCancelled();
	}
}
