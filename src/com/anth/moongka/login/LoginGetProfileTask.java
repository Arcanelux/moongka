package com.anth.moongka.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.anth.moongka.comment.CommentActivity;
import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.friend.FriendActivity;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.main.MainActivity;
import com.anth.moongka.main.MainSingleActivity;
import com.anth.moongka.message.MessageActivity;

public class LoginGetProfileTask extends MkTask{
	private boolean isCert = false;
	private Context mContext;
	private Activity mActivity;

	public LoginGetProfileTask(String msg, boolean isProgressDialog,
			Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity) mContext;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.getProfileMain(C.mem_idx, C.MAIN_CAR_PROFILE);
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Intent intent = null;
			/** Push에서 왔을 때 **/
			Log.i("asdf", ""+C.GCM_CURRENT);
			if(C.isFromGCM){
				switch(C.GCM_CURRENT){
				case C.GCM_TO_FRIEND:
					intent = new Intent(mActivity, FriendActivity.class);
					break;
				case C.GCM_TO_MESSAGE:
					intent = new Intent(mActivity, MessageActivity.class);
					intent.putExtra("oth_idx", C.GCM_KEY_ID);
					intent.putExtra("isFromGCM", true);
					//intent.putExtra("own_idx", C.)
					break;
				case C.GCM_TO_COMMENT_TXT:
					intent = new Intent(mActivity, CommentActivity.class);
					intent.putExtra("post_idx", C.GCM_KEY_ID);
					intent.putExtra("post_type", "CFB_ID");
					break;
				case C.GCM_TO_COMMENT_IMAGE:
					intent = new Intent(mActivity, CommentActivity.class);
					intent.putExtra("post_idx", C.GCM_KEY_ID);
					intent.putExtra("post_type", "CPR_ID");
					break;
				case C.GCM_TO_POST:
					intent = new Intent(mActivity, MainSingleActivity.class);
					break;
				}
				intent.putExtra("keyId", C.GCM_KEY_ID);
			}
			/** 일반 로그인 **/
			else{
				intent = new Intent(mActivity, MainActivity.class);
			}
			mActivity.startActivity(intent);
			C.isFromGCM=false;
			mActivity.finish();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

	

}
