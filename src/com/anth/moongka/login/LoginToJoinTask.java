/**
 * LoginToJoinTask
 * 로그인 화면에서 회원가입 버튼을 눌렀을 때 액션
 *  차량목록은 필요할때만 불러쓰므로(그 외 데이터는 로딩에서 전부 받아와 전역변수에 저장)
 *  회원가입을 들어가기전에 이 액션에서 차량목록을 받은 후 회원가입화면으로 이동
 */
package com.anth.moongka.login;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.join.Join1Activity;
import com.anth.moongka.setting.MkCar;

public class LoginToJoinTask extends MkTask {
	private final String TAG = "MK_LoginToJoinTask";
	private Context mContext;
	private Activity mActivity;
	
	private ArrayList<MkCar> mkCarList = new ArrayList<MkCar>();
	private boolean isCert;

	public LoginToJoinTask(String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (Activity) mContext;
	}
	
	

	@Override
	protected Void doInBackground(Void... params) {
		//isCert = MkApi.getMkCarList();
		isCert = MkApi.getMkCarMainList();
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			Intent intent = new Intent(mActivity, Join1Activity.class);
			mActivity.startActivity(intent);
		} else{
			Toast.makeText(mContext, "차량 목록로딩에 실패했습니다", Toast.LENGTH_SHORT).show();
		}
	}

}
