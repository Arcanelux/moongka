/**
 * FriendReqArrayAdapter
 *  模备夸没格废 ArrayAdapter
 */
package com.anth.moongka.friend;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.profile.ProfileActivity;
import com.anth.moongka.profile.ProfileImeActivity;

public class FriendReqArrayAdapter extends ArrayAdapter<MkFriend> implements OnClickListener{
	private final String TAG = "MK_FriendAA";
	private Context mContext;
	private Activity mActivity;
	private ImageDownloader mImageDownloader;
	private ArrayList<MkFriend> friendList;

	public FriendReqArrayAdapter(Context context, int textViewResourceId, ArrayList<MkFriend> friendList) {
		super(context, textViewResourceId, friendList);
		mContext = context;
		mImageDownloader = new ImageDownloader(mContext);
		this.friendList = friendList;
		this.mActivity = (Activity)mContext;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.friend_req_list_item, null);
		}
		
		MkFriend curFriend = friendList.get(position);
		ImageView ivThumbnail = (ImageView) curView.findViewById(R.id.ivFriendItemThumbnail);
		Button btnRight = (Button) curView.findViewById(R.id.btnFriendItemRightBtn);
		Button btnRight2 = (Button) curView.findViewById(R.id.btnFriendItemRightBtn2);
		TextView tvName = (TextView) curView.findViewById(R.id.tvFriendItemName);
		ImageButton ibMessage = (ImageButton) curView.findViewById(R.id.ibFriendItemMessage);
		
		ivThumbnail.setOnClickListener(this);
		btnRight.setOnClickListener(this);
		btnRight2.setOnClickListener(this);
		tvName.setOnClickListener(this);
		ibMessage.setOnClickListener(this);
		
		View viewList[] = new View[] { ivThumbnail, btnRight, btnRight2, tvName, ibMessage };
		Lhy_Function.setViewTag(viewList, position);
		
		mImageDownloader.download(C.MK_BASE + curFriend.getFriend_url_photo().replaceAll("/data/", "/thumb_data/data/"), ivThumbnail);
		tvName.setText(curFriend.getFriend_nm());
		
		return curView;
	}

	@Override
	public void onClick(View v) {
		int position;
		String friend_idx;
		switch(v.getId()){
		case R.id.ivFriendItemThumbnail:
		case R.id.tvFriendItemName:
			position = (Integer) v.getTag();
			friend_idx = friendList.get(position).getFriend_idx();
			Intent intent = new Intent(mActivity, ProfileActivity.class);
			intent.putExtra("profile_idx", friend_idx);
			mActivity.startActivity(intent);
			break;
		case R.id.ibFriendItemMessage:
			position = (Integer) v.getTag();
			friend_idx = friendList.get(position).getFriend_idx();
			intent = new Intent(mActivity, ProfileImeActivity.class);
			intent.putExtra("opp_idx", friend_idx);
			mActivity.startActivity(intent);
			break;
		case R.id.btnFriendItemRightBtn:
			position = (Integer) v.getTag();
			Log.d(TAG, "pos: " + position);
			new FriendAddTask(friendList.get(position).getFriend_idx(), position, friendList, this, "模备 荐遏吝...", true, mContext).execute();
			break;
		case R.id.btnFriendItemRightBtn2:
			position = (Integer) v.getTag();
			Log.d(TAG, "pos: " + position);
			new FriendDenyTask(friendList.get(position).getFriend_idx(), position, friendList, this, "模备 芭例吝...", true, mContext).execute();
			break;
		}
	}
}
