/**
 * FriendAddTask
 * 친구 추가 액션
 *  친구요청목록에서의 액션
 */
package com.anth.moongka.friend;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.function.MkApi;

public class FriendDenyTask extends MkTask {
	private final String TAG = "MK_FriendDelTask";
	private Context mContext;
	
	private boolean isCert = false;
	private ArrayList<MkFriend> friendList;
	private int position;
	private ArrayAdapter mAdapter;
	private String frd_idx;

	public FriendDenyTask(String frd_idx, int position, ArrayList<MkFriend> friendList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.frd_idx = frd_idx;
		this.friendList = friendList;
		this.mAdapter = mAdapter;
		this.position = position;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.friendDeny(frd_idx);
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			friendList.remove(position);
			mAdapter.notifyDataSetChanged();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
		
	}


	
}
