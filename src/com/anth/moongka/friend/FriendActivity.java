/** 
 * FriendActivity
 *  친구
 *   Push에서 왔을경우, 알림(Notify)에서 왔을경우, 메뉴에서 클릭해서 왔을경우 따로 작동
 */
package com.anth.moongka.friend;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.function.C;

public class FriendActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_FriendActivity";
	private Context mContext;
	
	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;
	
	private ArrayList<MkFriend> friendList = new ArrayList<MkFriend>();
	private ListView lvFriend;
	private FriendListArrayAdapter mListAA;
	private FriendReqArrayAdapter mReqAA;
	
	private Button btnList, btnReq;
	private final int LIST = 192;
	private final int REQ = 193;
	private int status = LIST;
	private String inviteF;
	private int intInvite;
	
	Button btnReqFriend;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.friend);
		
		viewTop = findViewById(R.id.viewTopFriend);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("친구");
//		
		inviteF = C.frd_invite;
		Log.i("invite", ""+inviteF);
		
		btnReqFriend = (Button) findViewById(R.id.btnFriendReq);
		
		lvFriend = (ListView) findViewById(R.id.lvFriend);
		mListAA = new FriendListArrayAdapter(mContext, R.layout.friend_list_item, friendList);
		mReqAA = new FriendReqArrayAdapter(mContext, android.R.layout.simple_list_item_1, friendList);
		lvFriend.setAdapter(mListAA);
		
		//intInvite = Integer.parseInt(inviteF);
		
		if(!C.frd_invite.equals("0")){
			
			btnReqFriend.setText("요청목록  +" + C.frd_invite);
		}else{
			btnReqFriend.setText("요청목록");
		}
		
		if(C.isFromGCM || C.isFromNotify){
			lvFriend.setAdapter(mReqAA);
			new FriendReqListTask(C.mem_idx, friendList, mReqAA, "신청목록 불러오는 중...", true, mContext).execute();
			status = REQ;
			C.isFromGCM = false;
			C.isFromNotify = false;
		} else{
			new FriendCurListTask(C.mem_idx, friendList, mListAA, "친구목록 불러오는 중...", true, mContext).execute();	
		}
		
		btnList = (Button) findViewById(R.id.btnFriendList);
		btnReq = (Button) findViewById(R.id.btnFriendReq);
		btnList.setOnClickListener(this);
		btnReq.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnFriendList:
			if(status==REQ){
				friendList.clear();
				lvFriend.setAdapter(mListAA);
				new FriendCurListTask(C.mem_idx, friendList, mListAA, "친구목록 불러오는 중...", true, mContext).execute();
				if(!C.frd_invite.equals("0")){
					
					btnReqFriend.setText("요청목록  +" + C.frd_invite);
				}else{
					btnReqFriend.setText("요청목록");
				}
				status = LIST;
			}
			break;
		case R.id.btnFriendReq:
			if(status==LIST){
				friendList.clear();
				lvFriend.setAdapter(mReqAA);
				new FriendReqListTask(C.mem_idx, friendList, mReqAA, "신청목록 불러오는 중...", true, mContext).execute();
				if(!C.frd_invite.equals("0")){
					Log.i("o", C.frd_invite);
					btnReqFriend.setText("요청목록  +" + C.frd_invite);
				}else{
					btnReqFriend.setText("요청목록");
				}
				status = REQ;
			}
			break;
		}
	}
	
	
}
