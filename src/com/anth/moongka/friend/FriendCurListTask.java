/**
 * FriendCurListTask
 *  친구목록 액션
 */
package com.anth.moongka.friend;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkFriend;
import com.anth.moongka.function.MkApi;

public class FriendCurListTask extends MkTask{
	private final String TAG = "MK_FriendListTask";
	private Context mContext;
	
	private boolean isCert = false;
	private String mem_idx;
	private ArrayList<MkFriend> friendList;
	private ArrayAdapter mAdapter;

	public FriendCurListTask(String mem_idx, ArrayList<MkFriend> friendList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext= context;
		this.friendList = friendList;
		this.mAdapter = mAdapter;
		this.mem_idx = mem_idx;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.friendList(mem_idx, friendList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		
		if(isCert){
			mAdapter.notifyDataSetChanged();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

	
	
}
