/**
 * SearchActivity
 *  투명 검색창
 */
package com.anth.moongka.main;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkCode;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkPref;

public class SearchNewActivity extends MkActivity implements OnClickListener,
		OnItemSelectedListener {
	private final String TAG = "MK_SearchNewActivity";
	private Context mContext;

	private View viewFull;
	private EditText et;
	private Spinner spSearch;
	private SearchSpAA mAdapter;
	private ArrayList<MkCode> codeList = new ArrayList<MkCode>();
	private Button btnSearch;

	private String searchType;
	private String searchTxt;
	private String searchSort;

	private View viewBlank;
	public boolean isFirst = true;
	private String order;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_new);
		mContext = this;

		codeList.add(new MkCode("전체", ""));
		codeList.add(new MkCode("일반", "C101"));
		codeList.add(new MkCode("튜닝", "C102"));
		codeList.add(new MkCode("여행", "C103"));
		codeList.add(new MkCode("구매/시승", "C104"));
		// codeList.add(new MkCode("보험", "C105"));
		codeList.add(new MkCode("기타", "C106"));

		order = MkPref.getSort();
		Log.i("sort_search", order);

		viewFull = findViewById(R.id.viewSearchFull);
		viewBlank = findViewById(R.id.viewSearchBlank);
		et = (EditText) findViewById(R.id.etSearch);
		btnSearch = (Button) findViewById(R.id.btnSearch);
		spSearch = (Spinner) findViewById(R.id.spSearch);
		mAdapter = new SearchSpAA(mContext,
				android.R.layout.simple_list_item_1, codeList);
		spSearch.setAdapter(mAdapter);

		viewBlank.setOnClickListener(this);
		btnSearch.setOnClickListener(this);
		spSearch.setOnItemSelectedListener(this);

		et.post(new Runnable() {
			@Override
			public void run() {
				Lhy_Function.forceShowKeyboard(mContext, et);
			}
		});

		viewFull.post(new Runnable() {
			@Override
			public void run() {
				viewFull.setY(C.TOP_HEIGHT);
			}
		});
		viewBlank.post(new Runnable() {
			@Override
			public void run() {
				viewBlank.setY(C.TOP_HEIGHT * 2.5f);
			}
		});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.viewSearchBlank:
			finish();
			break;
		case R.id.btnSearch:
			searchTxt = et.getText().toString();

			MkPref.setSort(order);
			Intent intent = new Intent();
			intent.putExtra("type", searchType);
			intent.putExtra("txt", searchTxt);
			intent.putExtra("sort", searchSort);
			setResult(RESULT_OK, intent);
			// startActivity(intent);
			finish();

			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position,
			long id) {
		switch (parent.getId()) {
		case R.id.spSearch:
			searchType = codeList.get(position).getCode();
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

}
