/**
 * MainPostGCMTask
 *  GCM으로부터 왔을경우의 ArrayAdapter
 */
package com.anth.moongka.main;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class MainPostGCMTask extends MkTask{
	private final String TAG = "MK_MainPostGCMTask";
	private Context mContext;
	private MainActivity mActivity;
	
	private ArrayList<MkPost> postList;
	private ArrayAdapter mAdapter;
	private String mem_idx, writer_idx, keyId;
	private int page_num;
	private boolean isCert = false;
	private boolean isRefresh = false;
	
	public MainPostGCMTask(String keyId, ArrayList<MkPost> postList, ArrayAdapter mAdapter, String msg, boolean isPdialog, Context context) {
		super(msg, isPdialog, context);
		mContext = context;
		this.postList = postList;
		this.mAdapter = mAdapter;
		this.keyId = keyId;
		mActivity = (MainActivity) mContext;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mActivity.isAdding = true;
		Log.d(TAG, "MainPost Add Start. page_num : " + mActivity.page_num);
	}

	
	@Override
	protected Void doInBackground(Void... params) {
		Log.d(TAG, "PageNum : " + page_num);
		isCert = MkApi.postGCMList(keyId, postList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mAdapter.notifyDataSetChanged();
		mActivity.page_num += 1;
		mActivity.isAdding = false;
		if(isRefresh) mActivity.completeRefresh();
		C.isFromGCM = false;
		C.isFromNotify = false;
		Log.d(TAG, "MainPost Add End. page_num : " + mActivity.page_num);
	}

	

}
