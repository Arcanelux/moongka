/**
 * MainPostTask
 * �������ǵ� �ҷ����� �׼�
 */
package com.anth.moongka.main;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class MainPostTask extends MkTask{
	private final String TAG = "MK_MainPostTask";
	private Context mContext;
	private MainActivity mActivity;
	
	private ArrayList<MkPost> postList;
	private ArrayAdapter mAdapter;
	private String mem_idx, writer_idx;
	private int page_num;
	private String order;
	private boolean isCert = false;
	private boolean isRefresh = false;
	
	public MainPostTask(ArrayList<MkPost> postList, ArrayAdapter mAdapter, String mem_idx, String order, String writer_idx, int page_num, String msg, boolean isPdialog, boolean isRefresh, Context context) {
		super(msg, isPdialog, context);
		mContext = context;
		this.postList = postList;
		this.mAdapter = mAdapter;
		this.mem_idx = mem_idx;
		this.order = order;
		this.writer_idx = writer_idx;
		this.page_num = page_num;
		this.isRefresh = isRefresh;
		mActivity = (MainActivity) mContext;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mActivity.isAdding = true;
		
		Log.d(TAG, "MainPost Add Start. page_num : " + mActivity.page_num);
	}

	
	@Override
	protected Void doInBackground(Void... params) {
		Log.d(TAG, "PageNum : " + page_num);
		isCert = MkApi.postList(postList, mem_idx, order, writer_idx, page_num);
		
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mAdapter.notifyDataSetChanged();
		mActivity.page_num += 1;
		mActivity.isAdding = false;
		if(isRefresh) mActivity.completeRefresh();
		C.isFromGCM = false;
		C.isFromNotify = false;
		Log.d(TAG, "MainPost Add End. page_num : " + mActivity.page_num);
	}

	

}
