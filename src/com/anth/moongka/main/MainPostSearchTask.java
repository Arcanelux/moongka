/**
 * MainPostSearchTask
 *  �˻��׼�
 */
package com.anth.moongka.main;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.MkApi;

public class MainPostSearchTask extends MkTask{
	private final String TAG = "MK_MainPostTask";
	private Context mContext;
	private MainActivity mActivity;
	
	private ArrayList<MkPost> postList;
	private ArrayAdapter mAdapter;
	private String mem_idx, writer_idx;
	private String type, txt, sort;
	private int page_num;
	private boolean isCert = false;
	private boolean isRefresh = false;
	
	public MainPostSearchTask(ArrayList<MkPost> postList, ArrayAdapter mAdapter, String mem_idx, int page_num, String type, String txt, String sort, String msg, boolean isPdialog, Context context) {
		super(msg, isPdialog, context);
		mContext = context;
		this.postList = postList;
		this.mAdapter = mAdapter;
		this.mem_idx = mem_idx;
		this.writer_idx = writer_idx;
		this.page_num = page_num;
		this.type = type;
		this.txt = txt;
		this.sort = sort;
		mActivity = (MainActivity) mContext;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mActivity.isAdding = true;
		Log.d(TAG, "MainPost Add Start. page_num : " + mActivity.page_num);
	}

	
	@Override
	protected Void doInBackground(Void... params) {
		Log.d(TAG, "PageNum : " + page_num);
		isCert = MkApi.postSearchList(postList, mem_idx, page_num, type, txt, sort);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mAdapter.notifyDataSetChanged();
		MainActivity.isSearchFirst = false;
		mActivity.page_num += 1;
		mActivity.isAdding = false;
		if(isRefresh) mActivity.completeRefresh();
		Log.d(TAG, "MainPost Add End. page_num : " + mActivity.page_num);
	}

	

}
