package com.anth.moongka.main;

import android.content.Context;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class MainPostDelTask extends MkTask{
	private final String TAG = "MK_MainPostDelTask";
	private Context mContext;
	
	private String bod_idx;
	private boolean isCert = false;

	public MainPostDelTask(String bod_idx, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.bod_idx = bod_idx;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.postDel(bod_idx);
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		if(isCert){
			MainActivity.resetList();
		}
		
	}


}
