/**
 * SearchActivity
 *  투명 검색창
 */
package com.anth.moongka.main;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ToggleButton;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkPref;

public class SearchActivity extends MkActivity implements OnClickListener, OnCheckedChangeListener{
	private final String TAG = "MK_SearchActivity";
	private Context mContext;

	private ToggleButton tbAll, tbNormal, tbTravel, tbInsurance, tbTuning, tbBuy, tbOther;
	private ToggleButton tbAlignFavorite, tbAlignRecent;
	private EditText et;
	private Button btnSearch;

	private ToggleButton[] tbList1;
	private ToggleButton[] tbList2;

	private String searchType;
	private String searchTxt;
	private String searchSort;
	
	public boolean isFirst = true;
	private String order;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);

		tbAll = (ToggleButton) findViewById(R.id.tbSearchAll);
		tbNormal = (ToggleButton) findViewById(R.id.tbSearchNormal);
		tbTravel = (ToggleButton) findViewById(R.id.tbSearchTravel);
		tbInsurance = (ToggleButton) findViewById(R.id.tbSearchInsurance);
		tbTuning = (ToggleButton) findViewById(R.id.tbSearchTuning);
		tbBuy = (ToggleButton) findViewById(R.id.tbSearchBuy);
		tbOther = (ToggleButton) findViewById(R.id.tbSearchOther);

		order = MkPref.getSort();
		Log.i("sort_search", order);
		tbAlignFavorite = (ToggleButton) findViewById(R.id.tbSearchAlignFavorite);
		tbAlignRecent = (ToggleButton) findViewById(R.id.tbSearchAlignRecent);

		et = (EditText) findViewById(R.id.etSearch);
		btnSearch = (Button) findViewById(R.id.btnSearch);

		tbList1 = new ToggleButton[] { tbAll, tbNormal, tbTravel, tbInsurance, tbTuning, tbBuy, tbBuy };
		tbList2 = new ToggleButton[] { tbAlignFavorite, tbAlignRecent };
		Lhy_Function.setViewCheckListener(tbList1, this);
		Lhy_Function.setViewCheckListener(tbList2, this);

		btnSearch.setOnClickListener(this);

		tbAll.setChecked(true);
		tbAlignFavorite.setChecked(true);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnSearch:
			searchTxt = et.getText().toString();
			if(searchTxt.equals("")){
				MkPref.setSort(order);
				break;
			}else{
				
				Log.i("sort_search", ""+MkPref.getSort());
				Intent intent = new Intent(SearchActivity.this, MainActivity.class);
				intent.putExtra("type", searchType);
				intent.putExtra("txt", searchTxt);
				intent.putExtra("sort", searchSort);
				startActivity(intent);
				finish();
				break;
			}
			
			
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		for(ToggleButton curTb : tbList1){
			if(curTb.getId()==buttonView.getId()){
				if(isChecked) setToggleOffOther1(buttonView.getId());
			}
		}

		for(ToggleButton curTb : tbList2){
			if(curTb.getId()==buttonView.getId()){
				if(isChecked) setToggleOffOther2(buttonView.getId());
			}
		}

		if(isChecked){
			switch(buttonView.getId()){
			case R.id.tbSearchAll:
				searchType = "C100";
				break;
			case R.id.tbSearchNormal:
				searchType = "C101";
				break;
			case R.id.tbSearchTravel:
				searchType = "C102";
				break;
			case R.id.tbSearchInsurance:
				searchType = "C103";
				break;
			case R.id.tbSearchTuning:
				searchType = "C104";
				break;
			case R.id.tbSearchBuy:
				searchType = "C105";
				break;
			case R.id.tbSearchOther:
				searchType = "C106";
				break;
			}
		}
		if(isChecked){
			switch(buttonView.getId()){
			case R.id.tbSearchAlignFavorite:
				MkPref.setSort("like");
				break;
			case R.id.tbSearchAlignRecent:
				MkPref.setSort("date");
				break;
			}
		}
	}

	private void setToggleOffOther1(int tbId){
		for(ToggleButton curTb : tbList1){
			if(!(curTb.getId()==tbId)) { curTb.setChecked(false); }
		}
	}

	private void setToggleOffOther2(int tbId){
		for(ToggleButton curTb : tbList2){
			if(!(curTb.getId()==tbId)) { curTb.setChecked(false); }
		}
	}

}
