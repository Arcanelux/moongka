/**
 * MainActivity
 * 컨텐츠피드
 *  PullToRefresh 구현
 *  어디서 왔는지에 따라 내용표출 분기 (GCM, 알림, 검색)
 */
package com.anth.moongka.main;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkMenuActivity;
import com.anth.moongka.fix_value.MkDialog2;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.function.MkPref;
import com.anth.moongka.function.PullToRefreshView;
import com.anth.moongka.function.PullToRefreshView.Listener;
import com.anth.moongka.function.PullToRefreshView.MODE;
import com.flurry.android.FlurryAgent;

public class MainActivity extends MkMenuActivity implements OnItemClickListener {
	private final String TAG = "MK_MainActivity";
	private static Context mContext;
	private MkDialog2 mExitDialog;

	private TextView tvOil;
	private ListView lvMain;
	private PullToRefreshView pullView = null;
	private static ArrayList<MkPost> postList = new ArrayList<MkPost>();
	private static MainMkPostAA mPostAA;
	
	public int page_num = 1;
	// public static String order = "date";
	public boolean isAdding = false;
	public static boolean isSearchFirst = true;
	public static boolean isFromSearch = false;

	private static String order;

	/** FromSearch **/
	private String type, txt, sort;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.main_list);

		isFromSearch = false;

		order = MkPref.getSort();
		Log.i("sort_main", order);

		/** PullToRefresh ListView **/
		pullView = (PullToRefreshView) findViewById(R.id.pull_to_refresh);
		pullView.setListener(new Listener() {
			@Override
			public void onChangeMode(MODE mode) {
				Log.w("cranix", "pullView:" + mode);
				switch (mode) {
				case NORMAL:
					lvMain.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_DISABLED);
					break;
				case PULL:
				case READY_TO_REFRESH:
					if (pullView.isTop()) {
						lvMain.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_DISABLED);
					}
					break;
				case REFRESH:
					page_num = 1;
					order = MkPref.getSort();
					Log.i("sort_refresh", order);
					// new MainPostTask(postList, C.mem_idx, "", 1,
					// "포스트 로딩중...", false, true, mContext).execute();
					new MainPostTask(postList, mPostAA, C.mem_idx, order, "",
							page_num, "포스트 로딩중...", false, true, mContext)
							.execute();
					break;
				}
			}
		});

		tvOil = (TextView) findViewById(R.id.tvMainOil);
		tvOil.setTextColor(Color.WHITE);
		tvOil.setTextSize(20);
		tvOil.setSelected(true);
		lvMain = (ListView) findViewById(R.id.lvMain);
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View footer = inflater.inflate(R.layout.main_list_footer, null);
		lvMain.addFooterView(footer);

		mPostAA = new MainMkPostAA(mContext, R.layout.main_list_item, postList);
		lvMain.setAdapter(mPostAA);
		lvMain.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (!isFromSearch)
					pullView.touchDelegate(v, event);
				return false;
			}
		});
		lvMain.setOnScrollListener(new OnScrollListener() {
			// @Override
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}

			// @Override
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				View firstView = view.getChildAt(0);
				if (firstView == null) {
					return;
				}
				if (pullView == null) {
					return;
				}
				if (firstVisibleItem == 0 && firstView.getTop() == 0) {
					Log.w("cranix", "firstVisibleItem:" + firstVisibleItem
							+ ",firstView.getTop():" + firstView.getTop());
					pullView.setTop(true);
				} else {
					pullView.setTop(false);
				}
			}
		});
		/** From 분기 **/
		if (C.isFromGCM) {
			new MainPostGCMTask(C.GCM_KEY_ID, postList, mPostAA, "포스트 로딩중...",
					true, mContext).execute();
			pullView.setVisibility(View.GONE);
		} else if (C.isFromNotify) {
			Intent intent = getIntent();
			String keyId = intent.getStringExtra("keyId");
			new MainPostGCMTask(keyId, postList, mPostAA, "포스트 로딩중...", true,
					mContext).execute();
			pullView.setVisibility(View.GONE);
			C.isFromNotify = false;
		} else {
			order = MkPref.getSort();
			new MainPostTask(postList, mPostAA, C.mem_idx, order, "", 1,
					"포스트 로딩중...", true, true, mContext).execute();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

	}

	/**
	 * 리스트 추가 로직 MainPostTask에서 추가시작할때 isAdding을 true로 변환, 추가중일때는 더이상 실행되지않음
	 */
	public void addPost() {
		if (!isAdding && isFromSearch) {
			new MainPostSearchTask(postList, mPostAA, C.mem_idx, page_num,
					type, txt, sort, "검색 포스트 로딩중...", isSearchFirst, mContext)
					.execute();
			Log.d(TAG, "FromSearch " + page_num + " : " + type + ", " + txt
					+ ", " + sort);
		} else if (!isAdding) {
			order = MkPref.getSort();
			new MainPostTask(postList, mPostAA, C.mem_idx, order, "", page_num,
					"포스트 로딩중...", false, true, mContext).execute();
		}
	}

	public void completeRefresh() {
		pullView.completeRefresh();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Lhy_Function.clearCache(mContext);
		C.isFromGCM = false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
//			case 10:
//				Intent intent = new Intent(mContext,MkCarProfileActivity.class);
//				startActivity(intent);
//			break;
		}
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case 2:
				int commentNum = data.getIntExtra("commentNum", 0);
				int postPosition = data.getIntExtra("postPosition", 0);
				postList.get(postPosition).setNumComment(commentNum);
				mPostAA.notifyDataSetChanged();
				break;
				
			case MkMenuActivity.TO_SEARCH:
				type = data.getStringExtra("type");
				txt = data.getStringExtra("txt");
				sort = data.getStringExtra("sort");
				isFromSearch = true;
				order = MkPref.getSort();
				postList.clear();
				page_num = 1;
				new MainPostSearchTask(postList, mPostAA, C.mem_idx, page_num,
						type, txt, order, "포스트 로딩중...", isSearchFirst, mContext)
						.execute();
				Log.d(TAG, "FromSearch " + page_num + " : " + type + ", " + txt
						+ ", " + sort);
				break;
			}
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	public static void resetList() {
		postList.clear();
		order = MkPref.getSort();
		new MainPostTask(postList, mPostAA, C.mem_idx, order, "", 1,
				"포스트 로딩중...", true, true, mContext).execute();
	}

	// @Override
	// public void finish() {
	// mExitDialog = new MkDialog2(mContext);
	// mExitDialog.setContent("정말로 종료하시겠습니까?");
	// mExitDialog.setBtn1("예");
	// mExitDialog.setBtn1ClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// mExitDialog.dismiss();
	// System.exit(0);
	// }
	// });
	// mExitDialog.setBtn2("아니오");
	// mExitDialog.setBtn2ClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// mExitDialog.dismiss();
	// }
	// });
	// mExitDialog.show();
	// }

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, "87JF7K564SHB2KKSQV43");
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == 4) {
			setDialog();
			return false;
		} else
			return super.onKeyDown(keyCode, event);
	}

	public void setDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("종료하시겠습니까?")
				.setCancelable(false)
				.setPositiveButton("아니오",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {

							}
						})
				.setNegativeButton("예", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				});
		builder.create().show();
	}
}
