/**
 * MainMkPostAA
 *  컨텐츠피드 ArrayAdapter
 *  각 Item의 클릭리스너 포함
 */
package com.anth.moongka.main;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.comment.CommentActivity;
import com.anth.moongka.fix_task.MkAlbumDetailTask;
import com.anth.moongka.fix_task.MkLikeTask;
import com.anth.moongka.fix_task.MkScrapTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;
import com.anth.moongka.function.Lhy_Function;
import com.anth.moongka.profile.ProfileActivity;

public class MainMkSinglePostAA extends ArrayAdapter<MkPost> implements
OnClickListener {
	private final String TAG = "MK_PostMainAA";
	private Context mContext;
	private Activity mActivity;
	private ImageDownloader mImageDownloader;

	private ArrayList<MkPost> postList;
	private boolean isUpdateList = true;

	public MainMkSinglePostAA(Context context, int textViewResourceId,
			ArrayList<MkPost> postList) {
		super(context, textViewResourceId, postList);
		this.postList = postList;
		mContext = context;
		mActivity = (Activity) mContext;
		mImageDownloader = new ImageDownloader(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View curView = convertView;
		if (curView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.main_list_item, null);
		}

		MkPost curPost = postList.get(position);
		if (curPost != null) {
			TextView tvName = (TextView) curView
					.findViewById(R.id.tvMainItemName);
			TextView tvDate = (TextView) curView
					.findViewById(R.id.tvMainItemDate);
			TextView tvContent = (TextView) curView
					.findViewById(R.id.tvMainItemContent);
			TextView tvTag = (TextView) curView
					.findViewById(R.id.tvMainItemTag);
			TextView tvLike = (TextView) curView
					.findViewById(R.id.tvMainItemLike);
			TextView tvComment = (TextView) curView
					.findViewById(R.id.tvMainItemComment);
			ImageView ivPhoto = (ImageView) curView
					.findViewById(R.id.ivMainItemPhoto);
			ImageView ivThumbnail = (ImageView) curView
					.findViewById(R.id.ivMainItemThumbnail);

			String name = curPost.getWriter_name();
			String date = curPost.getStrDate();
			String content = curPost.getContent();

			String tag = curPost.getTag();
			int numLike = curPost.getNumLike();
			int numComment = curPost.getNumComment();
			boolean checkLike = curPost.isLike();
			boolean checkScrap = curPost.isScrap();
			String categoryTag = curPost.getCategory_cd();
			Log.i("checkLike", "" + checkLike);
			String urlImg = curPost.getPhoto_url();
			// Log.d(TAG, "urlImg : " + urlImg);
			String urlThumbnail = curPost.getPhoto_url_thumbnail();
			//Log.d(TAG, "urlThumbnail : " + urlThumbnail);

			tvName.setText(name);
			tvDate.setText(date);
			tvContent.setText(content);

			tvTag.setText(tag);
			tvLike.setText(numLike + "");
			tvComment.setText(numComment + "");

			/** 게시물 이미지 다운로드 **/
			if (urlImg == null || urlImg.equals("")) {
				Log.d(TAG, "NoImage!");
				ivPhoto.setVisibility(View.GONE);
			} else {
				ivPhoto.setVisibility(View.VISIBLE);
				urlImg = C.MK_BASE + urlImg;
				mImageDownloader.download(
						urlImg.replaceAll("/data/", "/thumb_data/data/"),
						ivPhoto);
			}

			/** 썸네일 이미지 다운로드 **/
			if (urlThumbnail == null || urlThumbnail.equals("")) {
				Log.d(TAG, "NoThumbImage!");
			} else {
				urlThumbnail = C.MK_BASE + urlThumbnail;
				mImageDownloader.download(
						urlThumbnail.replaceAll("/data/", "/thumb_data/data/"),
						ivThumbnail);
			}
			ImageView ivTag = (ImageView) curView
					.findViewById(R.id.ibmainTagView);
			if (categoryTag.equals("일반")) {
				ivTag.setImageResource(R.drawable.category_normal);
			}else if(categoryTag.equals("튜닝")){
				ivTag.setImageResource(R.drawable.category_tuning);
			}else if(categoryTag.equals("여행")){
				ivTag.setImageResource(R.drawable.category_travel);
			}else if(categoryTag.equals("구매/시승")){
				ivTag.setImageResource(R.drawable.category_purchase);
			}else if(categoryTag.equals("기타")){
				ivTag.setImageResource(R.drawable.category_etc);
			}

			ImageButton ibLike = (ImageButton) curView
					.findViewById(R.id.ibMainItemLike);
			ImageButton ibComment = (ImageButton) curView
					.findViewById(R.id.ibMainItemComment);
			ImageButton ibScrap = (ImageButton) curView
					.findViewById(R.id.ibMainItemScrap);
			View[] viewList = { ivPhoto, ivThumbnail, tvName, tvDate, ibLike,
					ibComment, ibScrap, ivPhoto };
			Lhy_Function.setViewTag(viewList, position);
			if (checkLike) {
				ibLike.setImageResource(R.drawable.bt_like);

			} else {
				ibLike.setImageResource(R.drawable.bt_like_gray);	
			}

			if (checkScrap) {
				ibScrap.setImageResource(R.drawable.bt_scrap);
			} else {
				ibScrap.setImageResource(R.drawable.bt_scrap_gray);	
			}
			ivPhoto.setOnClickListener(this);
			ivThumbnail.setOnClickListener(this);
			tvName.setOnClickListener(this);
			tvDate.setOnClickListener(this);

			ibLike.setOnClickListener(this);
			ibComment.setOnClickListener(this);
			ibScrap.setOnClickListener(this);
		}

		/**
		 * 리스트 추가 로직 마지막 리스트를 보고있을경우 addPost()를 실행
		 */

		return curView;
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		int position = 0;
		position = (Integer) v.getTag();
		MkPost curPost = postList.get(position);
		switch (v.getId()) {
		/** Profile로 이동 **/
		case R.id.ivMainItemThumbnail:
		case R.id.tvMainItemName:
		case R.id.tvMainItemDate:
			intent = new Intent(mActivity, ProfileActivity.class);
			intent.putExtra("profile_idx", curPost.getWriter_idx());
			mActivity.startActivity(intent);
			break;
		case R.id.ivMainItemPhoto:
			String bod_idx = curPost.getPost_idx();
			new MkAlbumDetailTask(bod_idx, "이미지 로딩중...", true, mContext)
			.execute();
			break;
		case R.id.ibMainItemLike:
			new MkLikeTask(curPost.getPost_idx(), "CFB_ID", postList, this,
					position, "좋아요 처리중...", true, mContext).execute();
			break;
		case R.id.ibMainItemComment:
			intent = new Intent(mActivity, CommentActivity.class);
			String post_idx = curPost.getPost_idx();
			Log.d(TAG, "post_idx : " + post_idx);
			intent.putExtra("post_idx", post_idx);
			intent.putExtra("post_type", "CFB_ID");
			intent.putExtra("postPosition", position);
			mActivity.startActivityForResult(intent, 2);
			break;
		case R.id.ibMainItemScrap:
			//			new MkScrapTask(curPost.getPost_idx(), "스크랩 처리중...", true, mContext)
			//					.execute();
			new MkScrapTask(curPost.getPost_idx(),postList,position,
					"스크랩 처리중...", true, mContext,this)
			.execute();
			break;
		}
	}
}
