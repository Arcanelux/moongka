/**
 * MainPostGCMTask
 *  GCM으로부터 왔을경우의 ArrayAdapter
 */
package com.anth.moongka.main;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPost;
import com.anth.moongka.function.MkApi;

public class MainPostSingleTask extends MkTask{
	private final String TAG = "MK_MainPostGCMTask";
	private Context mContext;
	private MainSingleActivity mActivity;
	private ArrayList<MkPost> postList;
	private ArrayAdapter mAdapter;
	private String mem_idx, writer_idx, keyId;
	private int page_num;
	private boolean isCert = false;
	private boolean isRefresh = false;
	
	public MainPostSingleTask(String keyId, ArrayList<MkPost> postList, ArrayAdapter mAdapter, String msg, boolean isPdialog, Context context) {
		super(msg, isPdialog, context);
		mContext = context;
		this.postList = postList;
		this.mAdapter = mAdapter;
		this.keyId = keyId;
		//mActivity = (MainSingleActivity) mContext;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	
	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.postGCMList(keyId, postList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mAdapter.notifyDataSetChanged();
	}

	

}
