/**
 * MainActivity
 * 컨텐츠피드
 *  PullToRefresh 구현
 *  어디서 왔는지에 따라 내용표출 분기 (GCM, 알림, 검색)
 */
package com.anth.moongka.main;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkMenuActivity;
import com.anth.moongka.fix_value.MkPost;

public class MainSingleActivity extends MkMenuActivity implements OnItemClickListener{
	private final String TAG = "MK_MainActivity";
	private static Context mContext;

	private ListView lvMain;
	private static ArrayList<MkPost> postList = new ArrayList<MkPost>();
	private static MainMkSinglePostAA mPostAA;

	public int page_num = 1;
	public static String order = "date";
	public boolean isAdding = false;
	public static boolean isSearchFirst = true;
	public static boolean isFromSearch = false;

	/** FromSearch **/
	private String type, txt, sort;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.main_list);

		lvMain = (ListView) findViewById(R.id.lvMain);
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View footer = inflater.inflate(R.layout.main_list_footer, null);
		lvMain.addFooterView(footer);

		mPostAA = new MainMkSinglePostAA(mContext, R.layout.main_list_item, postList);
		lvMain.setAdapter(mPostAA);
		
		Intent intent = getIntent();
		String keyId = intent.getStringExtra("keyId");
		new MainPostSingleTask(keyId, postList, mPostAA,  "포스트 로딩중...", true, mContext).execute();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

	}

}
