/**
 * MkFriend
 *  ��ī ģ�� ��ü
 */
package com.anth.moongka.fix_value;

import java.io.Serializable;

public class MkFriend implements Serializable{
	String friend_idx, friend_id, friend_nm, friend_url_photo;
	boolean isChecked = false;

	public MkFriend(String friend_idx, String friend_id, String friend_nm,
			String friend_url_photo) {
		this.friend_idx = friend_idx;
		this.friend_id = friend_id;
		this.friend_nm = friend_nm;
		this.friend_url_photo = friend_url_photo;
	}

	public MkFriend(String friend_idx, String friend_id, String friend_nm,
			String friend_url_photo, boolean isChecked) {
		this.friend_idx = friend_idx;
		this.friend_id = friend_id;
		this.friend_nm = friend_nm;
		this.friend_url_photo = friend_url_photo;
		this.isChecked = isChecked;
	}

	public String getFriend_id() {
		return friend_id;
	}

	public void setFriend_id(String friend_id) {
		this.friend_id = friend_id;
	}

	public String getFriend_nm() {
		return friend_nm;
	}

	public void setFriend_nm(String friend_nm) {
		this.friend_nm = friend_nm;
	}

	public String getFriend_url_photo() {
		return friend_url_photo;
	}

	public void setFriend_url_photo(String friend_url_photo) {
		this.friend_url_photo = friend_url_photo;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getFriend_idx() {
		return friend_idx;
	}

	public void setFriend_idx(String friend_idx) {
		this.friend_idx = friend_idx;
	}

	
}
