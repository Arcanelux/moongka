/**
 * MkMsg
 * 뭉카 메시지 객체
 */
package com.anth.moongka.fix_value;

import java.io.Serializable;

public class MkMsg implements Serializable{
	String idx;
	String own_user_idx;
	String opp_user_idx;
	String content;
	String type;	//R은 받은, S는 보낸
	String sendTime;
	String receiveTime;
	String writeTime;
	String opp_user_name;
	String opp_user_url_photo;
	
	public MkMsg(String idx, String own_user_idx, String opp_user_idx,
			String content, String type, String sendTime, String receiveTime,
			String writeTime, String opp_user_name, String opp_user_url_photo) {
		this.idx = idx;
		this.own_user_idx = own_user_idx;
		this.opp_user_idx = opp_user_idx;
		this.content = content;
		this.type = type;
		this.sendTime = sendTime;
		this.receiveTime = receiveTime;
		this.writeTime = writeTime;
		this.opp_user_name = opp_user_name;
		this.opp_user_url_photo = opp_user_url_photo;
	}

	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}

	public String getOwn_user_idx() {
		return own_user_idx;
	}

	public void setOwn_user_idx(String own_user_idx) {
		this.own_user_idx = own_user_idx;
	}

	public String getOpp_user_idx() {
		return opp_user_idx;
	}

	public void setOpp_user_idx(String opp_user_idx) {
		this.opp_user_idx = opp_user_idx;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(String receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getWriteTime() {
		return writeTime;
	}

	public void setWriteTime(String writeTime) {
		this.writeTime = writeTime;
	}

	public String getOpp_user_name() {
		return opp_user_name;
	}

	public void setOpp_user_name(String opp_user_name) {
		this.opp_user_name = opp_user_name;
	}

	public String getOpp_user_url_photo() {
		return opp_user_url_photo;
	}

	public void setOpp_user_url_photo(String opp_user_url_photo) {
		this.opp_user_url_photo = opp_user_url_photo;
	}
	
	
}
