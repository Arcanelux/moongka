/**
 * MkPlace
 * ��ī ��� ��ü
 */
package com.anth.moongka.fix_value;

public class MkPlace {
	String name;
	String idx;
	String latX, lonY;
	String distance;
	boolean isCheck = false;
	
	public MkPlace(String name, String idx, String latX, String lonY,
			String distance,boolean isCheck) {
		this.name = name;
		this.idx = idx;
		this.latX = latX;
		this.lonY = lonY;
		this.distance = distance;
		this.isCheck = isCheck;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}

	public String getLatX() {
		return latX;
	}

	public void setLatX(String latX) {
		this.latX = latX;
	}

	public String getLonY() {
		return lonY;
	}

	public void setLonY(String lonY) {
		this.lonY = lonY;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public boolean isCheck() {
		return isCheck;
	}

	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}
	
	
	
}
