/**
 * MkComment
 *  ��ī ��� ��ü
 */
package com.anth.moongka.fix_value;

public class MkComment {
	private String comment_idx;
	private String post_idx;
	private String writer_idx;
	private String writer_id;
	private String writer_name;
	private String photo_url_thumbnail;
	private String content;
	private String strDate;
	



	public MkComment(String comment_idx, String post_idx, String writer_idx,
			String writer_id, String writer_name, String photo_url_thumbnail,
			String content, String strDate) {
		this.comment_idx = comment_idx;
		this.post_idx = post_idx;
		this.writer_idx = writer_idx;
		this.writer_id = writer_id;
		this.writer_name = writer_name;
		this.photo_url_thumbnail = photo_url_thumbnail;
		this.content = content;
		this.strDate = strDate;
	}

	public String getComment_idx() {
		return comment_idx;
	}

	public void setComment_idx(String comment_idx) {
		this.comment_idx = comment_idx;
	}

	public String getWriter_idx() {
		return writer_idx;
	}

	public void setWriter_idx(String writer_idx) {
		this.writer_idx = writer_idx;
	}

	public String getWriter_id() {
		return writer_id;
	}

	public void setWriter_id(String writer_id) {
		this.writer_id = writer_id;
	}

	public String getPhoto_url_thumbnail() {
		return photo_url_thumbnail;
	}

	public void setPhoto_url_thumbnail(String photo_url_thumbnail) {
		this.photo_url_thumbnail = photo_url_thumbnail;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getWriter_name() {
		return writer_name;
	}

	public void setWriter_name(String writer_name) {
		this.writer_name = writer_name;
	}

	public String getPost_idx() {
		return post_idx;
	}

	public void setPost_idx(String post_idx) {
		this.post_idx = post_idx;
	}

	public String getStrDate() {
		return strDate;
	}

	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}
	
	
}
