/**
 * MkDialog
 *  뭉카 기본 Dialog
 *  set(...) 함수에 따라 Dialog의 형태가 변함
 */
package com.anth.moongka.fix_value;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.function.C;

public class MkDialog2 extends Dialog{
	// 양 버튼 색깔이 같음
	private final String TAG = "MK_MoongkarDialog";
	private Context mContext;
//	private static int themeTranslucent = android.R.style.Theme_Translucent_NoTitleBar;
	private static int themeTranslucent = android.R.style.Theme_Dialog;
	
	private TextView tvTitle, tvContent;
	private Button btn1, btn2;
	
	/** 함수로 전달되어온 값 저장. onCreate에서 사용 **/
	private String strTitle, strContent, strBtn1, strBtn2;
	private boolean isTitle, isContent, isBtn1, isBtn2, isBtn1Listener, isBtn2Listener;
	private View.OnClickListener listenerBtn1, listenerBtn2;

	public MkDialog2(Context context) {
		super(context, themeTranslucent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.width = (int)(C.DISPLAY_WIDTH * 1.0f);
//		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//		lpWindow.dimAmount = 1.0f;
		getWindow().setAttributes(lpWindow);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		setContentView(R.layout.moongkar_dialog2);
		
		tvTitle = (TextView) findViewById(R.id.tvMoongkar_DialogTitle);
		tvContent = (TextView) findViewById(R.id.tvMoongkar_DialogContent);
		btn1 = (Button) findViewById(R.id.btnMoongkar_Dialog1);
		btn2 = (Button) findViewById(R.id.btnMoongkar_Dialog2);
		
		tvTitle.setVisibility(View.GONE);
		tvContent.setVisibility(View.GONE);
		btn1.setVisibility(View.GONE);
		btn2.setVisibility(View.GONE);
		
		if(isTitle){
			tvTitle.setVisibility(View.VISIBLE);
			tvTitle.setText(strTitle);
		}
		if(isContent){
			tvContent.setVisibility(View.VISIBLE);
			tvContent.setText(strContent);	
		}
		if(isBtn1){
			btn1.setVisibility(View.VISIBLE);
			btn1.setText(strBtn1);
		}
		if(isBtn2){
			btn2.setVisibility(View.VISIBLE);
			btn2.setText(strBtn2);
		}
		if(isBtn1Listener){
			btn1.setOnClickListener(listenerBtn1);
		}
		if(isBtn2Listener){
			btn2.setOnClickListener(listenerBtn2);
		}
	}
	
	public void setTitle(String msg){
		strTitle = msg;
		isTitle = true;
	}
	public void setContent(String msg){
		strContent = msg;
		isContent = true;
	}
	public void setBtn1(String msg){
		strBtn1 = msg;
		isBtn1 = true;
	}
	public void setBtn2(String msg){
		strBtn2 = msg;
		isBtn2 = true;
	}
	public void setBtn1ClickListener(View.OnClickListener listener){
		listenerBtn1 = listener;
		isBtn1Listener = true;
	}
	public void setBtn2ClickListener(View.OnClickListener listener){
		listenerBtn2 = listener;
		isBtn2Listener = true;
	}
	
	
}
