/**
 * MkPost
 *  ��ī �Խñ� ��ü
 */
package com.anth.moongka.fix_value;


public class MkPost {
	private String post_idx;
	private String writer_idx;
	private String writer_id;
	private String category_cd;
	private String content;
	private int numLike;
	private int numComment;
//	private int numScrap;
	private String strDate;
	private String photo_idx;
	private String photo_url;
	private String photo_url_thumbnail;
	private String writer_name;
	private String tag;
	private boolean isLike;
	private boolean isScrap;
	private String imgCnt;


	public MkPost(String post_idx, String writer_idx, String writer_id,
			String category_cd, String content, int numLike, int numComment,
			String strDate, String photo_idx, String photo_url,
			String photo_url_thumbnail, String writer_name, String tag, boolean isLike,boolean isScrap, String imgCnt) {
		this.post_idx = post_idx;
		this.writer_idx = writer_idx;
		this.writer_id = writer_id;
		this.category_cd = category_cd;
		this.content = content;
		this.numLike = numLike;
		this.numComment = numComment;
//		this.numScrap = numScrap;
		this.strDate = strDate;
		this.photo_idx = photo_idx;
		this.photo_url = photo_url;
		this.photo_url_thumbnail = photo_url_thumbnail;
		this.writer_name = writer_name;
		this.tag = tag;
		this.isLike = isLike;
		this.isScrap = isScrap;
		this.imgCnt = imgCnt;
	}
	public MkPost(String post_idx, String writer_idx, String writer_id,
			String category_cd, String content, int numLike, int numComment,
			String strDate, String photo_idx, String photo_url,
			String photo_url_thumbnail, String writer_name, String tag, boolean isLike,boolean isScrap) {
		this.post_idx = post_idx;
		this.writer_idx = writer_idx;
		this.writer_id = writer_id;
		this.category_cd = category_cd;
		this.content = content;
		this.numLike = numLike;
		this.numComment = numComment;
//		this.numScrap = numScrap;
		this.strDate = strDate;
		this.photo_idx = photo_idx;
		this.photo_url = photo_url;
		this.photo_url_thumbnail = photo_url_thumbnail;
		this.writer_name = writer_name;
		this.tag = tag;
		this.isLike = isLike;
		this.isScrap = isScrap;
	}

	public String getPost_idx() {
		return post_idx;
	}

	public void setPost_idx(String post_idx) {
		this.post_idx = post_idx;
	}

	public String getWriter_idx() {
		return writer_idx;
	}

	public void setWriter_idx(String writer_idx) {
		this.writer_idx = writer_idx;
	}

	public String getWriter_id() {
		return writer_id;
	}

	public void setWriter_id(String writer_id) {
		this.writer_id = writer_id;
	}

	public String getCategory_cd() {
		return category_cd;
	}

	public void setCategory_cd(String category_cd) {
		this.category_cd = category_cd;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getNumLike() {
		return numLike;
	}

	public void setNumLike(int numLike) {
		this.numLike = numLike;
	}

	public int getNumComment() {
		return numComment;
	}

	public void setNumComment(int numComment) {
		this.numComment = numComment;
	}



	public String getStrDate() {
		return strDate;
	}

	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}

	public String getPhoto_idx() {
		return photo_idx;
	}

	public void setPhoto_idx(String photo_idx) {
		this.photo_idx = photo_idx;
	}

	public String getPhoto_url() {
		return photo_url;
	}

	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}

	public String getPhoto_url_thumbnail() {
		return photo_url_thumbnail;
	}

	public void setPhoto_url_thumbnail(String photo_url_thumbnail) {
		this.photo_url_thumbnail = photo_url_thumbnail;
	}

	public String getWriter_name() {
		return writer_name;
	}

	public void setWriter_name(String writer_name) {
		this.writer_name = writer_name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean isLike) {
		this.isLike = isLike;
	}
	public boolean isScrap(){
		return isScrap;
	}
	public void setScrap(boolean isScrap){
		this.isScrap = isScrap;
	}

	public String getImgCnt() {
		return imgCnt;
	}

	public void setImgCnt(String imgCnt) {
		this.imgCnt = imgCnt;
	}

}
