/**
 * MkCode
 * 기본 뭉카코드
 * 뭉카의 모든 코드는 이 형식으로 작성됨
 */
package com.anth.moongka.fix_value;

public class MkCode {
	String name;
	String code;
	
	public MkCode(String name, String code) {
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
