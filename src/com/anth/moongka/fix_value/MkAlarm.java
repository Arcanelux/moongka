/**
 * MkAlarm
 *  ��ī �˸� ��ü
 */
package com.anth.moongka.fix_value;

import java.io.Serializable;

public class MkAlarm implements Serializable{
	private String idx;
	private String content;
	private String url_photo;
	private String write_time;
	private String kindCode;
	private String keyId;
	private String opp_name;
	public MkAlarm(String idx, String content, String url_photo,
			String write_time, String kindCode, String keyId,String opp_name) {
		super();
		this.idx = idx;
		this.content = content;
		this.url_photo = url_photo;
		this.write_time = write_time;
		this.kindCode = kindCode;
		this.keyId = keyId;
		this.opp_name = opp_name;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUrl_photo() {
		return url_photo;
	}
	public void setUrl_photo(String url_photo) {
		this.url_photo = url_photo;
	}
	public String getWrite_time() {
		return write_time;
	}
	public void setWrite_time(String write_time) {
		this.write_time = write_time;
	}
	public String getKindCode() {
		return kindCode;
	}
	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}
	public String getKeyId() {
		return keyId;
	}
	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}
	public String getOpp_name() {
		return opp_name;
	}
	public void setOpp_name(String opp_name) {
		this.opp_name = opp_name;
	}

	
	
}
