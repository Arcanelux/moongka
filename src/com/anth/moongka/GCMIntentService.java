package com.anth.moongka;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.anth.moongka.function.C;
import com.anth.moongka.function.MkPref;
import com.anth.moongka.function.SavedValue;
import com.anth.moongka.loading.LoadingActivity;
import com.anth.moongka.menu.MenuTop;
import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
	private static final String tag = "MK_GCMIntentService";
	/**
	 * 실서버
	 */
	public static final String SEND_ID = "974177009947";
	/**
	 * 테스트용
	 */
	// public static final String SEND_ID = "825326893660";
	public String message;

	private String str;

	public GCMIntentService() {
		this(SEND_ID);
	}
	

	public GCMIntentService(String senderId) {
		super(senderId);
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			Log.d(TAG, "send messaged : " + message);
			Context context = getApplicationContext();
			int duration = Toast.LENGTH_LONG;
			Toast toast = Toast.makeText(context, message, duration);
			toast.show();
		}
	};

	public void GET_GCM() {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				handler.sendEmptyMessage(0);
			}
		});
		thread.start();
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		String msg = intent.getStringExtra("msg");
		String kindCode = intent.getStringExtra("NN_KIND_CD");
		String keyId = intent.getStringExtra("NN_KEY_ID");
		//String yourId = intent.getStringExtra("oth_idx");
		Log.i("msg",msg );
		Log.i("kindCode",kindCode);
		Log.i("id",keyId );
		try {
			// 진동
			//			Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
			//			vibrator.vibrate(1000);

			if (BuildConfig.DEBUG)
				Log.d(TAG, "PushMsg : " + msg);
		} catch (Exception e) {
			e.printStackTrace();
		}

		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> runList = am.getRunningTasks(10);
		ComponentName name = runList.get(0).topActivity;
		String className = name.getClassName();
		boolean isAppRunning = false;
		// 가로안에 패키지명 기입
		if (className.contains("com.anth.moongka")) {
			isAppRunning = true;
		}
		if (isAppRunning == true) {
			// 앱이 실행중일 경우 로직 구현
//			setNotification(getApplicationContext(), msg, kindCode, keyId);
			//MenuTop.setNewIconVisible(View.VISIBLE);
			MenuTop.startDelayHandler(View.VISIBLE);
		} else {
			// 앱이 실행중이 아닐 때 로직 구현
			SavedValue sValue = MkPref.getSavedValue(getApplicationContext());
			boolean isAlarm = sValue.isAlarm();

//			isAlarm = true;

			if(isAlarm){
				setNotification(getApplicationContext(), msg, kindCode, keyId);
			} else{

			}
		}
	}

	@Override
	protected void onError(Context context, String errorId) {
		Log.d(tag, "onError. errorId : " + errorId);
	}

	@Override
	protected void onRegistered(Context context, String regId) {
		Log.d(tag, "onRegistered. regId : " + regId);
		MkPref.saveGCM(context, regId);
	}

	@Override
	protected void onUnregistered(Context context, String regId) {
		Log.d(tag, "onUnregistered. regId : " + regId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		Log.d(tag, "onRecoverableError. errorId : " + errorId);
		return super.onRecoverableError(context, errorId);
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private void setNotification(Context context, String message,
			String kindCode, String keyId) {
		NotificationManager notificationManager = null;
		Notification notification = null;
		try {
			notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			notification = new Notification(R.drawable.ic_launcher, message,
					System.currentTimeMillis());
			// NotificationCompat.Builder builder = new
			// NotificationCompat.Builder(k
			// GCMIntentService.this);
			// builder.setSmallIcon(R.drawable.ic_launcher)
			// .setContentTitle("알립니다")
			// .setContentText("왔다네~왔다네~ 내~가 왔다네~")
			// .setAutoCancel(true)
			// // 알림바에서 자동 삭제
			// .setVibrate(
			// new long[] { 1000, 2000, 1000, 3000, 1000, 4000 });
			// secondactivity로 넘겨줌
			Intent intent = new Intent(context, LoadingActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// 알림 여러개를 위해 할당
			intent.setAction("" + Math.random());

			// 데이터 전달
			intent.putExtra("kindCode", kindCode);
			Log.i(TAG, "" + kindCode);
			intent.putExtra("keyId", keyId);
			intent.putExtra("fromGCM", true);

			/** kindCode에 따른 분기 전달 **/
			int to = 0;
			if (kindCode.equals("FRND")) {
				to = C.GCM_TO_FRIEND;
			} else if (kindCode.equals("MSGR")) {
				to = C.GCM_TO_MESSAGE;
			} else if (kindCode.equals("RPL1")) {
				to = C.GCM_TO_COMMENT_TXT;
			} else if (kindCode.equals("RPL2")) {
				to = C.GCM_TO_COMMENT_IMAGE;
			} else if (kindCode.equals("NEWA")) {
				to = C.GCM_TO_POST;
			}
			intent.putExtra("toIntent", to);
			intent.putExtra("keyId", keyId);

			PendingIntent pi = PendingIntent.getActivity(context, 0, intent, 0);
			notification.setLatestEventInfo(context, "뭉카", message, pi);
			notification.flags = Notification.FLAG_AUTO_CANCEL;
			// 0 값에 노티카운터를 넣어주면 됨
			// 겹치지않는 랜덤값으로 사용
			notificationManager.notify((int) System.currentTimeMillis(),
					notification);
			// notificationManager.cancel(i);

		} catch (Exception e) {
			Log.e("GCMIntentService",
					"[setNotification] Exception : " + e.getMessage());
		}
	}
}