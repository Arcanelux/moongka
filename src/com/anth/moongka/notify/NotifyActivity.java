/**
 * NotifyActivity
 *  알림
 *  아이콘을 클릭하면 알림 목록을 불러옴
 *  알림이 하나도 없을때의 예외처리 잘못되어있음
 *  알림 클릭했을때의 처리 안되어있음
 */
package com.anth.moongka.notify;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;

import com.anth.moongka.R;
import com.anth.moongka.comment.CommentActivity;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.fix_value.MkAlarm;
import com.anth.moongka.friend.FriendActivity;
import com.anth.moongka.function.C;
import com.anth.moongka.main.MainSingleActivity;
import com.anth.moongka.message.MessageDetailActivity;

public class NotifyActivity extends MkActivity implements OnClickListener, OnItemClickListener{
	private final String TAG = "MK_NotifyActivity";
	private Context mContext;
	
	private View viewBlank;
	private ListView lvNotify;
	private NotifyArrayAdapter mNotifyAA;
	private ArrayList<MkAlarm> notifyList = new ArrayList<MkAlarm>();;
	
	private ImageButton btnClose;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notify);
		mContext = this;
		
		viewBlank = findViewById(R.id.viewNotifyBlank);
		viewBlank.setOnClickListener(this);
		lvNotify = (ListView) findViewById(R.id.lvNotify);
		mNotifyAA = new NotifyArrayAdapter(mContext, R.layout.notify_list_item, notifyList);
		lvNotify.setAdapter(mNotifyAA);
		lvNotify.setOnItemClickListener(this);
		
		btnClose = (ImageButton) findViewById(R.id.ibNotifyClose);
		btnClose.setOnClickListener(this);
		
		new NotifyTask(notifyList, mNotifyAA, "알림 불러오는 중...", true, mContext).execute();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.viewNotifyBlank:
		case R.id.ibNotifyClose:
			finish();
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		Intent intent = null;
		MkAlarm curAlarm = notifyList.get(position);
		String kindCode = curAlarm.getKindCode();
		String idx = curAlarm.getIdx();
		if(kindCode.equals("FRND")){
			C.isFromNotify = true;
			intent = new Intent(NotifyActivity.this, FriendActivity.class);
		} else if(kindCode.equals("MSGR")){
			intent = new Intent(NotifyActivity.this, MessageDetailActivity.class);
			intent.putExtra("own_idx", C.mem_idx);
			intent.putExtra("opp_idx", curAlarm.getKeyId());
			intent.putExtra("opp_name", curAlarm.getOpp_name());
		} else if(kindCode.equals("RPL1")){
			intent = new Intent(NotifyActivity.this, CommentActivity.class);
			intent.putExtra("post_idx", curAlarm.getKeyId());
			intent.putExtra("post_type", "CFB_ID");
			intent.putExtra("keyId", curAlarm.getKeyId());
		} else if(kindCode.equals("RPL2")){
			intent = new Intent(NotifyActivity.this, CommentActivity.class);
			intent.putExtra("post_idx", curAlarm.getKeyId());
			intent.putExtra("post_type", "CPR_ID");
			intent.putExtra("keyId", curAlarm.getKeyId());
		} else if(kindCode.equals("NEWA")){
			C.isFromNotify = true;
			intent = new Intent(NotifyActivity.this, MainSingleActivity.class);
			intent.putExtra("keyId", curAlarm.getKeyId());
		}
		new NotifyCheckTask(idx, notifyList, "알림 체크중...", false, mContext).execute();
		startActivity(intent);
		finish();
	}
}
