/**
 * NotifyTask
 *  알림 목록 액션
 */
package com.anth.moongka.notify;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkAlarm;
import com.anth.moongka.function.MkApi;

public class NotifyTask extends MkTask {
	private final String TAG = "MK_NotifyTask";
	private Context mContext;

	private boolean isCert = false;
	private ArrayList<MkAlarm> alarmList;
	private ArrayAdapter mAdapter;

	public NotifyTask(ArrayList<MkAlarm> alarmList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext= context;
		this.alarmList = alarmList;
		this.mAdapter = mAdapter;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.alarmList(alarmList);
		return super.doInBackground(params);
	}
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			mAdapter.notifyDataSetChanged();
		} else{
			((Activity) mContext).finish();
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}




}
