package com.anth.moongka.notify;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkAlarm;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;
import com.anth.moongka.menu.MenuTop;

public class NotifyCheckTask extends MkTask{
	private final String TAG = "MK_NotifyCheckTask";
	private Context mContext;
	
	private String NN_ID;
	private boolean isCert = false;
	private ArrayList<MkAlarm> notifyList;

	public NotifyCheckTask(String NN_ID, ArrayList<MkAlarm> notifyList, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		this.NN_ID = NN_ID;
		this.notifyList = notifyList;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.alarmCheck(NN_ID);
		return super.doInBackground(params);
	}
	
	@Override
	protected void onPostExecute(Void result) {
		if(isCert){
			if(notifyList.size()<=1){
				C.isExistNotify = false;
				MenuTop.setNewIconVisible(View.INVISIBLE);
			}
			Log.d(TAG, NN_ID + " Alarm Checked");
		} else{
			Log.d(TAG, NN_ID + " Alarm Check Failed");
		}
		super.onPostExecute(result);
	}


}
