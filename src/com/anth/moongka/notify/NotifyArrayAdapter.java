/**
 * NotifyArrayAdapter
 * �˸� ArrayAdapter
 */
package com.anth.moongka.notify;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkAlarm;
import com.anth.moongka.function.C;
import com.anth.moongka.function.ImageDownloader;

public class NotifyArrayAdapter extends ArrayAdapter<MkAlarm>{
	private final String TAG = "MK_NotifyArrayAdapter";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	
	private ArrayList<MkAlarm> notifyLIst;

	public NotifyArrayAdapter(Context context, int textViewResourceId, ArrayList<MkAlarm> notifyList) {
		super(context, textViewResourceId, notifyList);
		mContext = context;
		this.notifyLIst = notifyList;
		mImageDownloader = new ImageDownloader(mContext);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.notify_list_item, null);
		}
		
		MkAlarm curNotify = notifyLIst.get(position);
		ImageView ivThumbnail = (ImageView) curView.findViewById(R.id.ivNotifyItemThumbnail);
		TextView tvContent = (TextView) curView.findViewById(R.id.tvNotifyItemContent);
		TextView tvDate = (TextView) curView.findViewById(R.id.tvNotifyItemDate);
		
		mImageDownloader.download(C.MK_BASE + curNotify.getUrl_photo().replaceAll("/data/", "/thumb_data/data/"), ivThumbnail);
		tvContent.setText(curNotify.getContent());
		tvDate.setText(curNotify.getWrite_time());
		
		return curView;
	}

}
