/**
 * AlbumTask
 *  �ٹ���� �׼�
 */
package com.anth.moongka.album;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.anth.moongka.R;
import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class AlbumTask extends MkTask{
	private final String TAG = "MK_AlbumTask";
	private Context mContext;
	private AlbumActivity mActivity;
	
	private ArrayList<Album> albumList;
	private ArrayAdapter mAdapter;
	private boolean isCert = false;
	private String mem_idx;

	public AlbumTask(String mem_idx, ArrayList<Album> albumList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (AlbumActivity) mContext;
		this.albumList = albumList;
		this.mAdapter = mAdapter;
		this.mem_idx = mem_idx;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.album(mem_idx, albumList);
		Log.i("album", ""+isCert);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			mAdapter.notifyDataSetChanged();
			
			Log.i("albumsize", ""+albumList.size());

			if(albumList.size()==0){
				Log.i("1", "2");
				
			}
		} else{
			FrameLayout mainFrame = (FrameLayout) mActivity.findViewById(R.id.albumMain);
			mainFrame.setBackgroundResource(R.drawable.img_data_android);
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
			
		}
	}
}
