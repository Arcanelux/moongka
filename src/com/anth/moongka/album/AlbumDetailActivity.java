/**
 * AlbumDetailActivity
 *  이전 액티비티에서 imageList와 albumList를 받아온다
 *  
 */
package com.anth.moongka.album;

import java.util.ArrayList;

import lhy.library.imageviewerpager.Image;
import lhy.library.imageviewerpager.ImageViewerPager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.comment.CommentActivity;
import com.anth.moongka.fix_activity.MkActivity;

public class AlbumDetailActivity extends MkActivity implements OnClickListener{
	private final String TAG = "MK_AlbumDetailActivity";
	private Context mContext;

	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;
	
	private ImageViewerPager mImageViewerPager;
	private ArrayList<Image> imageList;
	private ArrayList<Album> albumList;

	private Button btnClose;
	private ImageButton ibLike, ibComment;
	//, ibScrap;
	private TextView tvLike, tvComment;

	int position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.album_detail);
		mContext = this;
		
		viewTop = findViewById(R.id.viewTopAlbumDetail);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("앨범");

		/** Intent로부터 albumList, imageList, position 할당 **/
		Intent intent = getIntent();
		imageList = (ArrayList<Image>) intent.getSerializableExtra("imageList");
		albumList = (ArrayList<Album>) intent.getSerializableExtra("albumList");
		position = intent.getIntExtra("position", 0);

		/** UI Connect **/
		btnClose = (Button) findViewById(R.id.btnAlbumDetailClose);
		ibLike = (ImageButton) findViewById(R.id.ibAlbumDetailLike);
		ibComment = (ImageButton) findViewById(R.id.ibAlbumDetailComment);
		//ibScrap = (ImageButton) findViewById(R.id.ibAlbumDetailScrap);
		tvLike = (TextView) findViewById(R.id.tvAlbumDetailLike);
		tvComment = (TextView) findViewById(R.id.tvAlbumDetailComment);
		
		/** UI ClickListener */
		btnClose.setOnClickListener(this);
		ibLike.setOnClickListener(this);
		ibComment.setOnClickListener(this);
		//ibScrap.setOnClickListener(this);

		/** 현재 Album정보를 레이아웃의 TextView에 삽입 **/
		Album curAlbum = albumList.get(position);
		tvLike.setText(curAlbum.getNumLike()+"");
		tvComment.setText(curAlbum.getNumComment()+"");

		/** ImageViewerPager 설정 **/
		mImageViewerPager = (ImageViewerPager) findViewById(R.id.lhyIVPAlbumDetail);
		mImageViewerPager.setCirculation(false);
		mImageViewerPager.setImageList(imageList);
		mImageViewerPager.setCurrentItem(position);
		mImageViewerPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				int count = mImageViewerPager.COUNT;
				if (position > count) position -= count;

				Album curAlbum = albumList.get(position);
				tvLike.setText(curAlbum.getNumLike()+"");
				tvComment.setText(curAlbum.getNumComment()+"");
				AlbumDetailActivity.this.position = position;
			}
			@Override public void onPageScrolled(int arg0, float arg1, int arg2) { }
			@Override public void onPageScrollStateChanged(int arg0) { }
		});
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnAlbumDetailClose:
			finish();
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			break;
		case R.id.ibAlbumDetailLike:
			new AlbumLikeTask(albumList.get(position).getIdx(), albumList.get(position), tvLike, "좋아요 처리중...", true, mContext).execute();
			break;
		case R.id.ibAlbumDetailComment:
			/** 
			 * Album -> Comment
			 *  bod_idx, bod_type, position을 전달
			 *  onActivityResult에서 변경된 comment의 개수를 받는다
			 */
			intent = new Intent(AlbumDetailActivity.this, CommentActivity.class);
			String post_idx = albumList.get(position).getIdx();
			intent.putExtra("post_idx", post_idx);
			intent.putExtra("post_type", "CPR_ID");
			intent.putExtra("postPosition", position);
			intent.putExtra("imageList", imageList);
			intent.putExtra("iden", 99);
			startActivityForResult(intent, 2);
			break;
//		case R.id.ibAlbumDetailScrap:
////			new MkScrapTask(albumList.get(position).getIdx(), "스크랩 처리중...", true, mContext).execute();
//			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
		case 2:
			/** 
			 * CommentActivity에서 통신 후, 변경이 완료되면
			 * comment의 총 개수를 전달받아 현재 레이아웃의 TextView에 적용
			 * 이 방법의 경우, albumList의 값을 바꾸지는 못하는 오류가 있음
			 */
			int commentNum = data.getIntExtra("commentNum", 0);
			tvComment.setText(commentNum+"");
			break;
		}
	}


}
