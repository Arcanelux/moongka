/**
 * AlbumFromPostTask
 *  컨텐츠피드 등, 앨범목록이 아닌곳에서 AlbumDetail을 불러올때 작동하는 액션
 */
package com.anth.moongka.album;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.MkApi;

public class AlbumFromPostTask extends MkTask{
	private final String TAG = "MK_AlbumTask";
	private Context mContext;
	private AlbumActivity mActivity;
	
	private ArrayList<Album> albumList;
	private ArrayAdapter mAdapter;
	private boolean isCert = false;
	private String bod_idx;

	public AlbumFromPostTask(String bod_idx, ArrayList<Album> albumList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		mActivity = (AlbumActivity) mContext;
		this.albumList = albumList;
		this.bod_idx = bod_idx;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.albumInPost(bod_idx, albumList);
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			mAdapter.notifyDataSetChanged();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}
}
