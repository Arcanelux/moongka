/**
 * AlbumActivity
 *  앨범 액티비티
 *  시작시 AlbumTask(앨범목록 액션)을 실행, GridView에 앨범이미지를 목록으로 보여준다
 *  아이템 클릭시, AlbumDetailActivity로 넘어감
 */
package com.anth.moongka.album;

import java.util.ArrayList;

import lhy.library.imageviewerpager.Image;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;

public class AlbumActivity extends MkActivity implements OnItemClickListener, OnClickListener{
	private final String TAG = "MK_AlbumActivity";
	private Context mContext;
	
	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;
	
	private GridView gvAlbum;
	
	private ArrayList<Album> albumList = new ArrayList<Album>();
	private AlbumArrayAdapter mAlbumAA;
	
	private String mem_idx;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.album);
		
		viewTop = findViewById(R.id.viewTopAlbum);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("앨범");
		
		
		Intent intent = getIntent();
		mem_idx = intent.getStringExtra("mem_idx");
		
		gvAlbum = (GridView) findViewById(R.id.gvAlbum);
		mAlbumAA = new AlbumArrayAdapter(mContext, R.layout.album_grid_item, albumList);
		gvAlbum.setAdapter(mAlbumAA);
		gvAlbum.setOnItemClickListener(this);
		
		new AlbumTask(mem_idx, albumList, mAlbumAA, "앨범 로딩중...", true, mContext).execute();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		ArrayList<Image> imageList = new ArrayList<Image>();
		for(int i=0; i<albumList.size(); i++){
			Album curAlbum = albumList.get(i);
			Image curImage = new Image(curAlbum.getUrlOri());
			imageList.add(curImage);
		}
		Intent intent = new Intent(AlbumActivity.this, AlbumDetailActivity.class);
		intent.putExtra("albumList", albumList);
		intent.putExtra("imageList", imageList);
		intent.putExtra("mem_idx", mem_idx);
		intent.putExtra("position", position);
		startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		}
	}
}
