/**
 * AlbumArrayAdapter
 *  ArrayList<Album> 리스트를 받는 Adapter
 */
package com.anth.moongka.album;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.anth.moongka.R;
import com.anth.moongka.function.ImageDownloader;

public class AlbumArrayAdapter extends ArrayAdapter<Album>{
	private final String TAG = "MK_AlbumArrayAdapter";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	
	private ArrayList<Album> albumList;

	public AlbumArrayAdapter(Context context, int textViewResourceId, ArrayList<Album> albumList) {
		super(context, textViewResourceId, albumList);
		mContext = context;
		this.albumList = albumList;
		mImageDownloader = new ImageDownloader(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.album_grid_item, null);
		}
		
		Album curAlbum = albumList.get(position);
		ImageView ivAlbum = (ImageView) curView.findViewById(R.id.ivAlbumItem);
		mImageDownloader.download(curAlbum.getUrlThumbnail().replaceAll("/data/", "/thumb_data/data/"), ivAlbum);
//		mImageDownloader.download(curAlbum.getUrlThumbnail().replaceAll("/data/", "/data/data/"), ivAlbum);
		
		return curView;
	}
}
