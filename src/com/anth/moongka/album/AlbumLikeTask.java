/**
 * AlbumLikeTask
 *  앨범 좋아요 액션
 */
package com.anth.moongka.album;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.function.C;
import com.anth.moongka.function.MkApi;

public class AlbumLikeTask extends MkTask{
	private final String TAG = "MK_AlbumLikeTask";
	private Context mContext;
	
	private Album mAlbum;
	private TextView tvLike;
	private String bod_idx;
	private boolean isCert = false;

	public AlbumLikeTask(String bod_idx, Album mAlbum, TextView tvLike, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.bod_idx = bod_idx;
		this.tvLike = tvLike;
		this.mAlbum = mAlbum;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.likeToggle(C.mem_idx, bod_idx, "CPR_ID");
		return super.doInBackground(params);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if(isCert){
			int oriNumLike = mAlbum.getNumLike();
			boolean oriIsLike = mAlbum.isLike();
			if(oriIsLike){
				mAlbum.setNumLike(oriNumLike-1);
				tvLike.setText((oriNumLike-1) + "");
				mAlbum.setLike(false);
			} else{
				mAlbum.setNumLike(oriNumLike+1);
				tvLike.setText((oriNumLike+1) + "");
				mAlbum.setLike(true);
			}
			
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		} else{
			Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		}
	}

}
