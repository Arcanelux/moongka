/**
 * Album
 *  �ٹ�����
 */
package com.anth.moongka.album;

import java.io.Serializable;

public class Album implements Serializable{
	private String idx;
	private String urlThumbnail;
	private String urlOri;
	private int numLike;
	private int numComment;
	private int numScrap;
	
	private boolean isLike;
	
	public Album(String idx, String urlThumbnail, String urlOri, int numLike,
			int numComment, int numScrap, boolean isLike) {
		this.idx = idx;
		this.urlThumbnail = urlThumbnail;
		this.urlOri = urlOri;
		this.numLike = numLike;
		this.numComment = numComment;
		this.numScrap = numScrap;
		this.isLike = isLike;
	}

	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}

	public String getUrlThumbnail() {
		return urlThumbnail;
	}

	public void setUrlThumbnail(String urlThumbnail) {
		this.urlThumbnail = urlThumbnail;
	}

	public String getUrlOri() {
		return urlOri;
	}

	public void setUrlOri(String urlOri) {
		this.urlOri = urlOri;
	}

	public int getNumLike() {
		return numLike;
	}

	public void setNumLike(int numLike) {
		this.numLike = numLike;
	}

	public int getNumComment() {
		return numComment;
	}

	public void setNumComment(int numComment) {
		this.numComment = numComment;
	}

	public int getNumScrap() {
		return numScrap;
	}

	public void setNumScrap(int numScrap) {
		this.numScrap = numScrap;
	}

	public boolean isLike() {
		return isLike;
	}

	public void setLike(boolean isLike) {
		this.isLike = isLike;
	}
	
	
	
}
