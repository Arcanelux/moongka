package com.anth.moongka.place;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkMapActivity;
import com.anth.moongka.fix_value.MkPlace;
import com.anth.moongka.function.C;
import com.anth.moongka.mapviewballoons.MyItemizedOverlay;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class PlaceActivity extends MkMapActivity implements
		OnCheckedChangeListener, OnClickListener {
	private final String TAG = "MK_PlaceActivity";
	private Context mContext;

	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;

	/** Place Common **/
	private ToggleButton tbD1, tbD2, tbD5, tbD10;
	private Button btnSearch, btnPlaceBack;

	/** Place1 **/
	private View viewPlace1;
	private ListView lvPlace1;
	private ArrayList<MkPlace> place1List = new ArrayList<MkPlace>();
	private MkPlaceAA mAdapter;
	private MkPlaceOPNetAA mOPNetAdapter;
	private ToggleButton tbType1, tbType2, tbType3, tbType4, tbType5;
	private ToggleButton[] tbList;
	private String curType;
	ArrayList<String> checkList;

	private View viewPlaceOPNET;
	/** Place2 **/
	private View viewPlace2;
	private MapView mvPlace2;
	private ArrayList<Place2> place2List = new ArrayList<Place2>();

	/** Place3 **/
	private View viewPlace3;
	private ListView lvPlace3;
	
	List<Overlay> mapOverlayList;
	private Drawable dMarker1, dMarker2;
	private MyItemizedOverlay itemizedOverlay, itemizedOverlay2;

	/** Location **/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.place);

		Log.i(TAG, "" + TAG);
		viewTop = findViewById(R.id.viewTopPlace);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle = (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("LBS");

		/** Place Common **/
		tbD1 = (ToggleButton) findViewById(R.id.tbPlaceDistance1km);
		tbD2 = (ToggleButton) findViewById(R.id.tbPlaceDistance2km);
		tbD5 = (ToggleButton) findViewById(R.id.tbPlaceDistance5km);
		tbD10 = (ToggleButton) findViewById(R.id.tbPlaceDistance10km);
		btnSearch = (Button) findViewById(R.id.btnPlaceSearch);
		btnPlaceBack = (Button) findViewById(R.id.btnPlaceback);

		btnSearch.setOnClickListener(this);
		btnPlaceBack.setOnClickListener(this);

		/** Place1 **/
		viewPlace1 = findViewById(R.id.viewPlace1);
		lvPlace1 = (ListView) viewPlace1.findViewById(R.id.lvPlace1);

		mAdapter = new MkPlaceAA(mContext, android.R.layout.simple_list_item_1,
				place1List);
		mOPNetAdapter = new MkPlaceOPNetAA(mContext,
				android.R.layout.simple_list_item_1, place1List);
		lvPlace1.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
				
		lvPlace1.setAdapter(mAdapter);
		
		viewPlaceOPNET = findViewById(R.id.viewPlaceOPNET);

		tbType2 = (ToggleButton) findViewById(R.id.tbCheckInWash);
		tbType1 = (ToggleButton) findViewById(R.id.tbCheckInRepairShop);
		tbType3 = (ToggleButton) findViewById(R.id.tbCheckInGas);
		tbType4 = (ToggleButton) findViewById(R.id.tbCheckInTuningShop);
		//tbType5 = (ToggleButton) findViewById(R.id.tbCheckInOther);
		tbType1.setOnCheckedChangeListener(this);
		tbType2.setOnCheckedChangeListener(this);
		tbType3.setOnCheckedChangeListener(this);
		tbType4.setOnCheckedChangeListener(this);
		//tbType5.setOnCheckedChangeListener(this);

		tbList = new ToggleButton[] { tbType1, tbType2, tbType3, tbType4 };
//				,tbType5}
				

		/** Place2 **/
		viewPlace2 = findViewById(R.id.viewPlace2);
		mvPlace2 = (MapView) viewPlace2.findViewById(R.id.mvPlace);
		mapOverlayList = mvPlace2.getOverlays();

		// MapView Balloons
		dMarker1 = getResources().getDrawable(R.drawable.icon_location);
		itemizedOverlay = new MyItemizedOverlay(dMarker1, mvPlace2);
		// BalloonBottomMargin
		// itemizedOverlay.setBalloonBottomOffset(dMarker1.getIntrinsicHeight());

		// Test
		double latitude = 37.55927540399934;
		double longitude = 126.92298889160156;
		int a = (int) (latitude * 1E6);
		int b = (int) (longitude * 1E6);
		GeoPoint curPoint = new GeoPoint(a, b);

		for (int i = 0; i < 10; i++) {
			latitude += 0.01;
			longitude += 0.01;
			a = (int) (latitude * 1E6);
			b = (int) (longitude * 1E6);
			GeoPoint point = new GeoPoint(a, b);

		}

		mapOverlayList.add(itemizedOverlay);

		final MapController mc = mvPlace2.getController();
		mc.animateTo(curPoint);
		mc.setZoom(16);

		place1Enable();        

		tbType1.setChecked(true);
	}

	/** Place1 Function **/

	/** Place2 Function **/

	/** Common Function **/
	public void place1Enable() {
		viewPlace1.setVisibility(View.VISIBLE);
		viewPlace2.setVisibility(View.GONE);
		btnSearch.setVisibility(View.VISIBLE);
		btnPlaceBack.setVisibility(View.GONE);
	}

	public void place2Enable() {
		viewPlace1.setVisibility(View.GONE);
		viewPlace2.setVisibility(View.VISIBLE);
		btnSearch.setVisibility(View.GONE);
		btnPlaceBack.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		for (ToggleButton curTb : tbList) {
			if (curTb.getId() == buttonView.getId()) {
				if (isChecked)
					setToggleOffOther(buttonView.getId());
			}
		}
		if (isChecked) {
			// checkList.clear();
			switch (buttonView.getId()) {
			case R.id.tbCheckInGas:
				viewPlaceOPNET.setVisibility(View.GONE);
				curType = "A102";
				lvPlace1.setAdapter(mAdapter);
				break;
			case R.id.tbCheckInWash:
				viewPlaceOPNET.setVisibility(View.GONE);
				curType = "A101";	
				lvPlace1.setAdapter(mAdapter);
				break;
			case R.id.tbCheckInRepairShop:
				viewPlaceOPNET.setVisibility(View.VISIBLE);
				mAdapter.clear();
				curType = "A103";
				lvPlace1.setAdapter(mOPNetAdapter);
				break;
			case R.id.tbCheckInTuningShop:	
				viewPlaceOPNET.setVisibility(View.GONE);
				curType = "A104";
				lvPlace1.setAdapter(mAdapter);
				break;
//			case R.id.tbCheckInOther:
//				viewPlaceOPNET.setVisibility(View.GONE);
//				curType = "A105";
//				lvPlace1.setAdapter(mAdapter);
//				break;
			}
			for (int i = 0; i < place1List.size(); i++) {
				MkPlace curPlace = place1List.get(i);
				curPlace.setCheck(false);
			}
			if (C.myLocation != null) {
				new Place1LBSTask(curType, C.myLocation.getLatitude(),
						C.myLocation.getLongitude(), place1List, mAdapter,
						"LBS 로딩중..", true, mContext).execute();
			} else {
				Toast.makeText(mContext, "현재 위치정보를 불러올 수 없습니다",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void setToggleOffOther(int tbId) {
		for (ToggleButton curTb : tbList) {
			if (!(curTb.getId() == tbId)) {
				curTb.setChecked(false);
			}
		}
	}

	@Override
	public void onClick(View v) {
		Log.d(TAG, "onClick : " + v.getId());
		switch (v.getId()) {
		case R.id.btnCert1:
			finish();
			break;
		case R.id.btnPlaceSearch:
			// new Place1Task(place1List, mContext).execute();
			Intent intent = new Intent(PlaceActivity.this, PlaceWebView.class);
			intent.putExtra("userLat", C.myLocation.getLatitude());
			intent.putExtra("userLng", C.myLocation.getLongitude());
			checkList = new ArrayList<String>();
			for (int i = 0; i < place1List.size(); i++) {
				MkPlace curPlace = place1List.get(i);

				if (curPlace.isCheck()) {
					checkList.add(curPlace.getIdx());
				}
			}
			intent.putExtra("checkList", checkList);
			startActivity(intent);

			break;
		case R.id.btnPlaceback:
			place1Enable();
			break;
		}
	}
}