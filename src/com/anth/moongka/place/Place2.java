package com.anth.moongka.place;

import com.google.android.maps.GeoPoint;

public class Place2 {
	private String name;
	private String content;
	private String distance;
	private String telNum;
	private String urlThumbnail;
	private String userId;
	private GeoPoint geoPoint;
	
	private int numLike;
	private int numScrap;
	private int numComment;
	
	
	
	
	public Place2(String name, String content, String distance, String telNum,
			String urlThumbnail, String userId, GeoPoint geoPoint, int numLike,
			int numScrap, int numComment) {
		super();
		this.name = name;
		this.content = content;
		this.distance = distance;
		this.telNum = telNum;
		this.urlThumbnail = urlThumbnail;
		this.userId = userId;
		this.geoPoint = geoPoint;
		this.numLike = numLike;
		this.numScrap = numScrap;
		this.numComment = numComment;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getTelNum() {
		return telNum;
	}
	public void setTelNum(String telNum) {
		this.telNum = telNum;
	}
	public String getUrlThumbnail() {
		return urlThumbnail;
	}
	public void setUrlThumbnail(String urlThumbnail) {
		this.urlThumbnail = urlThumbnail;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getNumLike() {
		return numLike;
	}
	public void setNumLike(int numLike) {
		this.numLike = numLike;
	}
	public int getNumScrap() {
		return numScrap;
	}
	public void setNumScrap(int numScrap) {
		this.numScrap = numScrap;
	}
	public int getNumComment() {
		return numComment;
	}
	public void setNumComment(int numComment) {
		this.numComment = numComment;
	}



	public GeoPoint getGeoPoint() {
		return geoPoint;
	}



	public void setGeoPoint(GeoPoint geoPoint) {
		this.geoPoint = geoPoint;
	}
	
}
