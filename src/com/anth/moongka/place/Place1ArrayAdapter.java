package com.anth.moongka.place;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.anth.moongka.R;

public class Place1ArrayAdapter extends ArrayAdapter<Place1> {
	private final String TAG = "MK_Place1AA";
	private Context mContext;
	
	private ArrayList<Place1> place1List;

	public Place1ArrayAdapter(Context context, int textViewResourceId, ArrayList<Place1> place1List) {
		super(context, textViewResourceId, place1List);
		mContext= context;
		this.place1List = place1List;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View curView = convertView;
		if(curView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.place1_list_item, null);
		}
		
		Place1 curPlace1 = place1List.get(position);
		TextView tvName = (TextView) curView.findViewById(R.id.tvPlace1ItemName);
		CheckBox cbPlace1 = (CheckBox) curView.findViewById(R.id.cbPlace1item);
		
		tvName.setText(curPlace1.getName());
		
		return curView;
	}
}
