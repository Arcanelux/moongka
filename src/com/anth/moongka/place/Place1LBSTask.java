package com.anth.moongka.place;

import java.util.ArrayList;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.anth.moongka.fix_task.MkTask;
import com.anth.moongka.fix_value.MkPlace;
import com.anth.moongka.function.MkApi;

public class Place1LBSTask extends MkTask{
	private final String TAG = "MK_Place1WebViewTask";
	private Context mContext;
	
	private boolean isCert = false;
	private String type;
	private double lat, lon;
	private ArrayList<MkPlace> placeList;
	private ArrayAdapter mAdapter;

	public Place1LBSTask(String type, double d, double e, ArrayList<MkPlace> placeList, ArrayAdapter mAdapter, String msg, boolean isProgressDialog, Context context) {
		super(msg, isProgressDialog, context);
		mContext = context;
		this.type = type;
		this.lat = d;
		this.lon = e;
		this.placeList = placeList;
		this.mAdapter = mAdapter;
	}

	@Override
	protected Void doInBackground(Void... params) {
		isCert = MkApi.lbs(type, lat, lon, placeList);
		return super.doInBackground(params);
	}
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Toast.makeText(mContext, MkApi.returnMsg, Toast.LENGTH_SHORT).show();
		if(isCert){
			mAdapter.notifyDataSetChanged();
		}
		
	}


	
}
