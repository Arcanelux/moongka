package com.anth.moongka.place;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class Place1Task extends AsyncTask<Void, Void, Void>{
	private final String TAG = "MK_Place1Task";
	private Context mContext;
	private PlaceActivity mActivity;
	
	private ProgressDialog mProgressDialog;
	
	private ArrayList<Place1> place1List;

	public Place1Task(ArrayList<Place1> place1List, Context context) {
		mContext = context;
		mActivity = (PlaceActivity) mContext;
		this.place1List = place1List;
	}
	

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage("Loading...");
		mProgressDialog.show();
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	@Override
	protected void onPostExecute(Void result) {
		mProgressDialog.dismiss();
		mActivity.place2Enable();
	}

	@Override
	protected void onCancelled() {
		mProgressDialog.dismiss();
		
	}

}
