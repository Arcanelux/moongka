package com.anth.moongka.place;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_value.MkPlace;
import com.anth.moongka.function.Lhy_Function;

public class MkPlaceAA extends ArrayAdapter<MkPlace> implements OnCheckedChangeListener {
	private final String TAG = "MK_MkPlaceAA";
	private Context mContext;

	private ArrayList<MkPlace> placeList;
	
	private ArrayList<String> checkList;
	
	public CheckBox cb;
	int checkPosition;
//	public MkPlaceAA(Context context, int textViewResourceId, ArrayList<MkPlace> placeList,ArrayList<String> checkList) {
//		super(context, textViewResourceId, placeList);
//		mContext = context;
//		this.placeList = placeList;
//		this.checkList = checkList;
//	}
	public MkPlaceAA(Context context, int textViewResourceId, ArrayList<MkPlace> placeList) {
		super(context, textViewResourceId, placeList);
		mContext = context;
		this.placeList = placeList;
		
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View curView = convertView;
		checkPosition = position;
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		curView = inflater.inflate(R.layout.place1_list_item, null);
		if(curView==null){

		}

		MkPlace curPlace = placeList.get(position);
		TextView tvName = (TextView) curView.findViewById(R.id.tvPlace1ItemName);
		TextView tvDistance = (TextView) curView.findViewById(R.id.tvPlace1ItemDistance);
		cb = (CheckBox) curView.findViewById(R.id.cbPlace1item);
		cb.setChecked(placeList.get(position).isCheck());

		tvName.setText(curPlace.getName());
		tvDistance.setText(curPlace.getDistance());

		cb.setOnCheckedChangeListener(this);
		
		Lhy_Function.setViewTag(cb, position);

		return curView;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		Log.i(TAG, ""+TAG);
		int position = (Integer) buttonView.getTag();
		MkPlace curPlace = placeList.get(position);
		Log.i(TAG, ""+position);
		placeList.get(position).setCheck(isChecked);
	}

}
