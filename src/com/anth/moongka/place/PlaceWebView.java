package com.anth.moongka.place;

import java.util.ArrayList;

import org.apache.http.util.EncodingUtils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.anth.moongka.R;
import com.anth.moongka.fix_activity.MkActivity;
import com.anth.moongka.function.C;

public class PlaceWebView extends MkActivity implements OnClickListener {
	private final String TAG = "MK_PlaceWebView";
	private Context mContext;
	
	private View viewTop;
	private ImageButton btnBack;
	private TextView tvTitle;
	
	private WebView wvPlace;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.place_webview);
		mContext = this;
		
		viewTop = findViewById(R.id.viewTopPlaceWebView);
		btnBack = (ImageButton) viewTop.findViewById(R.id.btnCert1);
		btnBack.setOnClickListener(this);
		tvTitle= (TextView) viewTop.findViewById(R.id.tvCertTitle);
		tvTitle.setText("��ġ����");
		
		Intent intent = getIntent();
		ArrayList<String> checkList = (ArrayList<String>)intent.getSerializableExtra("checkList");
		double userLat = intent.getDoubleExtra("userLat", 0);
		double userLng = intent.getDoubleExtra("userLng", 0);
		String url = C.API_PLACE;
		
		wvPlace = (WebView) findViewById(R.id.wvPlace);
		wvPlace.getSettings().setJavaScriptEnabled(true);
		String postData = "userLat=" + userLat + "&userLng=" + userLng;
		for(int i=0; i<checkList.size(); i++){
			postData += "&check" + "[" + i + "]" + "=" + checkList.get(i);
		}
		Log.d(TAG, "PostData : " + postData);
//		wvPlace.postUrl(url, EncodingUtils.getBytes(postData, "UTF-8"));
		wvPlace.postUrl(url, EncodingUtils.getBytes(postData, "BASE64"));
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCert1:
			finish();
			break;
		}
	}
}
