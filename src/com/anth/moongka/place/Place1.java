package com.anth.moongka.place;

public class Place1 {
	private String name;
	private boolean isChecked;
	
	public Place1(){
		
	}
	public Place1(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isChecked() {
		return isChecked;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
}
