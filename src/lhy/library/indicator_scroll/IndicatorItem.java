package lhy.library.indicator_scroll;

public class IndicatorItem {
	private String name;
	private int resMarker1, resMarker2;
	private String strMarker1, strMarker2;
	private int width, height;
	
	public IndicatorItem(String name) {
		this.name = name;
	}
	
	public IndicatorItem(String name, int resMarker1) {
		super();
		this.name = name;
		this.resMarker1 = resMarker1;
	}

	public IndicatorItem(String name, int resMarker1, int width, int height) {
		super();
		this.name = name;
		this.resMarker1 = resMarker1;
		this.width = width;
		this.height = height;
	}

	public IndicatorItem(String name, int resMarker1, int resMarker2) {
		this.name = name;
		this.resMarker1 = resMarker1;
		this.resMarker2 = resMarker2;
	}

	public IndicatorItem(String name, int resMarker1, int resMarker2, int width, int height) {
		this.name = name;
		this.resMarker1 = resMarker1;
		this.resMarker2 = resMarker2;
		this.width = width;
		this.height = height;
	}
	
	public IndicatorItem(String name, String strMarker1) {
		super();
		this.name = name;
		this.strMarker1 = strMarker1;
	}
	
	public IndicatorItem(String name, String strMarker1, int width, int height) {
		super();
		this.name = name;
		this.strMarker1 = strMarker1;
		this.width = width;
		this.height = height;
	}

	public IndicatorItem(String name, String strMarker1, String strMarker2) {
		super();
		this.name = name;
		this.strMarker1 = strMarker1;
		this.strMarker2 = strMarker2;
	}
	
	public IndicatorItem(String name, String strMarker1, String strMarker2, int width, int height) {
		super();
		this.name = name;
		this.strMarker1 = strMarker1;
		this.strMarker2 = strMarker2;
		this.width = width;
		this.height = height;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getResMarker1() {
		return resMarker1;
	}
	public void setResMarker1(int resMarker1) {
		this.resMarker1 = resMarker1;
	}
	public int getResMarker2() {
		return resMarker2;
	}
	public void setResMarker2(int resMarker2) {
		this.resMarker2 = resMarker2;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}

	public String getStrMarker1() {
		return strMarker1;
	}

	public void setStrMarker1(String strMarker1) {
		this.strMarker1 = strMarker1;
	}

	public String getStrMarker2() {
		return strMarker2;
	}

	public void setStrMarker2(String strMarker2) {
		this.strMarker2 = strMarker2;
	}
	
	
}
