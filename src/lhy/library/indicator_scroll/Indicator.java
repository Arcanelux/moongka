package lhy.library.indicator_scroll;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class Indicator extends FrameLayout{
	private final String TAG = "lhy_IndicatorLayout";
	private Context mContext;
	private ViewPager mPager;
	private GestureDetector mGestureDetector;
	private boolean isFirstInit = true;

	/** Mode **/
	private boolean isDebug = false;
	private TextView tvDebug;
	private boolean isScrollByIndicator = false;		// Indicator의 스크롤을 따라갈지, 멈춘 후 Pager를 바꿀지
	private boolean isSmoothScroll = false;
	private int touchMode;
	// Scroll시 사용
	private final int TOUCH = 9832;
	private final int NOT_TOUCH = 9833;

	private boolean isMoving = false; 

	/** Variable **/
	private int dWidth, dHeight;
	private int itemCount = 5;
	private int indicatorWidth, indicatorHeight;
	private int startLocation;
	private int startTextLocation;
	private int endLocation;

	private ArrayList<IndicatorItem> itemList;
	private int curPosition = 0;
	private ArrayList<ImageView> ivList = new ArrayList<ImageView>();
	private ArrayList<TextView> tvList = new ArrayList<TextView>();
	//Fling
	private FlingThread mFlingThread;
	private FlingHandler mFlingHandler;
	private ScrollThread mScrollThread;
	private ScrollHandler mScrollHandler;

	/** Border ScrollStop Variable**/
	private final int TO_NEXT = 890;
	private final int TO_PREV = 891;
	private int modeScroll, modeFling;

	/** Decide Position Variable **/
	private int oneStepValue;
	private int oneStepItemWidth;

	/** Indicator Item Setting Variable **/
	private int itemWidth, itemHeight;
	private boolean isSetSize = false;
	private int resUnselected, resSelected;
	private boolean isSetItemSelectedRes = false;
	private boolean isSetItemUnSelectedRes = false;
	private float itemTextSizeNotSelected, itemTextSizeSelected;
	private String itemColorSelectedString = "#FF999999";
	private String itemColorNotSelectedString = "#FFBBBBBB";

	/** Connect ViewPager Variable **/
	private int oriPageNum, curPageNum;

	/** Indicator TextView Variable **/
	private int oriText, curText;

	/** Constructors **/
	public Indicator(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		firstInit(context);
	}
	public Indicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		firstInit(context);
	}
	public Indicator(Context context) {
		super(context);
		firstInit(context);
	}
	private void firstInit(Context context){
		mContext = context;
		Display display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		dWidth = display.getWidth();
		dHeight = display.getHeight();
		itemTextSizeNotSelected = (float) dWidth/65;
		itemTextSizeSelected = (float) dWidth/55;

		// xml표시내용
		if(isInEditMode()){
			setBackgroundColor(0xffdddddd);
			TextView tvSample = new TextView(mContext);
			tvSample.setText("lhy_ScrollIndicator");
			tvSample.setTextColor(Color.BLACK);
			tvSample.setTextSize(12);
			tvSample.setGravity(Gravity.RIGHT|Gravity.BOTTOM);
			tvSample.bringToFront();
			addView(tvSample);
		}
		// 실제동작
		else{
			mGestureDetector = new GestureDetector(mContext, new IndicatorGestureListener());
		}
	}

	// Setting
	/** ViewPager Set **/
	public void setViewPager(ViewPager viewPager){
		mPager = viewPager;
	}
	/** IndicatorItem List Set **/
	public void setItemList(ArrayList<IndicatorItem> itemList){
		this.itemList = itemList;
	}
	/** IndicatorItem UnSelected Resource Set **/
	public void setItemResUnSelected(int resItem){
		this.resUnselected = resItem;
		this.isSetItemUnSelectedRes = true;
	}
	/** IndicatorItem Selected Resource Set **/
	public void setItemResSelected(int resItem){
		this.resSelected = resItem;
		this.isSetItemSelectedRes = true;
	}
	/** IndicatorItem Size Set **/
	public void setItemResSize(int width, int height){
		this.itemWidth = width;
		this.itemHeight = height;
		this.isSetSize = true;
	}
	/** IndicatorItem Selected Text Color Set **/
	public void setItemTextSelectedColor(String strColor){
		this.itemColorSelectedString = strColor;
	}
	/** IndicatorItem NotSelected Text Color Set **/
	public void setItemTextNotSelectedColor(String strColor){
		this.itemColorNotSelectedString = strColor;
	}
	/** IndicatorItem Selected Text Size(Percentage) **/
	public void setItemTextSelectedSize(float value){
		this.itemTextSizeSelected = value;
	}
	/** IndicatorItem NotSelected Text Size(Percentage) **/
	public void setItemTextNotSelectedSize(float value){
		this.itemTextSizeNotSelected = value;
	}
	/** Current Position Set **/
	public void setPosition(int value){
		this.curPosition = value;
	}

	/** Initializing **/
	public void init(){
		mPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				if(getCurrentPosition()!=position){
					int diff = getCurrentPosition()-position;
					mScrollThread.addLength(oneStepValue*diff);
				}
				showDebugText();
			}
			@Override public void onPageScrolled(int arg0, float arg1, int arg2) { }
			@Override public void onPageScrollStateChanged(int arg0) { }
		});

		// View가 생성된 이후 실행
		if(isFirstInit){
			this.post(new Runnable() {
				@Override
				public void run() {
					initProcess();
					isFirstInit = false;
				}
			});
		} else{
			initProcess();
		}
	}


	/** 맨 처음인지를 리턴 **/
	private boolean isBorderFirst(){
		if(ivList.get(0).getX() >= startLocation){
			return true;
		} else{
			return false;
		}
	}
	/** 맨 마지막인지를 리턴 **/
	private boolean isBorderLast(){
		if(ivList.get(ivList.size()-1).getX() <= startLocation){
			return true;
		} else{
			return false;
		}
	}
	/** 첫 부분의 X좌표를 리턴 **/
	private int getFirstLocation(){
		return (int)(ivList.get(0).getX());
	}
	/** 마지막 부분의 X좌표를 리턴 **/
	private int getLastLocation(){
		return (int)(ivList.get(ivList.size()-1).getX());
	}


	/** GestureListener **/
	private class IndicatorGestureListener extends GestureDetector.SimpleOnGestureListener{
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			//			Log.d(TAG, "onFling, velocityX : " + velocityX);
			isMoving = true;
			mFlingThread.setVelocity(velocityX);
			return true;
		}
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			//			Log.d(TAG, "onScroll, distanceX : " + distanceX);
			// 방향 저장
			if(distanceX < 0) 			modeScroll = TO_PREV;
			else if(distanceX > 0) 	modeScroll = TO_NEXT;

			//			if((modeScroll==TO_PREV) && isBorderFirst()){
			//				// 왼쪽 끝에서 더이상 스크롤되지않음 (1번째 터치 좌표 < 2번째 터치 좌표 일 때(결과가 음수, 오른쪽으로 스크롤))
			//				return true;
			//			} else if((modeScroll==TO_NEXT) && isBorderLast()){
			//				// 오른쪽 끝에서 더이상 스크롤되지않음 (1번째 터치 좌표 > 2번째 터치 좌표 일 때(결과가 양수, 왼쪽 스크롤))
			//				return true;
			//			} else{

			// 그 외에는 ImageView위치 조정
			for(int i=0; i<ivList.size(); i++){
				float ori = ivList.get(i).getX();
				ivList.get(i).setX(-distanceX + ori);
				float oriText = tvList.get(i).getX();
				tvList.get(i).setX(-distanceX + oriText);
			}
			mFlingThread.setVelocity(0);
			isMoving = true;

			// Position값 실시간 수정
			notifyPagerPosition();
			return false;
			//			}
		}
		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			Log.d(TAG, "onSingleTapUp");
			return false;
		}
		@Override
		public boolean onDown(MotionEvent e) {
			// true여야 다른 이벤트 발생
			touchMode = TOUCH;
			return true;
		}
		@Override
		public boolean onDoubleTap(MotionEvent e) {
			Log.d(TAG, "onDoubleTap");
			return super.onDoubleTap(e);
		}
	}

	/** Fling 동작 Thread **/
	class FlingThread extends Thread{
		private boolean canRun = true;
		private float velocityX;
		private int mode;
		private final int FLING2 = 89234;
		private final int FLING3 = 89235;

		@Override
		public void run(){
			while(canRun){
				try{
					Thread.sleep(10);
					float remainVelocity = Math.abs(velocityX);
					if(remainVelocity > 30){
						// Fling으로 가속도값이 전달되고, 멈출때까지 Handler에서 ImageView를 이동시킴
						mFlingHandler.sendMessage(mFlingHandler.obtainMessage(0, velocityX));
						if(isBorderFirst() || isBorderLast()){
							// 가장자리일 경우 가속도값을 많이 줄여서 크게 벗어나지않게한다
							velocityX = velocityX*0.5f;
						} else{
							velocityX = velocityX*0.9f;
						}
					} else if(remainVelocity > 0 && remainVelocity <=30){
						// Fling으로 인한 ImageView의 이동이 멈춘 후. 이미 이동했기때문에 끝부분을 넘어서므로 다시 돌려준다
						// resetBorder() 호출시에도 이쪽으로 옴
						int moveValue = 0;
						if(isBorderFirst()){
							moveValue = startLocation - getFirstLocation();
						} else if(isBorderLast()){
							moveValue = startLocation - getLastLocation();
						} else{
							// 경계바깥이 아닐경우
							moveValue = -getDiffFromPoint();
						}
						float sendValue = 0;
						// 거리가 줄어들수록 좀 더 빨리움직임
						if(moveValue > indicatorWidth/10){
							sendValue = (float)moveValue/25;
						} else{
							sendValue = (float)moveValue/15;
						}
						// 1픽셀 미만일경우 남은 픽셀 보내고 멈춰줌
						if(Math.abs(moveValue) < 1){
							isMoving = false;
							mFlingHandler.sendMessage(mFlingHandler.obtainMessage(1, (float)moveValue));
							remainVelocity = 0;
							if(!isScrollByIndicator) mFlingHandler.sendMessage(mFlingHandler.obtainMessage(2));
						} else{
							mFlingHandler.sendMessage(mFlingHandler.obtainMessage(1, sendValue));
						}
					} else if(touchMode==NOT_TOUCH){
						int moveValue = -getDiffFromPoint();
						float sendValue = (float)moveValue/10;
						if(Math.abs(moveValue) < 1){
							isMoving = false;
							mFlingHandler.sendMessage(mFlingHandler.obtainMessage(1, (float)moveValue));
							moveValue = 0;
							if(!isScrollByIndicator) mFlingHandler.sendMessage(mFlingHandler.obtainMessage(2));
						} else{
							mFlingHandler.sendMessage(mFlingHandler.obtainMessage(1, sendValue));
						}
					} 
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		public void setVelocity(float value){
			velocityX = value;
		}
		/** 처음이나 끝을 벗어났을 때 호출, 위치를 천천히 맞춰줌 **/
		public void fixLocation(){
			setVelocity(30.0f);
		}

		public void startThread(){
			canRun = true;
			synchronized (this) {
				this.notify();
			}
		}
		public void stopThread(){
			canRun = false;
			synchronized (this) {
				this.notify();
			}
		}
	}

	/** FlingThread에서 보내온 값으로 ImageView의 위치를 조정해주는 Handler **/
	class FlingHandler extends Handler{
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
			case 0:
				// Fling 이동
				for(int i=0; i<ivList.size(); i++){
					float ori = ivList.get(i).getX();
					float value = (Float) msg.obj;
					ivList.get(i).setX(value/100 + ori);

					float oriText = tvList.get(i).getX();
					tvList.get(i).setX(value/100 + oriText);

					// Position값 실시간 수정
					notifyPagerPosition();
				}
				break;
			case 1:
				// Fling 이동 완료 후 다시 돌려줄 때
				for(int i=0; i<ivList.size(); i++){
					float ori = ivList.get(i).getX();
					float value = (Float) msg.obj;
					ivList.get(i).setX(value + ori);

					float oriText = tvList.get(i).getX();
					tvList.get(i).setX(value + oriText);

					// Position값 실시간 수정
					notifyPagerPosition();
				}
				break;
			case 2:
				notifyPagerPositionForce();
				break;
			}
		}
	}

	/** 터치 없이 스크롤수행해주는 Thread **/
	class ScrollThread extends Thread{
		private boolean canRun = true;
		private float remainLength = 0;

		@Override
		public void run(){
			while(canRun){
				try{
					Thread.sleep(10);
					float remainAbs = Math.abs(remainLength);
					if(remainAbs>1){
						float sendValue = remainLength/5;
						mScrollHandler.sendMessage(mScrollHandler.obtainMessage(0, sendValue));
						remainLength -= sendValue;
					} else if(remainAbs>0 && remainAbs<=1){
						mScrollHandler.sendMessage(mScrollHandler.obtainMessage(0, remainLength));
						remainLength = 0;
						mScrollHandler.sendMessage(mScrollHandler.obtainMessage(1));
					}
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}

		public void addLength(float value){
			remainLength += value;
		}
		public void startThread(){
			canRun = true;
			synchronized (this) {
				this.notify();
			}
		}
		public void stopThread(){
			canRun = false;
			synchronized (this) {
				this.notify();
			}
		}
	}

	/** ScrollThread의 Handler **/
	class ScrollHandler extends Handler{
		@Override
		public void handleMessage(Message msg) {
			switch(msg.what){
			case 0:
				for(int i=0; i<ivList.size(); i++){
					float ori = ivList.get(i).getX();
					float value = (Float) msg.obj;
					ivList.get(i).setX(value + ori);

					float oriText = tvList.get(i).getX();
					tvList.get(i).setX(value + oriText);
				}
				break;
			case 1:
				notifyText();
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			//			Log.d(TAG, "ACTION_DOWN, TOUCH");
			touchMode = TOUCH;
			break;
		case MotionEvent.ACTION_UP:
			//			Log.d(TAG, "ACTION_UP, NOT_TOUCH");
			touchMode = NOT_TOUCH;
			isMoving = false;
			if(isBorderFirst() || isBorderLast()){
				mFlingThread.fixLocation();
			}
			break;
		}
		return mGestureDetector.onTouchEvent(event);
	}	

	/** 현재 Position값을 리턴해줌 **/
	private int getCurrentPosition(){
		int diff = getFirstLocation() - startLocation - oneStepValue/2;
		int diffStep = -diff/oneStepValue;
		if(diffStep<0){
			diffStep = 0;
		} else if(diffStep > itemList.size()-1){
			diffStep = itemList.size()-1;
		}
		return diffStep;
	}

	/** Postion값 지정시 그 위치를 가운데로하여 전체 이미지뷰 이동. 부드럽지않음 **/
	private void movePosition(int value){
		Log.d(TAG, "movePosition value : " + value);
		for(int i=0; i<ivList.size(); i++){
			int setLocation = startLocation + ((indicatorWidth/(itemCount-1)) * i);
			setLocation -= value*oneStepValue;
			ivList.get(i).setX(setLocation);

			int setTextLocation = startTextLocation + ((indicatorWidth/(itemCount-1)) * i);
			tvList.get(i).setX(setTextLocation);
		}
		mPager.setCurrentItem(value, false);
		notifyPagerPosition();
	}

	/** 기준값과 얼마나 벗어났는지 리턴 **/
	private int getDiffFromPoint(){
		int curItemLocation = (int)(ivList.get(getCurrentPosition()).getX());
		int diffCur = curItemLocation - startLocation;
		return diffCur;
	}

	/** 
	 * ViewPager외의 곳에서 페이지를 변경할 경우(Indicator에서의 조작)
	 * ViewPager의 위치를 이동시켜주며, 현재 페이지를 지정
	 */
	private void notifyPagerPosition(){
		int diff = getFirstLocation() - startLocation - oneStepValue/2;
		int diffStep = -diff/oneStepValue;
		int diffCur = diffStep;
		if(oriPageNum!=diffCur){
			//			Log.d(TAG, "isMoving : " + isMoving + ", curPageNum : " + diffCur + ", oriPageNum : " + oriPageNum);
			if(isScrollByIndicator) {
				curPageNum = diffStep;
				mPager.setCurrentItem(curPageNum, isSmoothScroll);
			}
			//				else if(!isScrollByIndicator && !isMoving) {
			//				curPageNum = diffStep;
			//				mPager.setCurrentItem(curPageNum, true);
			//			}
		}
		oriPageNum = diffStep;
		notifyText();
		showDebugText();
	}

	/** 강제 뷰페이저 조정 **/
	private void notifyPagerPositionForce(){
		//		Log.d(TAG, "notifyForce, getCurPos : " + getCurrentPosition() + ", mPagerCurPageNum : " + curPageNum);
		if(getCurrentPosition()!=curPageNum){
			mPager.setCurrentItem(getCurrentPosition(), isSmoothScroll);
			curPageNum = getCurrentPosition();
		}
	}

	/** 텍스트뷰 조정 **/
	private void notifyText(){
		int cur = getCurrentPosition();

		if(oriText!=cur){
			TextView oriTv = tvList.get(oriText);
			TextView curTv = tvList.get(cur);
			oriTv.setTextSize(itemTextSizeNotSelected);
			oriTv.setTextColor(Color.parseColor(itemColorNotSelectedString));
			curTv.setTextSize(itemTextSizeSelected);
			curTv.setTextColor(Color.parseColor(itemColorSelectedString));
		}
		//		for(int i=0; i<tvList.size(); i++){
		//			if(i!=cur){
		//				tvList.get(i).setTextSize(itemTextSizeNotSelected);
		//				tvList.get(i).setTextColor(Color.parseColor(itemColorNotSelectedString));
		//			} else{
		//				tvList.get(i).setTextSize(itemTextSizeSelected);
		//				tvList.get(i).setTextColor(Color.parseColor(itemColorSelectedString));
		//			}
		//		}
		oriText = cur;
	}

	private void showDebugText(){
		if(isDebug){
			String debugText = "FirstLoc : " + getFirstLocation() + ", LastLoc : " + getLastLocation();
			debugText += "\n DiffStep : " + getCurrentPosition();
			tvDebug.setText(debugText);
		}
	}

	public void removeAllItem(){
		for(int i=0; i<ivList.size(); i++){
			Indicator.this.removeView(ivList.get(i));
			Indicator.this.removeView(tvList.get(i));
		}
		ivList.clear();
		tvList.clear();
		curPageNum = 0;
		curPosition = 0;
		curText = 0;
	}

	private void initProcess(){
		isMoving = false;
		indicatorWidth = getWidth();
		indicatorHeight = getHeight();
		Log.d(TAG, "Width : " + indicatorWidth + ", Height : " + indicatorHeight);

		if(isSetSize){
			oneStepItemWidth = itemWidth;
		} else{
			oneStepItemWidth = (indicatorWidth/itemCount)/2;
			itemHeight = oneStepItemWidth;
		}
		Log.d(TAG, "oneStepItemWidth : " + oneStepItemWidth);
		// 첫번째 아이템의 시작점(화면의 중간)
		startLocation = indicatorWidth/2 - oneStepItemWidth/2;
		Log.d(TAG, "startLocation : " + startLocation);

		// 화면의 중간부터 시작해서 아이템 개수만큼 이미지뷰를 만들어준다. 오른쪽으로 계속해서 바깥으로 생성
		// 양 옆 여분 위해 2개 추가, 보이는 부분의 양 옆은 반씩 잘리기때문에 5개일 경우 4개만 보여줌. +2는 안보이는 부분의 아이템
		// 위 취소, 전부 다 만들어주자!
		for(int i=0; i<itemList.size(); i++){
			ImageView ivCur = new ImageView(mContext);					
			// Size Set					
			if(isSetSize){
				ivCur.setLayoutParams(new LayoutParams(itemWidth, itemHeight, Gravity.CENTER_VERTICAL));
			} else{
				ivCur.setLayoutParams(new LayoutParams(oneStepItemWidth, oneStepItemWidth, Gravity.CENTER_VERTICAL));
			}
			// Resource Set
			if(isSetItemSelectedRes || isSetItemUnSelectedRes){
				if(isSetItemSelectedRes){
					ivCur.setImageResource(resSelected);
				} else{
					ivCur.setImageResource(resUnselected);
				}
			} else{
				ivCur.setImageDrawable(new ColorDrawable(Color.parseColor("#FF454545")));
				//						ivCur.setImageResource(R.drawable.sample_circle);
			}

			// 텍스트뷰 생성
			TextView tvCur = new TextView(mContext);
			int tvSize = (indicatorWidth/(itemCount-1));
			int setTextLocation = startTextLocation + ((indicatorWidth/(itemCount-1)) * i);
			Log.d(TAG, "setTextLoc " + i + " : " + setTextLocation);

			//					tvCur.setWidth(tvSize);
			//					tvCur.setGravity(Gravity.CENTER);
			LayoutParams tvParams = new LayoutParams(tvSize, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
			tvParams.topMargin = (int) (itemHeight/2 + dWidth*0.03f);
			tvCur.setGravity(Gravity.CENTER);
			tvCur.setLayoutParams(tvParams);
			tvCur.setX(setTextLocation);
			tvCur.setText(itemList.get(i).getName());
			tvCur.setTextColor(Color.parseColor(itemColorNotSelectedString));
			tvCur.setTextSize(itemTextSizeNotSelected);
			tvList.add(tvCur);
			addView(tvCur);

			// 시작점부터 오른쪽으로 순서대로 이미지뷰 생성
			int setLocation = startLocation + ((indicatorWidth/(itemCount-1)) * i);
			ivCur.setX(setLocation);
			Log.d(TAG, i + " ImageView setX : " + setLocation);
			ivList.add(ivCur);
			addView(ivCur);

			// 한 칸거리 파악위한 변수 세팅
			oneStepValue = indicatorWidth/(itemCount-1);

			// 테스트 모드
			if(isDebug){
				tvDebug = new TextView(mContext);
				tvDebug.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.RIGHT));
				addView(tvDebug);
			}
		}
		
		if(mFlingThread!=null){
			mFlingThread.stopThread();
			mFlingThread = null;
		}
		if(mScrollThread!=null){
			mScrollThread.stopThread();
			mScrollThread = null;
		}
		mFlingThread = new FlingThread();
		mFlingHandler = new FlingHandler();
		mFlingThread.start();

		mScrollThread = new ScrollThread();
		mScrollHandler = new ScrollHandler();
		mScrollThread.start();

		oriText = curPosition;
		movePosition(curPosition);
		tvList.get(curPosition).setTextSize(itemTextSizeSelected);
		tvList.get(curPosition).setTextColor(Color.parseColor(itemColorSelectedString));
	}
}
