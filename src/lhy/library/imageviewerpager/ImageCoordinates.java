/**
 * Author : Lee HanYeong
 * File Name : ImageCoordinates.java
 * Created Date : 2012. 10. 26.
 * Description
 */
package lhy.library.imageviewerpager;

public /** 터치 지점 클래스 **/
class ImageCoordinates {
	int x, y;
	int bounds = 17;
	String name;
	String detail;
	/**
	 * 범위 미지정/지정 생성자
	 * 기본적으로 터치좌표의 이름과 x,y값을 받으며
	 * 범위값은 기본값10에 따로 받을경우 지정
	 * Detail 문자열값도 받을수있음
	 */
	public ImageCoordinates(int x, int y){
		this.x = x;
		this.y = y;
	}
	public ImageCoordinates(String name, int x, int y){
		this.x = x;
		this.y = y;
		this.name = name;
	}
	public ImageCoordinates(String name, int x, int y, int bounds){
		this.x = x;
		this.y = y;
		this.bounds = bounds;
		this.name = name;
	}
	public ImageCoordinates(String name, int x, int y, String detail){
		this.x = x;
		this.y = y;
		this.name = name;
		this.detail = detail;
	}
	public ImageCoordinates(String name, int x, int y, String detail, int bounds){
		this.x = x;
		this.y = y;
		this.bounds = bounds;
		this.name = name;
		this.detail = detail;
	}
	/** Getter/Setter **/
	public int getX() { return x; }
	public int getY() { return y; }
	public String getName() { return name; }
	public void setX(int x) { this.x = x; }
	public void setY(int y) { this.y = y; }
	public void setName(String name) { this.name = name; }
	public int getBounds() { return bounds; }
	public String getDetail() { return detail; }
	public void setBounds(int bounds) { this.bounds = bounds; }
	public void setDetail(String detail) { this.detail = detail; }
}