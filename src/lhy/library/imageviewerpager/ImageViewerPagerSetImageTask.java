package lhy.library.imageviewerpager;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

public class ImageViewerPagerSetImageTask extends AsyncTask<Void, Void, Void> {
	
	private ImageView iv;
	private Bitmap bm;
	
	public ImageViewerPagerSetImageTask(ImageView iv, Bitmap bm){
		this.iv = iv;
		this.bm = bm;
	}
	@Override
	protected Void doInBackground(Void... params) {
		iv.setImageBitmap(bm);
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}

}
