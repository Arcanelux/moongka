package lhy.library.imageviewerpager;

import java.io.Serializable;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.widget.ImageView;

public class Image implements Serializable{
	private int mode;
	private ImageView.ScaleType scaleType = ImageView.ScaleType.FIT_CENTER;
	private int imageRes;
	private String imageAddress;
	private ArrayList<ImageCoordinates> coordinatesList;
	
	private Bitmap bitmap;
	
	/** 내부이미지 / 외부다운로드 이미지에 따라 생성자 변화 **/
	public Image(int imageRes){
		this.mode = ImageViewer_C.MODE_RES_FOLDER;
		this.imageRes = imageRes;
		coordinatesList = new ArrayList<ImageCoordinates>();
	}
	public Image(String imageAddress){
		this.mode = ImageViewer_C.MODE_DOWNLOAD;
		this.imageAddress = imageAddress;
		coordinatesList = new ArrayList<ImageCoordinates>();
	}
	public Image(Bitmap bitmap){
		this.mode = ImageViewer_C.MODE_BITMAP;
		this.bitmap = bitmap;
		coordinatesList = new ArrayList<ImageCoordinates>();
	}
	
	/** 터치 지점 추가(범위 미지정/지정 & 상세설명 미지정/지정) **/
	public void addCoordinates(int x, int y){
		ImageCoordinates curCoordinates = new ImageCoordinates(x, y);
		coordinatesList.add(curCoordinates);
	}
	public void addCoordinates(String name, int x, int y){
		ImageCoordinates curCoordinates = new ImageCoordinates(name, x, y);
		coordinatesList.add(curCoordinates);
	}
	public void addCoordinates(String name, int x, int y, int bounds){
		ImageCoordinates curCoordinates = new ImageCoordinates(name, x, y, bounds);
		coordinatesList.add(curCoordinates);
	}
	public void addCoordinates(String name, int x, int y, String detail){
		ImageCoordinates curCoordinates = new ImageCoordinates(name, x, y, detail);
		coordinatesList.add(curCoordinates);
	}
	public void addCoordinates(String name, int x, int y, String detail, int bounds){
		ImageCoordinates curCoordinates = new ImageCoordinates(name, x, y, detail, bounds);
		coordinatesList.add(curCoordinates);
	}
	
	/**
	 * 터치 지점 이름으로 터치좌표들을 받아옴
	 *  String name 전달 후 일치하는 이름의 x,y 좌표 목록을
	 *  ArrayList<int[]> 형태로 리턴
	 */
	public ArrayList<int[]> findCoordinatesByName(String name){
		ArrayList<int[]> resultList = new ArrayList<int[]>();
		for(ImageCoordinates curCoordinates : coordinatesList){
			if(curCoordinates.getName().equals("name")){
				int[] result = { curCoordinates.getX(), curCoordinates.getY() };
				resultList.add(result);
			}
		}
		return resultList;
	}

	/** Getter/Setter **/
	public int getMode() { return mode; }
	public int getImageRes() { return imageRes; }
	public String getImageAddress() { return imageAddress; }
	public void setMode(int mode) { this.mode = mode; }
	public void setImageRes(int imageRes) { this.imageRes = imageRes; }
	public void setImageAddress(String imageAddress) { this.imageAddress = imageAddress; }
	public ArrayList<ImageCoordinates> getCoordinatesList() { return coordinatesList; }
	public void setCoordinatesList(ArrayList<ImageCoordinates> coordinatesList) { this.coordinatesList = coordinatesList; }
	public ImageView.ScaleType getScaleType() { return scaleType; }
	public void setScaleType(ImageView.ScaleType scaleType) { this.scaleType = scaleType; }
	public Bitmap getBitmap() {
		return bitmap;
	}
	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
}
