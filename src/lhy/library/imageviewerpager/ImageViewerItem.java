package lhy.library.imageviewerpager;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class ImageViewerItem extends FrameLayout{
	private final String TAG = "ImageViewerItem";
	private Context mContext;
	private GestureDetector mGestureDetector;
	private Matrix matrix = new Matrix();
	private Matrix savedMatrix = new Matrix();
	private Matrix tuningMatrix = new Matrix();

	// We can be in one of these 3 states
	int mode = NONE;
	private static final int NONE = 0;
	private static final int DRAG = 1;
	private static final int ZOOM = 2;

	private static int MAX_SCALE = 3; // 10배 이상 확대 하지 않도록 최대 배율 설정

	private PointF start = new PointF();
	private PointF mid = new PointF();

	private float oldDist = 1f;

	private float orgScaleX = 0f;
	private float orgScaleY = 0f;
	private float orgX = 0f;
	private float orgY = 0f;

	private int scaleWidth = -1;
	private int scaleHeight = -1;

	private float transX = 0f;
	private int mLayoutWidth = 0;

	private ImageView ivItem;
	private ImageViewerPager mPager;

	public ImageViewerItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		firstInit(context);
	}

	public ImageViewerItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		firstInit(context);
	}

	public ImageViewerItem(Context context) {
		super(context);
		firstInit(context);
	}

	private void firstInit(Context context){
		mContext = context;
		ivItem = new ImageView(mContext);
		ivItem.setScaleType(ScaleType.FIT_CENTER);
		addView(ivItem);
		
		mGestureDetector = new GestureDetector(mContext, new ViewerGestureDetector());
	}

	public ImageView getIvItem(){
		return ivItem;
	}
	public void setPager(ImageViewerPager pager){
		mPager = pager;
	}

	public boolean onLayoutTouchEvent(MotionEvent event) {
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_UP:
			//			Log.d(TAG, "Action_Up");
			break;
		case MotionEvent.ACTION_DOWN:
			//			Log.d(TAG, "Action_Down");
			ivItem.setScaleType(ScaleType.MATRIX);
			savedMatrix.set(matrix);
			start.set(event.getX(), event.getY());
			mode = DRAG;
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			//			Log.d(TAG, "Action_Pointer_Down");
			oldDist = spacing(event);
			if (oldDist > 10f) {
				savedMatrix.set(matrix);
				midPoint(mid, event);
				mode = ZOOM;
			}
			break;
		case MotionEvent.ACTION_POINTER_UP:
			//			Log.d(TAG, "Action_Pointer_Up");
			mode = NONE;
			break;
		case MotionEvent.ACTION_MOVE:
			if (mode == DRAG) {
				//				Log.d(TAG, "Action_Move - DRAG");
				matrix.set(savedMatrix);
				matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
			} else if (mode == ZOOM) {
				/** 가로크기가 화면보다 클 경우 ImageViewerPager의 스크롤을 막는다 **/
				if(scaleWidth > mLayoutWidth){
					mPager.setPageScrollDisabled();
				} else{
					mPager.setPageScrollEnabled();
				}
				//				Log.d(TAG, "Action_Move - ZOOM");
				float newDist = spacing(event);
				if (newDist > 10f) {
					matrix.set(savedMatrix);
					float scale = newDist / oldDist;
					matrix.postScale(scale, scale, mid.x, mid.y);
				}
			}
			break;
		}

		// 드래그 하거나 zoom 인 경우에만 Tune matrix
		if (mode == ZOOM || mode == DRAG) {
			tuneMatrix(matrix);
		}
		/**  줌기능시 **/
		return mGestureDetector.onTouchEvent(event);

		/** 스크롤시 **/
		//		return onCustomToucnEvent(event);
		//				return super.onTouchEvent(event);
		//		return true;

	}

	/**
	 * 두포인트 사이의 간격 계산
	 * 
	 * @param event
	 * @return
	 */
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

	/**
	 * 두포인트의 가운데 포인트 계산
	 * 
	 * @param point
	 * @param event
	 */
	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

	private void tuneMatrix(Matrix matrix) {
		ImageView imageView = ivItem;
		if (imageView != null) {
			float[] value = new float[9];
			matrix.getValues(value);
			float[] savedValue = new float[9];
			tuningMatrix.getValues(savedValue);

			int width = imageView.getWidth();
			int height = imageView.getHeight();

			Drawable d = imageView.getDrawable();
			if (d == null) return;
			int imageWidth = d.getIntrinsicWidth();
			int imageHeight = d.getIntrinsicHeight();

			scaleWidth = (int)(imageWidth * value[Matrix.MSCALE_X]);
			scaleHeight = (int)(imageHeight * value[Matrix.MSCALE_Y]);

			// 이미지가 바깥으로 나가지 않도록.
			if (value[Matrix.MTRANS_X] < width - scaleWidth)
				value[Matrix.MTRANS_X] = width - scaleWidth;
			if (value[Matrix.MTRANS_Y] < height - scaleHeight)
				value[Matrix.MTRANS_Y] = height - scaleHeight;
			if (value[Matrix.MTRANS_X] > 0)
				value[Matrix.MTRANS_X] = 0;
			if (value[Matrix.MTRANS_Y] > 0)
				value[Matrix.MTRANS_Y] = 0;

			// 10배 이상 확대 하지 않도록
			if (value[Matrix.MSCALE_X] > MAX_SCALE || value[Matrix.MSCALE_Y] > MAX_SCALE) {
				value[Matrix.MSCALE_X] = savedValue[Matrix.MSCALE_X];
				value[Matrix.MSCALE_Y] = savedValue[Matrix.MSCALE_Y];
				value[Matrix.MTRANS_X] = savedValue[Matrix.MTRANS_X];
				value[Matrix.MTRANS_Y] = savedValue[Matrix.MTRANS_Y];
			}

			// 최소 사이즈 조정
			if (value[Matrix.MSCALE_X] < orgScaleX)
				value[Matrix.MSCALE_X] = orgScaleX;
			if (value[Matrix.MSCALE_Y] < orgScaleY)
				value[Matrix.MSCALE_Y] = orgScaleY;

			scaleWidth = (int)(imageWidth * value[Matrix.MSCALE_X]);
			scaleHeight = (int)(imageHeight * value[Matrix.MSCALE_Y]);

			if (scaleWidth < width) {
				value[Matrix.MTRANS_X] = (float)width / 2 - (float)scaleWidth / 2;
			}
			if (scaleHeight < height) {
				value[Matrix.MTRANS_Y] = (float)height / 2 - (float)scaleHeight / 2;
			}

			transX = value[Matrix.MTRANS_X];

			matrix.setValues(value);
			tuningMatrix.set(matrix);

			imageView.setImageMatrix(matrix);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return onCustomToucnEvent(event);
	}

	public boolean onOriTouchEvent(MotionEvent event){
		return this.onTouchEvent(event);
	}

	public boolean onCustomToucnEvent(MotionEvent event){
		return this.onLayoutTouchEvent(event);
	}

	private class ViewerGestureDetector extends GestureDetector.SimpleOnGestureListener {
		@Override
		public boolean onDown(MotionEvent e) {
			//
			//			//			if(C.Debug) Log.d(TAG, "onDown");
			//			// Stop animation
			//			mIsTouched = true;
			//
			//			// Reset fling state
			//			mFlingDirection = 0;
			return true;
		}

		@Override public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { return false; }
		@Override public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) { return false; }
		@Override public void onLongPress(MotionEvent e) { }
		@Override public void onShowPress(MotionEvent e) { }
		@Override public boolean onSingleTapUp(MotionEvent e) {
			return true;
		}
		@Override
		public boolean onDoubleTap(MotionEvent e){
			int sWidth;
			int sHeight;
			Matrix matrix = ivItem.getMatrix();
			float[] value = new float[9];
			matrix.getValues(value);

			/** 작은지 판단 **/
			Drawable d = ivItem.getDrawable();
			int imageWidth = d.getIntrinsicWidth();
			int imageHeight = d.getIntrinsicHeight();

			sWidth = (int)(imageWidth * value[Matrix.MSCALE_X]);
			sHeight = (int)(imageHeight * value[Matrix.MSCALE_Y]);

//			Log.d(TAG, "sWidth : " + sWidth + "/" + mLayoutWidth);
			value[Matrix.MSCALE_X] = orgScaleX;
			value[Matrix.MSCALE_Y] = orgScaleY;
			value[Matrix.MTRANS_X] = orgX;
			value[Matrix.MTRANS_Y] = orgY;

			Matrix matrix2 = new Matrix();
			matrix2.setValues(value);

			ivItem.setImageMatrix(matrix2);
			// 매트릭스초기화(공부!)
			// 잘 안되지만(계속해서 scaleWidth값이 변해있음...) 일단 Enable로 해결
			/** matrix에 값 저장후 Enable시켜줌 **/
			ImageViewerItem.this.matrix.set(matrix2);
			mPager.setPageScrollEnabled();
//			savedMatrix.set(matrix2); // 안해도됨
			
			/** scale값 재조정(안됨 **/
//			float[] afterValue = new float[9];
//			matrix2.getValues(afterValue);
//			scaleWidth = (int)(imageWidth * afterValue[Matrix.MSCALE_X]);
//			scaleHeight = (int)(imageHeight * afterValue[Matrix.MSCALE_Y]);
			return true;
		}
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		// Calculate our view width
		mLayoutWidth = right - left;

		/** 이미지 로딩이 완료된 후에 처리 **/
		if (ivItem != null){
			Drawable d = ivItem.getDrawable();
			int instrinsicWidth = -1;
			int instrinsicHeight = -1;
			try{
				instrinsicWidth = d.getIntrinsicWidth();
				instrinsicHeight = d.getIntrinsicHeight();
			} catch(Exception e){
				e.printStackTrace();
			}

			if(instrinsicWidth != -1 && instrinsicHeight != -1){
				Matrix matrix = ivItem.getImageMatrix();
				float[] values = new float[9];
				matrix.getValues(values);
				// 원래 이미지 scale 저장
				orgScaleX = values[Matrix.MSCALE_X];
				orgScaleY = values[Matrix.MSCALE_Y];
				orgX = values[Matrix.MTRANS_X];
				orgY = values[Matrix.MTRANS_Y];
				
				this.matrix.set(matrix);
				ivItem.setScaleType(ScaleType.MATRIX);
				tuneMatrix(matrix);
			}
		}
	}


}
