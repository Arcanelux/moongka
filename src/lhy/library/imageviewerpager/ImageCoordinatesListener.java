/**
 * Author : Lee HanYeong
 * File Name : TouchCoordinatesListener.java
 * Created Date : 2012. 10. 26.
 * Description
 */
package lhy.library.imageviewerpager;


public interface ImageCoordinatesListener {
	void onTouch(ImageCoordinates coordinates, int position);
}
