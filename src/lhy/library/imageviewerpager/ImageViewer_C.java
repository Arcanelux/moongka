/**
 * Author : Lee HanYeong
 * File Name : C.java
 * Created Date : 2012. 10. 12.
 * Description
 */
package lhy.library.imageviewerpager;

public class ImageViewer_C {
	public static final boolean Debug = true;
	
	public static final int MODE_RES_FOLDER = 10;
	public static final int MODE_DOWNLOAD = 11;
	public static final int MODE_BITMAP = 12;
}
