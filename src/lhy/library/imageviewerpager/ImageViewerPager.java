package lhy.library.imageviewerpager;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageViewerPager extends ViewPager{
	private final String TAG = "ImageViewerPager";
	private Context mContext;
	private ImageDownloader mImageDownloader;
	private ArrayList<Image> imageList;

	public int COUNT;
	private int prevPos = 0;	// 시작번호로 바꾸어주면된다. 페이지 스크롤할경우 이전페이지 position을 가짐
	private boolean isPageScrollEnable = true;
	private boolean canScroll = true;
	private boolean isCirculation = true;

	public ImageViewerPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		firstInit(context);
	}
	public ImageViewerPager(Context context) {
		super(context);
		firstInit(context);
	}

	private void firstInit(Context context){
		mContext = context;
		/** XML 그래픽뷰에 표시할 내용 **/
		if(isInEditMode()){
			setBackgroundColor(0xffdddddd);
			TextView tvSample = new TextView(mContext);
			tvSample.setText("lhy_ImageViewerPager");
			tvSample.setTextColor(Color.BLACK);
			tvSample.setTextSize(20);
			tvSample.setGravity(Gravity.CENTER);
			tvSample.bringToFront();
			addView(tvSample);
		} else{
			mImageDownloader = new ImageDownloader(mContext);
			this.setPageMargin(15);
			this.setOnPageChangeListener(new OnPageChangeListener() {
				@Override
				public void onPageSelected(int position) {
					if(isCirculation){
						if(position < COUNT) setCurrentItem(position+COUNT, false);
						else if(position >= COUNT*2) setCurrentItem(position - COUNT, false);
						else{
							position -= COUNT;
							Log.d(TAG, "curPos : " + position);
							Log.d(TAG, "prevPos : " + prevPos);
						}
					}
				}
				@Override public void onPageScrolled(int arg0, float arg1, int arg2) {}
				@Override public void onPageScrollStateChanged(int arg0) {}
			});
		}
	}

	/** 이미지 리스트 전달하며 어댑터 설정, 어댑터는 이미지 리스트를 받는다 **/
	public void setImageList(ArrayList<Image> imageList){
		this.imageList = imageList;
		COUNT = imageList.size();
		IVPagerAdapter adapter = new IVPagerAdapter(imageList, mContext);
		setAdapter(adapter);
	}

	private class IVPagerAdapter extends PagerAdapter{
		private Context mContext;
		private ArrayList<Image> imageList;

		public IVPagerAdapter(ArrayList<Image> imageList, Context context){
			mContext = context;
			this.imageList = imageList;
		}
		@Override public int getCount() { 
			if(isCirculation){
				return COUNT*3;
			} else{
				return COUNT;
			}
		}
		@Override public boolean isViewFromObject(View view, Object obj) { return view==obj; }
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			if(isCirculation) position %= COUNT;
			ImageViewerItem curPagerItem = new ImageViewerItem(mContext);
			ImageView ivItem = curPagerItem.getIvItem();
			Image curImage = imageList.get(position);
			switch(curImage.getMode()){
			case ImageViewer_C.MODE_RES_FOLDER:
				ivItem.setImageResource(curImage.getImageRes());
				break;
			case ImageViewer_C.MODE_DOWNLOAD:
				// 내부에서 저장된 이미지 불러옴
				String packageName = mContext.getPackageName();
				String imageUrl = curImage.getImageAddress();
				String imageName = Lhy_Function.getImageNameExceptSlash(imageUrl);
				String imgPath = "data/data/" + packageName + "/files/" + imageName;
				try{
					Bitmap bm = BitmapFactory.decodeFile(imgPath);
					if(bm!=null){
						// 거의 성능차이 없음
//						new ImageViewerPagerSetImageTask(ivItem, bm);
						ivItem.setImageBitmap(bm);
						Log.d(TAG, "SavedImage Load : " + imageName);
					} else{
						mImageDownloader.download(imageUrl, ivItem);
					}
				} catch(Exception e){
					e.printStackTrace();
				}
				break;
			}
			curPagerItem.setPager(ImageViewerPager.this);
			((ViewPager)container).addView(curPagerItem, 0);

			return curPagerItem;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object view) {
			((ViewPager)container).removeView((View)view);
		}
	}

	/** 스크롤 불가능할경우 false 리턴 **/
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		try {
			if (this.isPageScrollEnable && canScroll) {
				//				Log.i("INFO", "스크롤 중..");
				return super.onTouchEvent(event);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/** 
	 * 스크롤 불가능할경우 false 리턴
	 *  onTouchEvent와 onInterceptTouchEvent모두 false를 주어야 정상작동
	 */

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if (this.isPageScrollEnable && canScroll) {
			return super.onInterceptTouchEvent(event);
		}
		return false;
	}

	public void setPageScrollEnabled() { //이 메소드를 이용해서 스크롤을 풀어주고
		this.isPageScrollEnable = true;
	}

	public void setPageScrollDisabled() { //이 메소드를 이용해서 스크롤을 막아줍니다.
		this.isPageScrollEnable = false;
	}

	public void setPageScrollAlwaysDisable(){
		this.canScroll = false;
	}

	public void setCirculation(boolean value){
		this.isCirculation = value;
	}


}
