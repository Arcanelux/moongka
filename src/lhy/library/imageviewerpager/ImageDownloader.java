/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lhy.library.imageviewerpager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

/**
 * This helper class download images from the Internet and binds those with the provided ImageView.
 *
 * <p>It requires the INTERNET permission, which should be added to your application's manifest
 * file.</p>
 *
 * A local cache of downloaded images is maintained internally to improve performance.
 */
public class ImageDownloader {
	private static final String LOG_TAG = "ImageDownloader";
	private static final String TAG = "lhy_ImageDownloader";

	private Context mContext;
	//	public static Bitmap staticBitmap;
	private boolean enableCheckSize = true;
	private boolean enableSaveFile = true;
	private boolean enableSaveCache = false;
	private boolean enableLoadFile = true;
	private boolean enableProgressDialog = false;


	public enum Mode {
		NO_ASYNC_TASK, NO_DOWNLOADED_DRAWABLE, CORRECT
	}


	public ImageDownloader(Context context){
		mContext = context;
	}

	private Mode mode = Mode.CORRECT;
	//	private boolean progressDialogEnable = false;


	/** MyFunction **/
	public void setProgressDialogEnable(boolean value){
		enableProgressDialog = value;
	}

	/**
	 * Download the specified image from the Internet and binds it to the provided ImageView. The
	 * binding is immediate if the image is found in the cache and will be done asynchronously
	 * otherwise. A null bitmap will be associated to the ImageView if an error occurs.
	 *
	 * @param url The URL of the image to download.
	 * @param imageView The ImageView to bind the downloaded image to.
	 */
	public void download(String url, ImageView imageView) {
		if (imageView != null) {
			boolean isExist = false;

			if(enableLoadFile){
				String pkgName = mContext.getPackageName();
				String imageName = Lhy_Function.getImageNameExceptSlash(url);
				String filePath = "";
				if(enableSaveFile) {
					filePath = "data/data/" + pkgName + "/files/" + imageName;
				} else if(enableSaveCache){
					filePath = "data/data/" + pkgName + "/cache/" + imageName;
				}
				Log.d(TAG, "FilePath : " + filePath);
				File file = new File(filePath);
				if(file.exists()){
					Log.d(TAG, "File Exist : "  + filePath);
					Bitmap existBitmap = BitmapFactory.decodeFile(filePath);
					imageView.setImageBitmap(existBitmap);
					isExist = true;
				} else{
					Log.d(TAG, "File Don't Exist : "  + filePath);
				}
			}

			/** 이미지가 존재하지않을경우(enableLoadFile이 false이면 자동으로 동작) **/
			if(!isExist){
				resetPurgeTimer();
				Bitmap bitmap = getBitmapFromCache(url);

				if (bitmap == null) {
					forceDownload(url, imageView);
				} else {
					cancelPotentialDownload(url, imageView);
					imageView.setImageBitmap(bitmap);
				}
			}
		}
	}

	//
	//	public void downloadNSave(String url, ImageView imageView, Context context) {
	//		resetPurgeTimer();
	//		Bitmap bitmap = getBitmapFromCache(url);
	//
	//		if (bitmap == null) {
	//			forceDownload(url, imageView);
	//		} else {
	//			cancelPotentialDownload(url, imageView);
	//			imageView.setImageBitmap(bitmap);
	//
	//			/** 파일로 저장 **/
	//			try{
	//				String imageName = Function.getImageName(url);
	//				File file = new File(imageName);
	//				if(!file.exists()){
	//					FileOutputStream fos = mContext.openFileOutput(imageName, 0);
	//					staticBitmap.compress(CompressFormat.PNG, 100, fos);
	//					fos.flush();
	//					fos.close();
	//					Log.d(TAG, "SaveImageFile \"" + imageName + "\" Success");
	//				}
	//				Log.d(TAG, "ImageFile \"" + imageName + "\" is already Exist!");
	//			} catch(Exception e){
	//				Log.d(TAG, "SaveImageFile Failed Cause : " + e);
	//			}
	//		}
	//		Log.d(TAG, "saveImage \"" + url + "\" Success");
	//	}

	/*
	 * Same as download but the image is always downloaded and the cache is not used.
	 * Kept private at the moment as its interest is not clear.
	   private void forceDownload(String url, ImageView view) {
	      forceDownload(url, view, null);
	   }
	 */

	/**
	 * Same as download but the image is always downloaded and the cache is not used.
	 * Kept private at the moment as its interest is not clear.
	 */
	private void forceDownload(String url, ImageView imageView) {
		// State sanity: url is guaranteed to never be null in DownloadedDrawable and cache keys.
		if (url == null) {
			imageView.setImageDrawable(null);
			return;
		}

		if (cancelPotentialDownload(url, imageView)) {
			switch (mode) {
			case NO_ASYNC_TASK:
				Bitmap bitmap = downloadBitmap(url);
				addBitmapToCache(url, bitmap);
				imageView.setImageBitmap(bitmap);
				break;

			case NO_DOWNLOADED_DRAWABLE:
				imageView.setMinimumHeight(156);
				BitmapDownloaderTask task = new BitmapDownloaderTask(imageView);
				task.execute(url);
				break;

			case CORRECT:
				task = new BitmapDownloaderTask(imageView);
				DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
				imageView.setImageDrawable(downloadedDrawable);
				imageView.setMinimumHeight(156);
				task.execute(url);
				break;
			}
		}
	}

	/**
	 * Returns true if the current download has been canceled or if there was no download in
	 * progress on this image view.
	 * Returns false if the download in progress deals with the same url. The download is not
	 * stopped in that case.
	 */
	private static boolean cancelPotentialDownload(String url, ImageView imageView) {
		BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

		if (bitmapDownloaderTask != null) {
			String bitmapUrl = bitmapDownloaderTask.url;
			if ((bitmapUrl == null) || (!bitmapUrl.equals(url))) {
				bitmapDownloaderTask.cancel(true);
			} else {
				// The same URL is already being downloaded.
				return false;
			}
		}
		return true;
	}

	/**
	 * @param imageView Any imageView
	 * @return Retrieve the currently active download task (if any) associated with this imageView.
	 * null if there is no such task.
	 */
	private static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
		if (imageView != null) {
			Drawable drawable = imageView.getDrawable();
			if (drawable instanceof DownloadedDrawable) {
				DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
				return downloadedDrawable.getBitmapDownloaderTask();
			}
		}
		return null;
	}

	/** 실제 이미지 다운로드 부분 **/
	Bitmap downloadBitmap(String url) {
		int width=0, height=0;
		Bitmap curBitmap;

		final HttpClient client = (mode == Mode.NO_ASYNC_TASK) ? new DefaultHttpClient() :
			AndroidHttpClient.newInstance("Android");
		final HttpGet getRequest = new HttpGet(url);

		try {
			HttpResponse response = client.execute(getRequest);
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				Log.w("ImageDownloader", "Error " + statusCode + " while retrieving bitmap from " + url);
				return null;
			}

			/** 첫번째 entity. 크기 판단에 사용한다 **/
			if(enableCheckSize){
				final HttpEntity entity = response.getEntity();
				if (entity != null) {
					InputStream inputStream = null;
					try {
						inputStream = entity.getContent();
						// return BitmapFactory.decodeStream(inputStream);
						// Bug on slow connections, fixed in future release.
						/**
						 * BitmapFactory.Options -1
						 *  inJustDecodeBound=true일 경우 Options.outWidth, Options.outHeight로 크기를 알 수 있다
						 */
						BitmapFactory.Options boundsOp = new BitmapFactory.Options();
						boundsOp.inJustDecodeBounds = true;
						BitmapFactory.decodeStream(new FlushedInputStream(inputStream), null, boundsOp);
						if(boundsOp.outWidth == -1) { Log.d(TAG, "boundOp Error"); }

						width = boundsOp.outWidth;
						height = boundsOp.outHeight;
//						Log.d(TAG, "Image boundsOp.outWidth : " + width);
//						Log.d(TAG, "Image boundsOp.outHeight : " + height);

						boundsOp.inJustDecodeBounds = false;
					} finally {
						if (inputStream != null) {
							inputStream.close();
						}
						entity.consumeContent();
					}
				}
			}

			/**
			 * 두번째 entity. 이미지로 만든다
			 *  크기를 체크했을경우, 가로+세로가 2200픽셀 이상일경우 줄여줌
			 *  2000픽셀 이하로 고정되도록 SampleSize조절
			 */
			response = client.execute(getRequest);
			final HttpEntity reEntity = response.getEntity();
			InputStream reInputStream = reEntity.getContent();
			/**
			 * BitmapFacotry.Options -2
			 *  inSampleSize : 2는 1/2, 3은 1/3
			 *  inPreferredConfig : 픽셀당 색상정보
			 */
			BitmapFactory.Options resampleOp = new BitmapFactory.Options();
			if(width+height > 2000){
				int sampleSize = (width+height)/1000;
				resampleOp.inSampleSize = sampleSize;
			}

			/** RGB_565 모드를 사용한다 **/
			resampleOp.inPreferredConfig = Config.RGB_565;
			curBitmap = BitmapFactory.decodeStream(new FlushedInputStream(reInputStream), null, resampleOp);

			/** 파일로 저장 **/
			if(enableSaveFile){
				try{
					String imageName = Lhy_Function.getImageNameExceptSlash(url);
					File file = null;
					if(enableSaveFile){
						file = new File(imageName);
					} else if(enableSaveCache){
						file = new File(mContext.getCacheDir(), imageName);
					}
					
					if(file.exists()){
						Log.d(TAG, "ImageFile \"" + imageName + "\" is already Exist!");
					} else{
						FileOutputStream fos = mContext.openFileOutput(imageName, 0);
						curBitmap.compress(CompressFormat.PNG, 100, fos);
						fos.flush();
						fos.close();
						Log.d(TAG, "SaveImageFile \"" + imageName + "\" Success");
					}
				} catch(Exception e){
					Log.d(TAG, "SaveImageFile Failed Cause : " + e);
				}
			}
			return curBitmap;
		} catch (IOException e) {
			getRequest.abort();
			Log.w(LOG_TAG, "I/O error while retrieving bitmap from " + url, e);
		} catch (IllegalStateException e) {
			getRequest.abort();
			Log.w(LOG_TAG, "Incorrect URL: " + url);
		} catch (Exception e) {
			getRequest.abort();
			Log.w(LOG_TAG, "Error while retrieving bitmap from " + url, e);
		} finally {
			if ((client instanceof AndroidHttpClient)) {
				((AndroidHttpClient)client).close();
			}
		}
		return null;
	}

	/*
	 * An InputStream that skips the exact number of bytes provided, unless it reaches EOF.
	 */
	static class FlushedInputStream extends FilterInputStream {
		public FlushedInputStream(InputStream inputStream) {
			super(inputStream);
		}

		@Override
		public long skip(long n) throws IOException {
			long totalBytesSkipped = 0L;
			while (totalBytesSkipped < n) {
				long bytesSkipped = in.skip(n - totalBytesSkipped);
				if (bytesSkipped == 0L) {
					int b = read();
					if (b < 0) {
						break; // we reached EOF
					} else {
						bytesSkipped = 1; // we read one byte
					}
				}
				totalBytesSkipped += bytesSkipped;
			}
			return totalBytesSkipped;
		}
	}

	/**
	 * The actual AsyncTask that will asynchronously download the image.
	 */
	class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
		private String url;
		private final WeakReference<ImageView> imageViewReference;

		/** MyVariable **/
		private ProgressDialog mProgressDialog;

		public BitmapDownloaderTask(ImageView imageView) {
			imageViewReference = new WeakReference<ImageView>(imageView);
		}

		@Override
		protected void onPreExecute() {
			if(enableProgressDialog){
				mProgressDialog = new ProgressDialog(mContext);
				mProgressDialog.setMessage("Loading...");
				mProgressDialog.show();
			}
			super.onPreExecute();
		}

		/**
		 * Actual download method.
		 */
		@Override
		protected Bitmap doInBackground(String... params) {
			Log.d(TAG, "doInBackground");
			url = params[0];
			return downloadBitmap(url);
		}

		/**
		 * Once the image is downloaded, associates it to the imageView
		 */
		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if(enableProgressDialog){
				mProgressDialog.dismiss();
			}
			Log.d(TAG, "onPostExecute start");
			if (isCancelled()) {
				bitmap = null;

			}

			addBitmapToCache(url, bitmap);

			if (imageViewReference != null) {
				ImageView imageView = imageViewReference.get();
				BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
				// Change bitmap only if this process is still associated with it
				// Or if we don't use any bitmap to task association (NO_DOWNLOADED_DRAWABLE mode)
				if ((this == bitmapDownloaderTask) || (mode != Mode.CORRECT)) {
					imageView.setImageBitmap(bitmap);
				}
			}
		}
	}

	/**
	 * A fake Drawable that will be attached to the imageView while the download is in progress.
	 *
	 * <p>Contains a reference to the actual download task, so that a download task can be stopped
	 * if a new binding is required, and makes sure that only the last started download process can
	 * bind its result, independently of the download finish order.</p>
	 */
	static class DownloadedDrawable extends ColorDrawable {
		private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

		public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask) {
			super(Color.BLACK);
			bitmapDownloaderTaskReference =
					new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);
		}

		public BitmapDownloaderTask getBitmapDownloaderTask() {
			return bitmapDownloaderTaskReference.get();
		}
	}

	public void setMode(Mode mode) {
		this.mode = mode;
		clearCache();
	}

	/*
	 * Cache-related fields and methods.
	 * 
	 * We use a hard and a soft cache. A soft reference cache is too aggressively cleared by the
	 * Garbage Collector.
	 */

	private static final int HARD_CACHE_CAPACITY = 10;
	private static final int DELAY_BEFORE_PURGE = 10 * 1000; // in milliseconds

	// Hard cache, with a fixed maximum capacity and a life duration
	private final HashMap<String, Bitmap> sHardBitmapCache =
			new LinkedHashMap<String, Bitmap>(HARD_CACHE_CAPACITY / 2, 0.75f, true) {
		/**
		 * UID
		 */
		private static final long serialVersionUID = 1L;

		@Override
		protected boolean removeEldestEntry(LinkedHashMap.Entry<String, Bitmap> eldest) {
			if (size() > HARD_CACHE_CAPACITY) {
				// Entries push-out of hard reference cache are transferred to soft reference cache
				sSoftBitmapCache.put(eldest.getKey(), new SoftReference<Bitmap>(eldest.getValue()));
				return true;
			} else
				return false;
		}
	};

	// Soft cache for bitmaps kicked out of hard cache
	private final static ConcurrentHashMap<String, SoftReference<Bitmap>> sSoftBitmapCache =
			new ConcurrentHashMap<String, SoftReference<Bitmap>>(HARD_CACHE_CAPACITY / 2);

	private final Handler purgeHandler = new Handler();

	private final Runnable purger = new Runnable() {
		@Override
		public void run() {
			clearCache();
		}
	};

	/**
	 * Adds this bitmap to the cache.
	 * @param bitmap The newly downloaded bitmap.
	 */
	private void addBitmapToCache(String url, Bitmap bitmap) {
		if (bitmap != null) {
			synchronized (sHardBitmapCache) {
				sHardBitmapCache.put(url, bitmap);
			}
		}
	}

	/**
	 * @param url The URL of the image that will be retrieved from the cache.
	 * @return The cached bitmap or null if it was not found.
	 */
	private Bitmap getBitmapFromCache(String url) {
		// First try the hard reference cache
		synchronized (sHardBitmapCache) {
			final Bitmap bitmap = sHardBitmapCache.get(url);
			if (bitmap != null) {
				// Bitmap found in hard cache
				// Move element to first position, so that it is removed last
				sHardBitmapCache.remove(url);
				sHardBitmapCache.put(url, bitmap);
				return bitmap;
			}
		}

		// Then try the soft reference cache
		SoftReference<Bitmap> bitmapReference = sSoftBitmapCache.get(url);
		if (bitmapReference != null) {
			final Bitmap bitmap = bitmapReference.get();
			if (bitmap != null) {
				// Bitmap found in soft cache
				return bitmap;
			} else {
				// Soft reference has been Garbage Collected
				sSoftBitmapCache.remove(url);
			}
		}

		return null;
	}

	/**
	 * Clears the image cache used internally to improve performance. Note that for memory
	 * efficiency reasons, the cache will automatically be cleared after a certain inactivity delay.
	 */
	public void clearCache() {
		sHardBitmapCache.clear();
		sSoftBitmapCache.clear();
	}

	/**
	 * Allow a new delay before the automatic cache clear is done.
	 */
	private void resetPurgeTimer() {
		purgeHandler.removeCallbacks(purger);
		purgeHandler.postDelayed(purger, DELAY_BEFORE_PURGE);
	}
}
